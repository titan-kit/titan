package validate

import (
	"context"

	"gitee.com/titan-kit/titan/errors"
	"gitee.com/titan-kit/titan/middleware"
)

type validator interface {
	Validate() error
}

// Validator 是校验器中间件.
func Validator() middleware.Middleware {
	return func(handler middleware.Handler) middleware.Handler {
		return func(ctx context.Context, req interface{}) (reply interface{}, err error) {
			if v, ok := req.(validator); ok {
				if err := v.Validate(); err != nil {
					return nil, errors.BadRequest("VALIDATOR", err.Error())
				}
			}
			return handler(ctx, req)
		}
	}
}