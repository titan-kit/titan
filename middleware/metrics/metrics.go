package metrics

import (
	"context"
	"strconv"
	"time"

	"gitee.com/titan-kit/titan/errors"
	"gitee.com/titan-kit/titan/metrics"
	"gitee.com/titan-kit/titan/middleware"
	"gitee.com/titan-kit/titan/starter/rpc"
	"gitee.com/titan-kit/titan/starter/web"
)

// Option 是指标选项.
type Option func(*options)

// WithRequests 带请求计数器.
func WithRequests(c metrics.Counter) Option {
	return func(o *options) {
		o.requests = c
	}
}

// WithObserver 带有观察者.
func WithObserver(c metrics.Observer) Option {
	return func(o *options) {
		o.observer = c
	}
}

type options struct {
	// 计数器: <kind>_<client/server>_requests_code_total{method, path, code}
	requests metrics.Counter
	// 柱状图: <kind>_<client/server>_requests_observer_bucket{method, path}
	observer metrics.Observer
}

// Metrics 是性能指标中间件.
func Metrics(kind string, caller string, opts ...Option) middleware.Middleware {
	options := options{}
	for _, o := range opts {
		o(&options)
	}
	return func(handler middleware.Handler) middleware.Handler {
		return func(ctx context.Context, req interface{}) (interface{}, error) {
			var method, path string
			var code uint32
			if kind == "client" {
				if info, ok := rpc.FromClientContext(ctx); ok {
					method = "POST"
					path = info.FullMethod
				} else if info, ok := web.FromClientContext(ctx); ok {
					method = info.Request.Method
					path = info.Request.RequestURI
				}
			} else {
				if info, ok := rpc.FromServerContext(ctx); ok {
					method = "POST"
					path = info.FullMethod
				} else if info, ok := web.FromServerContext(ctx); ok {
					method = info.Request.Method
					if rct := web.CurrentRoute(info.Request); rct != nil {
						// /path/123 -> /path/{id}
						path = rct.Path
					} else {
						path = info.Request.RequestURI
					}
				}
			}
			startTime := time.Now()
			reply, err := handler(ctx, req)
			if err != nil {
				code = uint32(errors.Code(err))
			}
			if options.requests != nil {
				options.requests.With(caller, method, path, strconv.Itoa(int(code))).Inc()
			}
			if options.observer != nil {
				options.observer.With(caller, method, path).Observe(time.Since(startTime).Seconds())
			}
			return reply, err
		}
	}
}