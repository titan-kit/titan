package logging

import (
	"context"
	"fmt"

	"gitee.com/titan-kit/titan/errors"
	"gitee.com/titan-kit/titan/log"
	"gitee.com/titan-kit/titan/middleware"
	"gitee.com/titan-kit/titan/starter/rpc"
	"gitee.com/titan-kit/titan/starter/web"

	"go.opentelemetry.io/otel/trace"
)

const loggerName = "middleware/logging"

// Server 是服务器记录中间件.
func Server(l log.Logger) middleware.Middleware {
	return Logging("server", l)
}

// Client 是客户端记录中间件.
func Client(l log.Logger) middleware.Middleware {
	return Logging("client", l)
}
func Logging(kind string, l log.Logger) middleware.Middleware {
	logger := log.NewSlf4g(loggerName, l)
	return func(handler middleware.Handler) middleware.Handler {
		return func(ctx context.Context, req interface{}) (interface{}, error) {
			var path, method, args, component, query, traceId string
			if tid := trace.SpanContextFromContext(ctx).TraceID(); tid.IsValid() {
				traceId = tid.String()
			}
			if stringer, ok := req.(fmt.Stringer); ok {
				args = stringer.String()
			} else {
				args = fmt.Sprintf("%+v", req)
			}
			if kind == "client" {
				if info, ok := web.FromClientContext(ctx); ok {
					component = "WEB"
					path = info.Request.RequestURI
					method = info.Request.Method
					query = info.Request.URL.RawQuery
				} else if info, ok := rpc.FromClientContext(ctx); ok {
					path = info.FullMethod
					method = "POST"
					component = "RPC"
				}
			} else {
				if info, ok := web.FromServerContext(ctx); ok {
					component = "WEB"
					path = info.Request.RequestURI
					method = info.Request.Method
					query = info.Request.URL.RawQuery
				} else if info, ok := rpc.FromServerContext(ctx); ok {
					path = info.FullMethod
					method = "POST"
					component = "RPC"
				}
			}
			reply, err := handler(ctx, req)
			if component == "WEB" {
				if err != nil {
					logger.ErrorW("kind", kind, "component", component, "traceId", traceId, "path", path, "method", method, "args", args, "query", query, "code", uint32(errors.Code(err)), "error", err.Error())
					return nil, err
				}
				logger.InfoW("kind", kind, "component", component, "traceId", traceId, "path", path, "method", method, "args", args, "query", query, "code", 0)
			} else {
				if err != nil {
					logger.ErrorW("kind", kind, "component", component, "traceId", traceId, "path", path, "method", method, "args", args, "code", uint32(errors.Code(err)), "error", err.Error())
					return nil, err
				}
				logger.InfoW("kind", kind, "component", component, "traceId", traceId, "path", path, "method", method, "args", args, "code", 0)
			}
			return reply, nil
		}
	}
}