package middleware

import "context"

// Handler 定义由中间件调用的处理程序.
type Handler func(ctx context.Context, req interface{}) (interface{}, error)

// Middleware 是MVC/gRPC传输中间件.
type Middleware func(Handler) Handler

// Chain 返回指定服务节点的链式处理程序的中间件.
func Chain(m ...Middleware) Middleware {
	return func(next Handler) Handler {
		for i := len(m) - 1; i >= 0; i-- {
			next = m[i](next)
		}
		return next
	}
}