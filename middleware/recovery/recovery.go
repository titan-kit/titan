package recovery

import (
	"context"
	"fmt"
	"runtime"

	"gitee.com/titan-kit/titan/errors"
	"gitee.com/titan-kit/titan/log"
	"gitee.com/titan-kit/titan/middleware"
)

const loggerName = "middleware/recovery"

// HandlerFunc 是恢复处理程序函数.
type HandlerFunc func(ctx context.Context, req, err interface{}) error

// Option 是恢复选项.
type Option func(*options)

type options struct {
	handler HandlerFunc
	logger  log.Logger
}

// WithHandler 与恢复处理程序.
func WithHandler(h HandlerFunc) Option {
	return func(o *options) {
		o.handler = h
	}
}

// WithLogger 使用恢复记录器.
func WithLogger(logger log.Logger) Option {
	return func(o *options) {
		o.logger = logger
	}
}

// Recovery 是从任何紧急情况中恢复的服务器中间件.
func Recovery(opts ...Option) middleware.Middleware {
	options := options{
		logger: log.DefaultLogger,
		handler: func(ctx context.Context, req, err interface{}) error {
			return errors.InternalServer("recovery", fmt.Sprintf("panic triggered: %v", err))
		},
	}
	for _, o := range opts {
		o(&options)
	}
	logger := log.NewSlf4g(loggerName, options.logger)
	return func(handler middleware.Handler) middleware.Handler {
		return func(ctx context.Context, req interface{}) (reply interface{}, err error) {
			defer func() {
				if rer := recover(); rer != nil {
					buf := make([]byte, 64<<10)
					n := runtime.Stack(buf, false)
					buf = buf[:n]
					logger.ErrorF("%v: %+v\n%s\n", rer, req, buf)
					err = options.handler(ctx, req, rer)
				}
			}()
			return handler(ctx, req)
		}
	}
}