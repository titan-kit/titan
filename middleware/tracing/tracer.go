package tracing

import (
	"context"
	"fmt"

	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/codes"
	"go.opentelemetry.io/otel/propagation"
	"go.opentelemetry.io/otel/trace"
)

// Tracer 是otel跨度追踪器(Span，可以被翻译为跨度，可以被理解为一次方法调用,一个程序块的调用, 或者一次RPC/数据库访问.只要是一个具有完整时间周期的程序访问，都可以被认为是一个span.)
type Tracer struct {
	tracer trace.Tracer
	kind   trace.SpanKind
}

// NewTracer 创建跟踪器实例
func NewTracer(kind trace.SpanKind, opts ...Option) *Tracer {
	options := options{}
	for _, o := range opts {
		o(&options)
	}
	if options.TracerProvider != nil {
		otel.SetTracerProvider(options.TracerProvider)
	}
	if options.Propagators != nil {
		otel.SetTextMapPropagator(options.Propagators)
	} else {
		otel.SetTextMapPropagator(propagation.NewCompositeTextMapPropagator(propagation.Baggage{}, propagation.TraceContext{}))
	}
	var name string
	if kind == trace.SpanKindServer {
		name = "server"
	} else if kind == trace.SpanKindClient {
		name = "client"
	} else {
		panic(fmt.Sprintf("unsupported span kind: %v", kind))
	}
	tracer := otel.Tracer(name)
	return &Tracer{tracer: tracer, kind: kind}
}

// Start 开始跟踪跨度
func (t *Tracer) Start(ctx context.Context, component string, operation string, carrier propagation.TextMapCarrier) (context.Context, trace.Span) {
	if t.kind == trace.SpanKindServer {
		ctx = otel.GetTextMapPropagator().Extract(ctx, carrier)
	}
	ctx, span := t.tracer.Start(ctx, operation, trace.WithAttributes(attribute.String("component", component)), trace.WithSpanKind(t.kind))
	if t.kind == trace.SpanKindClient {
		otel.GetTextMapPropagator().Inject(ctx, carrier)
	}
	return ctx, span
}

// End 完成跟踪跨度
func (t *Tracer) End(ctx context.Context, span trace.Span, err error) {
	if err != nil {
		span.RecordError(err)
		span.SetAttributes(attribute.String("event", "error"), attribute.String("message", err.Error()))
		span.SetStatus(codes.Error, err.Error())
	} else {
		span.SetStatus(codes.Ok, "OK")
	}
	span.End()
}