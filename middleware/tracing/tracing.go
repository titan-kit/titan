package tracing

import (
	"context"
	"gitee.com/titan-kit/titan/middleware"
	"gitee.com/titan-kit/titan/starter/rpc"
	"gitee.com/titan-kit/titan/starter/web"

	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/propagation"
	"go.opentelemetry.io/otel/trace"
	"google.golang.org/grpc/metadata"
)

// Option 正在跟踪选项.
type Option func(*options)

type options struct {
	TracerProvider trace.TracerProvider
	Propagators    propagation.TextMapPropagator
}

func WithTracerProvider(provider trace.TracerProvider) Option {
	return func(opts *options) {
		opts.TracerProvider = provider
	}
}

func WithPropagators(propagators propagation.TextMapPropagator) Option {
	return func(opts *options) {
		opts.Propagators = propagators
	}
}

// MetadataCarrier 是grpc元数据载体
type MetadataCarrier metadata.MD

// Get 返回与传递的键关联的值.
func (mc MetadataCarrier) Get(key string) string {
	values := metadata.MD(mc).Get(key)
	if len(values) == 0 {
		return ""
	}
	return values[0]
}

// Set 存储键值对.
func (mc MetadataCarrier) Set(key string, value string) {
	metadata.MD(mc).Set(key, value)
}

// Keys 列出存储在此载体中的键.
func (mc MetadataCarrier) Keys() []string {
	keys := make([]string, 0, metadata.MD(mc).Len())
	for key := range metadata.MD(mc) {
		keys = append(keys, key)
	}
	return keys
}

// Del 删除键
func (mc MetadataCarrier) Del(key string) {
	delete(mc, key)
}

// Clone 复制MetadataCarrier
func (mc MetadataCarrier) Clone() MetadataCarrier {
	return MetadataCarrier(metadata.MD(mc).Copy())
}

// Server 返回用于OpenTelemetry的新服务器中间件.
func Server(opts ...Option) middleware.Middleware {
	tracer := NewTracer(trace.SpanKindServer, opts...)
	return func(handler middleware.Handler) middleware.Handler {
		return func(ctx context.Context, req interface{}) (reply interface{}, err error) {

			var component, operation string
			var carrier propagation.TextMapCarrier

			if info, ok := web.FromServerContext(ctx); ok { // WEB 跨度
				component = "WEB"
				if route := web.CurrentRoute(info.Request); route != nil {
					// /path/123 -> /path/{id}
					operation = route.Path
				} else {
					operation = info.Request.RequestURI
				}
				carrier = propagation.HeaderCarrier(info.Request.Header)
				ctx = otel.GetTextMapPropagator().Extract(ctx, propagation.HeaderCarrier(info.Request.Header))
			} else if info, ok := rpc.FromServerContext(ctx); ok { // gRPC 跨度
				component = "RPC"
				operation = info.FullMethod
				if md, ok := metadata.FromIncomingContext(ctx); ok {
					carrier = MetadataCarrier(md)
				}
			}

			ctx, span := tracer.Start(ctx, component, operation, carrier)

			defer func() {
				tracer.End(ctx, span, err)
			}()

			return handler(ctx, req)
		}
	}
}

// Client 返回用于OpenTelemetry的新客户端中间件.
func Client(opts ...Option) middleware.Middleware {
	tracer := NewTracer(trace.SpanKindClient, opts...)
	return func(handler middleware.Handler) middleware.Handler {
		return func(ctx context.Context, req interface{}) (reply interface{}, err error) {

			var component, operation string
			var carrier propagation.TextMapCarrier

			if info, ok := web.FromClientContext(ctx); ok { // WEB 跨度
				component = "WEB"
				operation = info.Request.RequestURI
				carrier = propagation.HeaderCarrier(info.Request.Header)
			} else if info, ok := rpc.FromClientContext(ctx); ok { // gRPC 跨度
				component = "RPC"
				operation = info.FullMethod
				md, ok := metadata.FromOutgoingContext(ctx)
				if !ok {
					md = metadata.Pairs()
				}
				carrier = MetadataCarrier(md)
				ctx = metadata.NewOutgoingContext(ctx, md)
			}

			ctx, span := tracer.Start(ctx, component, operation, carrier)

			defer func() {
				tracer.End(ctx, span, err)
			}()

			return handler(ctx, req)
		}
	}
}