package cache

import "time"

// Provider 分布式的缓存操作接口。
type Provider interface {
	// Name 接口实现的名称
	Name() string

	// Exists 判断缓存中是否存在指定的key
	//
	// @param key 缓存唯一键
	Exists(key string) bool

	// String 根据给定的key从分布式缓存中读取数据并返回，如果不存在或已过期则返回Null。
	//
	//	@param key 缓存唯一键
	String(key string) string

	// GetByProvider 使用给定的key从缓存中查询数据，如果查询不到则使用给定的数据提供器来查询数据，然后将数据存入缓存中再返回。
	// @param key 缓存唯一键
	// @param dataProvider 数据提供器
	GetByProvider(key string, provider DataProvider) string
	// Set 使用指定的key将对象存入分布式缓存中，并使用缓存的默认过期设置，注意，存入的对象必须是可序列化的。
	//
	// @param key   缓存唯一键
	// @param value 对应的值
	Set(key, value string)

	// SetExpires 使用指定的key将对象存入分部式缓存中，并指定过期时间，注意，存入的对象必须是可序列化的
	//
	// @param key     缓存唯一键
	// @param value   对应的值
	// @param expires 过期时间
	SetExpires(key, value string, expires time.Duration) bool

	// Delete 从缓存中删除指定key的缓存数据。
	//
	// @param key 缓存唯一键
	Delete(key string)

	// BatchDelete 批量删除缓存中的key。
	//
	// @param keys 缓存唯一键数组
	BatchDelete(keys ...string)

	// HSet 将指定key的map数据的某个字段设置为给定的值。
	//
	// @param key   map数据的键
	// @param field map的字段名称
	// @param value 要设置的字段值
	HSet(key, field, value string)

	// HGet 获取指定key的map数据某个字段的值，如果不存在则返回Null
	//
	// @param key   map数据的键
	// @param field map的字段名称
	HGet(key, field string) string

	// HGetAll 获取指定key的map对象，如果不存在则返回Null
	//
	// @param key map数据的键
	HGetAll(key string) map[string]string

	// HDelete 将指定key的map数据中的某个字段删除。
	//
	// @param key   map数据的键
	// @param field map中的key名称
	HDelete(key string, fields ...string)

	// HExists 判断缓存中指定key的map是否存在指定的字段，如果key或字段不存在则返回false。
	//
	// @param key 要操作的缓存key
	// @param field map中的key名称
	HExists(key, field string) bool

	// Val 对指定的key结果集执行指定的脚本并返回最终脚本执行的结果。
	//
	// @param script 脚本
	// @param key    要操作的缓存key
	// @param args   脚本的参数列表
	Val(script string, keys []string, args ...interface{})

	// Operate 通过直接调用缓存客户端进行缓存操作，该操作适用于高级操作，如果执行失败会返回Null。
	//
	//	@param operator 高级操作函数
	Operate(operator interface{}) error

	// Close 关闭客户端
	Close()
}

// DataProvider 缓存的数据提供器接口定义。
type DataProvider interface {
	// Expires 获取数据对应的过期时间（单位秒），如果返回-1则表明使用缓存的默认设置。
	Expires() int
	// Data 获取缓存的数据，返回的数据必须是可序列化的对象。
	Data(key string) string
}