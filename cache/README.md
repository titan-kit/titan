# cache缓存引擎

缓存引擎定义，通过缓存引擎获取缓存客户端并进行数据缓存操作。

## 示例：

```go
    "gitee.com/titan-kit/titan/cache"

    gr "github.com/go-redis/redis"
    "gitee.com/titan-kit/titan/backends/redis"
```

### 获取缓存实体

```go
prov:=redis.New(*gr.Options{})
```

### 判断缓存中是否存在指定的key

```go
prov.Exists(key string) bool
```

### 根据给定的key从分布式缓存中读取数据并返回，如果不存在或已过期则返回Null。

```go
prov.String(key string) string
```

### 使用给定的key从缓存中查询数据，如果查询不到则使用给定的数据提供器来查询数据，然后将数据存入缓存中再返回。

```go
prov.GetByProvider(key string, provider DataProvider) string
```

### 使用指定的key将对象存入分布式缓存中，并使用缓存的默认过期设置，注意，存入的对象必须是可序列化的。

```go
prov.Set(key, value string)
```

### 使用指定的key将对象存入分部式缓存中，并指定过期时间，注意，存入的对象必须是可序列化的

```go
prov.SetExpires(key, value string, expires time.Duration) bool
```

### 从缓存中删除指定key的缓存数据。

```go
prov.Delete(key string)
```

### 批量删除缓存中的key。

```go
prov.BatchDelete(keys ...string)
```

### 将指定key的map数据的某个字段设置为给定的值。

```go
prov.HSet(key, field, value string)
```

### 获取指定key的map数据某个字段的值，如果不存在则返回Null

```go
prov.HGet(key, field string) string
```

### 获取指定key的map对象，如果不存在则返回Null

```go
prov.HGetAll(key string) map[string]string
```

### 将指定key的map数据中的某个字段删除。

```go
prov.HDelete(key string, fields ...string)
```

### 判断缓存中指定key的map是否存在指定的字段，如果key或字段不存在则返回false。

```go
prov.HExists(key, field string) bool
```

### 对指定的key结果集执行指定的脚本并返回最终脚本执行的结果。

```go
prov.Val(script string, keys []string, args ...interface{})
```

### 关闭客户端

```go
prov.Close()
```