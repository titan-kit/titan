package authing

import (
	auth "github.com/authing/authing-go-sdk"

	"gitee.com/titan-kit/titan/log"
)

func NewAuthing(userPoolId, userPoolSecret string, logger log.Logger) *authing {
	client := auth.NewClient(userPoolId, userPoolSecret, false)
	if logger == nil {
		logger = log.DefaultLogger
	}
	slf4g := log.NewSlf4g("integration/authing", logger)
	// 启用graphql客户端的调试信息
	client.Client.Log = func(s string) {
		slf4g.Debug(s)
	}
	return &authing{client, userPoolId}
}

type authing struct {
	client     *auth.Client
	userPoolId string
}