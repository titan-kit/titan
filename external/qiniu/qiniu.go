package qiniu

import (
	"context"
	"fmt"
	"io"
	"strings"
	"time"

	"github.com/qiniu/go-sdk/v7/auth/qbox"
	qny "github.com/qiniu/go-sdk/v7/storage"

	"gitee.com/titan-kit/titan/storage"
)

var _ storage.Provider = &qiniuProvider{}

const (
	DefaultDownloadUrlExpire       = time.Minute * 5
	DefaultSimpleUploadTokenExpire = 7200
)

type Config struct {
	AccessKey string `json:"accessKey"`
	SecretKey string `json:"secretKey"`
	Bucket    string `json:"bucket"`
	Domain    string `json:"domain"`
}
type qiniuProvider struct {
	protocol storage.Protocol
	conf     Config
}

func New(protocol storage.Protocol, conf Config) *qiniuProvider {
	fmt.Println("Loading Qiniu Storage Engine ver: 7.0.0", protocol)
	provider := &qiniuProvider{protocol, conf}
	storage.Register(protocol, provider)
	return provider
}
func (q qiniuProvider) Protocol() storage.Protocol {
	return q.protocol
}

func (q qiniuProvider) Save(reader io.Reader, fileSize int64, filePath string) (string, error) {
	upToken := q.simpleUploadToken(0)
	cfg := qny.Config{}
	// 是否使用https域名
	cfg.UseHTTPS = false
	// 上传是否使用CDN上传加速
	cfg.UseCdnDomains = false
	formUploader := qny.NewFormUploader(&cfg)
	ret := qny.PutRet{}
	putExtra := qny.PutExtra{Params: map[string]string{}}
	err := formUploader.Put(context.Background(), &ret, upToken, filePath, reader, fileSize, &putExtra)
	if err != nil {
		return "", err
	}
	return fmt.Sprintf("%s://%s", q.Protocol(), ret.Key), nil
}

func (q qiniuProvider) Get(path string) ([]byte, error) {
	// 校验path合法性
	if ok := storage.CheckPath(path, q.Protocol()); !ok {
		return nil, storage.ErrUnExpectedPath
	}
	// 文件是否存在
	has, err := q.Has(path)
	if err != nil {
		return nil, err
	}
	if !has {
		return nil, storage.ErrFileOrDirectoryNotExists
	}
	// 获取文件下载链接
	url, err := q.Url(path, false)
	if err != nil {
		return nil, err
	}
	// 处理下载资源链接
	byteArray, err := storage.DownloadUrlToBytes(url)
	if err != nil {
		return nil, err
	}
	return byteArray, nil
}

func (q qiniuProvider) Write(path string, file *[]byte) error {
	// 校验path合法性
	if ok := storage.CheckPath(path, q.Protocol()); !ok {
		return storage.ErrUnExpectedPath
	}
	// 文件是否存在
	has, err := q.Has(path)
	if err != nil {
		return err
	}
	if !has {
		return storage.ErrFileOrDirectoryNotExists
	}
	// 获取文件下载链接
	url, err := q.Url(path, false)
	if err != nil {
		return err
	}
	// 获取字节数组
	p, err := storage.DownloadUrlToBytes(url)
	if err != nil {
		return err
	}
	*file = p
	return nil
}

func (q qiniuProvider) Delete(path string) error {
	// 校验path合法性
	if ok := storage.CheckPath(path, q.Protocol()); !ok {
		return storage.ErrUnExpectedPath
	}
	// 删除
	err := q.generateBucketManager(q.conf.AccessKey, q.conf.SecretKey).Delete(q.conf.Bucket, storage.GetFilePathFromTotalPath(path))
	if err != nil {
		return err
	}
	return nil
}

func (q qiniuProvider) Url(path string, secure bool, expire ...time.Duration) (string, error) {
	// 校验path合法性
	if ok := storage.CheckPath(path, q.Protocol()); !ok {
		return "", storage.ErrUnExpectedPath
	}
	// 判断文件是否存在
	has, err := q.Has(storage.FilterParams(path))
	if err != nil {
		return "", err
	}
	if !has {
		return "", storage.ErrFileOrDirectoryNotExists
	}
	// 判断secure参数
	if !secure {
		publicAccessURL := qny.MakePublicURL(q.conf.Domain, storage.GetFilePathFromTotalPath(path))
		return publicAccessURL, nil
	}
	// 安全访问链接
	mac := qbox.NewMac(q.conf.AccessKey, q.conf.SecretKey)
	duration := DefaultDownloadUrlExpire // 默认五分钟有效期
	if len(expire) != 0 {
		duration = expire[0]
	}
	deadline := time.Now().Add(duration).Unix() // 下载链接设置有效期
	privateAccessURL := qny.MakePrivateURL(mac, q.conf.Domain, storage.GetFilePathFromTotalPath(path), deadline)
	return privateAccessURL, nil
}

func (q qiniuProvider) Has(path string) (bool, error) {
	// 校验path合法性
	if ok := storage.CheckPath(path, q.Protocol()); !ok {
		return false, storage.ErrUnExpectedPath
	}
	// 获取资源管理器
	_, err := q.generateBucketManager(q.conf.AccessKey, q.conf.SecretKey).Stat(q.conf.Bucket, storage.GetFilePathFromTotalPath(path))
	if err != nil {
		if strings.Contains(err.Error(), storage.ErrFileOrDirectoryNotExists.Error()) { // 文件不存在
			return false, nil
		}
		// 其他错误
		return false, err
	}
	return true, nil
}

func (q qiniuProvider) SimpleUploadToken() string {
	return q.simpleUploadToken(DefaultSimpleUploadTokenExpire)
}
func (q qiniuProvider) simpleUploadToken(expires uint64) string {
	putPolicy := qny.PutPolicy{Scope: q.conf.Bucket}
	putPolicy.Expires = expires
	return putPolicy.UploadToken(qbox.NewMac(q.conf.AccessKey, q.conf.SecretKey))
}

// generateBucketManager 创建空间资源管理器
func (q qiniuProvider) generateBucketManager(accessKey string, secretKey string) *qny.BucketManager {
	mac := qbox.NewMac(accessKey, secretKey)
	cfg := qny.Config{
		// 是否使用https域名进行资源管理
		UseHTTPS: false,
	}
	// 指定空间所在的区域，如果不指定将自动探测
	// 如果没有特殊需求，默认不需要指定
	// cfg.Zone=&qiniuStorage.ZoneHuabei
	bucketManager := qny.NewBucketManager(mac, &cfg)
	return bucketManager
}