package work

import (
	"gitee.com/titan-kit/titan/cache"
	"gitee.com/titan-kit/titan/external/weixin/common"
	"gitee.com/titan-kit/titan/log"
)

func NewWechatWork(cp cache.Provider, log *log.Slf4g) *WechatWork {
	api := common.NewApi("https://qyapi.weixin.qq.com/cgi-bin")
	return &WechatWork{api, cp, log}
}

type WechatWork struct {
	*common.Api
	cache cache.Provider
	log   *log.Slf4g
}