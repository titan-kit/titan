package open

import "gitee.com/titan-kit/titan/external/weixin/common"

// ThirdPlatformAdapter 以下为第三方平台(Third)
type ThirdPlatformAdapter interface {
	VerifyTicketMessageAdapter() common.Adapter
	AuthorizationMessageAdapter() common.Adapter
}

// VerifyTicketMessageAdapter 验证票据
// 验证票据（component_verify_ticket），在第三方平台创建审核通过后，微信服务器会向其 ”授权事件接收URL” 每隔 10 分钟以 POST 的方式推送 component_verify_ticket
//
// 接收 POST 请求后，只需直接返回字符串 success。
//
// 注意：component_verify_ticket 的有效时间为12小时，比 component_access_token 更长，建议保存最近可用的component_verify_ticket，
// 在 component_access_token 过期之前都可以直接使用该 component_verify_ticket 进行更新，避免出现因为 component_verify_ticket 接收失败而无法更新 component_access_token 的情况。
// 详见:third.VerifyTicketMessage
func VerifyTicketMessageAdapter(call func(common.CallbackModel, common.Adapter) string) common.Adapter {
	return common.NewAdapter([]string{"component_verify_ticket"}, call)
}
func AuthorizationMessageAdapter(call func(common.CallbackModel, common.Adapter) string) common.Adapter {
	return common.NewAdapter([]string{"authorized", "unauthorized", "updateauthorized"}, call)
}