package open

// PlatformMessage 第三方平台推送消息
type PlatformMessage struct {
	AppId      string `xml:"AppId"`      // 第三方平台 appid
	CreateTime int64  `xml:"CreateTime"` // 时间戳，单位：s
	InfoType   string `xml:"InfoType"`   // 消息类型
}

// VerifyTicketMessage 票据示例:
// <xml>
// <AppId>some_appid</AppId>
// <CreateTime>1413192605</CreateTime>
// <InfoType>component_verify_ticket</InfoType>
// <ComponentVerifyTicket>some_verify_ticket</ComponentVerifyTicket>
// </xml>
// 详情:https://developers.weixin.qq.com/doc/oplatform/Third-party_Platforms/api/component_verify_ticket.html
type VerifyTicketMessage struct {
	PlatformMessage
	ComponentVerifyTicket string `xml:"ComponentVerifyTicket"` // Ticket 内容
}

// AuthorizationMessage 授权变更通知推送
// 当公众号/小程序对第三方平台进行授权、取消授权、更新授权后，微信服务器会向第三方平台方的授权事件接收 URL（创建时由第三方平台时填写）以 POST 的方式推送相关通知。
//
// 接收 POST 请求后，只需直接返回字符串 success。为了加强安全性，postdata 中的 xml 将使用服务申请时的加解密 key 来进行加密，具体请见《加密解密技术方案》, 在收到推送后需进行解密（详细请见《消息加解密接入指引》）,
//
// 字段说明
// 参数	                        类型	字段描述
// AppId	                    string	第三方平台 appid
// CreateTime	                number	时间戳
// InfoType	                    string	通知类型，详见InfoType 说明
// AuthorizerAppid	            string	公众号或小程序的 appid
// AuthorizationCode	        string	授权码，可用于获取授权信息
// AuthorizationCodeExpiredTime	nubmer	授权码过期时间 单位秒
// PreAuthCode	                string	预授权码
//
// InfoType 说明
// type	            说明
// unauthorized	    取消授权
// updateauthorized	更新授权
// authorized	    授权成功
// 推送内容解密后的示例：
//
// 授权成功通知
// <xml>
//  <AppId>第三方平台appid</AppId>
//  <CreateTime>1413192760</CreateTime>
//  <InfoType>authorized</InfoType>
//  <AuthorizerAppid>公众号appid</AuthorizerAppid>
//  <AuthorizationCode>授权码</AuthorizationCode>
//  <AuthorizationCodeExpiredTime>过期时间</AuthorizationCodeExpiredTime>
//  <PreAuthCode>预授权码</PreAuthCode>
// <xml>
// 取消授权通知
// <xml>
//  <AppId>第三方平台appid</AppId>
//  <CreateTime>1413192760</CreateTime>
//  <InfoType>unauthorized</InfoType>
//  <AuthorizerAppid>公众号appid</AuthorizerAppid>
// </xml>
// 授权更新通知
// <xml>
//  <AppId>第三方平台appid</AppId>
//  <CreateTime>1413192760</CreateTime>
//  <InfoType>updateauthorized</InfoType>
//  <AuthorizerAppid>公众号appid</AuthorizerAppid>
//  <AuthorizationCode>授权码</AuthorizationCode>
//  <AuthorizationCodeExpiredTime>过期时间</AuthorizationCodeExpiredTime>
//  <PreAuthCode>预授权码</PreAuthCode>
// <xml>
type AuthorizationMessage struct {
	PlatformMessage
	AuthorizerAppid              string `xml:"AuthorizerAppid"`              // 公众号或小程序的 appid
	AuthorizationCode            string `xml:"AuthorizationCode"`            // 授权码，可用于获取授权信息
	AuthorizationCodeExpiredTime int64  `xml:"AuthorizationCodeExpiredTime"` // 授权码过期时间 单位秒
	PreAuthCode                  string `xml:"PreAuthCode"`                  // 授权码过期时间 单位秒
}