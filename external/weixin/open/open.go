package open

import (
	"encoding/json"
	"fmt"
	"net/url"
	"time"

	"gitee.com/titan-kit/titan/cache"
	"gitee.com/titan-kit/titan/external/weixin/common"
	"gitee.com/titan-kit/titan/integrated/convert"
	"gitee.com/titan-kit/titan/log"
)

// 此项目所有开发接口文档来源于:https://developers.weixin.qq.com/doc/oplatform/Third-party_Platforms/Third_party_platform_appid.html
const (
	AuthorizationUrl       = "https://mp.weixin.qq.com/cgi-bin/componentloginpage?component_appid=%s&pre_auth_code=%s&redirect_uri=%s"
	TokenUrl               = "/cgi-bin/component/api_component_token"
	CreatePreAuthCodeUrl   = "/cgi-bin/component/api_create_preauthcode?component_access_token="
	QueryAuthUrl           = "/cgi-bin/component/api_query_auth?component_access_token="
	AuthorizerTokenUrl     = "/cgi-bin/component/api_authorizer_token?component_access_token="
	AuthorizerInfoUrl      = "/cgi-bin/component/api_get_authorizer_info?component_access_token="
	AuthorizerOptionUrl    = "/cgi-bin/component/api_get_authorizer_option?component_access_token="
	SetAuthorizerOptionUrl = "/cgi-bin/component/api_set_authorizer_option?component_access_token="
	AuthorizerListUrl      = "/cgi-bin/component/api_get_authorizer_list?component_access_token="
	ClearQuotaUrl          = "/cgi-bin/component/clear_quota?component_access_token="
	OpenCreateUrl          = "/cgi-bin/open/create?access_token="
	OpenBindUrl            = "/cgi-bin/open/bind?access_token="
	OpenUnbindUrl          = "/cgi-bin/open/unbind?access_token="
	OpenGetUrl             = "/cgi-bin/open/get?access_token="
)

type ThirdPlatformConfig struct {
	AppId            string   `json:"appId"`
	Secret           string   `json:"secret"`
	Token            string   `json:"token"`
	AesKey           string   `json:"aesKey"`
	AuthorizationUrl string   `json:"authorizationUrl"`
	Refreshed        []string `json:"refreshed"`
}

func NewThirdPlatform(cp cache.Provider, log *log.Slf4g, cfg ThirdPlatformConfig) *ThirdPlatform {
	api := common.NewApi(common.ApiUrl)
	return &ThirdPlatform{api, cfg, cp, log}
}

type ThirdPlatform struct {
	*common.Api
	ThirdPlatformConfig
	cache  cache.Provider
	logger *log.Slf4g
}

// SyncToken 令牌
// 令牌（component_access_token）是第三方平台接口的调用凭据。令牌的获取是有限制的，每个令牌的有效期为 2 小时，请自行做好令牌的管理，在令牌快过期时（比如1小时50分），重新调用接口获取。
//
// 如未特殊说明，令牌一般作为被调用接口的 GET 参数 component_access_token 的值使用
//
// 请求地址(POST)https://api.weixin.qq.com/cgi-bin/component/api_component_token
// 参数                       类型	必填	说明
// component_appid	        string	 是	    第三方平台 appid
// component_appsecret	    string	 是	    第三方平台 appsecret
// component_verify_ticket	string	 是	    微信后台推送的 ticket
// 数据示例：
//
// {
//  "component_appid":  "appid_value" ,
//  "component_appsecret":  "appsecret_value",
//  "component_verify_ticket": "ticket_value"
// }
// 返回详情: TokenModel
func (op *ThirdPlatform) SyncToken(ticket string) bool {
	var tm TokenModel
	if err := op.Post(TokenUrl, map[string]interface{}{"component_appid": op.AppId, "component_appsecret": op.Secret, "component_verify_ticket": ticket}).Do(&tm); err == nil {
		if tm.Success() {
			op.cache.SetExpires(fmt.Sprintf(common.TokenKey, op.AppId), tm.AccessToken, time.Duration(tm.ExpiresIn)*time.Second)
			op.cache.SetExpires(fmt.Sprintf(common.RefreshTime, op.AppId), convert.ToStr(time.Now().Unix()), common.CacheNeverTimeout)
			return true
		} else {
			op.logger.ErrorF("返回错误(%d)信息:%s", tm.ErrCode, tm.ErrMsg)
		}
	} else {
		op.logger.ErrorF("同步令牌发生错误:%+v", err)
	}
	return false
}

// MaskAuthorizationUrl 下面对其进行详细介绍：
//
// 步骤 1：第三方平台方获取预授权码（pre_auth_code）
//
// 详见
// 步骤 2：引入用户进入授权页
//
// 第三方平台型服务商可以在自己的网站中放置“微信公众号授权”或者“小程序授权”的入口，或生成授权链接放置在移动网页中，引导公众号和小程序管理员进入授权页。
//
// 方式一：授权注册页面扫码授权
//
// 授权页网址为：
//
// https://mp.weixin.qq.com/cgi-bin/componentloginpage?component_appid=xxxx&pre_auth_code=xxxxx&redirect_uri=xxxx&auth_type=xxx。
//
// 参数	            必填	参数说明
// component_appid	是	    第三方平台方 appid
// pre_auth_code	是	    预授权码
// redirect_uri	    是	    回调 URI
// auth_type	    否	    要授权的帐号类型， 1 则商户扫码后，手机端仅展示公众号、2 表示仅展示小程序，3 表示公众号和小程序都展示。如果为未指定，则默认小程序和公众号都展示。第三方平台开发者可以使用本字段来控制授权的帐号类型。
// biz_appid	    否	    指定授权唯一的小程序或公众号
func (op *ThirdPlatform) MaskAuthorizationUrl(preAuthCode string) string {
	return fmt.Sprintf(AuthorizationUrl, op.AppId, preAuthCode, url.QueryEscape(op.AuthorizationUrl))
}
func (op *ThirdPlatform) maskComponentAccessTokenUrl(url string) (string, bool) {
	if token := op.cache.String(fmt.Sprintf(common.TokenKey, op.AppId)); token != "" {
		return url + token, true
	} else {
		op.logger.InfoF("不存在第三方平台[%s]的access token", op.AppId)
		return url, false
	}
}

// CreatePreAuthCode 预授权码
// 预授权码（pre_auth_code）是第三方平台方实现授权托管的必备信息，每个预授权码有效期为 10 分钟。需要先获取令牌才能调用。使用过程中如遇到问题，可在开放平台服务商专区发帖交流。
//
// 请求地址
// POST https://api.weixin.qq.com/cgi-bin/component/api_create_preauthcode?component_access_token=COMPONENT_ACCESS_TOKEN
// 请求参数说明
// 参数	                    类型	必填	说明
// component_access_token	string	 是	    第三方平台component_access_token，不是authorizer_access_token
// component_appid	        string	 是	    第三方平台 appid
// POST 数据示例：
//
// {
//  "component_appid": "appid_value"
// }
// 返回参数详见: PreAuthCodeModel
func (op *ThirdPlatform) CreatePreAuthCode() (t bool, pac PreAuthCodeModel) {
	if uri, ok := op.maskComponentAccessTokenUrl(CreatePreAuthCodeUrl); ok {
		if err := op.Post(uri, map[string]interface{}{"component_appid": op.AppId}).Do(&pac); err == nil {
			t = pac.Success()
		} else {
			op.logger.ErrorF("获取授权码发生错误:%+v", err)
		}
	}
	return
}

// AuthorizationInfo 使用授权码获取授权信息
// 当用户在第三方平台授权页中完成授权流程后，第三方平台开发者可以在回调 URI 中通过 URL 参数获取授权码。使用以下接口可以换取公众号/小程序的授权信息。建议保存授权信息中的刷新令牌（authorizer_refresh_token）。使用过程中如遇到问题，可在开放平台服务商专区发帖交流。
//
// 注意： 公众号/小程序可以自定义选择部分权限授权给第三方平台，因此第三方平台开发者需要通过该接口来获取公众号/小程序具体授权了哪些权限，而不是简单地认为自己声明的权限就是公众号/小程序授权的权限。
//
// 请求地址
// POST https://api.weixin.qq.com/cgi-bin/component/api_query_auth?component_access_token=COMPONENT_ACCESS_TOKEN
// 请求参数说明
// 参数	                    类型	必填	说明
// component_access_token	string	 是	    第三方平台component_access_token，不是authorizer_access_token
// component_appid	        string	 是	    第三方平台 appid
// authorization_code	    string	 是	    授权码, 会在授权成功时返回给第三方平台，详见第三方平台授权流程说明
// POST 数据示例：
//
// {
// "component_appid":"appid_value" ,
// "authorization_code": "auth_code_value"
// }
// 返回参数详见: AuthorizationInfoModel
func (op *ThirdPlatform) AuthorizationInfo(authCode string) (t bool, aim AuthorizationInfoModel) {
	if uri, ok := op.maskComponentAccessTokenUrl(QueryAuthUrl); ok {
		if err := op.Post(uri, map[string]interface{}{"component_appid": op.AppId, "authorization_code": authCode}).Do(&aim); err == nil {
			op.cache.SetExpires(fmt.Sprintf(common.AuthorizationKey, aim.AuthorizationInfo.AuthorizerAppId), common.JsonTo(&aim.AuthorizationInfo, false), common.CacheNeverTimeout)
			t = aim.Success()
		} else {
			op.logger.ErrorF("获取授权信息发生错误:%+v", err)
		}
	}
	return
}

// AuthorizerToken 获取/刷新接口调用令牌
// 在公众号/小程序接口调用令牌（authorizer_access_token）失效时，可以使用刷新令牌（authorizer_refresh_token）获取新的接口调用令牌。使用过程中如遇到问题，可在开放平台服务商专区发帖交流。
//
// 注意： authorizer_access_token 有效期为 2 小时，开发者需要缓存 authorizer_access_token，避免获取/刷新接口调用令牌的 API 调用触发每日限额。缓存方法可以参考：https://developers.weixin.qq.com/doc/offiaccount/Basic_Information/Get_access_token.html
//
// 请求地址
// POST https://api.weixin.qq.com/cgi-bin/component/api_authorizer_token?component_access_token=COMPONENT_ACCESS_TOKEN
// 请求参数说明
// 参数	                    类型	必填	说明
// component_access_token	string	 是	    第三方平台component_access_token
// component_appid	        string	 是	    第三方平台 appid
// authorizer_appid	        string	 是	    授权方 appid
// authorizer_refresh_token	string	 是	    刷新令牌，获取授权信息时得到
// POST 数据示例：
//
// {
//  "component_appid": "appid_value",
//  "authorizer_appid": "auth_appid_value",
//  "authorizer_refresh_token": "refresh_token_value"
// }
// 返回参数详见: AuthorizerTokenModel
func (op *ThirdPlatform) AuthorizerToken(authorizerAppId string) (t bool) {
	if convert.ContainsString(op.Refreshed, authorizerAppId) == -1 {
		return
	}
	if uri, ok := op.maskComponentAccessTokenUrl(AuthorizerTokenUrl); ok {
		if value := op.cache.String(fmt.Sprintf(common.AuthorizationKey, authorizerAppId)); value != "" {
			var ai common.AuthorizationInfo
			var err error
			if err = json.Unmarshal([]byte(value), &ai); err == nil {
				op.logger.InfoF("授权信息结果为：%+v", ai)
				var atm AuthorizerTokenModel
				if err = op.Post(uri, map[string]interface{}{"component_appid": op.AppId, "authorizer_appid": authorizerAppId, "authorizer_refresh_token": ai.AuthorizerRefreshToken}).Do(&atm); err == nil {
					t = atm.Success()
					if t {
						ai.AuthorizerAccessToken = atm.AuthorizerAccessToken
						ai.ExpiresIn = atm.ExpiresIn
						op.cache.SetExpires(fmt.Sprintf(common.AuthorizationKey, ai.AuthorizerAppId), common.JsonTo(&ai, false), common.CacheNeverTimeout)
						op.cache.SetExpires(fmt.Sprintf(common.RefreshTime, authorizerAppId), convert.ToStr(time.Now().Unix()), common.CacheNeverTimeout)
					}
				}
			}
			if err != nil {
				op.logger.ErrorF("获取/刷新接口调用令牌发生错误:%+v", err)
			}
		}
	}
	return
}

// AuthorizerInfo 获取授权方的帐号基本信息
// 该 API 用于获取授权方的基本信息，包括头像、昵称、帐号类型、认证类型、微信号、原始ID和二维码图片URL。使用过程中如遇到问题，可在开放平台服务商专区发帖交流。
//
// 注意： 公众号和小程序的接口返回结果不一样。
//
// 请求地址
// POST https://api.weixin.qq.com/cgi-bin/component/api_get_authorizer_info?component_access_token=COMPONENT_ACCESS_TOKEN
// 请求参数说明
// 参数	                    类型	    必填	    说明
// component_access_token	string	 是	    第三方平台component_access_token，不是authorizer_access_token
// component_appid	        string	 是	    第三方平台 appid
// authorizer_appid	        string	 是	    授权方 appid
// POST 数据示例：
//
// {
//  "component_appid": "appid_value" ,
//  "authorizer_appid": "auth_appid_value"
// }
// 返回参数详见: AuthorizerInfoModel
func (op *ThirdPlatform) AuthorizerInfo(authorizerAppId string) (t bool, aim AuthorizerInfoModel) {
	if uri, ok := op.maskComponentAccessTokenUrl(AuthorizerInfoUrl); ok {
		if err := op.Post(uri, map[string]interface{}{"component_appid": op.AppId, "authorizer_appid": authorizerAppId}).Do(&aim); err == nil {
			t = aim.Success()
		} else {
			op.logger.ErrorF("获取授权方的帐号基本信息错误:%+v", err)
		}
	}
	return
}

// AuthorizerOption 获取授权方选项信息
// 本 API 用于获取授权方的公众号/小程序的选项设置信息，如：地理位置上报，语音识别开关，多客服开关。使用过程中如遇到问题，可在开放平台服务商专区发帖交流。
//
// 注意： 获取各项选项设置信息，需要有授权方的授权，详见权限集说明。
//
// 请求地址
// POST https://api.weixin.qq.com/cgi-bin/component/api_get_authorizer_option?component_access_token=COMPONENT_ACCESS_TOKEN
// 请求参数说明
// 参数	类型	必填	说明
// component_access_token	string	是	第三方平台component_access_token，不是authorizer_access_token
// component_appid	string	是	第三方平台 appid
// authorizer_appid	string	是	授权公众号或小程序的 appid
// option_name	string	是	选项名称
// POST 数据示例：
//
// {
//  "component_appid": "appid_value",
//  "authorizer_appid": "auth_appid_value ",
//  "option_name": "option_name_value"
// }
// 返回参数详见: AuthorizerOptionModel
func (op *ThirdPlatform) AuthorizerOption(authorizerAppId string, option AuthorizerOption) (t bool, aom AuthorizerOptionModel) {
	if uri, ok := op.maskComponentAccessTokenUrl(AuthorizerOptionUrl); ok {
		if err := op.Post(uri, map[string]interface{}{"component_appid": op.AppId, "authorizer_appid": authorizerAppId, "option_name": option}).Do(&aom); err == nil {
			t = aom.Success()
		} else {
			op.logger.ErrorF(" 获取授权方选项错误:%+v", err)
		}
	}
	return
}

// SetAuthorizerOption 设置授权方选项信息
// 本 API 用于设置授权方的公众号/小程序的选项信息，如：地理位置上报，语音识别开关，多客服开关。使用过程中如遇到问题，可在开放平台服务商专区发帖交流。
//
// 注意： 设置各项选项设置信息，需要有授权方的授权，详见权限集说明。
//
// 请求地址
// POST https://api.weixin.qq.com/cgi-bin/component/api_set_authorizer_option?component_access_token=COMPONENT_ACCESS_TOKEN
// 请求参数说明
// 参数	                    类型	必填	说明
// component_access_token	string	是	    第三方平台component_access_token，不是authorizer_access_token
// component_appid	        string	是	    第三方平台 appid
// authorizer_appid	        string	是	    授权公众号或小程序的 appid
// option_name	            string	是	    选项名称
// option_value	            string	是	    设置的选项值
// POST 数据示例：
//
// {
//  "component_appid": "appid_value",
//  "authorizer_appid": "auth_appid_value ",
//  "option_name": "option_name_value",
//  "option_value": "option_value_value"
// }
// 返回参数详见: common.ErrorModel
func (op *ThirdPlatform) SetAuthorizerOption(authorizerAppId string, option AuthorizerOption, value string) (t bool, em common.ErrorModel) {
	if uri, ok := op.maskComponentAccessTokenUrl(SetAuthorizerOptionUrl); ok {
		if err := op.Post(uri, map[string]interface{}{"component_appid": op.AppId, "authorizer_appid": authorizerAppId, "option_name": option, "option_value": value}).Do(&em); err == nil {
			t = em.Success()
		} else {
			op.logger.ErrorF(" 设置授权方选项错误:%+v", err)
		}
	}
	return
}

// AuthorizerList 拉取所有已授权的帐号信息
// 使用本 API 拉取当前所有已授权的帐号基本信息。使用过程中如遇到问题，可在开放平台服务商专区发帖交流。
//
// 请求地址
// POST https://api.weixin.qq.com/cgi-bin/component/api_get_authorizer_list?component_access_token=COMPONENT_ACCESS_TOKEN
// 请求参数说明
// 参数	类型	必填	说明
// component_access_token	string	是	第三方平台component_access_token，不是authorizer_access_token
// component_appid	string	是	第三方平台 APPID
// offset	number	是	偏移位置/起始位置
// count	number	是	拉取数量，最大为 500
// POST 数据示例：
//
// {
//  "component_appid": "appid_value",
//  "offset": 0,
//  "count": 100
// }
// 返回参数详见: AuthorizerInfoListModel
func (op *ThirdPlatform) AuthorizerList(offset, count int) (t bool, ail AuthorizerInfoListModel) {
	if uri, ok := op.maskComponentAccessTokenUrl(AuthorizerListUrl); ok {
		if err := op.Post(uri, map[string]interface{}{"component_appid": op.AppId, "offset": offset, "count": count}).Do(&ail); err == nil {
			t = ail.Success()
		} else {
			op.logger.ErrorF(" 拉取所有已授权的帐号错误:%+v", err)
		}
	}
	return
}

// OpenCreate 创建开放平台帐号并绑定公众号/小程序
// 该 API 用于创建一个开放平台帐号，并将一个尚未绑定开放平台帐号的公众号/小程序绑定至该开放平台帐号上。新创建的开放平台帐号的主体信息将设置为与之绑定的公众号或小程序的主体。使用过程中如遇到问题，可在开放平台服务商专区发帖交流。
//
// 请求地址
// POST https://api.weixin.qq.com/cgi-bin/open/create?access_token=ACCESS_TOKEN
// 请求参数说明
// 参数	        类型	必填	说明
// access_token	String	是	    第三方平台接口调用令牌authorizer_access_token
// appid	    String	是	    授权公众号或小程序的 appid
// POST 数据示例
//
// {
//  "appid": "auth_appid_value"
// }
// 返回参数详见: OpenPlatformModel
func (op *ThirdPlatform) OpenCreate(appId string) (t bool, ocm OpenPlatformModel) {
	if token, ok := common.MaskAccessTokenUrl(op.cache, op.logger, appId); ok {
		if err := op.Post(OpenCreateUrl+token, map[string]interface{}{"appid": appId}).Do(&ocm); err == nil {
			t = ocm.Success()
		} else {
			op.logger.ErrorF(" 创建开放平台帐号并绑定公众号/小程序错误:%+v", err)
		}
	}
	return
}

// OpenBind 将公众号/小程序绑定到开放平台帐号下
// 该 API 用于将一个尚未绑定开放平台帐号的公众号或小程序绑定至指定开放平台帐号上。二者须主体相同。使用过程中如遇到问题，可在开放平台服务商专区发帖交流。
//
// 请求地址
// POST https://api.weixin.qq.com/cgi-bin/open/bind?access_token=xxxx
// 请求参数说明
// 参数	        类型	必填	说明
// access_token	String	是	    第三方平台接口调用令牌authorizer_access_token
// appid	    String	是	    授权公众号或小程序的 appid
// open_appid	String	是	    开放平台帐号 appid，由创建开发平台帐号接口返回
// POST 数据示例
//
// {
//  "appid": "auth_appid_value",
//  "open_appid": "open_appid_value"
// }
// 返回参数详见: common.ErrorModel
func (op *ThirdPlatform) OpenBind(appId, openAppId string) (bool, common.ErrorModel) {
	return op.bindOrUnbind(OpenBindUrl, appId, openAppId)
}

// OpenUnbind 将公众号/小程序从开放平台帐号下解绑
// 该 API 用于将一个公众号或小程序与指定开放平台帐号解绑。开发者须确认所指定帐号与当前该公众号或小程序所绑定的开放平台帐号一致。使用过程中如遇到问题，可在开放平台服务商专区发帖交流。
//
// 请求地址
// POST https://api.weixin.qq.com/cgi-bin/open/unbind?access_token=ACCESS_TOKEN
// 请求参数说明
// 参数	        类型	必填	说明
// access_token	String	是	    第三方平台接口调用令牌authorizer_access_token
// appid	    String	是	    授权公众号或小程序的 appid
// open_appid	String	是	    开放平台帐号 appid，由创建开发平台帐号接口返回
// POST 数据示例
//
// {
//  "appid": "auth_appid_value",
//  "open_appid": "open_appid_value"
// }
// 返回参数详见: common.ErrorModel
func (op *ThirdPlatform) OpenUnbind(appId, openAppId string) (bool, common.ErrorModel) {
	return op.bindOrUnbind(OpenUnbindUrl, appId, openAppId)
}

func (op *ThirdPlatform) bindOrUnbind(url, appId, openAppId string) (t bool, em common.ErrorModel) {
	if token, ok := common.MaskAccessTokenUrl(op.cache, op.logger, appId); ok {
		if err := op.Post(url+token, map[string]interface{}{"appid": appId, "open_appid": openAppId}).Do(&em); err == nil {
			t = em.Success()
		} else {
			op.logger.ErrorF("将公众号/小程序绑定或者解绑到开放平台帐号下发生错误:%+v", err)
		}
	}
	return
}

// OpenGet 获取公众号/小程序所绑定的开放平台帐号
// 该 API 用于获取公众号或小程序所绑定的开放平台帐号。使用过程中如遇到问题，可在开放平台服务商专区发帖交流。
//
// 请求地址
// POST https://api.weixin.qq.com/cgi-bin/open/get?access_token=ACCESS_TOKEN
// 请求参数说明
// 参数	        类型	必填	说明
// access_token	String	是	    第三方平台接口调用令牌authorizer_access_token
// appid	    String	是	    授权公众号或小程序的 appid
// POST 数据示例
//
// {
//  "appid": "auth_appid_value"
// }
// 返回参数详见: OpenPlatformModel
func (op *ThirdPlatform) OpenGet(appId string) (t bool, ocm OpenPlatformModel) {
	if token, ok := common.MaskAccessTokenUrl(op.cache, op.logger, appId); ok {
		if err := op.Post(OpenGetUrl+token, map[string]interface{}{"appid": appId}).Do(&ocm); err == nil {
			t = ocm.Success()
		} else {
			op.logger.ErrorF(" 创建开放平台帐号并绑定公众号/小程序错误:%+v", err)
		}
	}
	return
}

// ClearQuota 第三方平台对其所有 API 调用次数清零（只与第三方平台相关，与公众号无关，接口如 api_component_token）
//
// POST https://api.weixin.qq.com/cgi-bin/component/clear_quota?component_access_token=COMPONENT_ACCESS_TOKEN
// 调用示例：
//
// {
//  "component_appid": "appid_value"
// }
// 参数说明：
//
// 参数	                    是否必须	说明
// component_access_token	是	    调用接口凭据
// component_appid	        是	    第三方平台 APPID
// 返回参数详见: common.ErrorModel
func (op *ThirdPlatform) ClearQuota(appId string) (t bool, em common.ErrorModel) {
	if uri, ok := op.maskComponentAccessTokenUrl(ClearQuotaUrl); ok {
		if err := op.Post(uri, map[string]interface{}{"component_appid": appId}).Do(&em); err == nil {
			t = em.Success()
		} else {
			op.logger.ErrorF("第三方平台对其所有API调用次数清零发生错误:%+v", err)
		}
	}
	return
}