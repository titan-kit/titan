package open

import (
	"gitee.com/titan-kit/titan/external/weixin/common"
)

// TokenModel 令牌结果参数说明
// 参数	                    类型	说明
// component_access_token	string	第三方平台 access_token
// expires_in	            number	有效期，单位：秒
// 结果示例：
//
// {
//  "component_access_token": "61W3mEpU66027wgNZ_MhGHNQDHnFATkDa9-2llqrMBjUwxRSNPbVsMmyD-yq8wZETSoE5NQgecigDrSHkPtIYA",
//  "expires_in": 7200
// }
type TokenModel struct {
	common.ErrorModel
	AccessToken string `json:"component_access_token"` // 第三方平台 access_token
	ExpiresIn   int    `json:"expires_in"`             // 有效期，单位：秒
}

// PreAuthCodeModel 预授权码
// 结果参数说明
// 参数	            类型	说明
// pre_auth_code	string	预授权码
// expires_in	    number	有效期，单位：秒
// 返回结果示例：
//
// {
//  "pre_auth_code": "Cx_Dk6qiBE0Dmx4EmlT3oRfArPvwSQ-oa3NL_fwHM7VI08r52wazoZX2Rhpz1dEw",
//  "expires_in": 600
// }
type PreAuthCodeModel struct {
	common.ErrorModel
	PreAuthCode string `json:"pre_auth_code"` // 预授权码
	ExpiresIn   int    `json:"expires_in"`    // 有效期，单位：秒
}

// AuthorizationInfoModel 授权信息结果参数说明
// 参数	                类型	    说明
// authorization_info	Object	    授权信息
// 授权信息说明
// 参数	                    类型	    说明
// authorizer_appid	        string	    授权方 appid
// authorizer_access_token	string	    接口调用令牌（在授权的公众号/小程序具备 API 权限时，才有此返回值）
// expires_in	            number	    authorizer_access_token 的有效期（在授权的公众号/小程序具备API权限时，才有此返回值），单位：秒
// authorizer_refresh_token	string	    刷新令牌（在授权的公众号具备API权限时，才有此返回值），刷新令牌主要用于第三方平台获取和刷新已授权用户的 authorizer_access_token。
//                                      一旦丢失，只能让用户重新授权，才能再次拿到新的刷新令牌。用户重新授权后，之前的刷新令牌会失效
// func_info		授权给开发者的权限集列表
// 返回结果示例：
//
// {
//  "authorization_info": {
//    "authorizer_appid": "wxf8b4f85f3a794e77",
//    "authorizer_access_token": "QXjUqNqfYVH0yBE1iI_7vuN_9gQbpjfK7hYwJ3P7xOa88a89-Aga5x1NMYJyB8G2yKt1KCl0nPC3W9GJzw0Zzq_dBxc8pxIGUNi_bFes0qM",
//    "expires_in": 7200,
//    "authorizer_refresh_token": "dTo-YCXPL4llX-u1W1pPpnp8Hgm4wpJtlR6iV0doKdY",
//    "func_info": [
//      {
//        "funcscope_category": {
//          "id": 1
//        }
//      },
//      {
//        "funcscope_category": {
//          "id": 2
//        }
//      },
//      {
//        "funcscope_category": {
//          "id": 3
//        }
//      }
//    ]
//  }
// }
type AuthorizationInfoModel struct {
	common.ErrorModel
	AuthorizationInfo common.AuthorizationInfo `json:"authorization_info"`
}

// AuthorizerTokenModel 获取/刷新接口调用令牌结果参数说明
// 参数	                    类型	    说明
// authorizer_access_token	string	    授权方令牌
// expires_in	            nubmer      有效期，单位：秒
// authorizer_refresh_token	string	    刷新令牌
// 返回结果示例：
//
// {
//  "authorizer_access_token": "some-access-token",
//  "expires_in": 7200,
//  "authorizer_refresh_token": "refresh_token_value"
// }
type AuthorizerTokenModel struct {
	common.ErrorModel
	AuthorizerAccessToken  string `json:"authorizer_access_token"`  // 接口调用令牌（在授权的公众号/小程序具备 API 权限时，才有此返回值）
	ExpiresIn              int    `json:"expires_in"`               // 有效期，单位：秒
	AuthorizerRefreshToken string `json:"authorizer_refresh_token"` // 刷新令牌（在授权的公众号具备API权限时，才有此返回值），刷新令牌主要用于第三方平台获取和刷新已授权用户的 authorizer_access_token。
}

// AuthorizerInfoModel 返回参数说明（公众号）
// 参数	类型	说明
// authorization_info	object	授权信息，详见authorization_info
// authorizer_info	object	详见公众号帐号信息
// 公众号帐号信息
// 参数	类型	说明
// nick_name	string	昵称
// head_img	string	头像
// service_type_info	object	公众号类型
// verify_type_info	object	公众号认证类型
// user_name	string	原始 ID
// principal_name	string	主体名称
// alias	string	公众号所设置的微信号，可能为空
// business_info	object	用以了解功能的开通状况（0代表未开通，1代表已开通），详见business_info 说明
// qrcode_url	string	二维码图片的 URL，开发者最好自行也进行保存
// 公众号类型
// 类型	说明
// 0	订阅号
// 1	由历史老帐号升级后的订阅号
// 2	服务号
// 公众号认证类型
// 类型	说明
// -1	未认证
// 0	微信认证
// 1	新浪微博认证
// 2	腾讯微博认证
// 3	已资质认证通过但还未通过名称认证
// 4	已资质认证通过、还未通过名称认证，但通过了新浪微博认证
// 5	已资质认证通过、还未通过名称认证，但通过了腾讯微博认证
// 返回结果示例（公众号）：
//
// {
//  "authorizer_info": {
//    "nick_name": "微信SDK Demo Special",
//    "head_img": "http://wx.qlogo.cn/mmopen/GPy",
//    "service_type_info": {
//      "id": 2
//    },
//    "verify_type_info": {
//      "id": 0
//    },
//    "user_name": "gh_eb5e3a772040",
//    "principal_name": "腾讯计算机系统有限公司",
//    "business_info": {
//      "open_store": 0,
//      "open_scan": 0,
//      "open_pay": 0,
//      "open_card": 0,
//      "open_shake": 0
//    },
//    "alias": "paytest01",
//    "qrcode_url": "URL",
//  },
//  "authorization_info": {
//    "authorizer_appid": "wxf8b4f85f3a794e77",
//    "func_info": [
//      {
//        "funcscope_category": {
//          "id": 1
//        }
//      },
//      {
//        "funcscope_category": {
//          "id": 2
//        }
//      }
//    ]
//  }
// }
// 返回参数说明（小程序）
// 参数	类型	说明
// authorizer_info	object	详见小程序帐号信息
// authorization_info	object	授权信息，详见authorization_info
// 小程序帐号信息
// 参数	类型	说明
// nick_name	string	昵称
// head_img	string	头像
// service_type_info	object	id为0
// verify_type_info	object	小程序认证类型
// user_name	string	原始 ID
// principal_name	string	主体名称
// signature	string	帐号介绍
// business_info	object	用以了解功能的开通状况（0代表未开通，1代表已开通），详见business_info 说明
// qrcode_url	string	二维码图片的 URL，开发者最好自行也进行保存
// MiniProgramInfo	object	小程序配置，根据这个字段判断是否为小程序类型授权
// 小程序认证类型
// 类型	说明
// -1	未认证
// 0	微信认证
// 小程序配置说明
// 参数	类型	说明
// network	object	小程序配置的合法域名信息
// categories	object array	小程序配置的类目信息
// 返回结果示例（小程序）：
//
// {"authorizer_info":
//     {"nick_name":"找呀找呀找盲盒",
//      "head_img":"http:xxx",
//      "service_type_info":{"id":0},
//      "verify_type_info":{"id":-1},
//      "user_name":"gh_3dacad47dc6b",
//      "alias":"",
//      "qrcode_url":"http:xxx",
//      "business_info":{"open_pay":0,"open_shake":0,"open_scan":0,"open_card":0,"open_store":0},
//      "idc":1,"principal_name":"个人","signature":"欢迎小伙伴一起参与盲盒游戏，集齐9款鞋卡，可以兑换大奖！",
//      "MiniProgramInfo":{"network":{"RequestDomain":["https:xxx","https:xxxx","https:xxx"],"WsRequestDomain":[],"UploadDomain":[],"DownloadDomain":[],"BizDomain":[],"UDPDomain":[]},
//                         "categories":[{"first":"工具","second":"效率"}],"visit_status":0}},
// "authorization_info":{"authorizer_appid":"wxf24d2dfc1a974128",
//                       "authorizer_refresh_token":"xxxxxx",
//                       "func_info":[{"funcscope_category":{"id":17}},
//                                    {"funcscope_category":{"id":18},
//                                     "confirm_info":{"need_confirm":0,"already_confirm":0,"can_confirm":0}},
//                                    {"funcscope_category":{"id":19}},{"funcscope_category":{"id":25},"confirm_info":{"need_confirm":0,"already_confirm":0,"can_confirm":0}},
//                                    {"funcscope_category":{"id":30},"confirm_info":{"need_confirm":0,"already_confirm":0,"can_confirm":0}},
//                                    {"funcscope_category":{"id":31},"confirm_info":{"need_confirm":0,"already_confirm":0,"can_confirm":0}},
//                                    {"funcscope_category":{"id":36}},{"funcscope_category":{"id":37}},{"funcscope_category":{"id":40}},
//                                    ]}
// }
type AuthorizerInfoModel struct {
	common.ErrorModel
	AuthorizationInfo common.AuthorizationInfo `json:"authorization_info"`
	AuthorizerInfo    AuthorizerInfo           `json:"authorizer_info"`
}

// AuthorizerInfo 公众号帐号信息
// 参数	                类型	说明
// nick_name	        string	昵称
// head_img	            string	头像
// service_type_info    object	公众号类型
// verify_type_info	    object	公众号认证类型
// user_name	        string	原始 ID
// principal_name	    string	主体名称
// alias	            string	公众号所设置的微信号，可能为空
// business_info	    object	用以了解功能的开通状况（0代表未开通，1代表已开通），详见business_info 说明
// qrcode_url	        string	二维码图片的 URL，开发者最好自行也进行保存
//
//
// business_info    说明
// 功能	            说明
// open_store	    是否开通微信门店功能
// open_scan	    是否开通微信扫商品功能
// open_pay	        是否开通微信支付功能
// open_card	    是否开通微信卡券功能
// open_shake	    是否开通微信摇一摇功能
//
type AuthorizerInfo struct {
	NickName        string `json:"nick_name"`
	HeadImg         string `json:"head_img"`
	ServiceTypeInfo struct {
		Id int `json:"id"`
	} `json:"service_type_info"`
	VerifyTypeInfo struct {
		Id int `json:"id"`
	} `json:"verify_type_info"`
	UserName      string `json:"user_name"`
	PrincipalName string `json:"principal_name"`
	Alias         string `json:"alias"`
	QrcodeUrl     string `json:"qrcode_url"`
	BusinessInfo  struct {
		OpenStore string `json:"open_store"`
		OpenScan  string `json:"open_scan"`
		OpenPay   string `json:"open_pay"`
		OpenCard  string `json:"open_card"`
		OpenShake string `json:"open_shake"`
	} `json:"business_info"`
	// 以下部分小程序专有
	Signature       string `json:"signature"`
	Idc             int    `json:"idc"`
	MiniProgramInfo struct {
		Network struct {
			RequestDomain   []string `json:"RequestDomain"`
			WsRequestDomain []string `json:"WsRequestDomain"`
			UploadDomain    []string `json:"UploadDomain"`
			DownloadDomain  []string `json:"DownloadDomain"`
			BizDomain       []string `json:"BizDomain"`
			UDPDomain       []string `json:"UDPDomain"`
		} `json:"network"`
		Categories  []map[string]string `json:"categories"`
		VisitStatus int                 `json:"visit_status"`
	} `json:"MiniProgramInfo"`
}

type AuthorizerOption string

const (
	LocationReport  AuthorizerOption = "location_report"
	VoiceRecognize  AuthorizerOption = "voice_recognize"
	CustomerService AuthorizerOption = "customer_service"
)

// AuthorizerOptionModel 获取授权方选项信息返回结果示例：
//
// {
//  "authorizer_appid": "wx7bc5ba58cabd00f4",
//  "option_name": "voice_recognize",
//  "option_value": "1"
// }
// 选项名称及可选值说明
// option_name	    选项名说明	    option_value	选项值说明
// location_report	地理位置上报选项	0	            无上报
//                                  1	            进入会话时上报
//                                  2	            每 5s 上报
// voice_recognize	语音识别开关选项	0	            关闭语音识别
//                                  1	            开启语音识别
// customer_service	多客服开关选项	0	            关闭多客服
//                                  1	            开启多客服
type AuthorizerOptionModel struct {
	common.ErrorModel
	AuthorizerAppid string `json:"authorizer_appid"` // 公众号或小程序的 appid
	OptionName      string `json:"option_name"`      // 消息类型
	OptionValue     string `json:"option_value"`     // 事件类型
}

// AuthorizerInfoListModel 返回参数说明
// 参数	        类型	说明
// total_count	number	授权的帐号总数
// list	object  array	当前查询的帐号基本信息列表
// 帐号基本信息
// 参数	            类型	说明
// authorizer_appid	string	已授权的 appid
// refresh_token	string	刷新令牌authorizer_access_token
// auth_time	    number	授权的时间
// 返回结果示例：
//
// {
//  "total_count": 33,
//  "list": [
//    {
//      "authorizer_appid": "authorizer_appid_1",
//      "refresh_token": "refresh_token_1",
//      "auth_time": 1558000607
//    },
//    {
//      "authorizer_appid": "authorizer_appid_2",
//      "refresh_token": "refresh_token_2",
//      "auth_time": 1558000607
//    }
//  ]
// }
type AuthorizerInfoListModel struct {
	common.ErrorModel
	TotalCount int64 `json:"total_count"` // 授权的帐号总数
	List       []struct {
		AuthorizerAppid string `json:"authorizer_appid"` // 已授权的公众号或小程序的appid
		RefreshToken    string `json:"refresh_token"`    // 刷新令牌authorizer_access_token
		AuthTime        int64  `json:"auth_time"`        // 授权的时间
	} `json:"list"` // 当前查询的帐号基本信息列表
}

// OpenPlatformModel 创建开放平台帐号并绑定公众号/小程序返回结果示例
//
// {
//  "open_appid": "appid_value",
//  "errcode": 0,
//  "errmsg": "ok"
// }
type OpenPlatformModel struct {
	common.ErrorModel
	OpenAppId string `json:"open_appid"` // 公众号或小程序的 appid
}