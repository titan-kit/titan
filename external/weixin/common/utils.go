package common

import "strings"

// Concat 字符串拼接
func Concat(args ...string) string {
	var b strings.Builder
	for _, str := range args {
		b.WriteString(str)
	}
	return b.String()
}