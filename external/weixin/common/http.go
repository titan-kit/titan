package common

import (
	"bytes"
	"context"
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io"
	"net"
	"net/http"
	"time"
)

const CallUserAgent = "Mozilla/5.0 (Linux x86_64) TitanWechat/1.0.0"

func JsonTo(v interface{}, indent bool) string {
	var n []byte
	if indent {
		n, _ = json.MarshalIndent(v, "", "\t")
	} else {
		n, _ = json.Marshal(v)
	}
	return string(n)
}
func XmlTo(v interface{}, indent bool) string {
	var n []byte
	if indent {
		n, _ = xml.MarshalIndent(v, "", "\t")
	} else {
		n, _ = xml.Marshal(v)
	}
	return string(n)
}

/**
get方法访问指定url地址
@param url 请求的地址
@param v   返回数据
*/
func get(url string, v interface{}) error {
	rc, err := httpCall(http.MethodGet, url, http.Header{"content-type": []string{"application/json"}}, nil)
	if err != nil {
		return err
	}
	defer rc.Close()
	err = json.NewDecoder(rc).Decode(v)
	if _, ok := err.(*json.SyntaxError); ok {
		return fmt.Errorf("JSON syntax error at %s", url)
	}
	return nil
}

/**
访问指定url地址,并设置http请求的头信息content-type为application/json
@param url 请求的地址
@param body 请求参数
@param v 返回数据
*/
func post(url string, body, v interface{}) error {
	data, err := json.Marshal(body)
	if err != nil {
		return err
	}
	rc, err := httpCall("POST", url, http.Header{"content-type": []string{"application/json"}}, bytes.NewBuffer(data))
	if err != nil {
		return err
	}
	defer rc.Close()
	err = json.NewDecoder(rc).Decode(v)
	if _, ok := err.(*json.SyntaxError); ok {
		return fmt.Errorf("JSON syntax error at %s", url)
	}
	return nil
}

// HttpCall makes HTTP method call.
func httpCall(method, url string, header http.Header, body io.Reader) (io.ReadCloser, error) {
	tr := &http.Transport{DialContext: func(ctx context.Context, network, addr string) (net.Conn, error) {
		conn, err := net.DialTimeout(network, addr, time.Second*5)
		if err != nil {
			return conn, err
		}
		tcpConn := conn.(*net.TCPConn)
		_ = tcpConn.SetKeepAlive(false)
		return tcpConn, err
	}, DisableKeepAlives: true}
	client := &http.Client{Transport: tr}
	req, err := http.NewRequest(method, url, body)
	if err != nil {
		return nil, err
	}
	req.Header.Set("User-Agent", CallUserAgent)
	for k, vs := range header {
		req.Header[k] = vs
	}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	if resp.StatusCode == 200 {
		return resp.Body, nil
	}
	defer resp.Body.Close()
	if resp.StatusCode == 404 { // 403 can be rate limit error.  || resp.StatusCode == 403 {
		err = fmt.Errorf("resource not found: %s", url)
	} else {
		err = fmt.Errorf("%s %s -> %d", method, url, resp.StatusCode)
	}
	return nil, err
}