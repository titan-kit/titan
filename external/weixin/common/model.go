package common

// CallbackMessage 开放平台(只有InfoType)与公众号(MsgType/Event)回调信息结构
type CallbackMessage struct {
	InfoType string `xml:"InfoType"` // 消息类型
	MsgType  string `xml:"MsgType"`  // 消息类型
	Event    string `xml:"Event"`    // 事件类型
}

func (cm CallbackMessage) Key() string {
	if len(cm.InfoType) == 0 {
		if cm.MsgType == "event" {
			return cm.MsgType + "-" + cm.Event
		} else {
			return cm.MsgType
		}
	} else {
		return cm.InfoType
	}
}

type ErrorModel struct {
	ErrCode uint32 `json:"errcode"`
	ErrMsg  string `json:"errmsg"`
}

func (em ErrorModel) Success() bool {
	return em.ErrCode == 0
}

// AuthorizationInfo
// 参数	类型	说明
// authorization_appid	string	授权方 appid
// func_info	object	授权给开发者的权限集列表
// {
//  "authorizer_appid": "wx58a6179c79e038e7",
//  "authorizer_refresh_token": "refreshtoken@@@XM0FapXdhUmRpVmm9o9_J1h8QeAwnAjUB5cpvGSR6tQ",
//  "func_info": [
//    {"funcscope_category": {"id": 23}},
//    {"funcscope_category": {"id": 24},"confirm_info": {"need_confirm": 0,"already_confirm": 0,"can_confirm": 0}},
//    {"funcscope_category": {"id": 26}},
//    {"funcscope_category": {"id": 27 },"confirm_info": {"need_confirm": 0,"already_confirm": 0,"can_confirm": 0}}
//  ]
// }
type AuthorizationInfo struct {
	AuthorizerAppId        string `json:"authorizer_appid"`         // 授权方 appid
	AuthorizerAccessToken  string `json:"authorizer_access_token"`  // 接口调用令牌（在授权的公众号/小程序具备 API 权限时，才有此返回值）
	ExpiresIn              int    `json:"expires_in"`               // 有效期，单位：秒
	AuthorizerRefreshToken string `json:"authorizer_refresh_token"` // 刷新令牌（在授权的公众号具备API权限时，才有此返回值），刷新令牌主要用于第三方平台获取和刷新已授权用户的 authorizer_access_token。
	FuncInfo               []struct {
		FuncScopeCategory struct {
			Id uint32 `json:"id"`
		} `json:"funcscope_category"`
		ConfirmInfo struct { // 小程序专有
			NeedConfirm    uint32 `json:"need_confirm"`
			AlreadyConfirm uint32 `json:"already_confirm"`
			CanConfirm     uint32 `json:"can_confirm"`
		} `json:"confirm_info"`
	} `json:"func_info"`
}

// AccessTokenModel 服务端accesstoken接口返回参数说明
// 属性	        类型	说明
// access_token	string	获取到的凭证
// expires_in	number	凭证有效时间，单位：秒。目前是7200秒之内的值。
// errcode	    number	错误码
// errmsg	    string	错误信息
//
// 返回errcode的合法值
// 值	    说明
// -1	    系统繁忙，此时请开发者稍候再试
// 0	    请求成功
// 40001	AppSecret 错误或者 AppSecret 不属于这个小程序，请开发者确认 AppSecret 的正确性
// 40002	请确保 grant_type 字段值为 client_credential
// 40013	不合法的 AppID，请开发者检查 AppID 的正确性，避免异常字符，注意大小写
type AccessTokenModel struct {
	ErrorModel
	AccessToken string `json:"access_token"`
	Expire      int    `json:"expires_in"`
}

type CallbackModel struct {
	OpenId    string            `json:"openId"`
	AppId     string            `json:"appId"`
	Timestamp string            `json:"timestamp"` //签名时间
	Nonce     string            `json:"nonce"`     //随机数
	Data      []byte            `json:"data"`      //post Data中的XML体
	Extra     map[string]string `json:"extra"`     //附加的数据
}

func (cm *CallbackModel) String() string {
	return JsonTo(&cm, false)
}