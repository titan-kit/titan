package common

import (
	"encoding/json"
	"fmt"
	"net/http"
	"sort"
	"strings"
	"time"

	"gitee.com/titan-kit/titan/cache"
	"gitee.com/titan-kit/titan/integrated/convert"
	"gitee.com/titan-kit/titan/log"
)

const (
	ApiUrl            = "https://api.weixin.qq.com"
	CacheNeverTimeout = -1 // 第三方平台或公众号接口调用令牌默认过期时间
	TextMessageKey    = "wechat:tm:%s"
	TicketKey         = "wechat:%s:ticket"
	TokenKey          = "wechat:%s:token"
	AuthorizationKey  = "wechat:%s:authorization"
	AccessTokenKey    = "wechat:%s:access_token"
	JsApiTicketKey    = "wechat:%s:js_api_ticket"
	RefreshTime       = "wechat:%s:refresh_time"
)

type Api struct {
	host string
}

func NewApi(host string) *Api {
	return &Api{host}
}
func (a *Api) Post(url string, body map[string]interface{}) *api {
	return &api{a.host, url, http.MethodPost, body}
}

func (a *Api) Get(url string, body map[string]interface{}) *api {
	return &api{a.host, url, http.MethodGet, body}
}

func NewWechatApi(cp cache.Provider, log *log.Slf4g, isOpen bool, appId, secret string) *WechatApi {
	return &WechatApi{Api: NewApi(ApiUrl), isOpen: isOpen, appId: appId, secret: secret, Cache: cp, Log: log}
}

type WechatApi struct {
	*Api
	isOpen        bool
	appId, secret string
	Cache         cache.Provider
	Log           *log.Slf4g
}

func (ba *WechatApi) Params() map[string]interface{} {
	return map[string]interface{}{"appid": ba.appId, "secret": ba.secret}
}

func (ba *WechatApi) MaskTokenUrl(url string) (token string, ok bool) {
	if ba.isOpen {
		token, ok = MaskAuthorizationAccessTokenUrl(ba.Cache, ba.Log, ba.appId)
	} else {
		token, ok = MaskAccessTokenUrl(ba.Cache, ba.Log, ba.appId)
	}
	return url + token, ok
}

func (ba *WechatApi) MaskAccessTokenByCodeUrl(url, componentUrl, componentAppId, code string) (result string, err error) {
	if ba.isOpen {
		str := fmt.Sprintf(componentUrl, ba.appId)
		result, err = MaskAccessTokenByCodeOpenUrl(ba.Cache, str, componentAppId, code)
	} else {
		str := fmt.Sprintf(url, ba.appId)
		result = Concat(str, "&secret=", ba.secret, "&code=", code)
		err = nil
	}
	return
}

// AccessToken
// 获取小程序全局唯一后台接口调用凭据（access_token）。调用绝大多数后台接口时都需使用 access_token
// 请求地址
// GET https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APPID&secret=APPSECRET
// 请求参数
// 属性	        类型	    必填	说明
// grant_type	string		是	    填写 client_credential
// appid	    string		是	    小程序唯一凭证，即 AppID，可在「微信公众平台 - 设置 - 开发设置」页中获得。（需要已经成为开发者，且帐号没有异常状态）
// secret	    string		是	    小程序唯一凭证密钥，即 AppSecret，获取方式同 appid
// 返回类型 AccessTokenModel
// 根据接口返回的有效期进行redis缓存
// 注意: 缓存后不能在其他地方重新生成access_token, 否则缓存的token会失效
func (ba *WechatApi) AccessToken() bool {
	token := &AccessTokenModel{}
	if err := ba.Get("/cgi-bin/token", ba.Params()).Add("grant_type", "client_credential").Do(token); err == nil {
		if token.ErrCode == 0 { // 微信返回token有效期才缓存，否则不缓存token
			ba.Cache.SetExpires(fmt.Sprintf(AccessTokenKey, ba.appId), token.AccessToken, time.Duration(token.Expire)*time.Second)
			return true
		} else {
			ba.Log.InfoF("获取/刷新接口调用令牌返回代码[%d]信息:%s", token.ErrMsg, token.ErrMsg)
		}
	} else {
		ba.Log.ErrorF("获取/刷新接口调用令牌发生错误:%+v", err)
	}
	return false

}

type api struct {
	host   string
	url    string
	method string
	body   map[string]interface{}
}

func (a api) Add(key string, value interface{}) api {
	a.body[key] = value
	return a
}
func (a api) Do(v interface{}) error {
	if a.method == http.MethodPost {
		return post(a.host+a.url, a.body, v)
	} else {
		return get(a.query(), v)
	}
}
func (a api) query() string {
	var buf strings.Builder
	buf.WriteString(a.host)
	buf.WriteString(a.url)
	size := len(a.body)
	if size > 0 {
		if strings.Index(a.url, "?") == -1 {
			buf.WriteString("?")
		} else {
			buf.WriteByte('&')
		}
		keys := make([]string, 0, size)
		for k := range a.body {
			keys = append(keys, k)
		}
		sort.Strings(keys)
		for i, k := range keys {
			if i > 0 {
				buf.WriteByte('&')
			}
			buf.WriteString(k)
			buf.WriteByte('=')
			buf.WriteString(convert.ToStr(a.body[k]))
		}
	}
	return buf.String()
}

func MaskAccessTokenUrl(cp cache.Provider, logger *log.Slf4g, appId string) (string, bool) {
	token := cp.String(fmt.Sprintf(AccessTokenKey, appId))
	ok := token != ""
	if ok {
		logger.InfoF("不存在小程序/公众号[%s]的access_token", appId)
	}
	return token, ok
}
func MaskAuthorizationAccessTokenUrl(cp cache.Provider, logger *log.Slf4g, appId string) (string, bool) {
	var ai AuthorizationInfo
	token := cp.String(fmt.Sprintf(AuthorizationKey, appId))
	ok := token != ""
	if ok {
		if err := json.Unmarshal([]byte(token), &ai); err == nil {
			token = ai.AuthorizerAccessToken
		} else {
			ok = false
		}
	} else {
		logger.InfoF("第三方平台不存在小程序[%s]的authorizer_access_token", appId)
	}
	return token, ok
}

func MaskAccessTokenByCodeOpenUrl(cp cache.Provider, url, componentAppId, code string) (result string, err error) {
	if token := cp.String(fmt.Sprintf(TokenKey, componentAppId)); token != "" {
		result = Concat(url, "&component_appid=", componentAppId, "&component_access_token=", token, "&code=", code)
		err = nil
	} else {
		err = fmt.Errorf("第三方平台token不存在[%s]", componentAppId)
	}
	return
}