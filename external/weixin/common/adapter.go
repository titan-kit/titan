package common

import (
	"encoding/xml"
)

type Adapter interface {
	GetKey() []string
	CallBack(CallbackModel) string
	GetValue(context []byte, value interface{}) error
}

func NewAdapter(key []string, call func(CallbackModel, Adapter) string) Adapter {
	return &callBackAdapter{key, call}
}

type callBackAdapter struct {
	key  []string
	call func(CallbackModel, Adapter) string
}

func (a *callBackAdapter) GetValue(context []byte, value interface{}) error {
	return xml.Unmarshal(context, value)
}
func (a *callBackAdapter) CallBack(cm CallbackModel) string {
	return a.call(cm, a)
}
func (a *callBackAdapter) GetKey() []string {
	return a.key
}
