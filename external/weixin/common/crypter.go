package common

import (
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"crypto/sha1"
	"encoding/base64"
	"encoding/binary"
	"fmt"
	"io"
	"sort"
	"strings"
	"time"
)

type EncryptedMessage struct {
	ToUserName string `xml:"ToUserName"`
	AppId      string `xml:"AppId"`
	Encrypt    string `xml:"Encrypt"`
}

type WeChatThirdCrypter struct {
	appId   string  // AppId 或 CorpId（企业号）
	token   string  // 开放平台配置的令牌
	crypter Crypter // 加解密
}

func NewWeChatThirdCrypter(appId, token, encodingAesKey string) *WeChatThirdCrypter {
	key, err := base64.StdEncoding.DecodeString(encodingAesKey + "=")
	if err != nil || len(key) != 32 {
		panic(err)
	}
	return &WeChatThirdCrypter{appId: appId, token: token, crypter: NewAesCrypter(key)}
}

func (w *WeChatThirdCrypter) EncryptMessage(msg, timestamp, nonce string) (string, error) {
	if len(timestamp) == 0 {
		timestamp = fmt.Sprintf("%d", time.Now().Unix())
	}
	encrypt, err := w.crypter.Encrypt(msg, w.appId)
	if err != nil {
		return "", err
	}
	var b bytes.Buffer
	b.WriteString("<xml><Encrypt><![CDATA[")
	b.WriteString(encrypt)
	b.WriteString("]]></Encrypt><MsgSignature><![CDATA[")
	b.WriteString(w.signature(timestamp, nonce, encrypt))
	b.WriteString("]]></MsgSignature><TimeStamp>")
	b.WriteString(timestamp)
	b.WriteString("</TimeStamp><Nonce><![CDATA[")
	b.WriteString(nonce)
	b.WriteString("]]></Nonce></xml>")
	return b.String(), nil
}

func (w *WeChatThirdCrypter) DecryptMessage(encrypt, msgSignature, timestamp, nonce string) ([]byte, error) {
	signature := w.signature(timestamp, nonce, encrypt)
	if msgSignature != signature {
		return []byte(""), fmt.Errorf("mismatched message signature: got: %s, expetecd: %s", signature, msgSignature)
	}
	return w.crypter.Decrypt(encrypt, w.appId)
}

// signature 对消息进行签名
func (w *WeChatThirdCrypter) signature(timestamp, nonce, encrypt string) string {
	s := []string{w.token, timestamp, nonce, encrypt}
	sort.Strings(s)
	h := sha1.New()
	_, _ = io.WriteString(h, strings.Join(s, ""))
	return fmt.Sprintf("%x", h.Sum(nil))
}

// 公众平台消息加解密接口
type Crypter interface {
	Encrypt(text, id string) (string, error)
	Decrypt(text, id string) ([]byte, error)
}
type aesCrypter struct {
	En cipher.BlockMode
	De cipher.BlockMode
}

func NewAesCrypter(key []byte) Crypter {
	iv := key[:16]
	block, err := aes.NewCipher(key)
	if err != nil {
		panic(err)
	}
	return &aesCrypter{cipher.NewCBCEncrypter(block, iv), cipher.NewCBCDecrypter(block, iv)}
}

func (p *aesCrypter) Encrypt(text, id string) (string, error) {
	// 生成随机字节
	randBytes := make([]byte, 16)
	if _, err := io.ReadFull(rand.Reader, randBytes); err != nil {
		return "", err
	}
	msg := []byte(text)
	buf := new(bytes.Buffer)
	if err := binary.Write(buf, binary.BigEndian, int32(len(msg))); err != nil {
		return "", err
	}
	msgLength := buf.Bytes()
	messageBytes := bytes.Join([][]byte{randBytes, msgLength, msg, []byte(id)}, nil)
	// PKCS7Encode 对要加密的明文进行填充
	const BlockSize = 32
	amountToPad := BlockSize - len(messageBytes)%BlockSize
	for i := 0; i < amountToPad; i++ {
		messageBytes = append(messageBytes, byte(amountToPad))
	}
	p.En.CryptBlocks(messageBytes, messageBytes) // 对消息文本进行加密
	return base64.StdEncoding.EncodeToString(messageBytes), nil
}

func (p *aesCrypter) Decrypt(text, id string) (msg []byte, err error) {
	var cipherText []byte
	if cipherText, err = base64.StdEncoding.DecodeString(text); err == nil {
		p.De.CryptBlocks(cipherText, cipherText) // 返回解密后的消息
		// 反填充并跳过16字节随机数,并删除解密后明文的填充
		decoded := cipherText[16:]
		pad := int(cipherText[len(cipherText)-1])
		if pad > 1 && pad < 32 {
			decoded = cipherText[16 : len(cipherText)-pad]
		}
		// 计算XML文档长度
		buf := bytes.NewBuffer(decoded[:4])
		var msgLength int32
		if err = binary.Read(buf, binary.BigEndian, &msgLength); err == nil {
			fromId := string(decoded[msgLength+4:]) // 获取来源appId
			if strings.Contains(fromId, id) {
				msg = decoded[4 : msgLength+4] // 获取XML文档正文
			} else {
				err = fmt.Errorf("mismatched appid: got: %s, expected: %s", fromId, id)
			}
		}
	}
	return
}
