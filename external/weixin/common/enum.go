package common

// WxType 微信类型枚举
type WxType uint32

const (
	_WxType WxType = iota
	CP             // 企业微信
	MP             // 微信公众号
	MiniApp        // 微信小程序
	Open           // 微信开放平台
	Pay            // 微信支付
)

// TicketType ticket类型枚举
type TicketType int

const (
	_TicketType TicketType = iota
	JSAPI                  // jsapi
	SDK                    // sdk
	WxCard                 // 微信卡券
)

func (tt TicketType) Code() string {
	return [...]string{"jsapi", "2", "wx_card"}[tt]
}

type MsgType string

const (
	_msgType     MsgType = "unknown"
	MsgTypeText  MsgType = "text"  // 文本消息
	MsgTypeImage MsgType = "image" // 图片消息
	MsgTypeVoice MsgType = "voice" // 语音消息
	MsgTypeVideo MsgType = "video" // 视频消息
	MsgTypeMusic MsgType = "music" // 音乐消息
	MsgTypeNews  MsgType = "news"  // 图文消息
	MsgTypeEvent MsgType = "event" // 事件消息
)