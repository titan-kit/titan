package weixin

import (
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"net/http"
	"reflect"
	"time"

	"gitee.com/titan-kit/titan/cache"
	"gitee.com/titan-kit/titan/external/weixin/common"
	"gitee.com/titan-kit/titan/external/weixin/mini"
	"gitee.com/titan-kit/titan/external/weixin/offiaccount"
	"gitee.com/titan-kit/titan/external/weixin/open"
	"gitee.com/titan-kit/titan/integrated/convert"
	"gitee.com/titan-kit/titan/log"
)

func NewWechat(tokenRefreshTime int64, cache cache.Provider, logger log.Logger) *Wechat {
	if logger == nil {
		logger = log.DefaultLogger
	}
	slf4g := log.NewSlf4g("integration/Wechat", logger)
	return &Wechat{tokenRefreshTime, cache, slf4g,
		make(map[string]*common.WeChatThirdCrypter, 0),
		make(map[string]*open.ThirdPlatform, 0),
		make(map[string]*offiaccount.OfficialAccounts, 0),
		make(map[string]*mini.MiniProgram, 0),
		make(map[string]common.Adapter, 0),
	}
}

type Wechat struct {
	tokenRefreshTime int64
	cache            cache.Provider
	logger           *log.Slf4g
	crypter          map[string]*common.WeChatThirdCrypter
	third            map[string]*open.ThirdPlatform
	offiaccount      map[string]*offiaccount.OfficialAccounts
	mini             map[string]*mini.MiniProgram
	adapter          map[string]common.Adapter
}

// AddThirdPlatform 增加第三方平台处理实例
func (w *Wechat) AddThirdPlatform(cfg open.ThirdPlatformConfig) *open.ThirdPlatform {
	w.crypter[cfg.AppId] = common.NewWeChatThirdCrypter(cfg.AppId, cfg.Token, cfg.AesKey)
	w.third[cfg.AppId] = open.NewThirdPlatform(w.cache, w.logger, cfg)
	return w.third[cfg.AppId]
}

// Crypter 根据appId获取加密引擎
func (w *Wechat) Crypter(appId string) *common.WeChatThirdCrypter {
	return w.crypter[appId]
}

// ThirdPlatform 根据appId获取三方平台实例
func (w *Wechat) ThirdPlatform(appId string) (bool, *open.ThirdPlatform) {
	tp, ok := w.third[appId]
	return ok, tp
}

// AddOfficialAccounts 增加公众号实例
func (w *Wechat) AddOfficialAccounts(cfg offiaccount.OfficialAccountsConfig) *offiaccount.OfficialAccounts {
	w.crypter[cfg.AppId] = common.NewWeChatThirdCrypter(cfg.AppId, cfg.Token, cfg.AesKey)
	w.offiaccount[cfg.AppId] = offiaccount.NewOfficialAccounts(w.cache, w.logger, cfg)
	return w.offiaccount[cfg.AppId]
}

// OfficialAccounts 根据appId获取公众号实例
func (w *Wechat) OfficialAccounts(appId string) (bool, *offiaccount.OfficialAccounts) {
	oa, ok := w.offiaccount[appId]
	return ok, oa
}

// AddMiniProgram 增加小程序实例
func (w *Wechat) AddMiniProgram(cfg mini.MiniProgramConfig) *mini.MiniProgram {
	w.mini[cfg.AppId] = mini.NewMiniProgram(w.cache, w.logger, cfg)
	return w.mini[cfg.AppId]
}

// MiniProgram 根据appId获取公众号实例
func (w *Wechat) MiniProgram(appId string) (bool, *mini.MiniProgram) {
	mp, ok := w.mini[appId]
	return ok, mp
}

// AddReceivingAdapter 增加回调信息处理适配器
func (w *Wechat) AddReceivingAdapter(a common.Adapter) *Wechat {
	if len(a.GetKey()) > 0 {
		for _, key := range a.GetKey() {
			if _, ok := w.adapter[key]; !ok {
				w.adapter[key] = a
			} else {
				panic("Register called twice for AddReceivingAdapter :" + key)
			}
		}
	}
	return w
}

// AddThirdPlatformAdapter 批量设置第三方平台回调消息处理引擎
func (w *Wechat) AddThirdPlatformAdapter(tra open.ThirdPlatformAdapter) *Wechat {
	return w.AddReceivingAdapter(tra.AuthorizationMessageAdapter()).AddReceivingAdapter(tra.AuthorizationMessageAdapter())
}

// AddOfficialAccountsReceivingAdapter 批量设置公众号回调消息处理引擎
func (w *Wechat) AddOfficialAccountsReceivingAdapter(ora offiaccount.OfficialAccountsReceivingAdapter) *Wechat {
	return w.AddBatchReceivingAdapter(ora.TextAdapter)
}

// AddBatchReceivingAdapter 批量设置回调消息处理引擎
func (w *Wechat) AddBatchReceivingAdapter(ad interface{}) *Wechat {
	t := reflect.TypeOf(ad) // 获取接口的类型
	v := reflect.ValueOf(ad)
	for i := 0; i < t.NumMethod(); i++ {
		m := t.Method(i)
		mv := v.MethodByName(m.Name)
		res := mv.Call([]reflect.Value{})
		w.AddReceivingAdapter(res[0].Interface().(common.Adapter))
	}
	return w
}

// ThirdPlatformAuthorizationUrl 生成第三方平台(为服务商型)为“微信公众号授权”或者“小程序授权”授权页网址
func (w *Wechat) ThirdPlatformAuthorizationUrl(appId string) (bool, string) {
	if ok, pac := w.third[appId].CreatePreAuthCode(); ok {
		return ok, w.third[appId].MaskAuthorizationUrl(pac.PreAuthCode)
	} else {
		return ok, ""
	}
}

// ThirdPlatformCallback 处理第三方平台回调事件解密回调的信息
// 为了加强安全性，post data 中的 xml 将使用服务申请时的加解密 key 来进行加密，具体请见《加密解密技术方案》(https://developers.weixin.qq.com/doc/oplatform/Third-party_Platforms/Message_Encryption/Technical_Plan.html),
// 在收到推送后需进行解密（详细请见《消息加解密接入指引》https://developers.weixin.qq.com/doc/oplatform/Third-party_Platforms/Message_Encryption/Message_encryption_and_decryption.html）。
func (w *Wechat) ThirdPlatformCallback(req *http.Request, extra map[string]string) (string, error) {
	return w.OfficialAccountsCallback("", "", extra, req)
}

// OfficialAccountsCallback 处理公众号回调事件
func (w *Wechat) OfficialAccountsCallback(openId, appId string, extra map[string]string, req *http.Request) (string, error) {
	return w.Callback(openId, appId, extra, req)
}

// Callback 获取微信回调的信息
// 概述
// 当关注者与已授权公众号/小程序进行交互时，第三方平台将接收到相应的消息推送、事件推送。由于第三方平台一般帮助众多公众号/小程序进行业务运营，所以为了加强安全性，微信服务器将对此过程进行 2 个措施：
//
// 在接收已授权公众号消息和事件的URL中，增加2个参数（此前已有 2 个参数，为时间戳 timestamp，随机数 nonce），分别是 encrypt_type（加密类型，为 aes）和 msg_signature（消息体签名，用于验证消息体的正确性）
// postdata 中的 XML 体，将使用第三方平台申请时的接收消息的加密 symmetric_key（也称为 EncodingAESKey）来进行加密。
// ok 			是否正确返回
// msgSignature 加密后信息
// timestamp 	签名时间
// noce 		随机数
func (w *Wechat) Callback(openId, appId string, extra map[string]string, req *http.Request) (str string, err error) {
	if data, err := ioutil.ReadAll(req.Body); err == nil {
		msgSignature := req.FormValue("msg_signature")
		timestamp := req.FormValue("timestamp")
		nonce := req.FormValue("nonce")
		var m common.EncryptedMessage
		if err = xml.Unmarshal(data, &m); err == nil {
			_appId := appId
			if len(openId) > 0 {
				_appId = openId
			}
			if len(m.AppId) > 0 {
				_appId = m.AppId
			}
			if msg, err := w.crypter[_appId].DecryptMessage(m.Encrypt, msgSignature, timestamp, nonce); err == nil {
				var v common.CallbackMessage
				if err = xml.Unmarshal(msg, &v); err == nil {
					if ad, ok := w.adapter[v.Key()]; ok {
						str = ad.CallBack(common.CallbackModel{OpenId: openId, AppId: appId, Timestamp: timestamp, Nonce: nonce, Data: msg, Extra: extra})
					} else {
						err = fmt.Errorf("不存在些消息[%s]适配器", v.Key())
					}
				} else {
					err = fmt.Errorf("json反序列化第三方平台回调事件发生错误:%+v", err)
				}
			} else {
				err = fmt.Errorf("解密第三方平台回调事件发生错误:%+v", err)
			}
		} else {
			err = fmt.Errorf("解析回调的信息发生错误:%+v", err)
		}
	} else {
		err = fmt.Errorf("读取回调的信息发生错误:%+v", err)
	}
	return
}

// ThirdPlatformAuthorization 处理小程序或者公众号授权给第三方平台的技术实现流程仅适用于平台型第三方平台
func (w *Wechat) ThirdPlatformAuthorization(req *http.Request) (ok bool, aim open.AuthorizationInfoModel) {
	appId := req.FormValue("appId")
	authCode := req.FormValue("auth_code")
	return w.third[appId].AuthorizationInfo(authCode)
}

// CheckRefreshToken 检测是否需要刷新token,返回true代表token已经过期
func (w *Wechat) CheckRefreshToken(appId string) bool {
	now := time.Now().Unix()
	if tot := w.cache.String(fmt.Sprintf(common.RefreshTime, appId)); tot != "" {
		return now > (convert.StrTo(tot).MustInt64() + w.tokenRefreshTime)
	}
	return true
}