package mini

import (
	"gitee.com/titan-kit/titan/cache"
	"gitee.com/titan-kit/titan/external/weixin/common"
	"gitee.com/titan-kit/titan/log"
)

const (
	code2SessionURI = "/sns/jscode2session"

	textSecurityCheckURI = "/wxa/msg_sec_check?access_token="
)

type MiniProgram struct {
	*common.WechatApi
	isOpen bool
}
type MiniProgramConfig struct {
	OpenPlatform bool   `json:"openPlatform"`
	AppId        string `json:"appId"`
	Secret       string `json:"secret"`
}

func NewMiniProgram(cache cache.Provider, log *log.Slf4g, cfg MiniProgramConfig) *MiniProgram {
	return &MiniProgram{WechatApi: common.NewWechatApi(cache, log, cfg.OpenPlatform, cfg.AppId, cfg.Secret)}
}

// Code2Session
// 登录凭证校验。通过 wx.login 接口获得临时登录凭证 code 后传到开发者服务器调用此接口完成登录流程
// 请求地址
// GET https://api.weixin.qq.com/sns/jscode2session?appid=APPID&secret=SECRET&js_code=JSCODE&grant_type=authorization_code
// 请求参数
// 属性	        类型	  	必填	    说明
// appid	    string		是	        小程序appId
// secret	    string		是	        小程序appSecret
// js_code	    string		是	        登录时获取的code
// grant_type	string		是	        授权类型，此处只需填写 authorization_code
//
// 返回详情: SecurityModel
func (mp *MiniProgram) Code2Session(code string) (t bool, sm SessionModel) {
	if err := mp.Get(code2SessionURI, mp.Params()).Add("js_code", code).Add("grant_type", "authorization_code").Do(&sm); err == nil {
		t = sm.Success()
	} else {
		mp.Log.ErrorF("登录凭证校验发生错误:%+v", err)
	}
	return
}

// TextSecurityCheck 检查一段文本是否含有违法违规内容
// 应用场景举例：
// 用户个人资料违规文字检测；
// 媒体新闻类用户发表文章，评论内容检测;
// 等等
// 请求地址
// POST https://api.weixin.qq.com/wxa/msg_sec_check?access_token=ACCESS_TOKEN
// 请求参数
// 属性	        类型	   	必填	说明
// access_token	string		是	    接口调用凭证
// content	    string		是	    要检测的文本内容，长度不超过 500KB
//
// 返回详情: common.ErrorModel
func (mp *MiniProgram) TextSecurityCheck(txt string) (t bool, em common.ErrorModel) {
	if url, ok := mp.MaskTokenUrl(textSecurityCheckURI); ok {
		if err := mp.Post(url, map[string]interface{}{"content": txt}).Do(&em); err == nil {
			t = em.Success()
		} else {
			mp.Log.ErrorF("检查一段文本是否含有违法违规内容发生错误:%+v", err)
		}
	}
	return
}