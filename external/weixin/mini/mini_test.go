package mini

import (
	"os"
	"testing"

	"gitee.com/titan-kit/titan/backends/redis"
	"gitee.com/titan-kit/titan/config"
	"gitee.com/titan-kit/titan/log"

	goRedis "github.com/go-redis/redis"
)

func TestApp_TextSecurityCheck(t *testing.T) {
	cache := redis.New(&goRedis.Options{Addr: "127.0.0.1:6379"})
	logger := log.NewSlf4g("mini/test", log.DefaultLogger)
	cn := config.New()
	appId, _ := cn.Value("appid").String()
	secret, _ := cn.Value("secret").String()
	we := NewMiniProgram(cache, logger, MiniProgramConfig{false, appId, secret})
	illegalText := "强奸童颜巨乳美女少妇"
	legalText := "我有一头小毛驴我从来也不骑"
	ok, _ := we.TextSecurityCheck(illegalText)
	if ok {
		t.Logf("check illegal text: %s, rerturn true", illegalText)
	} else {

	}
	ok, em := we.TextSecurityCheck(legalText)

	if !ok {
		t.Fatalf("check legal text: [%s] has msg:%s, rerturn false", legalText, em.ErrMsg)
	}
}

func TestApp_Code2Session(t *testing.T) {
	we := NewMiniProgram(nil, nil, MiniProgramConfig{false, os.Getenv("appid"), os.Getenv("secret")})
	code := os.Getenv("code")
	ok, sm := we.Code2Session(code)
	if !ok && sm.ErrCode != 0 {
		t.Fatalf(sm.ErrMsg)
	}
}