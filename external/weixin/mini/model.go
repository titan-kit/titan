package mini

import "gitee.com/titan-kit/titan/external/weixin/common"

const SecurityErrCode = 87014

type UserAuthorization struct {
	OpenId     string `json:"openid"`
	SessionKey string `json:"session_key"`
	UnionId    string `json:"unionid"`
}

// SessionModel 登陆凭证校验接口返回参数说明
// 属性	        类型	说明
// openid	    string	用户唯一标识
// session_key	string	会话密钥
// unionid	    string	用户在开放平台的唯一标识符，在满足 UnionID 下发条件的情况下会返回，详见 UnionID 机制说明。
// errcode	    number	错误码
// errmsg	    string	错误信息
// code码对应错误（-1:系统繁忙，此时请开发者稍候再试， 0:请求成功， 40029:code 无效， 45011:频率限制，每个用户每分钟100次）
type SessionModel struct {
	UserAuthorization
	common.ErrorModel
}

// SecurityModel 内容审核结果
// errcode 的合法值
//
// 值	    说明
// 0        内容正常
// 87014	内容含有违法违规内容
type SecurityModel struct {
	common.ErrorModel
}

// IsIllegal 判断内容检查是否合法
func (sm *SecurityModel) IsIllegal() bool {
	return sm.ErrCode == SecurityErrCode
}