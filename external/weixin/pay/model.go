package pay

// TransactionEvent 支付通知API 回调示例
//支付成功结果通知
//
//{
//    "id":"EV-2018022511223320873",
//    "create_time":"2015-05-20T13:29:35+08:00",
//    "resource_type":"encrypt-resource",
//    "event_type":"TRANSACTION.SUCCESS",
//    "resource" : {
//        "algorithm":"AEAD_AES_256_GCM",
//        "ciphertext": "...",
//        "nonce": "...",
//        "associated_data": ""
//    },
//	"summary":"支付成功"
//}
type TransactionEvent struct {
	Id           string `json:"id"`
	CtreateTime  string `json:"create_time"`
	ResourceType string `json:"resource_type"`
	EventType    string `json:"event_type"`
	Resource     struct {
		Algorithm      string `json:"algorithm"`
		Ciphertext     string `json:"ciphertext"`
		Nonce          string `json:"nonce"`
		AssociatedData string `json:"associated_data"`
	} `json:"resource"`
	Summary string `json:"summary"`
}
type TradeState string

const (
	TradeStateSuccess    TradeState = "SUCCESS"    //支付成功
	TradeStateRefund     TradeState = "REFUND"     //转入退款
	TradeStateNotpay     TradeState = "NOTPAY"     //未支付
	TradeStateClosed     TradeState = "CLOSED"     //已关闭
	TradeStateRevoked    TradeState = "REVOKED"    //已撤销（付款码支付）
	TradeStateUserpaying TradeState = "USERPAYING" //用户支付中（付款码支付）
	TradeStatePayerror   TradeState = "PAYERROR"   //支付失败(其他原因，如银行返回失败)
)

type TradeType string

const (
	TradeTypeJsapi    TradeType = "JSAPI"    //公众号支付
	TradeTypeNative   TradeType = "NATIVE"   //扫码支付
	TradeTypeApp      TradeType = "APP"      //APP支付
	TradeTypeMicropay TradeType = "MICROPAY" //付款码支付
	TradeTypeMweb     TradeType = "MWEB"     //H5支付
	TradeTypeFacepay  TradeType = "FACEPAY"  //刷脸支付
)

type BankType string

const (
	CMC BankType = "CMC"
)

// Transaction 商户对resource对象进行解密后，得到的资源对象示例
//
//
//{
//	"transaction_id": "1217752501201407033233368018",
//	"amount": {
//		"payer_total": 100,
//		"total": 100,
//		"currency": "CNY",
//		"payer_currency": "CNY"
//	},
//	"mchid": "1230000109",
//	"trade_state": "SUCCESS",
//	"bank_type": "CMC",
//	"promotion_detail": [{
//		"amount": 100,
//		"wechatpay_contribute": 0,
//		"coupon_id": "109519",
//		"scope": "GLOBALSINGLE",
//		"merchant_contribute": 0,
//		"name": "单品惠-6",
//		"other_contribute": 0,
//		"currency": "CNY",
//		"type": "CASHNOCASH",
//		"stock_id": "931386",
//		"goods_detail": [{
//			"goods_remark": "商品备注信息",
//			"quantity": 1,
//			"discount_amount": 1,
//			"goods_id": "M1006",
//			"unit_price": 100
//		}, {
//			"goods_remark": "商品备注信息",
//			"quantity": 1,
//			"discount_amount": 1,
//			"goods_id": "M1006",
//			"unit_price": 100
//		}]
//	}, {
//		"amount": 100,
//		"wechatpay_contribute": 0,
//		"coupon_id": "109519",
//		"scope": "GLOBALSINGLE",
//		"merchant_contribute": 0,
//		"name": "单品惠-6",
//		"other_contribute": 0,
//		"currency": "CNY",
//		"type": "CASHNOCASH",
//		"stock_id": "931386",
//		"goods_detail": [{
//			"goods_remark": "商品备注信息",
//			"quantity": 1,
//			"discount_amount": 1,
//			"goods_id": "M1006",
//			"unit_price": 100
//		}, {
//			"goods_remark": "商品备注信息",
//			"quantity": 1,
//			"discount_amount": 1,
//			"goods_id": "M1006",
//			"unit_price": 100
//		}]
//	}],
//	"success_time": "2018-06-08T10:34:56+08:00",
//	"payer": {
//		"openid": "oUpF8uMuAJO_M2pxb1Q9zNjWeS6o"
//	},
//	"out_trade_no": "1217752501201407033233368018",
//	"appid": "wxd678efh567hg6787",
//	"trade_state_desc": "支付失败，请重新下单支付",
//	"trade_type": "MICROPAY",
//	"attach": "自定义数据",
//	"scene_info": {
//		"device_id": "013467007045764"
//	}
//}
//支付成功通知参数
//参数名			变量				类型[长度限制]	必填	描述
//公众号ID		appid				string[1,32]	是		直连商户申请的公众号或移动应用appid。
//															示例值：wxd678efh567hg6787
//直连商户号		mchid				string[1,32]	是		直连商户的商户号，由微信支付生成并下发。
//															示例值：1230000109
//商户订单号		out_trade_no		string[6,32]	是		商户系统内部订单号，只能是数字、大小写字母_-*且在同一个商户号下唯一，详见【商户订单号】。
//															特殊规则：最小字符长度为6
//															示例值：1217752501201407033233368018
//微信支付订单号	transaction_id		string[1,32]	否		微信支付系统生成的订单号。
//															示例值：1217752501201407033233368018
//交易类型		trade_type			string[1,16]	否		交易类型，枚举值：
//															JSAPI：公众号支付
//															NATIVE：扫码支付
//															APP：APP支付
//															MICROPAY：付款码支付
//															MWEB：H5支付
//															FACEPAY：刷脸支付
//															示例值：MICROPAY
//交易状态		trade_state			string[1,32]	是		交易状态，枚举值：
//															SUCCESS：支付成功
//															REFUND：转入退款
//															NOTPAY：未支付
//															CLOSED：已关闭
//															REVOKED：已撤销（付款码支付）
//															USERPAYING：用户支付中（付款码支付）
//															PAYERROR：支付失败(其他原因，如银行返回失败)
//															示例值：SUCCESS
//交易状态描述	trade_state_desc	string[1,256]	是		交易状态描述
//															示例值：支付失败，请重新下单支付
//付款银行		bank_type			string[1,16]	否		银行类型，采用字符串类型的银行标识。
//															示例值：CMC
//附加数据		attach				string[1,128]	否		附加数据，在查询API和支付通知中原样返回，可作为自定义参数使用
//															示例值：自定义数据
//支付完成时间	success_time		string[1,64]	否		支付完成时间，遵循rfc3339标准格式，格式为YYYY-MM-DDTHH:mm:ss+TIMEZONE，YYYY-MM-DD表示年月日，T出现在字符串中，表示time元素的开头，HH:mm:ss表示时分秒，TIMEZONE表示时区（+08:00表示东八区时间，领先UTC 8小时，即北京时间）。
//															例如：2015-05-20T13:29:35+08:00表示，北京时间2015年5月20日 13点29分35秒。
//															示例值：2018-06-08T10:34:56+08:00
//-支付者		payer				object			否		支付者信息
//	参数名		变量	类型[长度限制]	必填	描述
//	用户标识		openid	string[1,128]	是		用户在直连商户appid下的唯一标识。
//												示例值：oUpF8uMuAJO_M2pxb1Q9zNjWeS6o
//-订单金额		amount	object	是	订单金额信息
//	参数名		变量			类型[长度限制]	必填	描述
//	总金额		total			int				否		订单总金额，单位为分。
//														示例值：100
//	用户支付金额	payer_total		int				否		用户支付金额，单位为分。
//														示例值：100
//	货币类型		currency		string[1,16]	否		CNY：人民币，境内商户号仅支持人民币。
//														示例值：CNY
//	用户支付币种	payer_currency	string[1,16]	否		用户支付币种
//														示例值：CNY
//场景信息	scene_info	object	否	支付场景信息描述
//+优惠功能	promotion_detail	array	否	优惠功能，享受优惠时返回该字段。
type Transaction struct {
	AppId          string     `json:"appid"`
	Mchid          string     `json:"mchid"`
	OutTradeNo     string     `json:"out_trade_no"`
	TransactionId  string     `json:"transaction_id"`
	TradeType      TradeType  `json:"trade_type"`
	TradeState     TradeState `json:"trade_state"`
	TradeStateDesc string     `json:"trade_state_desc"`
	Attach         string     `json:"attach"`
	Payer          struct {
		OpenId string `json:"openid"`
	} `json:"payer"`
	Amount struct {
		PayerTotal     int
		Total          int
		Currency       string
		Payer_currency string
	} `json:"amount"`
}