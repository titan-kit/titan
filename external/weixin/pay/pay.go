package pay

type WechatPayConfig struct {
	AppId    string `json:"appId"`
	MchId    string `json:"mchId"`
	ApiKey   string `json:"apiKey"`
	CertData string `json:"certData"`
	Sandbox  bool   `json:"sandbox"`
}
type WechatPay struct {
}
