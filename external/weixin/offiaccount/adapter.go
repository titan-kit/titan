package offiaccount

import "gitee.com/titan-kit/titan/external/weixin/common"

// OfficialAccountsReceivingAdapter 以下为公众号(OA)
type OfficialAccountsReceivingAdapter interface {
	TextAdapter() common.Adapter
	ImageMessageAdapter() common.Adapter
	VoiceMessageAdapter() common.Adapter
	VideoMessageAdapter() common.Adapter
	LocationMessageAdapter() common.Adapter
	LinkMessageAdapter() common.Adapter
	EventMessageAdapter() common.Adapter
	EventSubscribeMessageAdapter() common.Adapter
	EventLocationMessageAdapter() common.Adapter
}

// TextMessageAdapter 文本消息处理适配器
func TextMessageAdapter(call func(common.CallbackModel, common.Adapter) string) common.Adapter {
	return common.NewAdapter([]string{"text"}, call)
}

// ImageMessageAdapter 图片消息处理适配器
func ImageMessageAdapter(call func(common.CallbackModel, common.Adapter) string) common.Adapter {
	return common.NewAdapter([]string{"image"}, call)
}

// VoiceMessageAdapter 语音消息处理适配器
func VoiceMessageAdapter(call func(common.CallbackModel, common.Adapter) string) common.Adapter {
	return common.NewAdapter([]string{"voice"}, call)
}

// VideoMessageAdapter 视频消息/小视频消息处理适配器
func VideoMessageAdapter(call func(common.CallbackModel, common.Adapter) string) common.Adapter {
	return common.NewAdapter([]string{"video", "shortvideo"}, call)
}

// LocationMessageAdapter 地理位置消息处理适配器
func LocationMessageAdapter(call func(common.CallbackModel, common.Adapter) string) common.Adapter {
	return common.NewAdapter([]string{"location"}, call)
}

// LinkMessageAdapter 链接消息处理适配器
func LinkMessageAdapter(call func(common.CallbackModel, common.Adapter) string) common.Adapter {
	return common.NewAdapter([]string{"link"}, call)
}

// EventMessageAdapter 相关事件处理适配器
func EventMessageAdapter(call func(common.CallbackModel, common.Adapter) string) common.Adapter {
	return common.NewAdapter([]string{"event-CLICK", "event-VIEW"}, call)
}

// EventSubscribeMessageAdapter 用户相关关注事件处理适配器
func EventSubscribeMessageAdapter(call func(common.CallbackModel, common.Adapter) string) common.Adapter {
	return common.NewAdapter([]string{"event-subscribe", "event-unsubscribe", "event-SCAN"}, call)
}

// EventLocationMessageAdapter 上报地理位置事件处理适配器
func EventLocationMessageAdapter(call func(common.CallbackModel, common.Adapter) string) common.Adapter {
	return common.NewAdapter([]string{"event-LOCATION"}, call)
}