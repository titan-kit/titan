package offiaccount

import (
	"gitee.com/titan-kit/titan/external/weixin/common"
)

// IpListModel 获取微信服务器IP地址返回说明
//
// 正常情况下，微信会返回下述JSON数据包给公众号：
//
// {"ip_list": ["127.0.0.1","127.0.0.2","101.226.103.0/25"    ]}
// 参数	说明
// ip_list	微信服务器IP地址列表
type IpListModel struct {
	common.ErrorModel
	IpList []string `json:"ip_list"`
}

// ActionType 检测动作
type ActionType string

const (
	DnsActionType  ActionType = "dns"  // dns（做域名解析）
	PingActionType ActionType = "ping" // ping（做ping检测
	AllActionType  ActionType = "all"  // all（dns和ping都做）
)

// CheckOperator 指定平台从某个运营商进行检测，允许的值：CHINANET（电信出口）、UNICOM（联通出口）、CAP（腾讯自建出口）、DEFAULT（根据ip来选择运营商）
type CheckOperator string

const (
	ChinanetCheckOperator CheckOperator = "CHINANET" // dns（做域名解析）
	UnicomCheckOperator   CheckOperator = "UNICOM"   // ping（做ping检测
	CapCheckOperator      CheckOperator = "CAP"      // all（dns和ping都做）
	DefaultCheckOperator  CheckOperator = "DEFAULT"  // all（dns和ping都做）
)

// CheckNetworkModel 网络检测返回Json格式
//
// {
// "dns": [
//        {
//  "ip":"111.161.64.40",
//  "real_operator":"UNICOM"
//        },
//        {
//  "ip":"111.161.64.48",
//  "real_operator":"UNICOM"
//        }
//    ],
// "ping": [
//        {
//  "ip":"111.161.64.40",
//  "from_operator":"UNICOM",
//
//
//  "package_loss":"0%",
//  "time":"23.079ms"
//        },
//        {
//  "ip":"111.161.64.48",
//  "from_operator":"UNICOM",
//  "package_loss":"0%",
//  "time":"21.434ms"
//        }
//    ]
// }
// 参数说明
//
// 参数	                说明
// dns	                dns结果列表
// dns.ip	            解析出来的ip
// dns.real_operator	ip对应的运营商
// ping	                ping结果列表
// ping.ip	            ping的ip，执行命令为ping ip –c 1-w 1 -q
// ping.from_operator	ping的源头的运营商，由请求中的check_operator控制
// ping.package_loss	ping的丢包率，0%表示无丢包，100%表示全部丢包。因为目前仅发送一个ping包，因此取值仅有0%或者100%两种可能。
// ping.time	        ping的耗时，取ping结果的avg耗时。
type CheckNetworkModel struct {
	common.ErrorModel
	Dns []struct {
		Ip           string `json:"ip"`
		RealOperator string `json:"real_operator"`
	} `json:"dns"`
	Ping []struct {
		Ip           string `json:"ip"`
		FromOperator string `json:"from_operator"`
		PackageLoss  string `json:"package_loss"`
		Time         string `json:"time"`
	} `json:"ping"`
}
type UserInfoParm struct {
	OpenId string `json:"openid"`
	Lang   string `json:"lang"`
}

type SubscribeScene string

const (
	AddSceneSearch              SubscribeScene = "ADD_SCENE_SEARCH"               // 公众号搜索
	AddSceneAccountMigration    SubscribeScene = "ADD_SCENE_ACCOUNT_MIGRATION"    // 公众号迁移
	AddSceneProfileCard         SubscribeScene = "ADD_SCENE_PROFILE_CARD"         // 名片分享
	AddSceneQrcode              SubscribeScene = "ADD_SCENE_QR_CODE"              // 扫描二维码
	AddSceneProfileLink         SubscribeScene = "ADD_SCENE_PROFILE_LINK"         // 图文页内名称点击
	AddSceneProfileItem         SubscribeScene = "ADD_SCENE_PROFILE_ITEM"         // 图文页右上角菜单
	AddScenePaid                SubscribeScene = "ADD_SCENE_PAID"                 // 支付后关注
	AddSceneWechatAdvertisement SubscribeScene = "ADD_SCENE_WECHAT_ADVERTISEMENT" // 微信广告
	AddSceneOthers              SubscribeScene = "ADD_SCENE_OTHERS"               // 其他
)

type BaseUserInfo struct {
	Openid     string   `json:"openid"`     // 用户的标识，对当前公众号唯一
	Nickname   string   `json:"nickname"`   // 用户的昵称
	Sex        int      `json:"sex"`        // 用户的性别，值为1时是男性，值为2时是女性，值为0时是未知
	Province   string   `json:"province"`   // 用户所在省份
	City       string   `json:"city"`       // 用户所在城市
	Country    string   `json:"country"`    // 用户所在国家
	HeadImgUrl string   `json:"headimgurl"` // 用户头像，最后一个数值代表正方形头像大小（有0、46、64、96、132数值可选，0代表640*640正方形头像），用户没有头像时该项为空。若用户更换头像，原有头像URL将失效。
	Language   string   `json:"language"`   // 用户的语言，简体中文为zh_CN
	Unionid    string   `json:"unionid"`    // 只有在用户将公众号绑定到微信开放平台帐号后，才会出现该字段。
	Privilege  []string `json:"privilege"`  //用户特权信息，json 数组，如微信沃卡用户为（chinaunicom）
}

type BaseUserInfoModel struct {
	common.ErrorModel
	BaseUserInfo
}

type UserInfo struct {
	BaseUserInfo
	Subscribe      int            `json:"subscribe"`       // 用户是否订阅该公众号标识，值为0时，代表此用户没有关注该公众号，拉取不到其余信息。
	SubscribeTime  string         `json:"subscribe_time"`  // 用户关注时间，为时间戳。如果用户曾多次关注，则取最后关注时间
	Remark         string         `json:"remark"`          // 公众号运营者对粉丝的备注，公众号运营者可在微信公众平台用户管理界面对粉丝添加备注
	Groupid        uint32         `json:"groupid"`         // 用户所在的分组ID（兼容旧的用户分组接口）
	TagIdList      []int          `json:"tagid_list"`      // 用户被打上的标签ID列表
	SubscribeScene SubscribeScene `json:"subscribe_scene"` // 返回用户关注的渠道来源
	QrScene        int            `json:"qr_scene"`        // 二维码扫码场景（开发者自定义）
	QrSceneStr     string         `json:"qr_scene_str"`    // 二维码扫码场景描述（开发者自定义）
}

// UserInfoModel 获取用户基本信息返回说明
//
// 正常情况下，微信会返回下述JSON数据包给公众号：
//
// {
// 	"subscribe": 1,
// 	"openid":"o6_bmjrPTlm6_2sgVt7hMZOPfL2M",
// 	"nickname":"Band",
// 	"sex": 1,
// 	"language":"zh_CN",
// 	"city":"广州",
// 	"province":"广东",
// 	"country":"中国",
// 	"headimgurl":"http://thirdwx.qlogo.cn/mmopen/g3MonUZtNHkdmzicIlibx6iaFqAc56vxLSUfpb6n5WKSYVY0ChQKkiaJSgQ1dZuTOgvLLrhJbERQQ4eMsv84eavHiaiceqxibJxCfHe/0",
// 	"subscribe_time": 1382694957,
// 	"unionid":" o6_bmasdasdsad6_2sgVt7hMZOPfL"
// 	"remark":"",
// 	"groupid": 0,
// 	"tagid_list":[128,2],
// 	"subscribe_scene":"ADD_SCENE_QR_CODE",
// 	"qr_scene": 98765,
// 	"qr_scene_str":""
// }
// 参数说明
type UserInfoModel struct {
	common.ErrorModel
	UserInfo
}

// UserInfoListModel 批量获取用户基本信息返回说明
//
// 正常情况下，微信会返回下述JSON数据包给公众号（示例中为一次性拉取了2个openid的用户基本信息，第一个是已关注的，第二个是未关注的）：
//
// {
//   "user_info_list": [
//       {
//           "subscribe": 1,
//           "openid": "otvxTs4dckWG7imySrJd6jSi0CWE",
//           "nickname": "iWithery",
//           "sex": 1,
//           "language": "zh_CN",
//           "city": "揭阳",
//           "province": "广东",
//           "country": "中国",
//           "headimgurl": "http://thirdwx.qlogo.cn/mmopen/xbIQx1GRqdvyqkMMhEaGOX802l1CyqMJNgUzKP8MeAeHFicRDSnZH7FY4XB7p8XHXIf6uJA2SCunTPicGKezDC4saKISzRj3nz/0",
//          "subscribe_time": 1434093047,
//           "unionid": "oR5GjjgEhCMJFyzaVZdrxZ2zRRF4",
//           "remark": "",
//           "groupid": 0,
//           "tagid_list":[128,2],
//           "subscribe_scene": "ADD_SCENE_QR_CODE",
//           "qr_scene": 98765,
//           "qr_scene_str": ""
//      },
//       {
//           "subscribe": 0,
//           "openid": "otvxTs_JZ6SEiP0imdhpi50fuSZg"
//       }
//   ]
// }
type UserInfoListModel struct {
	common.ErrorModel
	UserInfoList []UserInfo `json:"user_info_list"`
}

// JsApiTicketModel 获取jsApiTicket返回信息
type JsApiTicketModel struct {
	common.ErrorModel
	Ticket  string `json:"ticket"`
	Expires int    `json:"expires_in"`
}

// QrCodeResponse 创建临时的二维码返回信息
type QrCodeResponse struct {
	common.ErrorModel
	Ticket        string `json:"ticket"`         // 用于获取二维码图片的 ticket
	ExpireSeconds int    `json:"expire_seconds"` //过期时间,秒
	Url           string `json:"url"`            //二维码地址
}

// SnsAccessTokenModel code换取网页授权access_token返回信息
type SnsAccessTokenModel struct {
	common.ErrorModel
	AccessToken  string `json:"access_token"`  //代公众号调用接口凭证
	Expires      int    `json:"expires_in"`    // 接口调用凭证超时时间，单位（秒）
	RefreshToken string `json:"refresh_token"` // 用户刷新 access_token
	Openid       string `json:"openid"`        // 授权用户唯一标识
	Scope        string `json:"scope"`         // 用户授权的作用域，使用逗号（,）分隔
}