package offiaccount

import (
	"fmt"

	"gitee.com/titan-kit/titan/cache"
	"gitee.com/titan-kit/titan/external/weixin/common"
	"gitee.com/titan-kit/titan/log"
)

// 此项目所有开发接口文档来源于:https://developers.weixin.qq.com/doc/offiaccount/Getting_Started/Overview.html
const (
	ClearQuotaUrl         = "/cgi-bin/clear_quota?access_token="
	ApiDomainIpUrl        = "/cgi-bin/get_api_domain_ip?access_token="
	CheckNetworkUrl       = "/cgi-bin/callback/check?access_token="
	UserInfoUrl           = "/cgi-bin/user/info?access_token="
	UserInfoBatchGetUrl   = "/cgi-bin/user/info/batchget?access_token="
	jsApiTicketUrl        = "/cgi-bin/ticket/getticket?access_token="
	SnsUserinfoUrl        = "/sns/userinfo"
	SnsAccessTokenUrl     = "/sns/oauth2/access_token?appid=%s&grant_type=authorization_code"
	OpenSnsAccessTokenUrl = "/sns/oauth2/component/access_token?appid=%s&grant_type=authorization_code"
	QrcodeUrl             = "/cgi-bin/qrcode/create?access_token="
)

type OfficialAccountsConfig struct {
	OpenPlatform bool   `json:"openPlatform"`
	AppId        string `json:"appId"`
	Secret       string `json:"secret"`
	Token        string `json:"token"`
	AesKey       string `json:"aesKey"`
}

func NewOfficialAccounts(cp cache.Provider, log *log.Slf4g, cfg OfficialAccountsConfig) *OfficialAccounts {
	return &OfficialAccounts{WechatApi: common.NewWechatApi(cp, log, cfg.OpenPlatform, cfg.AppId, cfg.Secret)}
}

type OfficialAccounts struct {
	*common.WechatApi
}

// ClearQuota 公众号调用或第三方平台帮公众号调用对公众号的所有api调用（包括第三方帮其调用）次数进行清零：
//
// HTTP请求：POST HTTP调用： https://api.weixin.qq.com/cgi-bin/clear_quota?access_token=ACCESS_TOKEN
//
// 调用示例：
//
// { “appid”:“APPID” }
// 参数说明：
//
// 参数	        是否必须	说明
// access_token	是	    调用接口凭据
// appid	    是	    公众号的APPID
//
// 返回详情: common.ErrorModel
func (oa *OfficialAccounts) ClearQuota(appId string) (t bool, em common.ErrorModel) {
	if uri, ok := oa.MaskTokenUrl(ClearQuotaUrl); ok {
		if err := oa.Post(uri, map[string]interface{}{"component_appid": appId}).Do(&em); err == nil {
			t = em.Success()
		} else {
			oa.Log.Error("第三方平台对其所有API调用次数清零发生错误:%+v", err)
		}
	}
	return
}

// ApiDomainIp 获取微信服务器IP地址
// 如果公众号基于安全等考虑，需要获知微信服务器的IP地址列表，以便进行相关限制，可以通过该接口获得微信服务器IP地址列表或者IP网段信息。
//
// 由于出口IP及入口IP可能存在变动，建议用户每天请求接口1次，以便于及时更新IP列表。为了避免造成单点故障，强烈建议用户不要长期使用旧的IP列表作为api.weixin.qq.com的访问入口。
//
// 1. 获取微信API接口 IP地址
// 使用固定IP访问api.weixin.qq.com时，请开发者注意运营商适配，跨运营商访问可能会存在高峰期丢包问题。
// API接口IP即api.weixin.qq.com的解析地址，由开发者调用微信侧的接入IP。
//
// 接口调用请求说明
//
// http请求方式: GET https://api.weixin.qq.com/cgi-bin/get_api_domain_ip?access_token=ACCESS_TOKEN
//
// 参数说明
//
// 参数	        是否必须	说明
// access_token	是	    公众号的access_token
//
// 返回详情: IpListModel
func (oa *OfficialAccounts) ApiDomainIp() (t bool, ilm IpListModel) {
	if uri, ok := oa.MaskTokenUrl(ApiDomainIpUrl); ok {
		if err := oa.Get(uri, nil).Do(&ilm); err == nil {
			t = ilm.Success()
		} else {
			oa.Log.Error("获取微信服务器IP地址发生错误:%+v", err)
		}
	}
	return
}

// CheckNetwork 网络检测
// 为了帮助开发者排查回调连接失败的问题，提供这个网络检测的API。它可以对开发者URL做域名解析，然后对所有IP进行一次ping操作，得到丢包率和耗时。
//
// HTTP Post请求： https://api.weixin.qq.com/cgi-bin/callback/check?access_token=ACCESS_TOKEN
//
// 请求JSON格式：
//
// {
//    "action": "all",
//    "check_operator": "DEFAULT"
// }
// 参数说明
//
// 参数	            是否必须	    说明
// action	        是	        执行的检测动作，允许的值：dns（做域名解析）、ping（做ping检测）、all（dns和ping都做）
// check_operator	是	        指定平台从某个运营商进行检测，允许的值：CHINANET（电信出口）、UNICOM（联通出口）、CAP（腾讯自建出口）、DEFAULT（根据ip来选择运营商）
//
// 返回详情: CheckNetworkModel
func (oa *OfficialAccounts) CheckNetwork(action ActionType, checkOperator CheckOperator) (t bool, cnm CheckNetworkModel) {
	if uri, ok := oa.MaskTokenUrl(CheckNetworkUrl); ok {
		if err := oa.Post(uri, map[string]interface{}{"action": action, "check_operator": checkOperator}).Do(&cnm); err == nil {
			t = cnm.Success()
		} else {
			oa.Log.Error("第三方平台对其所有API调用次数清零发生错误:%+v", err)
		}
	}
	return
}

//
func (oa *OfficialAccounts) UserTags() {

}

// UserInfo 获取用户基本信息(UnionID机制)
//
// 在关注者与公众号产生消息交互后，公众号可获得关注者的OpenID（加密后的微信号，每个用户对每个公众号的OpenID是唯一的。对于不同公众号，同一用户的openid不同）。公众号可通过本接口来根据OpenID获取用户基本信息，包括昵称、头像、性别、所在城市、语言和关注时间。
//
// 请注意，如果开发者有在多个公众号，或在公众号、移动应用之间统一用户帐号的需求，需要前往微信开放平台（open.weixin.qq.com）绑定公众号后，才可利用UnionID机制来满足上述需求。
//
// UnionID机制说明：
//
// 开发者可通过OpenID来获取用户基本信息。特别需要注意的是，如果开发者拥有多个移动应用、网站应用和公众帐号，可通过获取用户基本信息中的unionid来区分用户的唯一性，因为只要是同一个微信开放平台帐号下的移动应用、网站应用和公众帐号，用户的unionid是唯一的。换句话说，同一用户，对同一个微信开放平台下的不同应用，unionid是相同的。
//
// 请注意： 20年6月8日起，用户关注来源“微信广告（ADD_SCENE_WECHAT_ADVERTISEMENT）”从“其他（ADD_SCENE_OTHERS）”中拆分给出。
//
// 获取用户基本信息（包括UnionID机制）
//
// 开发者可通过OpenID来获取用户基本信息。请使用https协议。
//
// 接口调用请求说明 http请求方式: GET https://api.weixin.qq.com/cgi-bin/user/info?access_token=ACCESS_TOKEN&openid=OPENID&lang=zh_CN
//
// 参数说明
//
// 参数	是否必须	说明
// access_token	是	调用接口凭证
// openid	是	普通用户的标识，对当前公众号唯一
// lang	否	返回国家地区语言版本，zh_CN 简体，zh_TW 繁体，en 英语
//
// 返回详情: UserInfoModel
func (oa *OfficialAccounts) UserInfo(openId, lang string) (t bool, uim UserInfoModel) {
	if uri, ok := oa.MaskTokenUrl(UserInfoUrl); ok {
		if len(lang) == 0 {
			lang = "zh_CN"
		}
		if err := oa.Get(uri, map[string]interface{}{"openid": openId, "lang": lang}).Do(&uim); err == nil {
			t = uim.Success()
		} else {
			oa.Log.Error("获取用户基本信息(UnionID机制):%+v", err)
		}
	}
	return
}

// UserInfoBatchGet 批量获取用户基本信息
//
// 开发者可通过该接口来批量获取用户基本信息。最多支持一次拉取100条。
//
// 接口调用请求说明
//
// http请求方式: POST https://api.weixin.qq.com/cgi-bin/user/info/batchget?access_token=ACCESS_TOKEN
//
// POST数据示例
//
// {
//    "user_list": [
//        {
//            "openid": "otvxTs4dckWG7imySrJd6jSi0CWE",
//            "lang": "zh_CN"
//        },
//        {
//            "openid": "otvxTs_JZ6SEiP0imdhpi50fuSZg",
//            "lang": "zh_CN"
//        }
//    ]
// }
// 参数说明
//
// 参数	是否必须	说明
// openid	是	用户的标识，对当前公众号唯一
// lang	否	国家地区语言版本，zh_CN 简体，zh_TW 繁体，en 英语，默认为zh-CN
//
// 返回详情: UserInfoListModel
func (oa *OfficialAccounts) UserInfoBatchGet(uip []UserInfoParm) (t bool, uil UserInfoListModel) {
	if len(uip) > 100 {
		oa.Log.Error("开发者可通过该接口来批量获取用户基本信息。最多支持一次拉取100条")
	}
	if uri, ok := oa.MaskTokenUrl(UserInfoBatchGetUrl); ok {
		if err := oa.Post(uri, map[string]interface{}{"user_list": uip}).Do(&uil); err == nil {
			t = uil.Success()
		} else {
			oa.Log.Error("批量获取用户基本信息发生错误:%+v", err)
		}
	}
	return
}

// JsApiTicket GetJsApiTicket 获取 jsapi_ticket 用于 js_sdk 验证
// https://developers.weixin.qq.com/doc/offiaccount/OA_Web_Apps/JS-SDK.html#62
func (oa *OfficialAccounts) JsApiTicket() (jst JsApiTicketModel, err error) {
	if uri, ok := oa.MaskTokenUrl(jsApiTicketUrl); ok {
		e := oa.Get(uri, map[string]interface{}{"type": "jsapi"}).Do(&jst)
		if !jst.Success() {
			err = fmt.Errorf("获取jsapi_ticket发生错误:%s", jst.ErrMsg)
			return
		}
		if e != nil {
			err = fmt.Errorf("获取jsapi_ticket发生错误:%+v", e)
			return
		}
	} else {
		err = fmt.Errorf("获取jsapi_ticket发生错误:MaskTokenUrl异常")
		return
	}
	return jst, nil
}

// AccessTokenByCode 通过code换取网页授权access_token
// https://developers.weixin.qq.com/doc/offiaccount/OA_Web_Apps/Wechat_webpage_authorization.html
func (oa *OfficialAccounts) AccessTokenByCode(code string, componentAppId string) (s SnsAccessTokenModel, err error) {
	if uri, ee := oa.MaskAccessTokenByCodeUrl(SnsAccessTokenUrl, OpenSnsAccessTokenUrl, componentAppId, code); ee == nil {
		fmt.Println(uri)
		e := oa.Get(uri, nil).Do(&s)
		if !s.Success() {
			err = fmt.Errorf("通过code换取网页授权access_token发生错误:%s", s.ErrMsg)
			return
		}
		if e != nil {
			err = fmt.Errorf("通过code换取网页授权access_token发生错误:%+v", e)
			return
		}
		return s, nil
	} else {
		return s, ee
	}

}

// SnsUserInfo 授权后拉取用户信息:网页授权作用域为snsapi_userinfo，则此时开发者可以通过access_token和openid拉取用户信息
// https://developers.weixin.qq.com/doc/offiaccount/OA_Web_Apps/Wechat_webpage_authorization.html
func (oa *OfficialAccounts) SnsUserInfo(model SnsAccessTokenModel) (user *BaseUserInfoModel, err error) {
	e := oa.Get(SnsUserinfoUrl, map[string]interface{}{"access_token": model.AccessToken, "openid": model.Openid, "lang": "zh_CN"}).Do(&user)
	if !user.Success() {
		err = fmt.Errorf("获取SnsUserInfo发生错误:%s", user.ErrMsg)
		return
	}
	if e != nil {
		err = fmt.Errorf("获取SnsUserInfo发生错误:%+v", e)
		return
	}
	return user, nil
}

// CreateQrCode 临时二维码请求说明
//
//http请求方式: POST
//URL: https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token=TOKEN
//POST数据格式：json
//POST数据例子：{"expire_seconds": 604800, "action_name": "QR_SCENE", "action_info": {"scene": {"scene_id": 123}}}
//或者也可以使用
//以下POST数据创建字符串形式的二维码参数：{"expire_seconds": 604800, "action_name": "QR_STR_SCENE", "action_info": {"scene": {"scene_str": "test"}}}
func (oa *OfficialAccounts) CreateQrCode(sceneId int, expireSeconds int) (qr *QrCodeResponse, err error) {
	return oa.createQrCode("QR_SCENE", sceneId, expireSeconds)
}

// CreateLimitQrCode 永久二维码请求说明
//
//http请求方式: POST
//URL: https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token=TOKEN
//POST数据格式：json
//POST数据例子：{"action_name": "QR_LIMIT_SCENE","action_info": {"scene": {"scene_id": 123}}}
//或者也可以使用
//以下POST数据创建字符串形式的二维码参数：{"action_name": "QR_LIMIT_STR_SCENE", "action_info": {"scene": {"scene_str": "test"}}}
func (oa *OfficialAccounts) CreateLimitQrCode(sceneId int) (qr *QrCodeResponse, err error) {
	return oa.createQrCode("QR_LIMIT_SCENE", sceneId, 0)
}

// createQrCode 创建二维码ticket
//
//每次创建二维码ticket需要提供一个开发者自行设定的参数（scene_id），分别介绍临时二维码和永久二维码的创建二维码ticket过程。
//
// 参考 https://developers.weixin.qq.com/doc/offiaccount/Account_Management/Generating_a_Parametric_QR_Code.html
// action_name		二维码类型，QR_SCENE为临时的整型参数值，QR_LIMIT_SCENE为永久的整型参数值
// sceneId 			场景值ID，临时二维码时为32位非0整型，永久二维码时最大值为100000（目前参数只支持1--100000）
// expireSeconds 	该二维码有效时间，以秒为单位。 最大不超过2592000（即30天），此字段如果不填，则默认有效期为30秒。
func (oa *OfficialAccounts) createQrCode(actionName string, sceneId int, expireSeconds int) (qr *QrCodeResponse, err error) {
	if uri, ok := oa.MaskTokenUrl(QrcodeUrl); ok {
		params := map[string]interface{}{
			"action_name": actionName,
			"action_info": map[string]interface{}{"scene": map[string]int{"scene_id": sceneId}}}
		if expireSeconds > 0 {
			params["expire_seconds"] = expireSeconds
		}
		e := oa.Post(uri, params).Do(&qr)
		if !qr.Success() {
			err = fmt.Errorf("获取二维码发生错误:%s", qr.ErrMsg)
			return
		}
		if e != nil {
			err = fmt.Errorf("获取二维码发生错误:%+v", e)
			return
		}
	} else {
		err = fmt.Errorf("获取二维码发生错误:MaskTokenUrl异常")
		return
	}
	return qr, nil
}