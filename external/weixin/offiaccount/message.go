package offiaccount

import (
	"time"

	"gitee.com/titan-kit/titan/external/weixin/common"
)

// TODO 还有很多普通消息格式这里暂时没有定义。。。

// MessageHeader 公众号消息
// See also https://developers.weixin.qq.com/doc/offiaccount/Message_Management/Receiving_standard_messages.html
type MessageHeader struct {
	ToUserName   string         `xml:"ToUserName"`   // 开发者微信号
	FromUserName string         `xml:"FromUserName"` // 发送方帐号 (OpenID)
	CreateTime   int64          `xml:"CreateTime"`   // 消息创建时间
	MsgType      common.MsgType `xml:"MsgType"`      // 消息类型
}

func NewMessageHeader(to, from string, msgType common.MsgType) MessageHeader {
	return MessageHeader{
		ToUserName:   to,
		FromUserName: from,
		CreateTime:   time.Now().Unix(),
		MsgType:      msgType,
	}
}

// TextMessage 文本消息
// <xml>
//  <ToUserName><![CDATA[toUser]]></ToUserName>
//  <FromUserName><![CDATA[fromUser]]></FromUserName>
//  <CreateTime>1348831860</CreateTime>
//  <MsgType><![CDATA[text]]></MsgType>
//  <Content><![CDATA[this is a test]]></Content>
//  <MsgId>1234567890123456</MsgId>
// </xml>
type TextMessage struct {
	MessageHeader
	MsgId   int64  `xml:"MsgId"`   // 消息id，64位整型
	Content string `xml:"Content"` // 文本消息内容
}

// TextResponse 回复文本消息
//<xml>
//  <ToUserName><![CDATA[toUser]]></ToUserName>
//  <FromUserName><![CDATA[fromUser]]></FromUserName>
//  <CreateTime>12345678</CreateTime>
//  <MsgType><![CDATA[text]]></MsgType>
//  <Content><![CDATA[你好]]></Content>
//</xml>
//参数			是否必须		描述
//ToUserName	是			接收方帐号（收到的OpenID）
//FromUserName	是			开发者微信号
//CreateTime	是			消息创建时间 （整型）
//MsgType		是			消息类型，文本为text
//Content		是			回复的消息内容（换行：在content中能够换行，微信客户端就支持换行显示）
type TextResponse struct {
	XMLName struct{} `xml:"xml"`
	MessageHeader
	Content string `xml:"Content"` // 文本消息内容
}

func NewTextResponse(to, from, content string) *TextResponse {
	return &TextResponse{
		MessageHeader: NewMessageHeader(to, from, common.MsgTypeText),
		Content:       content,
	}
}

// ImageMessage 图片消息
// <xml>
//  <ToUserName><![CDATA[toUser]]></ToUserName>
//  <FromUserName><![CDATA[fromUser]]></FromUserName>
//  <CreateTime>1348831860</CreateTime>
//  <MsgType><![CDATA[image]]></MsgType>
//  <PicUrl><![CDATA[this is a url]]></PicUrl>
//  <MediaId><![CDATA[media_id]]></MediaId>
//  <MsgId>1234567890123456</MsgId>
// </xml>
type ImageMessage struct {
	MessageHeader
	MsgId   int64  `xml:"MsgId"`   // 消息id，64位整型
	MediaId string `xml:"MediaId"` // 消息媒体id
	PicUrl  string `xml:"PicUrl"`  // 图片链接（由系统生成）
}

// ImageResponse 回复图片消息
//<xml>
//  <ToUserName><![CDATA[toUser]]></ToUserName>
//  <FromUserName><![CDATA[fromUser]]></FromUserName>
//  <CreateTime>12345678</CreateTime>
//  <MsgType><![CDATA[image]]></MsgType>
//  <Image>
//    <MediaId><![CDATA[media_id]]></MediaId>
//  </Image>
//</xml>
//
//参数			是否必须		说明
//ToUserName	是			接收方帐号（收到的OpenID）
//FromUserName	是			开发者微信号
//CreateTime	是			消息创建时间 （整型）
//MsgType		是			消息类型，图片为image
//MediaId		是			通过素材管理中的接口上传多媒体文件，得到的id。
type ImageResponse struct {
	XMLName struct{} `xml:"xml"`
	MessageHeader
	Image struct {
		MediaId string `xml:"MediaId"` // 通过素材管理接口上传多媒体文件得到 MediaId
	} `xml:"Image"`
}

func NewImageResponse(to, from, mediaId string) *ImageResponse {
	image := &ImageResponse{MessageHeader: NewMessageHeader(to, from, common.MsgTypeImage)}
	image.Image.MediaId = mediaId
	return image
}

// VoiceMessage 语音消息
// <xml>
//  <ToUserName><![CDATA[toUser]]></ToUserName>
//  <FromUserName><![CDATA[fromUser]]></FromUserName>
//  <CreateTime>1357290913</CreateTime>
//  <MsgType><![CDATA[voice]]></MsgType>
//  <MediaId><![CDATA[media_id]]></MediaId>
//  <Format><![CDATA[Format]]></Format>
//  <MsgId>1234567890123456</MsgId>
// </xml>
type VoiceMessage struct {
	MessageHeader
	MsgId       int64  `xml:"MsgId"`       // 消息id，64位整型
	MediaId     string `xml:"MediaId"`     // 消息媒体id
	Format      string `xml:"Format"`      // 语音格式，如amr，speex等
	Recognition string `xml:"Recognition"` // 语音识别结果，UTF8编码（需要开通语音识别）
}

// VoiceResponse 回复语音消息
//<xml>
//  <ToUserName><![CDATA[toUser]]></ToUserName>
//  <FromUserName><![CDATA[fromUser]]></FromUserName>
//  <CreateTime>12345678</CreateTime>
//  <MsgType><![CDATA[voice]]></MsgType>
//  <Voice>
//    <MediaId><![CDATA[media_id]]></MediaId>
//  </Voice>
//</xml>
//
//参数	是否必须	说明
//ToUserName	是	接收方帐号（收到的OpenID）
//FromUserName	是	开发者微信号
//CreateTime	是	消息创建时间戳 （整型）
//MsgType	是	消息类型，语音为voice
//MediaId	是	通过素材管理中的接口上传多媒体文件，得到的id
type VoiceResponse struct {
	XMLName struct{} `xml:"xml"`
	MessageHeader
	Voice struct {
		MediaId string `xml:"MediaId"` // 通过素材管理接口上传多媒体文件得到 MediaId
	} `xml:"Voice"`
}

func NewVoiceResponse(to, from, mediaId string) *VoiceResponse {
	voice := &VoiceResponse{MessageHeader: NewMessageHeader(to, from, common.MsgTypeVoice)}
	voice.Voice.MediaId = mediaId
	return voice
}

// VideoMessage 视频消息
// <xml>
//  <ToUserName><![CDATA[toUser]]></ToUserName>
//  <FromUserName><![CDATA[fromUser]]></FromUserName>
//  <CreateTime>1357290913</CreateTime>
//  <MsgType><![CDATA[video]]></MsgType>
//  <MediaId><![CDATA[media_id]]></MediaId>
//  <ThumbMediaId><![CDATA[thumb_media_id]]></ThumbMediaId>
//  <MsgId>1234567890123456</MsgId>
// </xml>
// 小视频消息
// <xml>
//  <ToUserName><![CDATA[toUser]]></ToUserName>
//  <FromUserName><![CDATA[fromUser]]></FromUserName>
//  <CreateTime>1357290913</CreateTime>
//  <MsgType><![CDATA[shortvideo]]></MsgType>
//  <MediaId><![CDATA[media_id]]></MediaId>
//  <ThumbMediaId><![CDATA[thumb_media_id]]></ThumbMediaId>
//  <MsgId>1234567890123456</MsgId>
// </xml>
type VideoMessage struct {
	MessageHeader
	MsgId        int64  `xml:"MsgId"`        // 消息id，64位整型
	MediaId      string `xml:"MediaId"`      // 消息媒体id
	ThumbMediaId string `xml:"ThumbMediaId"` // 视频消息缩略图的媒体id，可以调用多媒体文件下载接口拉取数据
}

// VideoResponse 回复视频消息
//<xml>
//  <ToUserName><![CDATA[toUser]]></ToUserName>
//  <FromUserName><![CDATA[fromUser]]></FromUserName>
//  <CreateTime>12345678</CreateTime>
//  <MsgType><![CDATA[video]]></MsgType>
//  <Video>
//    <MediaId><![CDATA[media_id]]></MediaId>
//    <Title><![CDATA[title]]></Title>
//    <Description><![CDATA[description]]></Description>
//  </Video>
//</xml>
//
//参数			是否必须		说明
//ToUserName	是			接收方帐号（收到的OpenID）
//FromUserName	是			开发者微信号
//CreateTime	是			消息创建时间 （整型）
//MsgType		是			消息类型，视频为video
//MediaId		是			通过素材管理中的接口上传多媒体文件，得到的id
//Title			否			视频消息的标题
//Description	否			视频消息的描述
type VideoResponse struct {
	XMLName struct{} `xml:"xml"`
	MessageHeader
	Video struct {
		MediaId     string `xml:"MediaId"`     // 通过素材管理接口上传多媒体文件得到 MediaId
		Title       string `xml:"Title"`       // 视频消息的标题, 可以为空
		Description string `xml:"Description"` // 视频消息的描述, 可以为空
	} `xml:"Video"`
}

func NewVideoResponse(to, from, mediaId, title, description string) *VideoResponse {
	video := &VideoResponse{MessageHeader: NewMessageHeader(to, from, common.MsgTypeVideo)}
	video.Video.MediaId = mediaId
	video.Video.Title = title
	video.Video.Description = description
	return video
}

// MusicResponse 回复音乐消息
//<xml>
//  <ToUserName><![CDATA[toUser]]></ToUserName>
//  <FromUserName><![CDATA[fromUser]]></FromUserName>
//  <CreateTime>12345678</CreateTime>
//  <MsgType><![CDATA[music]]></MsgType>
//  <Music>
//    <Title><![CDATA[TITLE]]></Title>
//    <Description><![CDATA[DESCRIPTION]]></Description>
//    <MusicUrl><![CDATA[MUSIC_Url]]></MusicUrl>
//    <HQMusicUrl><![CDATA[HQ_MUSIC_Url]]></HQMusicUrl>
//    <ThumbMediaId><![CDATA[media_id]]></ThumbMediaId>
//  </Music>
//</xml>
//
//参数			是否必须		说明
//ToUserName	是			接收方帐号（收到的OpenID）
//FromUserName	是			开发者微信号
//CreateTime	是			消息创建时间 （整型）
//MsgType		是			消息类型，音乐为music
//Title			否			音乐标题
//Description	否			音乐描述
//MusicURL		否			音乐链接
//HQMusicUrl	否			高质量音乐链接，WIFI环境优先使用该链接播放音乐
//ThumbMediaId	是			缩略图的媒体id，通过素材管理中的接口上传多媒体文件，得到的id
type MusicResponse struct {
	XMLName struct{} `xml:"xml"`
	MessageHeader
	Music struct {
		Title        string `xml:"Title,omitempty"        json:"Title,omitempty"`       // 音乐标题
		Description  string `xml:"Description,omitempty"  json:"Description,omitempty"` // 音乐描述
		MusicURL     string `xml:"MusicUrl"               json:"MusicUrl"`              // 音乐链接
		HQMusicURL   string `xml:"HQMusicUrl"             json:"HQMusicUrl"`            // 高质量音乐链接, WIFI环境优先使用该链接播放音乐
		ThumbMediaId string `xml:"ThumbMediaId"           json:"ThumbMediaId"`          // 通过素材管理接口上传多媒体文件得到 ThumbMediaId
	} `xml:"Music"`
}

func NewMusicResponse(to, from, title, description, musicURL, hqMusicURL, mediaId string) *MusicResponse {
	music := &MusicResponse{MessageHeader: NewMessageHeader(to, from, common.MsgTypeMusic)}
	music.Music.Title = title
	music.Music.Description = description
	music.Music.MusicURL = musicURL
	music.Music.HQMusicURL = hqMusicURL
	music.Music.ThumbMediaId = mediaId
	return music
}

// Article 回复图文消息
//<xml>
//  <ToUserName><![CDATA[toUser]]></ToUserName>
//  <FromUserName><![CDATA[fromUser]]></FromUserName>
//  <CreateTime>12345678</CreateTime>
//  <MsgType><![CDATA[news]]></MsgType>
//  <ArticleCount>1</ArticleCount>
//  <Articles>
//    <item>
//      <Title><![CDATA[title1]]></Title>
//      <Description><![CDATA[description1]]></Description>
//      <PicUrl><![CDATA[picurl]]></PicUrl>
//      <Url><![CDATA[url]]></Url>
//    </item>
//  </Articles>
//</xml>
//
//参数	是否必须	说明
//ToUserName	是	接收方帐号（收到的OpenID）
//FromUserName	是	开发者微信号
//CreateTime	是	消息创建时间 （整型）
//MsgType	是	消息类型，图文为news
//ArticleCount	是	图文消息个数；当用户发送文本、图片、语音、视频、图文、地理位置这六种消息时，开发者只能回复1条图文消息；其余场景最多可回复8条图文消息
//Articles	是	图文消息信息，注意，如果图文数超过限制，则将只发限制内的条数
//Title	是	图文消息标题
//Description	是	图文消息描述
//PicUrl	是	图片链接，支持JPG、PNG格式，较好的效果为大图360*200，小图200*200
//Url	是	点击图文消息跳转链接
// 图文消息里的 Article
type Article struct {
	Title       string `xml:"Title,omitempty"`       // 图文消息标题
	Description string `xml:"Description,omitempty"` // 图文消息描述
	PicURL      string `xml:"PicUrl,omitempty"`      // 图片链接, 支持JPG, PNG格式, 较好的效果为大图360*200, 小图200*200
	URL         string `xml:"Url,omitempty"`         // 点击图文消息跳转链接
}
type NewsResponse struct {
	XMLName struct{} `xml:"xml"`
	MessageHeader
	ArticleCount int       `xml:"ArticleCount"`            // 图文消息个数, 限制为10条以内
	Articles     []Article `xml:"Articles>item,omitempty"` // 多条图文消息信息, 默认第一个item为大图, 注意, 如果图文数超过10, 则将会无响应
}

func NewNewsResponse(to, from string, articles []Article) *NewsResponse {
	news := &NewsResponse{MessageHeader: NewMessageHeader(to, from, common.MsgTypeVideo)}
	news.ArticleCount = len(articles)
	news.Articles = articles
	return news
}

// LocationMessage 地理位置消息
// <xml>
//  <ToUserName><![CDATA[toUser]]></ToUserName>
//  <FromUserName><![CDATA[fromUser]]></FromUserName>
//  <CreateTime>1351776360</CreateTime>
//  <MsgType><![CDATA[location]]></MsgType>
//  <Location_X>23.134521</Location_X>
//  <Location_Y>113.358803</Location_Y>
//  <Scale>20</Scale>
//  <Label><![CDATA[位置信息]]></Label>
//  <MsgId>1234567890123456</MsgId>
// </xml>
type LocationMessage struct {
	MessageHeader
	MsgId     int64   `xml:"MsgId"`      // 消息id，64位整型
	Latitude  float64 `xml:"Location_X"` // 地理位置纬度
	Longitude float64 `xml:"Location_Y"` // 地理位置经度
	Scale     float64 `xml:"Scale"`      // 地图缩放大小
	Label     float64 `xml:"Label"`      // 地理位置信息
}

// LinkMessage 链接消息
// <xml>
//  <ToUserName><![CDATA[toUser]]></ToUserName>
//  <FromUserName><![CDATA[fromUser]]></FromUserName>
//  <CreateTime>1351776360</CreateTime>
//  <MsgType><![CDATA[link]]></MsgType>
//  <Title><![CDATA[公众平台官网链接]]></Title>
//  <Description><![CDATA[公众平台官网链接]]></Description>
//  <Url><![CDATA[url]]></Url>
//  <MsgId>1234567890123456</MsgId>
// </xml>
type LinkMessage struct {
	MessageHeader
	MsgId       int64  `xml:"MsgId"`       // 消息id，64位整型
	Title       string `xml:"Title"`       // 消息标题
	Description string `xml:"Description"` // 消息描述
	Url         string `xml:"Url"`         // 消息链接
}
type BaseEventMessage struct {
	MessageHeader
	Event string `xml:"Event"` // 事件类型，subscribe(订阅)、unsubscribe(取消订阅)、SCAN(扫码)、LOCATION(地理位置上报)、CLICK(自定义菜单)、VIEW(点击菜单跳转链接时)
}

// EventMessage 自定义菜单事件
// 用户点击自定义菜单后，微信会把点击事件推送给开发者，请注意，点击菜单弹出子菜单，不会产生上报。
// <xml>
//  <ToUserName><![CDATA[toUser]]></ToUserName>
//  <FromUserName><![CDATA[FromUser]]></FromUserName>
//  <CreateTime>123456789</CreateTime>
//  <MsgType><![CDATA[event]]></MsgType>
//  <Event><![CDATA[CLICK]]></Event>
//  <EventKey><![CDATA[EVENTKEY]]></EventKey>
// </xml>
// 点击菜单跳转链接时的事件推送
// <xml>
//  <ToUserName><![CDATA[toUser]]></ToUserName>
//  <FromUserName><![CDATA[FromUser]]></FromUserName>
//  <CreateTime>123456789</CreateTime>
//  <MsgType><![CDATA[event]]></MsgType>
//  <Event><![CDATA[VIEW]]></Event>
//  <EventKey><![CDATA[www.qq.com]]></EventKey>
// </xml>
type EventMessage struct {
	BaseEventMessage
	EventKey string `xml:"EventKey"` // 事件KEY值，qrscene_为前缀，后面为二维码的参数值
}

// EventSubscribeMessage 事件消息
// 关注/取消关注/扫描带参数二维码事件(消息类型，event)
// <xml>
//  <ToUserName><![CDATA[toUser]]></ToUserName>
//  <FromUserName><![CDATA[FromUser]]></FromUserName>
//  <CreateTime>123456789</CreateTime>
//  <MsgType><![CDATA[event]]></MsgType>
//  <Event><![CDATA[subscribe]]></Event>
// </xml>
// 扫描带参数二维码事件
// 1.用户未关注时，进行关注后的事件推送
// <xml>
//  <ToUserName><![CDATA[toUser]]></ToUserName>
//  <FromUserName><![CDATA[FromUser]]></FromUserName>
//  <CreateTime>123456789</CreateTime>
//  <MsgType><![CDATA[event]]></MsgType>
//  <Event><![CDATA[subscribe]]></Event>
//  <EventKey><![CDATA[qrscene_123123]]></EventKey>
//  <Ticket><![CDATA[TICKET]]></Ticket>
// </xml>
// 2. 用户已关注时的事件推送
// <xml>
//  <ToUserName><![CDATA[toUser]]></ToUserName>
//  <FromUserName><![CDATA[FromUser]]></FromUserName>
//  <CreateTime>123456789</CreateTime>
//  <MsgType><![CDATA[event]]></MsgType>
//  <Event><![CDATA[SCAN]]></Event>
//  <EventKey><![CDATA[SCENE_VALUE]]></EventKey>
//  <Ticket><![CDATA[TICKET]]></Ticket>
// </xml>
type EventSubscribeMessage struct {
	EventMessage
	Ticket string `xml:"Ticket"` // 二维码的ticket，可用来换取二维码图片
}

// EventLocationMessage 上报地理位置事件
// 用户同意上报地理位置后，每次进入公众号会话时，都会在进入时上报地理位置，或在进入会话后每5秒上报一次地理位置，公众号可以在公众平台网站中修改以上设置。上报地理位置时，微信会将上报地理位置事件推送到开发者填写的URL。
// <xml>
//  <ToUserName><![CDATA[toUser]]></ToUserName>
//  <FromUserName><![CDATA[fromUser]]></FromUserName>
//  <CreateTime>123456789</CreateTime>
//  <MsgType><![CDATA[event]]></MsgType>
//  <Event><![CDATA[LOCATION]]></Event>
//  <Latitude>23.137466</Latitude>
//  <Longitude>113.352425</Longitude>
//  <Precision>119.385040</Precision>
// </xml>
type EventLocationMessage struct {
	BaseEventMessage
	Latitude  float64 `xml:"Latitude"`  // 地理位置纬度
	Longitude float64 `xml:"Longitude"` // 地理位置经度
	Precision float64 `xml:"Precision"` // 地理位置精度
}