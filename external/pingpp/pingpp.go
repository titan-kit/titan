package pingpp

import (
	"fmt"
	"github.com/pingplusplus/pingpp-go/pingpp"

	"gitee.com/titan-kit/titan/log"
)

func NewPingPP(key string, logger log.Logger) *pingPP {
	fmt.Printf("Loading Aliyun Storage Engine ver: %s\n", pingpp.Version())
	pingpp.Key = key
	if logger == nil {
		logger = log.DefaultLogger
	}
	slf4g := log.NewSlf4g("integration/pingpp", logger)

	return &pingPP{slf4g}
}

type pingPP struct {
	log *log.Slf4g
}