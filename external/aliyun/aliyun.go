package aliyun

import (
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/url"
	"strings"
	"time"

	"github.com/aliyun/aliyun-oss-go-sdk/oss"

	"gitee.com/titan-kit/titan/storage"
)

var _ storage.Provider = &aliyunProvider{}

const DefaultDownloadUrlExpire = 300 * time.Second

type Config struct {
	EndPoint  string `json:"endPoint"`
	AccessKey string `json:"accessKey"`
	SecretKey string `json:"secretKey"`
	Bucket    string `json:"bucket"`
	Domain    string `json:"domain"`
}

type aliyunProvider struct {
	protocol storage.Protocol
	conf     Config
	bucket   *oss.Bucket
	client   *oss.Client
}

func New(protocol storage.Protocol, conf Config) *aliyunProvider {
	fmt.Println("Loading Aliyun Storage Engine ver: v2.1.8", protocol)
	var err error
	provider := &aliyunProvider{protocol, conf, nil, nil}
	if provider.client, err = oss.New(conf.EndPoint, conf.SecretKey, conf.AccessKey); err == nil {
		if provider.bucket, err = provider.client.Bucket(conf.Bucket); err == nil {
			storage.Register(protocol, provider)
			return provider
		}
	}
	log.Fatal(err)
	return nil
}

// SimpleUploadToken 返回文件上传token
func (a aliyunProvider) SimpleUploadToken() string {
	return a.client.Config.CredentialsProvider.GetCredentials().GetSecurityToken()
}

// Protocol protocol获取
func (a aliyunProvider) Protocol() storage.Protocol {
	return a.protocol
}

// Save 上传文件
func (a aliyunProvider) Save(reader io.Reader, fileSize int64, filePath string) (string, error) {
	err := a.bucket.PutObject(filePath, reader)
	if err != nil {
		return "", err
	}
	return fmt.Sprintf("%s://%s", a.Protocol(), filePath), nil
}

//Get 获取文件内容
func (a aliyunProvider) Get(path string) ([]byte, error) {
	// 文件是否存在
	has, err := a.Has(path)
	if err != nil {
		return nil, err
	}
	if !has {
		return nil, storage.ErrFileOrDirectoryNotExists
	}
	body, err := a.bucket.GetObject(storage.GetFilePathFromTotalPath(path))
	if err != nil {
		return nil, err
	}
	defer body.Close()
	data, err := ioutil.ReadAll(body)
	if err != nil {
		return nil, err
	}
	return data, nil
}

//Write 读取文件内容到file中
func (a aliyunProvider) Write(path string, file *[]byte) error {
	// 文件是否存在
	has, err := a.Has(path)
	if err != nil {
		return err
	}
	if !has {
		return storage.ErrFileOrDirectoryNotExists
	}
	// 获取字节数组
	p, err := a.Get(path)
	if err != nil {
		return err
	}
	*file = p
	return nil
}

//Delete 删除文件
func (a aliyunProvider) Delete(path string) error {
	// 文件是否存在
	has, err := a.Has(path)
	if err != nil {
		return err
	}
	if !has {
		return storage.ErrFileOrDirectoryNotExists
	}
	err = a.bucket.DeleteObject(storage.GetFilePathFromTotalPath(path))
	return err
}

//Url 获取文件的访问地址
func (a aliyunProvider) Url(path string, secure bool, expire ...time.Duration) (string, error) {
	has, err := a.Has(storage.FilterParams(path))
	if err != nil {
		return "", err
	}
	if !has {
		return "", storage.ErrFileOrDirectoryNotExists
	}
	if !secure {
		return storage.MakePublicURL(a.conf.Domain, storage.GetFilePathFromTotalPath(path))
	}
	duration := DefaultDownloadUrlExpire
	if len(expire) > 0 {
		duration = expire[0]
	}
	options := make([]oss.Option, 0)
	if xOssProcess := getOptionValues(path, "x-oss-process"); xOssProcess != "" {
		options = append(options, oss.Process(xOssProcess))
	}
	signedURL, err := a.bucket.SignURL(storage.GetFilePathFromTotalPath(storage.FilterParams(path)), oss.HTTPGet, int64(duration/time.Second), options...)
	if err != nil {
		return "", err
	}
	if a.conf.Domain != "" {
		customUrl, err := storage.ReplaceDomain(signedURL, a.conf.Domain)
		if err == nil {
			signedURL = customUrl
		}
	}
	return signedURL, err
}

//Has 检查文件是否存在
func (a aliyunProvider) Has(path string) (bool, error) {
	// 校验path合法性
	if ok := storage.CheckPath(path, a.Protocol()); !ok {
		return false, storage.ErrUnExpectedPath
	}
	// 文件是否存在
	has, err := a.bucket.IsObjectExist(storage.GetFilePathFromTotalPath(path))
	return has, err
}

func getOptionValues(path, option string) string {
	stringArr := strings.Split(path, "?")
	if len(stringArr) < 2 {
		return ""
	}
	values, err := url.ParseQuery(stringArr[1])
	if err != nil {
		return ""
	}
	return values.Get(option)
}