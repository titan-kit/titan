package aliyun

import (
	"bytes"
	"crypto/md5"
	"fmt"
	"io/ioutil"
	"os"
	"testing"

	"gitee.com/titan-kit/titan/storage"
)

const IdpAliyunProtocol storage.Protocol = "idpAliyun"

var testAliProvider = &aliyunProvider{protocol: IdpAliyunProtocol}

func TestNew(t *testing.T) {
	accessKey := os.Getenv("aliYunAccessKey")
	secretKey := os.Getenv("aliYunSecretKey")
	testAliProvider = New(IdpAliyunProtocol, Config{"https://oss-cn-hangzhou.aliyuncs.com", accessKey, secretKey, "https://idp-test.pandateacher.com/", "idp-test1"})
}

func TestAliyunProvider(t *testing.T) {
	TestNew(t)
	fileName := "test.jpg"
	content, err := ioutil.ReadFile("./test.jpg")
	if err != nil {
		t.Error(err.Error())
	}

	//测试上传图片
	dataLen := int64(len(content))
	url, err := testAliProvider.Save(bytes.NewReader(content), dataLen, fileName)
	if err != nil {
		t.Fatalf("aliyunProvider::Save, upload file to oss error: %s\n", err.Error())
	}

	//测试检查是否存在
	has, err := testAliProvider.Has(url)
	if err != nil {
		t.Fatalf("aliyunProvider::Has, check file exists error: %s\n", err.Error())
	}
	if !has {
		t.Fatalf("aliyunProvider::Has, uploaded file but check has is false")
	}

	//测试获取文件内容
	ossContent, err := testAliProvider.Get(url)
	if err != nil {
		t.Fatalf("aliyunProvider::Get, get file: %s, content from oss error: %s\n", url, err.Error())
	}
	originMd5 := md5.Sum(content)
	ossMd5 := md5.Sum(ossContent)
	if fmt.Sprintf("%x", originMd5) != fmt.Sprintf("%x", ossMd5) {
		t.Fatalf("aliyunProvider::Get, content from oss is not match origin content")
	}

	//测试获取文件内容到目标字节数组
	f := make([]byte, 0)
	err = testAliProvider.Write(url, &f)
	if err != nil {
		t.Fatalf("aliyunProvider::Write, write oss content to dst error: %s\n", err.Error())
	}
	fMd5 := md5.Sum(f)
	if fmt.Sprintf("%x", originMd5) != fmt.Sprintf("%x", fMd5) {
		t.Fatalf("aliyunProvider::Write, content from oss is not match origin content ")
	}

	//测试获取文件访问链接
	ossURL, err := testAliProvider.Url(url, true, DefaultDownloadUrlExpire)
	if err != nil {
		t.Fatalf("aliyunProvider::Url, generate url error: %s\n", err.Error())
	}
	urlContent, err := storage.DownloadUrlToBytes(ossURL)
	if err != nil {
		t.Fatalf("download content from url: %s, error: %s", ossURL, err.Error())
	}
	uMds := md5.Sum(urlContent)
	if fmt.Sprintf("%x", originMd5) != fmt.Sprintf("%x", uMds) {
		t.Fatalf("aliyunProvider::Url, content download from oss url %s, is not match origin content", ossURL)
	}

	//测试protocol
	if IdpAliyunProtocol != testAliProvider.Protocol() {
		t.Fatalf("aliyunProvider::Protocol, check protocol error, expect: %s, get: %s\n", IdpAliyunProtocol, testAliProvider.Protocol())
	}
}

func TestGetOptionValues(t *testing.T) {
	testKey := "idpAliyun://6e291977-c76a-4c94-aa4e-6b72d5a359f3"
	ossProcessValue := getOptionValues(testKey, "x-oss-process")
	if ossProcessValue != "" {
		t.Fatalf("get x-oss-process values error, expect empty, get %s", ossProcessValue)
	}
	testKey += "?x-oss-process=image/resize,m_fill,w_450"
	ossProcessValue = getOptionValues(testKey, "x-oss-process")
	if ossProcessValue != "image/resize,m_fill,w_450" {
		t.Fatalf("get x-oss-process values error, expect %s, get %s", "image/resize,m_fill,w_450", ossProcessValue)
	}
	testKey += "?x-oss-process=image/resize,m_fill,w_450&sdadad=412380234dahs"
	ossProcessValue = getOptionValues(testKey, "x-oss-process")
	if ossProcessValue != "image/resize,m_fill,w_450" {
		t.Fatalf("get x-oss-process values error, expect %s, get %s", "image/resize,m_fill,w_450", ossProcessValue)
	}

}