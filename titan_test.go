package titan

import (
	"testing"
	"time"

	"gitee.com/titan-kit/titan/starter/rpc"
	"gitee.com/titan-kit/titan/starter/web"
)

func TestTitan(t *testing.T) {
	hs := web.NewServer()
	gs := rpc.NewServer()
	app := NewTitan(Name("9999"), Version("v1.0.0"), Server(hs, gs))
	time.AfterFunc(time.Second, func() {
		_ = app.Stop()
	})
	if err := app.Run(); err != nil {
		t.Fatal(err)
	}
}