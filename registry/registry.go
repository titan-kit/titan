package registry

import "context"

// Registrar 是服务注册商.
type Registrar interface {
	// Register 注册服务实例.
	Register(ctx context.Context, service *ServiceInstance) error
	// Deregister 注销服务实例.
	Deregister(ctx context.Context, service *ServiceInstance) error
}

// Discovery 是服务发现.
type Discovery interface {
	// GetService 根据服务名称返回内存中的服务实例.
	GetService(ctx context.Context, serviceName string) ([]*ServiceInstance, error)
	// Watch 根据服务名称创建观察者.
	Watch(ctx context.Context, serviceName string) (Watcher, error)
}

// Watcher 是服务观察者.
type Watcher interface {
	// Next 在以下两种情况下返回服务:
	// 1.第一次观看且服务实例列表不为空.
	// 2.找到任何服务实例更改.
	// 如果不满足上述两个条件,它将阻塞到超过或取消上下文截止日期为止.
	Next() ([]*ServiceInstance, error)
	// Stop 关闭观察者.
	Stop() error
}

// ServiceInstance 是发现系统中服务的实例.
type ServiceInstance struct {
	// ID 是已注册的唯一实例ID.
	ID string `json:"id"`
	// Name 是注册的服务名称.
	Name string `json:"name"`
	// Version 是已编译的版本.
	Version string `json:"version"`
	// Metadata 是与服务实例相关联的KV键值元数据.
	Metadata map[string]string `json:"metadata"`
	// Endpoints 是服务实例的端点地址.
	// 示例:
	//   http://127.0.0.1:8000?isSecure=false
	//   grpc://127.0.0.1:9000?isSecure=false
	Endpoints []string `json:"endpoints"`
}