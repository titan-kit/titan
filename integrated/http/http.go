package http

import (
	"net/http"
	"strings"

	"google.golang.org/grpc/codes"
)

const baseContentType = "application"

var (
	// HeaderAccept 是接受标标识.
	HeaderAccept = http.CanonicalHeaderKey("Accept")
	// HeaderContentType 是内容类型标识.
	HeaderContentType = http.CanonicalHeaderKey("Content-Type")
	// HeaderAcceptLanguage 是接受语言标识.
	HeaderAcceptLanguage = http.CanonicalHeaderKey("Accept-Language")
)

// ContentType 返回带有基本前缀的内容类型.
func ContentType(subtype string) string {
	return strings.Join([]string{baseContentType, subtype}, "/")
}

// ContentSubtype 返回给定内容类型的content-subtype.
// 给定的content-type必须是以开头的有效content-type，但不会返回content-subtype.
//
// 根据RFC7231.
// contentType的变量名假定都是小写字母.
func ContentSubtype(contentType string) string {
	left := strings.Index(contentType, "/")
	if left == -1 {
		return ""
	}
	right := strings.Index(contentType, ";")
	if right == -1 {
		right = len(contentType)
	}
	if right < left {
		return ""
	}
	return contentType[left+1 : right]
}

// GRPCCodeFromStatus 将HTTP错误代码转换为相应的gRPC响应状态.
// See: https://github.com/googleapis/googleapis/blob/master/google/rpc/code.proto
func GRPCCodeFromStatus(code int) codes.Code {
	switch code {
	case http.StatusOK:
		return codes.OK
	case http.StatusBadRequest:
		return codes.InvalidArgument
	case http.StatusUnauthorized:
		return codes.Unauthenticated
	case http.StatusForbidden:
		return codes.PermissionDenied
	case http.StatusNotFound:
		return codes.NotFound
	case http.StatusConflict:
		return codes.Aborted
	case http.StatusTooManyRequests:
		return codes.ResourceExhausted
	case http.StatusInternalServerError:
		return codes.Internal
	case http.StatusNotImplemented:
		return codes.Unimplemented
	case http.StatusServiceUnavailable:
		return codes.Unavailable
	case http.StatusGatewayTimeout:
		return codes.DeadlineExceeded
	}
	return codes.Unknown
}

// StatusFromGRPCCode 将gRPC错误代码转换为相应的HTTP响应状态.
// See: https://github.com/googleapis/googleapis/blob/master/google/rpc/code.proto
func StatusFromGRPCCode(code codes.Code) int {
	switch code {
	case codes.OK:
		return http.StatusOK
	case codes.Canceled:
		return http.StatusRequestTimeout
	case codes.Unknown:
		return http.StatusInternalServerError
	case codes.InvalidArgument:
		return http.StatusBadRequest
	case codes.DeadlineExceeded:
		return http.StatusGatewayTimeout
	case codes.NotFound:
		return http.StatusNotFound
	case codes.AlreadyExists:
		return http.StatusConflict
	case codes.PermissionDenied:
		return http.StatusForbidden
	case codes.Unauthenticated:
		return http.StatusUnauthorized
	case codes.ResourceExhausted:
		return http.StatusTooManyRequests
	case codes.FailedPrecondition:
		return http.StatusBadRequest
	case codes.Aborted:
		return http.StatusConflict
	case codes.OutOfRange:
		return http.StatusBadRequest
	case codes.Unimplemented:
		return http.StatusNotImplemented
	case codes.Internal:
		return http.StatusInternalServerError
	case codes.Unavailable:
		return http.StatusServiceUnavailable
	case codes.DataLoss:
		return http.StatusInternalServerError
	}
	return http.StatusInternalServerError
}