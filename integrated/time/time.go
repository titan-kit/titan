package time

import (
	"context"
	"fmt"
	"strconv"
	"strings"
	"time"
)

// Date 将unix时间整型格式化为字符串
func Date(ti int64, format string) string {
	t := time.Unix(int64(ti), 0)
	return DateT(t, format)
}

// DateS 将unix时间字符串格式化为字符串
func DateS(ts string, format string) string {
	i, _ := strconv.ParseInt(ts, 10, 64)
	return Date(i, format)
}

// DateT 将time.Time结构格式化为字符串
// MM - month - 01
// M - month - 1, single bit
// DD - day - 02
// D - day 2
// YYYY - year - 2006
// YY - year - 06
// HH - 24 hours - 03
// H - 24 hours - 3
// hh - 12 hours - 03
// h - 12 hours - 3
// mm - minute - 04
// m - minute - 4
// ss - second - 05
// s - second = 5
func DateT(t time.Time, format string) string {
	res := strings.Replace(format, "MM", t.Format("01"), -1)
	res = strings.Replace(res, "M", t.Format("1"), -1)
	res = strings.Replace(res, "DD", t.Format("02"), -1)
	res = strings.Replace(res, "D", t.Format("2"), -1)
	res = strings.Replace(res, "YYYY", t.Format("2006"), -1)
	res = strings.Replace(res, "YY", t.Format("06"), -1)
	res = strings.Replace(res, "HH", fmt.Sprintf("%02d", t.Hour()), -1)
	res = strings.Replace(res, "H", fmt.Sprintf("%d", t.Hour()), -1)
	res = strings.Replace(res, "hh", t.Format("03"), -1)
	res = strings.Replace(res, "h", t.Format("3"), -1)
	res = strings.Replace(res, "mm", t.Format("04"), -1)
	res = strings.Replace(res, "m", t.Format("4"), -1)
	res = strings.Replace(res, "ss", t.Format("05"), -1)
	res = strings.Replace(res, "s", t.Format("5"), -1)
	return res
}

// DateFormat规则.
var datePatterns = []string{
	// year
	"Y", "2006", // A full numeric representation of a year, 4 digits   Examples: 1999 or 2003
	"y", "06", // A two digit representation of a year   Examples: 99 or 03

	// month
	"m", "01", // Numeric representation of a month, with leading zeros 01 through 12
	"n", "1", // Numeric representation of a month, without leading zeros   1 through 12
	"M", "Jan", // A short textual representation of a month, three letters Jan through Dec
	"F", "January", // A full textual representation of a month, such as January or March   January through December

	// day
	"d", "02", // Day of the month, 2 digits with leading zeros 01 to 31
	"j", "2", // Day of the month without leading zeros 1 to 31

	// week
	"D", "Mon", // A textual representation of a day, three letters Mon through Sun
	"l", "Monday", // A full textual representation of the day of the week  Sunday through Saturday

	// time
	"g", "3", // 12-hour format of an hour without leading zeros    1 through 12
	"G", "15", // 24-hour format of an hour without leading zeros   0 through 23
	"h", "03", // 12-hour format of an hour with leading zeros  01 through 12
	"H", "15", // 24-hour format of an hour with leading zeros  00 through 23

	"a", "pm", // Lowercase Ante meridiem and Post meridiem am or pm
	"A", "PM", // Uppercase Ante meridiem and Post meridiem AM or PM

	"i", "04", // Minutes with leading zeros    00 to 59
	"s", "05", // Seconds, with leading zeros   00 through 59

	// time zone
	"T", "MST",
	"P", "-07:00",
	"O", "-0700",

	// RFC 2822
	"r", time.RFC1123Z,
}

// DateParse 解析日期使用PHP时间格式.
func DateParse(dateString, format string) (time.Time, error) {
	replacer := strings.NewReplacer(datePatterns...)
	format = replacer.Replace(format)
	return time.ParseInLocation(format, dateString, time.Local)
}

var TimeFunc = time.Now

type StringTime time.Time

// MarshalJSON 实现它的json序列化方法
func (st StringTime) MarshalJSON() ([]byte, error) {
	var stamp = fmt.Sprintf("\"%s\"", time.Time(st).Format("2006-01-02 15:04:05"))
	return []byte(stamp), nil
}

func (st *StringTime) UnmarshalJSON(data []byte) error {
	d, err := time.ParseInLocation("\"2006-01-02 15:04:05\"", string(data), time.UTC)
	*st = StringTime(d)
	return err
}

// Precision 确定此库对时间的精确度.
// 序列化和反序列化令牌时，时间值会自动截断至此精度.
// 有关更多详细信息，请参见时间包的Truncate方法。
const Precision = time.Microsecond

type NumberTime time.Time

func (nt NumberTime) MarshalJSON() ([]byte, error) {
	var stamp = fmt.Sprintf("%d", time.Time(nt).UnixNano())
	return []byte(stamp), nil
}
func (nt *NumberTime) UnmarshalJSON(data []byte) error {
	i, err := strconv.ParseInt(string(data), 10, 64)
	*nt = NumberTime(time.Unix(0, i))
	return err
}

// Duration 用于取消编组字符串时间，例如1s，500ms.
type Duration time.Duration

// UnmarshalText 将文本解组到持续时间.
func (d *Duration) UnmarshalText(text []byte) error {
	tmp, err := time.ParseDuration(string(text))
	if err == nil {
		*d = Duration(tmp)
	}
	return err
}

func NewDuration(str string) (dur Duration) {
	tmp, err := time.ParseDuration(str)
	if err == nil {
		dur = Duration(tmp)
	}
	return
}

// NewTime 从float64创建一个新的时间值
func NewTime(t float64) time.Time {
	return At(time.Unix(0, int64(t*float64(time.Second))))
}

// Now 使用当前时间返回新的时间值.
// 您可以通过更改TimeFunc的值来覆盖Now。
func Now() time.Time {
	return At(TimeFunc())
}

// At 根据标准库时间生成时间值。
func At(at time.Time) time.Time {
	return at.Truncate(Precision)
}

func (d Duration) Shrink(c context.Context) (Duration, context.Context, context.CancelFunc) {
	if deadline, ok := c.Deadline(); ok {
		if timeout := time.Until(deadline); timeout < time.Duration(d) {
			// deliver small timeout
			return Duration(timeout), c, func() {}
		}
	}
	ctx, cancel := context.WithTimeout(c, time.Duration(d))
	return d, ctx, cancel
}