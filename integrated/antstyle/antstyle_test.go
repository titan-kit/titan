package antstyle

import (
	"fmt"
	"testing"
	"time"

	. "github.com/smartystreets/goconvey/convey"
)

// TestIsPattern
func TestIsPattern(t *testing.T) {
	/**
	  规则：* | ** | ？
	*/
	// ---true---
	t.Log(matcher.IsPattern(""))
	t.Log(matcher.IsPattern("http://example.org"))
	t.Log(matcher.IsPattern("{"))

	// ---false---
	t.Log(matcher.IsPattern("*"))
	t.Log(matcher.IsPattern("?"))
	t.Log(matcher.IsPattern("*?"))
	t.Log(matcher.IsPattern("\\*"))
	// ant 标准语法 ？ * **
	t.Log(matcher.IsPattern("http://example.org?name=chao"))
	t.Log(matcher.IsPattern("/app/*.x"))
	t.Log(matcher.IsPattern("/app/p?ttern"))
	t.Log(matcher.IsPattern("/**/example"))
	t.Log(matcher.IsPattern("/app/**/dir/file."))
	t.Log(matcher.IsPattern("/**/*.jsp"))
}

func TestMatchLog(t *testing.T) {
	t.Log(matcher.Match("tes?", "test"))
	t.Log(matcher.Match("/hotels/{hotel}", "/hotels/1"))
	t.Log(matcher.Match("tes?", "tes"))
	t.Log(matcher.Match("tes?", "testt"))
	t.Log(matcher.Match("tes?", "tsst"))
}

// TestMatch
func TestMatch(t *testing.T) {
	Convey("Test Match", t, func() {
		Convey("test exact matching", func() {
			So(true, ShouldEqual, matcher.Match("test", "test"))
			So(true, ShouldEqual, matcher.Match("/test", "/test"))
			So(true, ShouldEqual, matcher.Match("http://example.org", "http://example.org")) // SPR-14141
			So(false, ShouldEqual, matcher.Match("/test.jpg", "test.jpg"))
			So(false, ShouldEqual, matcher.Match("test", "/test"))
			So(false, ShouldEqual, matcher.Match("/test", "test"))
		})

		Convey("test matching with ?'s", func() {
			So(true, ShouldEqual, matcher.Match("t?st", "test"))
			So(true, ShouldEqual, matcher.Match("??st", "test"))
			So(true, ShouldEqual, matcher.Match("tes?", "test"))
			So(true, ShouldEqual, matcher.Match("te??", "test"))
			So(true, ShouldEqual, matcher.Match("?es?", "test"))
			So(false, ShouldEqual, matcher.Match("tes?", "tes"))
			So(false, ShouldEqual, matcher.Match("tes?", "testt"))
			So(false, ShouldEqual, matcher.Match("tes?", "tsst"))
		})
		Convey("test matching with *'s", func() {
			So(true, ShouldEqual, matcher.Match("*", "test"))
			So(true, ShouldEqual, matcher.Match("test*", "test"))
			So(true, ShouldEqual, matcher.Match("test*", "testTest"))
			So(true, ShouldEqual, matcher.Match("test/*", "test/Test"))
			So(true, ShouldEqual, matcher.Match("test/*", "test/t"))
			So(true, ShouldEqual, matcher.Match("test/*", "test/"))
			So(true, ShouldEqual, matcher.Match("*test*", "AnothertestTest"))
			So(true, ShouldEqual, matcher.Match("*test", "Anothertest"))
			So(true, ShouldEqual, matcher.Match("*.*", "test."))
			So(true, ShouldEqual, matcher.Match("*.*", "test.test"))
			So(true, ShouldEqual, matcher.Match("*.*", "test.test.test"))
			So(true, ShouldEqual, matcher.Match("test*aaa", "testblaaaa"))
			So(false, ShouldEqual, matcher.Match("test*", "tst"))
			So(false, ShouldEqual, matcher.Match("test*", "tsttest"))
			So(false, ShouldEqual, matcher.Match("test*", "test/"))
			So(false, ShouldEqual, matcher.Match("test*", "test/t"))
			So(false, ShouldEqual, matcher.Match("test/*", "test"))
			So(false, ShouldEqual, matcher.Match("*test*", "tsttst"))
			So(false, ShouldEqual, matcher.Match("*test", "tsttst"))
			So(false, ShouldEqual, matcher.Match("*.*", "tsttst"))
			So(false, ShouldEqual, matcher.Match("test*aaa", "test"))
			So(false, ShouldEqual, matcher.Match("test*aaa", "testblaaab"))
		})
		Convey("test matching with ?'s and /'s", func() {
			So(true, ShouldEqual, matcher.Match("/?", "/a"))
			So(true, ShouldEqual, matcher.Match("/?/a", "/a/a"))
			So(true, ShouldEqual, matcher.Match("/a/?", "/a/b"))
			So(true, ShouldEqual, matcher.Match("/??/a", "/aa/a"))
			So(true, ShouldEqual, matcher.Match("/a/??", "/a/bb"))
			So(true, ShouldEqual, matcher.Match("/?", "/a"))
		})
		Convey("test matching with **'s", func() {
			So(true, ShouldEqual, matcher.Match("/**", "/testing/testing"))
			So(true, ShouldEqual, matcher.Match("/*/**", "/testing/testing"))
			So(true, ShouldEqual, matcher.Match("/**/*", "/testing/testing"))
			So(true, ShouldEqual, matcher.Match("/bla/**/bla", "/bla/testing/testing/bla"))
			So(true, ShouldEqual, matcher.Match("/bla/**/bla", "/bla/testing/testing/bla/bla"))
			So(true, ShouldEqual, matcher.Match("/**/test", "/bla/bla/test"))
			So(true, ShouldEqual, matcher.Match("/bla/**/**/bla", "/bla/bla/bla/bla/bla/bla"))
			So(true, ShouldEqual, matcher.Match("/bla*bla/test", "/blaXXXbla/test"))
			So(true, ShouldEqual, matcher.Match("/*bla/test", "/XXXbla/test"))
			So(false, ShouldEqual, matcher.Match("/bla*bla/test", "/blaXXXbl/test"))
			So(false, ShouldEqual, matcher.Match("/*bla/test", "XXXblab/test"))
			So(false, ShouldEqual, matcher.Match("/*bla/test", "XXXbl/test"))
			So(false, ShouldEqual, matcher.Match("/????", "/bala/bla"))
			So(false, ShouldEqual, matcher.Match("/**/*bla", "/bla/bla/bla/bbb"))
			So(false, ShouldEqual, matcher.Match("/????", "/bala/bla"))
			So(false, ShouldEqual, matcher.Match("/**/*bla", "/bla/bla/bla/bbb"))
			//
			So(true, ShouldEqual, matcher.Match("/*bla*/**/bla/**", "/XXXblaXXXX/testing/testing/bla/testing/testing/"))
			So(true, ShouldEqual, matcher.Match("/*bla*/**/bla/*", "/XXXblaXXXX/testing/testing/bla/testing"))
			So(true, ShouldEqual, matcher.Match("/*bla*/**/bla/**", "/XXXblaXXXX/testing/testing/bla/testing/testing"))
			So(true, ShouldEqual, matcher.Match("/*bla*/**/bla/**", "/XXXblaXXXX/testing/testing/bla/testing/testing.jpg"))

			So(true, ShouldEqual, matcher.Match("*bla*/**/bla/**", "XXXblaXXXX/testing/testing/bla/testing/testing/"))
			So(true, ShouldEqual, matcher.Match("*bla*/**/bla/*", "XXXblaXXXX/testing/testing/bla/testing"))
			So(true, ShouldEqual, matcher.Match("*bla*/**/bla/**", "XXXblaXXXX/testing/testing/bla/testing/testing"))
			So(false, ShouldEqual, matcher.Match("*bla*/**/bla/*", "XXXblaXXXX/testing/testing/bla/testing/testing"))

			So(false, ShouldEqual, matcher.Match("/x/x/**/bla", "/x/x/x/"))

			So(true, ShouldEqual, matcher.Match("/foo/bar/**", "/foo/bar"))

			So(true, ShouldEqual, matcher.Match("", ""))

			So(true, ShouldEqual, matcher.Match("/{bla}.*", "/testing.html"))
		})
	})
}

// TestMatchStart
func TestMatchStart(t *testing.T) {
	Convey("Test Match Start", t, func() {
		Convey("test exact matching", func() {
			So(true, ShouldEqual, matcher.MatchStart("test", "test"))
			So(true, ShouldEqual, matcher.MatchStart("/test", "/test"))
			So(false, ShouldEqual, matcher.MatchStart("/test.jpg", "test.jpg"))
			So(false, ShouldEqual, matcher.MatchStart("test", "/test"))
			So(false, ShouldEqual, matcher.MatchStart("/test", "test"))

		})
		Convey("test matching with ?'s", func() {
			So(true, ShouldEqual, matcher.MatchStart("t?st", "test"))
			So(true, ShouldEqual, matcher.MatchStart("??st", "test"))
			So(true, ShouldEqual, matcher.MatchStart("tes?", "test"))
			So(true, ShouldEqual, matcher.MatchStart("te??", "test"))
			So(true, ShouldEqual, matcher.MatchStart("?es?", "test"))
			So(false, ShouldEqual, matcher.MatchStart("tes?", "tes"))
			So(false, ShouldEqual, matcher.MatchStart("tes?", "testt"))
			So(false, ShouldEqual, matcher.MatchStart("tes?", "tsst"))

		})
		Convey("test matching with *'s", func() {
			So(true, ShouldEqual, matcher.MatchStart("*", "test"))
			So(true, ShouldEqual, matcher.MatchStart("test*", "test"))
			So(true, ShouldEqual, matcher.MatchStart("test*", "testTest"))
			So(true, ShouldEqual, matcher.MatchStart("test/*", "test/Test"))
			So(true, ShouldEqual, matcher.MatchStart("test/*", "test/t"))
			So(true, ShouldEqual, matcher.MatchStart("test/*", "test/"))
			So(true, ShouldEqual, matcher.MatchStart("*test*", "AnothertestTest"))
			So(true, ShouldEqual, matcher.MatchStart("*test", "Anothertest"))
			So(true, ShouldEqual, matcher.MatchStart("*.*", "test."))
			So(true, ShouldEqual, matcher.MatchStart("*.*", "test.test"))
			So(true, ShouldEqual, matcher.MatchStart("*.*", "test.test.test"))
			So(true, ShouldEqual, matcher.MatchStart("test*aaa", "testblaaaa"))
			So(false, ShouldEqual, matcher.MatchStart("test*", "tst"))
			So(false, ShouldEqual, matcher.MatchStart("test*", "test/"))
			So(false, ShouldEqual, matcher.MatchStart("test*", "tsttest"))
			So(false, ShouldEqual, matcher.MatchStart("test*", "test/"))
			So(false, ShouldEqual, matcher.MatchStart("test*", "test/t"))
			So(true, ShouldEqual, matcher.MatchStart("test/*", "test"))
			So(true, ShouldEqual, matcher.MatchStart("test/t*.txt", "test"))
			So(false, ShouldEqual, matcher.MatchStart("*test*", "tsttst"))
			So(false, ShouldEqual, matcher.MatchStart("*test", "tsttst"))
			So(false, ShouldEqual, matcher.MatchStart("*.*", "tsttst"))
			So(false, ShouldEqual, matcher.MatchStart("test*aaa", "test"))
			So(false, ShouldEqual, matcher.MatchStart("test*aaa", "testblaaab"))
		})
		Convey(" test matching with ?'s and /'s", func() {
			So(true, ShouldEqual, matcher.MatchStart("/?", "/a"))
			So(true, ShouldEqual, matcher.MatchStart("/?/a", "/a/a"))
			So(true, ShouldEqual, matcher.MatchStart("/a/?", "/a/b"))
			So(true, ShouldEqual, matcher.MatchStart("/??/a", "/aa/a"))
			So(true, ShouldEqual, matcher.MatchStart("/a/??", "/a/bb"))
			So(true, ShouldEqual, matcher.MatchStart("/?", "/a"))
		})
		Convey("test matching with **'s", func() {
			So(true, ShouldEqual, matcher.MatchStart("/**", "/testing/testing"))
			So(true, ShouldEqual, matcher.MatchStart("/*/**", "/testing/testing"))
			So(true, ShouldEqual, matcher.MatchStart("/**/*", "/testing/testing"))
			So(true, ShouldEqual, matcher.MatchStart("test*/**", "test/"))
			So(true, ShouldEqual, matcher.MatchStart("test*/**", "test/t"))
			So(true, ShouldEqual, matcher.MatchStart("/bla/**/bla", "/bla/testing/testing/bla"))
			So(true, ShouldEqual, matcher.MatchStart("/bla/**/bla", "/bla/testing/testing/bla/bla"))
			So(true, ShouldEqual, matcher.MatchStart("/**/test", "/bla/bla/test"))
			So(true, ShouldEqual, matcher.MatchStart("/bla/**/**/bla", "/bla/bla/bla/bla/bla/bla"))
			So(true, ShouldEqual, matcher.MatchStart("/bla*bla/test", "/blaXXXbla/test"))
			So(true, ShouldEqual, matcher.MatchStart("/*bla/test", "/XXXbla/test"))
			So(false, ShouldEqual, matcher.MatchStart("/bla*bla/test", "/blaXXXbl/test"))
			So(false, ShouldEqual, matcher.MatchStart("/*bla/test", "XXXblab/test"))
			So(false, ShouldEqual, matcher.MatchStart("/*bla/test", "XXXbl/test"))

			So(false, ShouldEqual, matcher.MatchStart("/????", "/bala/bla"))
			So(true, ShouldEqual, matcher.MatchStart("/**/*bla", "/bla/bla/bla/bbb"))

			So(true, ShouldEqual, matcher.MatchStart("/*bla*/**/bla/**", "/XXXblaXXXX/testing/testing/bla/testing/testing/"))
			So(true, ShouldEqual, matcher.MatchStart("/*bla*/**/bla/*", "/XXXblaXXXX/testing/testing/bla/testing"))
			So(true, ShouldEqual, matcher.MatchStart("/*bla*/**/bla/**", "/XXXblaXXXX/testing/testing/bla/testing/testing"))
			So(true, ShouldEqual, matcher.MatchStart("/*bla*/**/bla/**", "/XXXblaXXXX/testing/testing/bla/testing/testing.jpg"))

			So(true, ShouldEqual, matcher.MatchStart("*bla*/**/bla/**", "XXXblaXXXX/testing/testing/bla/testing/testing/"))
			So(true, ShouldEqual, matcher.MatchStart("*bla*/**/bla/*", "XXXblaXXXX/testing/testing/bla/testing"))
			So(true, ShouldEqual, matcher.MatchStart("*bla*/**/bla/**", "XXXblaXXXX/testing/testing/bla/testing/testing"))
			So(true, ShouldEqual, matcher.MatchStart("*bla*/**/bla/*", "XXXblaXXXX/testing/testing/bla/testing/testing"))

			So(true, ShouldEqual, matcher.MatchStart("/x/x/**/bla", "/x/x/x/"))

			So(true, ShouldEqual, matcher.MatchStart("", ""))
		})
	})

}

// TestExtractPathWithinPattern
func TestExtractPathWithinPattern(t *testing.T) {
	Convey("Test Extract Path Within Pattern", t, func() {
		So("", ShouldEqual, matcher.ExtractPathWithinPattern("/docs/commit.html", "/docs/commit.html"))

		So("cvs/commit", ShouldEqual, matcher.ExtractPathWithinPattern("/docs/*", "/docs/cvs/commit"))
		So("commit.html", ShouldEqual, matcher.ExtractPathWithinPattern("/docs/cvs/*.html", "/docs/cvs/commit.html"))
		So("cvs/commit", ShouldEqual, matcher.ExtractPathWithinPattern("/docs/**", "/docs/cvs/commit"))
		So("cvs/commit.html", ShouldEqual, matcher.ExtractPathWithinPattern("/docs/**/*.html", "/docs/cvs/commit.html"))
		So("commit.html", ShouldEqual, matcher.ExtractPathWithinPattern("/docs/**/*.html", "/docs/commit.html"))
		So("commit.html", ShouldEqual, matcher.ExtractPathWithinPattern("/*.html", "/commit.html"))
		So("docs/commit.html", ShouldEqual, matcher.ExtractPathWithinPattern("/*.html", "/docs/commit.html"))
		So("/commit.html", ShouldEqual, matcher.ExtractPathWithinPattern("*.html", "/commit.html"))
		So("/docs/commit.html", ShouldEqual, matcher.ExtractPathWithinPattern("*.html", "/docs/commit.html"))
		So("/docs/commit.html", ShouldEqual, matcher.ExtractPathWithinPattern("**/*.*", "/docs/commit.html"))
		So("/docs/commit.html", ShouldEqual, matcher.ExtractPathWithinPattern("*", "/docs/commit.html"))
		Convey(" SPR-10515", func() {
			So("/docs/cvs/other/commit.html", ShouldEqual, matcher.ExtractPathWithinPattern("**/commit.html", "/docs/cvs/other/commit.html"))
			So("cvs/other/commit.html", ShouldEqual, matcher.ExtractPathWithinPattern("/docs/**/commit.html", "/docs/cvs/other/commit.html"))
			So("cvs/other/commit.html", ShouldEqual, matcher.ExtractPathWithinPattern("/docs/**/**/**/**", "/docs/cvs/other/commit.html"))

			So("docs/cvs/commit", ShouldEqual, matcher.ExtractPathWithinPattern("/d?cs/*", "/docs/cvs/commit"))
			So("cvs/commit.html", ShouldEqual, matcher.ExtractPathWithinPattern("/docs/c?s/*.html", "/docs/cvs/commit.html"))
			So("docs/cvs/commit", ShouldEqual, matcher.ExtractPathWithinPattern("/d?cs/**", "/docs/cvs/commit"))
			So("docs/cvs/commit.html", ShouldEqual, matcher.ExtractPathWithinPattern("/d?cs/**/*.html", "/docs/cvs/commit.html"))
		})
	})
}

// TestExtractUriTemplateVariables
func TestExtractUriTemplateVariables(t *testing.T) {
	Convey("Test Match Start", t, func() {
		result := matcher.ExtractUriTemplateVariables("/hotels/{hotel}", "/hotels/1")
		So("1", ShouldEqual, (*result)["hotel"])

		result = matcher.ExtractUriTemplateVariables("/h?tels/{hotel}", "/hotels/1")
		So("1", ShouldEqual, (*result)["hotel"])

		result = matcher.ExtractUriTemplateVariables("/hotels/{hotel}/bookings/{booking}", "/hotels/1/bookings/2")
		So("1", ShouldEqual, (*result)["hotel"])
		So("2", ShouldEqual, (*result)["booking"])

		result = matcher.ExtractUriTemplateVariables("/**/hotels/**/{hotel}", "/foo/hotels/bar/1")
		So("1", ShouldEqual, (*result)["hotel"])

		result = matcher.ExtractUriTemplateVariables("/{page}.html", "/42.html")
		So("42", ShouldEqual, (*result)["page"])

		result = matcher.ExtractUriTemplateVariables("/{page}.*", "/42.html")
		So("42", ShouldEqual, (*result)["page"])

		result = matcher.ExtractUriTemplateVariables("/A-{B}-C", "/A-b-C")
		So("b", ShouldEqual, (*result)["B"])

		result = matcher.ExtractUriTemplateVariables("/{name}.{extension}", "/test.html")
		So("test", ShouldEqual, (*result)["name"])
		So("html", ShouldEqual, (*result)["extension"])
	})
}

// TestExtractUriTemplateVariablesRegex
func TestExtractUriTemplateVariablesRegex(t *testing.T) {
	Convey("Test Match Start", t, func() {
		result := matcher.ExtractUriTemplateVariables("{symbolicName:[\\w\\.]+}-{version:[\\w\\.]+}.jar", "com.example-1.0.0.jar")
		So("com.example", ShouldEqual, (*result)["symbolicName"])
		So("1.0.0", ShouldEqual, (*result)["version"])

		result = matcher.ExtractUriTemplateVariables("{symbolicName:[\\w\\.]+}-sources-{version:[\\w\\.]+}.jar", "com.example-sources-1.0.0.jar")
		So("com.example", ShouldEqual, (*result)["symbolicName"])
		So("1.0.0", ShouldEqual, (*result)["version"])
	})
}

/**
* SPR-7787
 */
// TestExtractUriTemplateVarsRegexQualifiers
func TestExtractUriTemplateVarsRegexQualifiers(t *testing.T) {
	Convey("Test Extract Uri Template Vars Regex Qualifiers", t, func() {
		result := matcher.ExtractUriTemplateVariables("{symbolicName:[\\p{L}\\.]+}-sources-{version:[\\p{N}\\.]+}.jar", "com.example-sources-1.0.0.jar")
		So("com.example", ShouldEqual, (*result)["symbolicName"])
		So("1.0.0", ShouldEqual, (*result)["version"])

		result = matcher.ExtractUriTemplateVariables("{symbolicName:[\\w\\.]+}-sources-{version:[\\d\\.]+}-{year:\\d{4}}{month:\\d{2}}{day:\\d{2}}.jar", "com.example-sources-1.0.0-20100220.jar")
		So("com.example", ShouldEqual, (*result)["symbolicName"])
		So("1.0.0", ShouldEqual, (*result)["version"])
		So("2010", ShouldEqual, (*result)["year"])
		So("02", ShouldEqual, (*result)["month"])
		So("20", ShouldEqual, (*result)["day"])

		result = matcher.ExtractUriTemplateVariables("{symbolicName:[\\p{L}\\.]+}-sources-{version:[\\p{N}\\.\\{\\}]+}.jar", "com.example-sources-1.0.0.{12}.jar")
		So("com.example", ShouldEqual, (*result)["symbolicName"])
		So("1.0.0.{12}", ShouldEqual, (*result)["version"])
	})

}

/**
 * SPR-8455
 */
// TestExtractUriTemplateVarsRegexCapturingGroups
func TestExtractUriTemplateVarsRegexCapturingGroups(t *testing.T) {
	Convey("Test Extract Uri Template Vars Regex Capturing Groups", t, func() {
		result := matcher.ExtractUriTemplateVariables("/web/{id:foo(bar)?}", "/web/foobar")
		So("foobar", ShouldEqual, (*result)["id"])
	})
}

// TestGetPatternComparator
func TestGetPatternComparator(t *testing.T) {

	Convey("Test Match Start", t, func() {
		comparator := matcher.GetPatternComparator("/hotels/new")
		So(0, ShouldEqual, comparator.Compare("", ""))
		So(1, ShouldEqual, comparator.Compare("", "/hotels/new"))
		So(-1, ShouldEqual, comparator.Compare("/hotels/new", ""))

		So(0, ShouldEqual, comparator.Compare("/hotels/new", "/hotels/new"))

		So(-1, ShouldEqual, comparator.Compare("/hotels/new", "/hotels/*"))
		So(1, ShouldEqual, comparator.Compare("/hotels/*", "/hotels/new"))
		So(0, ShouldEqual, comparator.Compare("/hotels/*", "/hotels/*"))

		So(-1, ShouldEqual, comparator.Compare("/hotels/new", "/hotels/{hotel}"))
		So(1, ShouldEqual, comparator.Compare("/hotels/{hotel}", "/hotels/new"))
		So(0, ShouldEqual, comparator.Compare("/hotels/{hotel}", "/hotels/{hotel}"))
		So(-1, ShouldEqual, comparator.Compare("/hotels/{hotel}/booking", "/hotels/{hotel}/bookings/{booking}"))
		So(1, ShouldEqual, comparator.Compare("/hotels/{hotel}/bookings/{booking}", "/hotels/{hotel}/booking"))
		Convey("SPR-10550", func() {
			So(-1, ShouldEqual, comparator.Compare("/hotels/{hotel}/bookings/{booking}/cutomers/{customer}", "/**"))
			So(1, ShouldEqual, comparator.Compare("/**", "/hotels/{hotel}/bookings/{booking}/cutomers/{customer}"))
			So(0, ShouldEqual, comparator.Compare("/**", "/**"))

			So(-1, ShouldEqual, comparator.Compare("/hotels/{hotel}", "/hotels/*"))
			So(1, ShouldEqual, comparator.Compare("/hotels/*", "/hotels/{hotel}"))

			So(-1, ShouldEqual, comparator.Compare("/hotels/*", "/hotels/*/**"))
			So(1, ShouldEqual, comparator.Compare("/hotels/*/**", "/hotels/*"))

			So(-1, ShouldEqual, comparator.Compare("/hotels/new", "/hotels/new.*"))
			So(2, ShouldEqual, comparator.Compare("/hotels/{hotel}", "/hotels/{hotel}.*"))
		})
		Convey("SPR-6741", func() {
			So(-1, ShouldEqual, comparator.Compare("/hotels/{hotel}/bookings/{booking}/cutomers/{customer}", "/hotels/**"))
			So(1, ShouldEqual, comparator.Compare("/hotels/**", "/hotels/{hotel}/bookings/{booking}/cutomers/{customer}"))
			So(1, ShouldEqual, comparator.Compare("/hotels/foo/bar/**", "/hotels/{hotel}"))
			So(-1, ShouldEqual, comparator.Compare("/hotels/{hotel}", "/hotels/foo/bar/**"))
			So(2, ShouldEqual, comparator.Compare("/hotels/**/bookings/**", "/hotels/**"))
			So(-2, ShouldEqual, comparator.Compare("/hotels/**", "/hotels/**/bookings/**"))

		})
		Convey("SPR-8683", func() {
			So(1, ShouldEqual, comparator.Compare("/**", "/hotels/{hotel}"))
		})
		Convey("longer is better", func() {
			So(1, ShouldEqual, comparator.Compare("/hotels", "/hotels2"))
		})
		Convey("SPR-13139", func() {
			So(-1, ShouldEqual, comparator.Compare("*", "*/**"))
			So(1, ShouldEqual, comparator.Compare("*/**", "*"))
		})
	})

}

// TestCombine
func TestCombine(t *testing.T) {
	Convey("Test Combine beginning...", t, func() {
		So("", ShouldEqual, matcher.Combine("", ""))
		So("/hotels", ShouldEqual, matcher.Combine("/hotels", ""))
		So("/hotels", ShouldEqual, matcher.Combine("", "/hotels"))
		So("/hotels/booking", ShouldEqual, matcher.Combine("/hotels/*", "booking"))
		So("/hotels/booking", ShouldEqual, matcher.Combine("/hotels/*", "/booking"))
		So("/hotels/**/booking", ShouldEqual, matcher.Combine("/hotels/**", "booking"))
		So("/hotels/**/booking", ShouldEqual, matcher.Combine("/hotels/**", "/booking"))
		So("/hotels/booking", ShouldEqual, matcher.Combine("/hotels", "/booking"))
		So("/hotels/booking", ShouldEqual, matcher.Combine("/hotels", "booking"))
		So("/hotels/booking", ShouldEqual, matcher.Combine("/hotels/", "booking"))
		So("/hotels/{hotel}", ShouldEqual, matcher.Combine("/hotels/*", "{hotel}"))
		So("/hotels/**/{hotel}", ShouldEqual, matcher.Combine("/hotels/**", "{hotel}"))
		So("/hotels/{hotel}", ShouldEqual, matcher.Combine("/hotels", "{hotel}"))
		So("/hotels/{hotel}.*", ShouldEqual, matcher.Combine("/hotels", "{hotel}.*"))
		So("/hotels/*/booking/{booking}", ShouldEqual, matcher.Combine("/hotels/*/booking", "{booking}"))
		Convey("SPR", func() {
			So("/hotel.html", ShouldEqual, matcher.Combine("/*.html", "/hotel.html"))
			So("/hotel.html", ShouldEqual, matcher.Combine("/*.html", "/hotel"))
			So("/hotel.html", ShouldEqual, matcher.Combine("/*.html", "/hotel.*"))
			So("/*.html", ShouldEqual, matcher.Combine("/**", "/*.html"))
			So("/*.html", ShouldEqual, matcher.Combine("/*", "/*.html"))
			So("/*.html", ShouldEqual, matcher.Combine("/*.*", "/*.html"))
			So("/{foo}/bar", ShouldEqual, matcher.Combine("/{foo}", "/bar"))                           // SPR-8858
			So("/user/user", ShouldEqual, matcher.Combine("/user", "/user"))                           // SPR-7970
			So("/{foo:.*[^0-9].*}/edit/", ShouldEqual, matcher.Combine("/{foo:.*[^0-9].*}", "/edit/")) // SPR-10062
			So("/1.0/foo/test", ShouldEqual, matcher.Combine("/1.0", "/foo/test"))                     // SPR-10554
			So("/hotel", ShouldEqual, matcher.Combine("/", "/hotel"))                                  // SPR-12975
			So("/hotel/booking", ShouldEqual, matcher.Combine("/hotel/", "/booking"))                  // SPR-12975

		})
	})
}

// SPR-14247
// TestMatchWithTrimTokensEnabled
func TestMatchWithTrimTokensEnabled(t *testing.T) {
	Convey("Test Match With Trim Tokens Enabled", t, func() {
		matcher.SetTrimTokens(true)
		So(true, ShouldEqual, matcher.Match("/foo/bar", "/foo /bar"))
	})
}

// TestUniqueDeliminator
func TestUniqueDeliminator(t *testing.T) {
	Convey("Test Unique Deliminator", t, func() {
		matcher.SetPathSeparator(".")
		Convey("test exact matching", func() {
			So(true, ShouldEqual, matcher.Match("test", "test"))
			So(true, ShouldEqual, matcher.Match(".test", ".test"))
			So(false, ShouldEqual, matcher.Match(".test/jpg", "test/jpg"))
			So(false, ShouldEqual, matcher.Match("test", ".test"))
			So(false, ShouldEqual, matcher.Match(".test", "test"))
		})
		Convey("test matching with ?'s", func() {
			So(true, ShouldEqual, matcher.Match("t?st", "test"))
			So(true, ShouldEqual, matcher.Match("??st", "test"))
			So(true, ShouldEqual, matcher.Match("tes?", "test"))
			So(true, ShouldEqual, matcher.Match("te??", "test"))
			So(true, ShouldEqual, matcher.Match("?es?", "test"))
			So(false, ShouldEqual, matcher.Match("tes?", "tes"))
			So(false, ShouldEqual, matcher.Match("tes?", "testt"))
			So(false, ShouldEqual, matcher.Match("tes?", "tsst"))
		})
		Convey("test matching with *'s", func() {
			So(true, ShouldEqual, matcher.Match("*", "test"))
			So(true, ShouldEqual, matcher.Match("test*", "test"))
			So(true, ShouldEqual, matcher.Match("test*", "testTest"))
			So(true, ShouldEqual, matcher.Match("*test*", "AnothertestTest"))
			So(true, ShouldEqual, matcher.Match("*test", "Anothertest"))
			So(true, ShouldEqual, matcher.Match("*/*", "test/"))
			So(true, ShouldEqual, matcher.Match("*/*", "test/test"))
			So(true, ShouldEqual, matcher.Match("*/*", "test/test/test"))
			So(true, ShouldEqual, matcher.Match("test*aaa", "testblaaaa"))
			So(false, ShouldEqual, matcher.Match("test*", "tst"))
			So(false, ShouldEqual, matcher.Match("test*", "tsttest"))
			So(false, ShouldEqual, matcher.Match("*test*", "tsttst"))
			So(false, ShouldEqual, matcher.Match("*test", "tsttst"))
			So(false, ShouldEqual, matcher.Match("*/*", "tsttst"))
			So(false, ShouldEqual, matcher.Match("test*aaa", "test"))
			So(false, ShouldEqual, matcher.Match("test*aaa", "testblaaab"))
		})
		Convey("test matching with ?'s and .'s", func() {
			So(true, ShouldEqual, matcher.Match(".?", ".a"))
			So(true, ShouldEqual, matcher.Match(".?.a", ".a.a"))
			So(true, ShouldEqual, matcher.Match(".a.?", ".a.b"))
			So(true, ShouldEqual, matcher.Match(".??.a", ".aa.a"))
			So(true, ShouldEqual, matcher.Match(".a.??", ".a.bb"))
			So(true, ShouldEqual, matcher.Match(".?", ".a"))
		})
		Convey("test matching with **'s", func() {
			So(true, ShouldEqual, matcher.Match(".**", ".testing.testing"))
			So(true, ShouldEqual, matcher.Match(".*.**", ".testing.testing"))
			So(true, ShouldEqual, matcher.Match(".**.*", ".testing.testing"))
			So(true, ShouldEqual, matcher.Match(".bla.**.bla", ".bla.testing.testing.bla"))
			So(true, ShouldEqual, matcher.Match(".bla.**.bla", ".bla.testing.testing.bla.bla"))
			So(true, ShouldEqual, matcher.Match(".**.test", ".bla.bla.test"))
			So(true, ShouldEqual, matcher.Match(".bla.**.**.bla", ".bla.bla.bla.bla.bla.bla"))
			So(true, ShouldEqual, matcher.Match(".bla*bla.test", ".blaXXXbla.test"))
			So(true, ShouldEqual, matcher.Match(".*bla.test", ".XXXbla.test"))
			So(false, ShouldEqual, matcher.Match(".bla*bla.test", ".blaXXXbl.test"))
			So(false, ShouldEqual, matcher.Match(".*bla.test", "XXXblab.test"))
			So(false, ShouldEqual, matcher.Match(".*bla.test", "XXXbl.test"))
		})
	})

}

// SPR-8687
// TestTrimTokensOff
func TestTrimTokensOff(t *testing.T) {
	Convey("Test Trim Tokens Off", t, func() {
		matcher.SetTrimTokens(false)
		So(true, ShouldEqual, matcher.Match("/group/{groupName}/members", "/group/sales/members"))
		So(true, ShouldEqual, matcher.Match("/group/{groupName}/members", "/group/  sales/members"))
		So(false, ShouldEqual, matcher.Match("/group/{groupName}/members", "/Group/  Sales/Members"))
	})
}

// TestDefaultCacheSetting
func TestDefaultCacheSetting(t *testing.T) {
	Convey("Test Cache Patterns Set To False", t, func() {
		t1 := time.Now().Nanosecond()
		for i := 0; i < 65536; i++ {
			matcher.Match(fmt.Sprint("test", i), fmt.Sprint("test", i))
		}
		// 缓存已超过阈值，因此已关闭
		So(true, ShouldEqual, matcher.PatternCacheSize() <= 0)
		t.Log(time.Now().Nanosecond() - t1)
	})
}

// TestCachePatternsSetToFalse
func TestCachePatternsSetToFalse(t *testing.T) {
	Convey("Test Cache Patterns Set To False", t, func() {
		matcherFalse := New()
		matcherFalse.SetCachePatterns(false)
		for i := 0; i < 10; i++ {
			matcherFalse.Match(fmt.Sprint("test", i), fmt.Sprint("test", i))
		}
		t.Log(matcherFalse.PatternCacheSize())
		So(true, ShouldEqual, matcherFalse.PatternCacheSize() <= 0)
	})
}

// SPR-13286
// TestCaseInsensitive
func TestCaseInsensitive(t *testing.T) {
	Convey("Test Case Insensitive", t, func() {
		matcher.SetCaseSensitive(false)
		So(true, ShouldEqual, matcher.Match("/group/{groupName}/members", "/group/sales/members"))
		So(true, ShouldEqual, matcher.Match("/group/{groupName}/members", "/group/sales/members"))
		So(true, ShouldEqual, matcher.Match("/group/{groupName}/members", "/Group/Sales/Members"))
		So(true, ShouldEqual, matcher.Match("/Group/{groupName}/Members", "/group/Sales/members"))
	})
}