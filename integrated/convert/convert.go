package convert

import (
	"bytes"
	"fmt"
	"hash/fnv"
	"reflect"
	"strconv"
	"strings"
	"unicode"
	"unicode/utf8"
	"unsafe"
)

// EmptySpace 字符中有空格
const EmptySpace = " "

// EmptyString 空字符串
const EmptyString = ""

// StrTo 转换字符串以指定类型。
type StrTo string

func (f StrTo) Exist() bool {
	return string(f) != strconv.Itoa(0x1E)
}

func (f StrTo) Uint8() (uint8, error) {
	v, err := strconv.ParseUint(f.String(), 10, 8)
	return uint8(v), err
}

func (f StrTo) Int() (int, error) {
	v, err := strconv.ParseInt(f.String(), 10, 0)
	return int(v), err
}

func (f StrTo) Int64() (int64, error) {
	v, err := strconv.ParseInt(f.String(), 10, 64)
	return v, err
}

func (f StrTo) MustUint8() uint8 {
	v, _ := f.Uint8()
	return v
}

func (f StrTo) MustInt() int {
	v, _ := f.Int()
	return v
}

func (f StrTo) MustInt64() int64 {
	v, _ := f.Int64()
	return v
}
func (f StrTo) Float64() float64 {
	v, _ := strconv.ParseFloat(f.String(), 10)
	return v
}
func (f StrTo) String() string {
	if f.Exist() {
		return string(f)
	}
	return ""
}
func (f StrTo) Hash() string {
	if len(f) > 0 {
		h := fnv.New32a()
		_, _ = h.Write([]byte(f))
		return strconv.FormatUint(uint64(h.Sum32()), 10)
	} else {
		return ""
	}
}

// ToStr 将任何类型转换为字符串。
func ToStr(value interface{}, args ...int) (s string) {
	switch v := value.(type) {
	case bool:
		s = strconv.FormatBool(v)
	case float32:
		s = strconv.FormatFloat(float64(v), 'f', ArgInt(args).Get(0, -1), ArgInt(args).Get(1, 32))
	case float64:
		s = strconv.FormatFloat(v, 'f', ArgInt(args).Get(0, -1), ArgInt(args).Get(1, 64))
	case int:
		s = strconv.FormatInt(int64(v), ArgInt(args).Get(0, 10))
	case int8:
		s = strconv.FormatInt(int64(v), ArgInt(args).Get(0, 10))
	case int16:
		s = strconv.FormatInt(int64(v), ArgInt(args).Get(0, 10))
	case int32:
		s = strconv.FormatInt(int64(v), ArgInt(args).Get(0, 10))
	case int64:
		s = strconv.FormatInt(v, ArgInt(args).Get(0, 10))
	case uint:
		s = strconv.FormatUint(uint64(v), ArgInt(args).Get(0, 10))
	case uint8:
		s = strconv.FormatUint(uint64(v), ArgInt(args).Get(0, 10))
	case uint16:
		s = strconv.FormatUint(uint64(v), ArgInt(args).Get(0, 10))
	case uint32:
		s = strconv.FormatUint(uint64(v), ArgInt(args).Get(0, 10))
	case uint64:
		s = strconv.FormatUint(v, ArgInt(args).Get(0, 10))
	case string:
		s = v
	case []byte:
		s = string(v)
	default:
		s = fmt.Sprintf("%v", v)
	}
	return s
}

type ArgInt []int

func (a ArgInt) Get(i int, args ...int) (r int) {
	if i >= 0 && i < len(a) {
		r = a[i]
	} else if len(args) > 0 {
		r = args[0]
	}
	return
}

type ArgString []string

func (a ArgString) Get(i int, args ...string) (r string) {
	if i >= 0 && i < len(a) {
		r = a[i]
	} else if len(args) > 0 {
		r = args[0]
	}
	return
}

type ArgAny []interface{}

func (a ArgAny) Get(i int, args ...interface{}) (r interface{}) {
	if i >= 0 && i < len(a) {
		r = a[i]
	}
	if len(args) > 0 {
		r = args[0]
	}
	return
}

// ArrayToString 整形数组转换成字符串
func ArrayToString(A []int, denim string) string {
	var buffer bytes.Buffer
	for i := 0; i < len(A); i++ {
		buffer.WriteString(strconv.Itoa(A[i]))
		if i != len(A)-1 {
			buffer.WriteString(denim)
		}
	}

	return buffer.String()
}

// Contains 返回数组中val的索引位置,当为-1时代表不包含
func Contains(array interface{}, val interface{}) (index int) {
	index = -1
	switch reflect.TypeOf(array).Kind() {
	case reflect.Slice:
		{
			s := reflect.ValueOf(array)
			for i := 0; i < s.Len(); i++ {
				if reflect.DeepEqual(val, s.Index(i).Interface()) {
					index = i
					return
				}
			}
		}
	}
	return
}

// ContainsString 返回string类型'val'在数组中的索引位置
func ContainsString(array []string, val string) (index int) {
	index = -1
	for i := 0; i < len(array); i++ {
		if array[i] == val {
			index = i
			return
		}
	}
	return
}

// ContainsInt 返回int64类型'val'在数组中的索引位置
func ContainsInt(array []int64, val int64) (index int) {
	index = -1
	for i := 0; i < len(array); i++ {
		if array[i] == val {
			index = i
			return
		}
	}
	return
}

// ContainsUint 返回uint64类型'val'在数组中的索引位置
func ContainsUint(array []uint64, val uint64) (index int) {
	index = -1
	for i := 0; i < len(array); i++ {
		if array[i] == val {
			index = i
			return
		}
	}
	return
}

// ContainsBool 返回bool类型'val'在数组中的索引位置
func ContainsBool(array []bool, val bool) (index int) {
	index = -1
	for i := 0; i < len(array); i++ {
		if array[i] == val {
			index = i
			return
		}
	}
	return
}

// ContainsFloat 返回float64类型'val'在数组中的索引位置
func ContainsFloat(array []float64, val float64) (index int) {
	index = -1
	for i := 0; i < len(array); i++ {
		if array[i] == val {
			index = i
			return
		}
	}
	return
}

// ContainsComplex 返回complex128类型'val'在数组中的索引位置
func ContainsComplex(array []complex128, val complex128) (index int) {
	index = -1
	for i := 0; i < len(array); i++ {
		if array[i] == val {
			index = i
			return
		}
	}
	return
}

// PowInt 是int类型的Math.Pow函数.
func PowInt(x int, y int) int {
	if y <= 0 {
		return 1
	} else {
		if y%2 == 0 {
			sqrt := PowInt(x, y/2)
			return sqrt * sqrt
		} else {
			return PowInt(x, y-1) * x
		}
	}
}

// TokenizeToStringArray 根据分隔符进行分割处理,形成包路径数组.默认分割符为:",; \t\n"
func TokenizeToStringArray(str, delimiters string, trimTokens, ignoreEmptyTokens bool) []*string {
	if str == EmptyString {
		return nil
	}
	tokens := make([]*string, 0)
	for _, token := range strings.Split(str, delimiters) {
		if trimTokens {
			token = strings.Trim(token, EmptySpace)
		}
		if !ignoreEmptyTokens || token != EmptyString {
			var item = token
			tokens = append(tokens, &item)
		}
	}
	return tokens
}

// TokenizeToStringArrayIgnoreEmptyTokens 根据分隔符进行分割处理并路过空字符
func TokenizeToStringArrayIgnoreEmptyTokens(str, delimiters string) []*string {
	return TokenizeToStringArray(str, delimiters, true, true)
}

func Str2Bytes(s string) []byte {
	x := (*[2]uintptr)(unsafe.Pointer(&s))
	h := [3]uintptr{x[0], x[1], x[1]}
	return *(*[]byte)(unsafe.Pointer(&h))
}
func Bytes2Str(b []byte) string {
	return *(*string)(unsafe.Pointer(&b))
}

func StartsWith(str, prefix string, offset int) bool {
	ta := Str2Bytes(str)
	to := offset
	pa := Str2Bytes(prefix)
	po := 0
	pc := utf8.RuneCountInString(prefix)
	// 注意:偏移量可能接近 -1>>>1.
	if (offset < 0) || (offset > utf8.RuneCountInString(str)-pc) {
		return false
	}
	for {
		if pc--; pc >= 0 {
			if ta[to] != pa[po] {
				to++
				po++
				return false
			}
		} else {
			break
		}
	}
	return true
}

// IsBlank 判断是否存在空格
func IsBlank(source string) bool {
	if strings.EqualFold(EmptyString, source) {
		return true
	}
	for i := len(source); i > 0; {
		r, size := utf8.DecodeLastRuneInString(source[0:i])
		i -= size
		if !unicode.IsSpace(r) {
			return false
		}
	}
	return true
}

// HasText 判断是否有值
func HasText(source string) bool {
	return !IsBlank(source)
}

// AppendStr 将字符串追加到数组中,且没有重复。
func AppendStr(sts []string, str string) []string {
	for _, s := range sts {
		if s == str {
			return sts
		}
	}
	return append(sts, str)
}