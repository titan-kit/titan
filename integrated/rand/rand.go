package rand

import (
	"bytes"
	"fmt"
	"math/rand"
	"time"
	"unsafe"
)

var src = rand.NewSource(time.Now().UnixNano())

const letterBytes = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
const (
	letterIdxBits = 6                    // 6 bits to represent a letter index
	letterIdxMask = 1<<letterIdxBits - 1 // All 1-bits, as many as letterIdxBits
	letterIdxMax  = 63 / letterIdxBits   // # of letter indices fitting in 63 bits
)

func RandomInt(start int, end int) int {
	// 范围检查
	if end < start {
		return 0
	}
	// 随机数生成器，加入时间戳保证每次生成的随机数不一样
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	// 生成随机数
	return r.Intn(end-start) + start
}

func RandomInt64(start int64, end int64) int64 {
	// 范围检查
	if end < start {
		return 0
	}
	// 随机数生成器，加入时间戳保证每次生成的随机数不一样
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	// 生成随机数
	return r.Int63n(end-start) + start
}

// GenerateRandomCode 生成六位随机码
func GenerateRandomCode() string {
	rnd := rand.New(rand.NewSource(time.Now().UnixNano()))
	code := fmt.Sprintf("%06v", rnd.Int31n(1000000))
	return code
}

// GenFixedLengthChineseChars 指定长度随机中文字符(包含复杂字符)
func GenFixedLengthChineseChars(length int) string {
	var buf bytes.Buffer
	for i := 0; i < length; i++ {
		buf.WriteRune(rune(RandomInt(19968, 40869)))
	}
	return buf.String()
}

// GenRandomLengthChineseChars 指定范围随机中文字符
func GenRandomLengthChineseChars(start, end int) string {
	length := RandomInt(start, end)
	return GenFixedLengthChineseChars(length)
}

// RandomStr 随机英文小写字母
func RandomStr(len int) string {
	rand.Seed(time.Now().UnixNano())
	data := make([]byte, len)
	for i := 0; i < len; i++ {
		data[i] = byte(rand.Intn(26) + 97)
	}
	return string(data)
}

// RandomString 生成指定长度的随机字母和数字字符串，包括0-9、a-z、A-Z的所有字符。
func RandomString(n int) string {
	b := make([]byte, n)
	// A src.Int63() generates 63 random bits, enough for letterIdxMax characters!
	for i, cache, remain := n-1, src.Int63(), letterIdxMax; i >= 0; {
		if remain == 0 {
			cache, remain = src.Int63(), letterIdxMax
		}
		if idx := int(cache & letterIdxMask); idx < len(letterBytes) {
			b[i] = letterBytes[idx]
			i--
		}
		cache >>= letterIdxBits
		remain--
	}

	return *(*string)(unsafe.Pointer(&b))
}