package mail

import (
	"bytes"
	"crypto/tls"
	"errors"
	"fmt"
	"io"
	"net"
	"net/smtp"
	"strings"
	"time"
)

// Sender 是包装Send方法的接口.
// 发送电子邮件到给定的地址.
type Sender interface {
	Send(from string, to []string, msg io.WriterTo) error
}

// SendCloser 是将Send和Close方法分组的接口.
type SendCloser interface {
	Sender
	Close() error
}

// loginAuth是一个实现LOGIN身份验证机制的smtp.Auth。
type loginAuth struct {
	username string
	password string
	host     string
}

func (a *loginAuth) Start(server *smtp.ServerInfo) (string, []byte, error) {
	if !server.TLS {
		advertised := false
		for _, mechanism := range server.Auth {
			if mechanism == "LOGIN" {
				advertised = true
				break
			}
		}
		if !advertised {
			return "", nil, errors.New("mail: unencrypted connection")
		}
	}
	if server.Name != a.host {
		return "", nil, errors.New("mail: wrong host name")
	}
	return "LOGIN", nil, nil
}

func (a *loginAuth) Next(fromServer []byte, more bool) ([]byte, error) {
	if !more {
		return nil, nil
	}
	switch {
	case bytes.Equal(fromServer, []byte("Username:")):
		return []byte(a.username), nil
	case bytes.Equal(fromServer, []byte("Password:")):
		return []byte(a.password), nil
	default:
		return nil, fmt.Errorf("mail: unexpected server challenge: %s", fromServer)
	}
}

// 存根进行测试.
var (
	netDialTimeout = net.DialTimeout
	tlsClient      = tls.Client
	smtpNewClient  = func(conn net.Conn, host string) (smtpClient, error) {
		return smtp.NewClient(conn, host)
	}
)

// Dialer 是SMTP服务器的拨号程序
type Dialer struct {
	Host      string      // 主机代表SMTP服务器的主机.
	Port      int         // 端口代表SMTP服务器的端口.
	Username  string      // 用户名是用于验证SMTP服务器的用户名.
	Password  string      // 密码是用于验证SMTP服务器的密码.
	Auth      smtp.Auth   // Auth表示用于对SMTP服务器进行身份验证的身份验证机制。
	SSL       bool        // SSL定义是否使用SSL连接.在大多数情况下,它应该为false,因为身份验证机制应改为使用STARTTLS扩展名.
	TLSConfig *tls.Config // TSLConfig表示用于TLS(使用STARTTLS扩展名时)或SSL连接的TLS配置.
	LocalName string      // LocalName是使用HELO命令发送到SMTP服务器的主机名.默认情况下,发送"localhost".
}

// NewDialer 返回一个新的SMTP拨号程序.给定的参数用于连接到SMTP服务器.
func NewDialer(host string, port int, username, password string) *Dialer {
	return &Dialer{Host: host, Port: port, Username: username, Password: password, SSL: port == 465}
}

// Dial 拨号并验证到SMTP服务器.使用完后应关闭返回的SendCloser.
func (d *Dialer) Dial() (SendCloser, error) {
	conn, err := netDialTimeout("tcp", addr(d.Host, d.Port), 10*time.Second)
	if err != nil {
		return nil, err
	}

	if d.SSL {
		conn = tlsClient(conn, d.tlsConfig())
	}

	c, err := smtpNewClient(conn, d.Host)
	if err != nil {
		return nil, err
	}

	if d.LocalName != "" {
		if err := c.Hello(d.LocalName); err != nil {
			return nil, err
		}
	}

	if !d.SSL {
		if ok, _ := c.Extension("STARTTLS"); ok {
			if err := c.StartTLS(d.tlsConfig()); err != nil {
				_ = c.Close()
				return nil, err
			}
		}
	}

	if d.Auth == nil && d.Username != "" {
		if ok, auths := c.Extension("AUTH"); ok {
			if strings.Contains(auths, "CRAM-MD5") {
				d.Auth = smtp.CRAMMD5Auth(d.Username, d.Password)
			} else if strings.Contains(auths, "LOGIN") &&
				!strings.Contains(auths, "PLAIN") {
				d.Auth = &loginAuth{
					username: d.Username,
					password: d.Password,
					host:     d.Host,
				}
			} else {
				d.Auth = smtp.PlainAuth("", d.Username, d.Password, d.Host)
			}
		}
	}

	if d.Auth != nil {
		if err = c.Auth(d.Auth); err != nil {
			_ = c.Close()
			return nil, err
		}
	}

	return &smtpSender{c, d}, nil
}

func (d *Dialer) tlsConfig() *tls.Config {
	if d.TLSConfig == nil {
		return &tls.Config{ServerName: d.Host}
	}
	return d.TLSConfig
}

func addr(host string, port int) string {
	return fmt.Sprintf("%s:%d", host, port)
}

// DialAndSend 打开到SMTP服务器的连接，发送给定的电子邮件并关闭连接.
func (d *Dialer) DialAndSend(m ...*Message) error {
	s, err := d.Dial()
	if err != nil {
		return err
	}
	defer s.Close()

	return Send(s, m...)
}

type smtpClient interface {
	Hello(string) error
	Extension(string) (bool, string)
	StartTLS(*tls.Config) error
	Auth(smtp.Auth) error
	Mail(string) error
	Rcpt(string) error
	Data() (io.WriteCloser, error)
	Quit() error
	Close() error
}
type smtpSender struct {
	smtpClient
	d *Dialer
}

func (c *smtpSender) Send(from string, to []string, msg io.WriterTo) error {
	if err := c.Mail(from); err != nil {
		if err == io.EOF {
			// 这可能是由于超时导致的，请重新连接并重试.
			if sc, err := c.d.Dial(); err == nil {
				if s, ok := sc.(*smtpSender); ok {
					*c = *s
					return c.Send(from, to, msg)
				}
			}
		}
		return err
	}

	for _, addr := range to {
		if err := c.Rcpt(addr); err != nil {
			return err
		}
	}

	w, err := c.Data()
	if err != nil {
		return err
	}

	if _, err = msg.WriteTo(w); err != nil {
		_ = w.Close()
		return err
	}

	return w.Close()
}

func (c *smtpSender) Close() error {
	return c.Quit()
}