package mail

import (
	"bytes"
	"io"
	"mime"
	"net/mail"
	"os"
	"path/filepath"
	"strings"
	"time"
)

var now = time.Now

type mimeEncoder struct {
	mime.WordEncoder
}

var (
	bEncoding     = mimeEncoder{mime.BEncoding}
	qEncoding     = mimeEncoder{mime.QEncoding}
	lastIndexByte = strings.LastIndexByte
)

type Encoding string // 表示MIME编码方案,例如quoted-printable或base64.

const (
	QuotedPrintable Encoding = "quoted-printable" // 表示RFC 2045中定义的带引号可打印的编码.
	Base64          Encoding = "base64"           // Base64 表示RFC 2045中定义的base64编码.
	Unencoded       Encoding = "8bit"             // 可用于避免对电子邮件的正文进行编码.标头仍将使用带引号的可打印编码进行编码.
)

type file struct {
	Name     string
	Header   map[string][]string
	CopyFunc func(w io.Writer) error
}

func (f *file) setHeader(field, value string) {
	f.Header[field] = []string{value}
}

// FileSetting 可用作Message.Attach或Message.Embed中的参数.
type FileSetting func(*file)

// SetHeader 是一个文件设置,用于设置包含文件内容的消息部分的MIME标头.
// 如果在发送电子邮件时未设置强制性标题，则会自动添加.
func SetHeader(h map[string][]string) FileSetting {
	return func(f *file) {
		for k, v := range h {
			f.Header[k] = v
		}
	}
}

// Rename 是一个文件设置,用于设置附件名称(如果名称与磁盘上的文件名不同).
func Rename(name string) FileSetting {
	return func(f *file) {
		f.Name = name
	}
}

// SetCopyFunc 是一个文件设置,用于替换发送消息时运行的功能.它应该将文件的内容复制到io.Writer.
// 默认的复制功能使用给定的文件名打开文件,然后将其内容复制到io.Writer.
func SetCopyFunc(f func(io.Writer) error) FileSetting {
	return func(fi *file) {
		fi.CopyFunc = f
	}
}

type part struct {
	contentType string
	copier      func(io.Writer) error
	encoding    Encoding
}

// PartSetting 用作Message.SetBody，Message.AddAlternative或Message.AddAlternativeWriter中的参数，以配置添加到消息中的零件.
type PartSetting func(*part)

// SetPartEncoding 添加到消息中的部分的编码。默认情况下，部件使用与消息相同的编码.
func SetPartEncoding(e Encoding) PartSetting {
	return func(p *part) { p.encoding = e }
}

// Message1 代表电子邮件
// 地址可以采用RFC 822允许的任何形式
type Message1 struct {
	Sender      string   // 必须设置发件人,并且必须是应用程序管理员或当前登录的用户.
	ReplyTo     string   //可能是空的
	To, Cc, Bcc []string // 这些片中的至少一个片必须具有非零长度.
	Subject     string
	charset     string
	encoding    Encoding
	hEncoder    mimeEncoder
	Body        string // Body或HTMLBody至少必须为非空
	HTMLBody    string
	Attachments []Attachment
	Headers     mail.Header // 额外的邮件标题.
}

// Attachment 代表电子邮件附件.
type Attachment struct {
	Name      string //名称必须设置为有效的文件名.
	Data      []byte
	ContentId string
}

// Message 代表电子邮件.
type Message struct {
	header      mail.Header
	parts       []*part
	attachments []*file
	embedded    []*file
	charset     string
	encoding    Encoding
	hEncoder    mimeEncoder
	buf         bytes.Buffer
}

// NewMessage 创建一条新消息.默认情况下,它使用UTF-8和带引号的可打印编码.
func NewMessage(settings ...MessageSetting) *Message {
	m := &Message{header: make(mail.Header), charset: "UTF-8", encoding: QuotedPrintable}
	m.applySettings(settings)
	if m.encoding == Base64 {
		m.hEncoder = bEncoding
	} else {
		m.hEncoder = qEncoding
	}
	return m
}

// Reset 重置消息,以便可以重复使用.该消息保留其先前的设置,因此与调用NewMessage之后的状态相同.
func (m *Message) Reset() {
	for k := range m.header {
		delete(m.header, k)
	}
	m.parts = nil
	m.attachments = nil
	m.embedded = nil
}

// GetHeader 获取标题字段.
func (m *Message) GetHeader(field string) []string {
	return m.header[field]
}

// SetHeader 为给定的标题字段设置一个值.
func (m *Message) SetHeader(field string, value ...string) {
	m.encodeHeader(value)
	m.header[field] = value
}

// SetHeaders 设置消息头.
func (m *Message) SetHeaders(h map[string][]string) {
	for k, v := range h {
		m.SetHeader(k, v...)
	}
}

// FormatAddress 将地址和名称格式化为有效的RFC 5322地址.
func (m *Message) FormatAddress(address, name string) string {
	if name == "" {
		return address
	}
	enc := m.encodeString(name)
	if enc == name {
		m.buf.WriteByte('"')
		for i := 0; i < len(name); i++ {
			b := name[i]
			if b == '\\' || b == '"' {
				m.buf.WriteByte('\\')
			}
			m.buf.WriteByte(b)
		}
		m.buf.WriteByte('"')
	} else if hasSpecials(name) {
		m.buf.WriteString(bEncoding.Encode(m.charset, name))
	} else {
		m.buf.WriteString(enc)
	}
	m.buf.WriteString(" <")
	m.buf.WriteString(address)
	m.buf.WriteByte('>')

	addr := m.buf.String()
	m.buf.Reset()
	return addr
}

// SetAddressHeader 将地址设置为给定的标题字段.
func (m *Message) SetAddressHeader(field, address, name string) {
	m.header[field] = []string{m.FormatAddress(address, name)}
}

// SetDateHeader 将日期设置为给定的标题字段.
func (m *Message) SetDateHeader(field string, date time.Time) {
	m.header[field] = []string{m.FormatDate(date)}
}

// FormatDate 将日期格式化为有效的RFC 5322日期.
func (m *Message) FormatDate(date time.Time) string {
	return date.Format(time.RFC1123Z)
}

// SetBody 设置消息的正文.它将替换以前由SetBody,AddAlternative或AddAlternativeWriter设置的任何内容.
func (m *Message) SetBody(contentType, body string, settings ...PartSetting) {
	m.parts = []*part{m.newPart(contentType, newCopier(body), settings)}
}

// AddAlternative 在消息中添加一个替代部分,通常用于发送默认为纯文本版本的HTML电子邮件,以实现向后兼容.
// AddAlternative将新部分追加到消息的末尾.因此,应在HTML部分之前添加纯文本部分. 请看http://en.wikipedia.org/wiki/MIME#Alternative
func (m *Message) AddAlternative(contentType, body string, settings ...PartSetting) {
	m.AddAlternativeWriter(contentType, newCopier(body), settings...)
}

// AddAlternativeWriter 在消息中添加替代部分.对text/template或html/template包很有用.
func (m *Message) AddAlternativeWriter(contentType string, f func(io.Writer) error, settings ...PartSetting) {
	m.parts = append(m.parts, m.newPart(contentType, f, settings))
}

// Attach 将文件附加到电子邮件.
func (m *Message) Attach(filename string, settings ...FileSetting) {
	m.attachments = m.appendFile(m.attachments, filename, settings)
}

// Embed 将图像嵌入到电子邮件中.
func (m *Message) Embed(filename string, settings ...FileSetting) {
	m.embedded = m.appendFile(m.embedded, filename, settings)
}

// WriteTo 实现io.WriterTo。它将整个消息转储到w中
func (m *Message) WriteTo(w io.Writer) (int64, error) {
	mw := &messageWriter{w: w}
	if _, ok := m.header["Mime-Version"]; !ok {
		mw.writeString("Mime-Version: 1.0\r\n")
	}
	if _, ok := m.header["Date"]; !ok {
		mw.writeHeader("Date", m.FormatDate(now()))
	}
	mw.writeHeaders(m.header)

	if m.hasMixedPart() {
		mw.openMultipart("mixed")
	}

	if m.hasRelatedPart() {
		mw.openMultipart("related")
	}

	if m.hasAlternativePart() {
		mw.openMultipart("alternative")
	}
	for _, part := range m.parts {
		mw.writePart(part, m.charset)
	}
	if m.hasAlternativePart() {
		mw.closeMultipart()
	}

	mw.addFiles(m.embedded, false)
	if m.hasRelatedPart() {
		mw.closeMultipart()
	}

	mw.addFiles(m.attachments, true)
	if m.hasMixedPart() {
		mw.closeMultipart()
	}
	return mw.n, mw.err
}

func (m *Message) encodeHeader(values []string) {
	for i := range values {
		values[i] = m.encodeString(values[i])
	}
}

func (m *Message) encodeString(value string) string {
	return m.hEncoder.Encode(m.charset, value)
}
func (m *Message) applySettings(settings []MessageSetting) {
	for _, s := range settings {
		s(m)
	}
}

func (m *Message) newPart(contentType string, f func(io.Writer) error, settings []PartSetting) *part {
	p := &part{
		contentType: contentType,
		copier:      f,
		encoding:    m.encoding,
	}
	for _, s := range settings {
		s(p)
	}
	return p
}

func (m *Message) appendFile(list []*file, name string, settings []FileSetting) []*file {
	f := &file{
		Name:   filepath.Base(name),
		Header: make(map[string][]string),
		CopyFunc: func(w io.Writer) error {
			h, err := os.Open(name)
			if err != nil {
				return err
			}
			if _, err := io.Copy(w, h); err != nil {
				_ = h.Close()
				return err
			}
			return h.Close()
		},
	}

	for _, s := range settings {
		s(f)
	}

	if list == nil {
		return []*file{f}
	}

	return append(list, f)
}

func (m *Message) hasMixedPart() bool {
	return (len(m.parts) > 0 && len(m.attachments) > 0) || len(m.attachments) > 1
}

func (m *Message) hasRelatedPart() bool {
	return (len(m.parts) > 0 && len(m.embedded) > 0) || len(m.embedded) > 1
}

func (m *Message) hasAlternativePart() bool {
	return len(m.parts) > 1
}

// MessageSetting 可以用作NewMessage中的参数来配置电子邮件.
type MessageSetting func(m *Message)

// SetCharset 是一个消息设置，用于设置电子邮件的字符集.
func SetCharset(charset string) MessageSetting {
	return func(m *Message) {
		m.charset = charset
	}
}

// SetEncoding 是用于设置电子邮件编码的消息设置.
func SetEncoding(enc Encoding) MessageSetting {
	return func(m *Message) {
		m.encoding = enc
	}
}
func newCopier(s string) func(io.Writer) error {
	return func(w io.Writer) error {
		_, err := io.WriteString(w, s)
		return err
	}
}
func hasSpecials(text string) bool {
	for i := 0; i < len(text); i++ {
		switch c := text[i]; c {
		case '(', ')', '<', '>', '[', ']', ':', ';', '@', '\\', ',', '.', '"':
			return true
		}
	}

	return false
}