package mail

import (
	"context"
)

type DirectMail interface {
	Send(c context.Context, msg *Message) error
}