package jwt

import (
	"encoding/hex"
	"encoding/json"
	"errors"
	"time"

	"github.com/dgrijalva/jwt-go"

	"gitee.com/titan-kit/titan/integrated/crypt"
)

// Provider jwt配置加载,token生成和读取
type Provider interface {
	ReadToken(tokenStr string, bean interface{}) (subject string, err error)
	MaskToken(subject string, bean interface{}) (signed string, err error)
}

func NewJwtProvider(key, claim, issuer string, exp int) Provider {
	if len(key) < 8 {
		panic("jwt的密钥长度必须大于等于8位")
	}
	return &jwtProvider{claim, []byte(key), []byte(key[:8]), exp, issuer}
}

type jwtProvider struct {
	claim    string
	claimKey []byte // claim密钥
	key      []byte // jti加密密钥
	exp      int    // 过期时间
	issuer   string // 签发主体
}

func (a jwtProvider) ReadToken(tokenStr string, bean interface{}) (subject string, err error) {
	// 将token字符串转换为token结构体
	token, err := jwt.Parse(tokenStr, func(token *jwt.Token) (i interface{}, e error) { return a.claimKey, nil })
	if err == nil {
		claims := token.Claims.(jwt.MapClaims)
		// 验证token，如果token被修改过则为false
		if !token.Valid {
			err = errors.New("token is invalid")
			return
		}
		if sub, ok := claims["sub"]; ok {
			subject = sub.(string)
		}
		if jti, ok := claims["jti"]; ok {
			if b, err := hex.DecodeString(jti.(string)); err == nil {
				if b, err = cyrpt.Decrypt(b, a.key); err == nil {
					err = json.Unmarshal(b, bean)
				}
			}
		}
	}
	return
}

func (a jwtProvider) MaskToken(subject string, bean interface{}) (signed string, err error) {
	claims := make(jwt.MapClaims, 0)
	exp := time.Now().Add(time.Second * time.Duration(a.exp)).Unix()
	now := time.Now().Unix()
	claims["sub"] = subject  // JWT 所面向的用户
	claims["iss"] = a.issuer // JWT 签发者("http://localhost:8000/user/sign_up")
	claims["aud"] = a.claim  // 接收 JWT 的一方
	claims["iat"] = now      // JWT 的签发时间
	claims["nbf"] = now      // 定义在什么时间之前，该 JWT 都是不可用的
	claims["exp"] = exp      // JWT 的过期时间，这个过期时间必须要大于签发时间
	var b []byte
	if b, err = json.Marshal(bean); err == nil {
		if b, err = cyrpt.Encrypt(b, a.key); err == nil {
			claims["jti"] = hex.EncodeToString(b) // JWT 的唯一身份标识，主要用来作为一次性 token, 从而回避重放攻击。
			token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
			signed, err = token.SignedString(a.claimKey)
		}
	}
	return
}