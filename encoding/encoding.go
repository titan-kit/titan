package encoding

import (
	"encoding/json"
	"encoding/xml"
	"google.golang.org/protobuf/proto"
	"reflect"
	"strings"
)

func init() {
	RegisterCodec(jsonCodec{})
	RegisterCodec(protoCodec{})
	RegisterCodec(xmlCodec{})
}

const (
	JsonName  = "json"
	ProtoName = "proto"
	XmlName   = "xml"
)

// Codec 定义传输用于编码和解码消息的接口.
// 注意:此接口的实现必须是线程安全的.
type Codec interface {
	// Marshal 返回将v一定格式导出.
	Marshal(v interface{}) ([]byte, error)
	// Unmarshal 编解码器解析data的数据，并将结果存储在v指向的值中.
	Unmarshal(data []byte, v interface{}) error
	// Name 返回编解码器实现的名称。返回的字符串将在传输中用作内容类型的一部分.结果必须是静态的；结果不能在调用之间更改.
	Name() string
}

var registeredCodecs = make(map[string]Codec, 0)

// RegisterCodec 注册提供的编解码器与各传输层一起使用.
func RegisterCodec(codec Codec) {
	if codec == nil {
		panic("cannot register a nil Codec")
	}
	if codec.Name() == "" {
		panic("cannot register Codec with empty string result for Name()")
	}
	contentSubtype := strings.ToLower(codec.Name())
	registeredCodecs[contentSubtype] = codec
}

// GetCodec 通过contentSubtype获取注册的编解码器，如果没有为contentSubtype注册任何编解码器，则返回nil.
//
// contentSubtype内的字母应该为小写.
func GetCodec(contentSubtype string) Codec {
	return registeredCodecs[contentSubtype]
}

// xmlCodec 是一个使用json的编解码器实现.
type jsonCodec struct{}

func (c jsonCodec) Marshal(v interface{}) ([]byte, error) {
	return json.Marshal(v)
}
func (c jsonCodec) Unmarshal(data []byte, v interface{}) error {
	rv := reflect.ValueOf(v)
	for rv.Kind() == reflect.Ptr {
		if rv.IsNil() {
			rv.Set(reflect.New(rv.Type().Elem()))
		}
		rv = rv.Elem()
	}
	return json.Unmarshal(data, v)
}

// Name 返回为json编解码器注册的名称.
func (c jsonCodec) Name() string {
	return JsonName
}

// protoCodec 是一个带有protobuf的编解码器实现。它是传输的默认编解码器.
type protoCodec struct{}

func (protoCodec) Marshal(v interface{}) ([]byte, error) {
	return proto.Marshal(v.(proto.Message))
}

func (protoCodec) Unmarshal(data []byte, v interface{}) error {
	return proto.Unmarshal(data, v.(proto.Message))
}

// Name 返回为proto编解码器注册的名称.
func (protoCodec) Name() string {
	return ProtoName
}

// xmlCodec 是一个使用xml的编解码器实现.
type xmlCodec struct{}

func (xmlCodec) Marshal(v interface{}) ([]byte, error) {
	return xml.Marshal(v)
}

func (xmlCodec) Unmarshal(data []byte, v interface{}) error {
	rv := reflect.ValueOf(v)
	for rv.Kind() == reflect.Ptr {
		if rv.IsNil() {
			rv.Set(reflect.New(rv.Type().Elem()))
		}
		rv = rv.Elem()
	}
	return xml.Unmarshal(data, v)
}

func (xmlCodec) Name() string {
	return XmlName
}