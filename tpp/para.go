package tpp

import (
	"bytes"
	"sort"
	"strings"
)

type RechargeStatus int

const (
	NEW RechargeStatus = iota
	PROCESS
	SUCCESS
	FAILED
	CANCEL
	REFUND
	// 以下状态是对于非人民币线上充值状态,

	PAYED //已支付

	// INIT 只对线下汇款充值才有效，为上传凭证之前的
	INIT
)

func (t RechargeStatus) String() string {
	if t < NEW || t > INIT {
		return "Unknown"
	}
	return [...]string{"Y", "N", "P", "F"}[t]
}
func (t RechargeStatus) Name() string {
	if t < NEW || t > INIT {
		return "未定义"
	}
	return [...]string{"新创建", "处理中", "交易成功", "交易失败", "已取消", "已退款", "人民币到账", "初始化"}[t]
}

// OrderStatus 第三方平台的订单状态。
type OrderStatus int

const (
	Y OrderStatus = iota
	N
	P
	F
)

func (t OrderStatus) String() string {
	if t < Y || t > F {
		return "Unknown"
	}
	return [...]string{"Y", "N", "P", "F"}[t]
}
func (t OrderStatus) Name() string {
	if t < Y || t > F {
		return "未定义"
	}
	return [...]string{"支付成功", "订单不存在", "处理中", "支付失败"}[t]
}

// ParaEncoder 请求参数的编码接口。
type ParaEncoder interface {
	// EncodeValue 对给定的参数值进行编码后返回。
	// @param name 参数名称
	// @param value 参数值
	// @return string, error
	EncodeValue(name, value string) string
}

// ParaSigner 请求参数的签名处理接口。
type ParaSigner interface {
	// Assembly 对指定的参数进行组织后返回该参数签名的表现形式。
	// @param name 参数名称
	// @param value 参数值
	// @param index 参数在所有需要签名的参数中的位置序号，从0开始
	// @param totals 所有需要签名的参数总数
	Assembly(name, value string, index, totals int) string
}

// ReqPara 第三方支付平台的请求参数封装。
type ReqPara struct {
	paras      map[string]*paraWrapper
	currentSeq int
}

func NewReqPara() *ReqPara {
	return &ReqPara{make(map[string]*paraWrapper, 0), 0}
}

// Add 添加一个参数信息，该方法会按照添加调用顺序来顺序组装相应的参数参与签名或提交。
// @param name 参数名称
// @param value 参数值
// @param sign 是否参与签名
// @param query 是否添加到请求参数中
func (rp *ReqPara) Add(name, value string, sign, query bool) *ReqPara {
	rp.currentSeq++
	rp.paras[name] = &paraWrapper{Name: name, Value: value, Sign: sign, Query: query, Seq: rp.currentSeq}
	return rp
}
func (rp *ReqPara) Get(name string) *paraWrapper {
	return rp.paras[name]
}

// AddQuery 添加只增加到请求参数列表中的参数，该参数不参与签名。
func (rp *ReqPara) AddQuery(name, value string) *ReqPara {
	return rp.Add(name, value, false, true)
}

// AddSign 添加只参与签名的参数，该参数不会被添加到请求参数列表中。
func (rp *ReqPara) AddSign(name, value string) *ReqPara {
	return rp.Add(name, value, true, false)
}

// ToUrl 将所有需要提交的参数附加到请求网关地址的后面后返回。
// @param gateway 请求网关的URL地址
// @param encoder 请求参数的编码器，如果不指定则不对参数和值做任何处理直接添加到网关地址后
func (rp *ReqPara) ToUrl(gateway string, encoder ParaEncoder) string {
	buffer := bytes.NewBufferString(gateway)
	buffer.WriteString(gateway)
	if strings.Index(gateway, "?") == -1 {
		buffer.WriteString("?")
	}
	paras := rp.makeParaWrappers()
	for _, v := range paras {
		if v.Query {
			buffer.WriteString("&")
			buffer.WriteString(v.Name)
			buffer.WriteString("=")
			if encoder != nil {
				buffer.WriteString(encoder.EncodeValue(v.Name, v.Value))
			} else {
				buffer.WriteString(v.Name)
			}
		}
	}
	return buffer.String()
}
func (rp *ReqPara) makeParaWrappers() paraWrappers {
	size := len(rp.paras)
	paras := make(paraWrappers, 0, size)
	for _, v := range rp.paras {
		paras = append(paras, v)
	}
	sort.Sort(paras) // 使用sort包进行排序
	return paras
}

// ToSign 将所有需要签名的参数进行组装后返回待签名字符串。
func (rp *ReqPara) ToSign(signer ParaSigner) string {
	var buffer bytes.Buffer
	paras := rp.makeParaWrappers()
	size := len(rp.paras)
	for i, v := range paras {
		buffer.WriteString(signer.Assembly(v.Name, v.Value, i, size))
	}
	return buffer.String()
}

type paraWrapper struct {
	Name  string // 参数名称
	Value string // 参数值
	Sign  bool   // 是否参与签名
	Query bool   // 是否加入请求参数
	Seq   int    // 参数的排序序号，越小越靠前
}

// 将参数指针的切片定义为paraWrappers类型
type paraWrappers []*paraWrapper

// Len 实现sort.Interface接口取元素数量方法
func (pw paraWrappers) Len() int {
	return len(pw)
}

// Less 实现sort.Interface接口比较元素方法
func (pw paraWrappers) Less(i, j int) bool {
	return pw[i].Seq < pw[j].Seq
}

// Swap 实现sort.Interface接口交换元素方法
func (pw paraWrappers) Swap(i, j int) {
	pw[i], pw[j] = pw[j], pw[i]
}

type Recharge struct {
	trader Trader
	status OrderStatus // 交易状态
}

// QueryResult 支付订单在第三方平台的查询结果封装。
type QueryResult struct {
	No        string      // 支付订单流水号
	TppStatus OrderStatus // 第三方平台上订单状态
	TppNo     string      // 第三方平台的订单号
	PayTime   string      // 订单支付时间(仅在状态为Y时才必须有),格式:yyyy-MM-dd HH:mm:ss
	PayMoney  int64       // 订单支付金额(仅在状态为Y时才必须有),单位分
}