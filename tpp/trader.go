package tpp

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
	"sync/atomic"

	"gitee.com/titan-kit/titan/integrated/convert"
)

type Trader int64

var (
	_messages atomic.Value         // 注意: 储存的map[int]string
	_nodes    = map[int]struct{}{} // 登记结点.
)

func Register(nm map[int]string) {
	_messages.Store(nm)
}
func NewTrader(c int) Trader {
	if c <= 0 {
		panic("business node must greater than zero")
	}
	return add(c)
}
func add(n int) Trader {
	if _, ok := _nodes[n]; ok {
		panic(fmt.Sprintf("Trader: %d already exist", n))
	}
	_nodes[n] = struct{}{}
	return Trader(n)
}

func (t Trader) String() string {
	if cm, ok := _messages.Load().(map[int]string); ok {
		if msg, ok := cm[int(t)]; ok {
			return msg
		}
	}
	return "Unknown"
}
func (t Trader) IsValid() error {
	if _, ok := _nodes[int(t)]; ok {
		return nil
	} else {
		return errors.New("invalid leave type")
	}
}

// SyncNotifyUrl 获取支付平台同步通知的接收地址(全http地址)
func (t Trader) SyncNotifyUrl() string {
	return "/notify/" + t.String() + "/sync"
}

// AsyncNotifyUrl 获取支付平台异步通知的接收地址(全http地址)
func (t Trader) AsyncNotifyUrl() string {
	return "/notify/" + t.String() + "/async"
}

// SyncResultUrl 获取支付成功或失败时第三方支付平台通过浏览器同步通知后(基于SyncNotifyUrl()的回调)的本地最终结果显示页面，可提示用户充值成功或失败，页面后追加参数success和no。
// @param success 支付是否成功
// @param no 本地充值流水号
func (t Trader) SyncResultUrl(success bool, no string) string {
	var url string
	if len(url) > 0 {
		if strings.Index(url, "?") == -1 {
			url = url + "?"
		}
		url += "&success=" + convert.ToStr(success) + "&no=" + no
	}
	return url
}

func Str2Node(n string) Trader {
	if n == "" {
		return -1
	}
	if i, err := strconv.Atoi(n); err != nil {
		return -1
	} else {
		return Trader(i)
	}
}