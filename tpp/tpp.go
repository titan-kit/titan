package tpp

import (
	"net/http"
	"sync"
)

var (
	providersMu sync.RWMutex
	providers   = make(map[Trader]Provider, 0)
)

func RegisterTrader(key Trader, provider Provider) {
	providersMu.Lock()
	defer providersMu.Unlock()
	if provider == nil {
		panic("TPP: Register provider is nil")
	}
	if _, dup := providers[key]; dup {
		panic("TPP: Register called twice for provider " + key.String())
	}
	providers[key] = provider
}
func GetProvider(key Trader) Provider {
	return providers[key]
}
func NewThirdPartyPayment(config ConfigService, callback Callback) ThirdPartyPayment {
	return ThirdPartyPayment{config, callback}
}

type ThirdPartyPayment struct {
	config   ConfigService
	callback Callback
}

func (tpp ThirdPartyPayment) Config(trader Trader, uid string) Config {
	return tpp.Config(trader, uid)
}
func (tpp ThirdPartyPayment) QueryConfig(uid string) Config {
	return tpp.QueryConfig(uid)
}

func (tpp ThirdPartyPayment) Query(rvo Recharge, sync bool) QueryResult {
	var result QueryResult
	if prov := GetProvider(rvo.trader); prov != nil {
		if qs := prov.QueryService(); qs != nil {
			var err error
			result, err = qs.Query(rvo)
			// 在同步的情况下通知本地系统订单已充值完成
			if sync && err != nil && result.TppStatus == Y {

			}
		}
	}
	return result
}

type Provider interface {
	Pay(para *ReqPara)
	NotifyService() NotifyService
	QueryService() QueryService
}

// NotifyService 第三方支付平台的通知接口定义。
type NotifyService interface {
	// Trader 获取当前第三方支付平台的信息。
	Trader() Trader
	// Sync 处理支付平台的同步通知，通过浏览器的重定向通知
	Sync(request *http.Request, response *http.Response)
	// Async 处理支付平台的异步通知，支付平台直接通知平台服务器
	Async(request *http.Request, response *http.Response)
}

// QueryService 第三方支付平台的查询业务接口定义。
type QueryService interface {
	// Trader 获取本查询结果支持的第三方平台。
	Trader() Trader
	// Query 根据给定的充值记录查询本平台上的订单信息并返回。
	Query(rv Recharge) (QueryResult, error)
}

// Callback 第三方支付平台的通知回调，业务系统实现。
type Callback interface {
	// Finish 第三方支付完成后的通知回调，业务系统根据回调处理相关的业务，如果业务处理失败则抛出异常。
	// @param no 业务系统的充值流水号
	// @param trader 第三方支付平台
	// @param tradeNo 交易平台中的唯一流水号
	// @param money 充值金额，不指定则使用订单默认的金额
	// @param arrivalTime 到账时间，不指定则使用系统时间
	// @param success 是否充值成功
	Finish(no string, trader Trader, traderNo string, money float64, arrivalTime int64, success bool)
	// FinishHasUID 第三方支付完成后的通知回调，业务系统根据回调处理相关的业务，如果业务处理失败则抛出异常。
	// @param no 业务系统的充值流水号
	// @param trader 第三方支付平台
	// @param tradeNo 交易平台中的唯一流水号
	// @param money 充值金额，不指定则使用订单默认的金额
	// @param arrivalTime 到账时间，不指定则使用系统时间
	// @param traderUID 交易会员号
	// @param success 是否充值成功
	FinishHasUID(no string, trader Trader, traderNo string, money float64, arrivalTime int64, traderUID string, success bool)
	// RechargeByNo 根据充值记录的唯一流水号获取对应的充值记录，不存在则返回Null。
	RechargeByNo(no string) Recharge
}