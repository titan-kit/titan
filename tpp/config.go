package tpp

type Config struct {
	Uid     string            `json:"uid"`     // 该账户的唯一标示(同平台内唯一)
	Weight  int               `json:"weight"`  // 权重值(正数数字，为0表示不使用)
	Account string            `json:"account"` // 支付平台账户（登录账号，非API接口账号）
	Key     string            `json:"key"`     // API接口的标示（如partner、merchantId等）
	Secret  string            `json:"secret"`  // API接口的密钥
	Props   map[string]string `json:"props"`   // 其他属性
}

type ConfigService interface {
	Config(trader Trader, uid string) Config
	QueryConfig(uid string) Config
}