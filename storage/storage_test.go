package storage

import "testing"

func TestMakePublicURL(t *testing.T) {
	domain := "https://12313131.com"
	path := "dsadadada"
	url, err := MakePublicURL(domain, path)
	if err != nil {
		t.Fatalf(err.Error())
	}
	if url != domain+"/"+path {
		t.Fatalf("expect: %s\ngot:%s", domain+"/"+path, url)
	}
	domain += "/"
	url, err = MakePublicURL(domain, path)
	if err != nil {
		t.Fatalf(err.Error())
	}
	if url != domain+path {
		t.Fatalf("expect: %s\ngot:%s", domain+"/"+path, url)
	}

}

func TestFilterParams(t *testing.T) {
	testPath1 := "hfiuew-123421-321321"
	newPath := FilterParams(testPath1)
	if testPath1 != newPath {
		t.Error("未带参数路径过滤错误")
		return
	}
	testPath2 := testPath1 + "?a=1&b=2"
	newPath = FilterParams(testPath2)
	if newPath != testPath1 {
		t.Error("携带参数的路径过滤错误")
		return
	}
}

func TestReplaceDomain(t *testing.T) {
	testURL := "https://www.dahjkshdfkas.com/dfsalkjfldias?dsahildsf=dsada&dsakjhda=1231#213321321"
	newURL, err := ReplaceDomain(testURL, "http://dsakldlsajlda.dsada.com")
	if err != nil {
		t.Fatalf(err.Error())
	}
	if newURL != "http://dsakldlsajlda.dsada.com/dfsalkjfldias?dsahildsf=dsada&dsakjhda=1231#213321321" {
		t.Fatalf("替换链接域名错误\n， expect%s\nget%s\n", "https://dsakldlsajlda.dsada.com/dfsalkjfldias?dsahildsf=dsada&dsakjhda=1231#213321321", newURL)
	}
	newURL, err = ReplaceDomain(testURL, "http://dsakldlsajlda.dsada.com/")
	if err != nil {
		t.Fatalf(err.Error())
	}
	if newURL != "http://dsakldlsajlda.dsada.com/dfsalkjfldias?dsahildsf=dsada&dsakjhda=1231#213321321" {
		t.Fatalf("替换链接域名错误\n， expect%s\nget%s\n", "https://dsakldlsajlda.dsada.com/dfsalkjfldias?dsahildsf=dsada&dsakjhda=1231#213321321", newURL)
	}
}