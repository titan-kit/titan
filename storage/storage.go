package storage

import (
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"sync"
	"time"
)

var (
	ErrFileOrDirectoryNotExists = errors.New("no such file or directory")
	ErrUnExpectedPath           = errors.New("unexpected path")
	ErrUnExpectedProtocol       = errors.New("unexpected protocol")
)

var (
	providersMu sync.RWMutex
	providers   = make(map[Protocol]Provider, 0)
)

func Register(name Protocol, provider Provider) {
	providersMu.Lock()
	defer providersMu.Unlock()
	if provider == nil {
		panic("storage: Register provider is nil")
	}
	if _, dup := providers[name]; dup {
		panic("storage: Register called twice for provider " + name)
	}
	providers[name] = provider
}
func GetProvider(key Protocol) Provider {
	return providers[key]
}

type Protocol string

func (p Protocol) Protocol() string {
	return string(p) + "://"
}

// Provider 文件存储的接口定义，提供统一的文件存储管理服务。
type Provider interface {
	// Protocol 获取当前存储管理器的存储文件协议，最终返回的文件路径会带上该协议和://分隔符。
	Protocol() Protocol
	// SimpleUploadToken 获取客户端简单上传凭证
	SimpleUploadToken() string
	// Save 将指定的文件进行存储并返回存储成功后该文件在该存储器中的文件路径，失败则抛出异常。
	// @return 文件路径,格式为:protocol://path
	Save(reader io.Reader, fileSize int64, filePath string) (string, error)
	// Get 根据指定的文件路径从存储器中获取文件内容，如果不存在或读取异常则抛出异常。
	// @param path 存储器中文件的路径，格式为：protocol://path
	Get(path string) ([]byte, error)
	// Write 从存储器中读取给定路径的文件并将文件流输出到指定的输出流中，如果出错则抛出异常。
	//@param  path 存储器中文件的路径，格式为：protocol://path
	Write(path string, file *[]byte) error
	// Delete 从存储器中删除指定路径的文件，如果删除出错则抛出异常。
	// @param path 存储器中文件的路径，格式为：protocol://path
	Delete(path string) error
	// Url 获取指定路径的文件的访问URL地址，必须是通过HTTP可以访问的URL路径地址。
	//	@param path 存储器中文件的路径，格式为：protocol://path
	//	@param secure 是否使用安全访问链接，如果是则链接有类似过期限制、访问来源限制等安全特性，取决于实现类。
	Url(path string, secure bool, expire ...time.Duration) (string, error)
	// Has 检查存储中是否存在指定文件路径的文件，如果不存在则返回false，否则返回true。
	// @param path 格式为：protocol://path
	Has(path string) (bool, error)
}

// Url 获取指定存储器中某个文件的HTTP访问路径。
// @param path 存储器中的文件路径，需要带存储协议
// @param secure 是否使用安全访问路径，如果是则通过诸如访问过期、访问来源等限制访问，否则不限制
func Url(path string, secure bool, expire ...time.Duration) (string, error) {
	stringArray := strings.Split(path, "://")
	if len(stringArray) != 2 {
		return "", ErrUnExpectedPath
	}
	provider := GetProvider(Protocol(stringArray[0]))
	if provider == nil {
		return "", ErrUnExpectedProtocol
	}
	if secure && len(expire) > 0 {
		return provider.Url(path, secure, expire[0])
	}
	return provider.Url(path, secure)
}

// CheckPath 检查path格式
// path格式如protocol://1.txt
func CheckPath(path string, protocol Protocol) bool {
	stringArray := strings.Split(path, "://")
	if len(stringArray) != 2 {
		return false
	}
	if stringArray[0] != string(protocol) {
		return false
	}
	if len(strings.TrimSpace(stringArray[1])) == 0 {
		return false
	}
	return true
}

// GetFilePathFromTotalPath 从protocol://filePath获取具体中文件路径
func GetFilePathFromTotalPath(path string) string {
	stringArray := strings.Split(path, "://")
	if len(stringArray) == 2 {
		return stringArray[1]
	}
	return ""
}

// DownloadUrlToBytes 读取下载链接对应资源为字节数组
func DownloadUrlToBytes(url string) ([]byte, error) {
	resp, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	byteArray, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	return byteArray, nil
}

func MakePublicURL(domain, path string) (string, error) {
	domain = strings.TrimRight(domain, "/")
	urlStr := fmt.Sprintf("%s/%s", domain, path)
	scUrl, err := url.Parse(urlStr)
	if err != nil {
		return "", err
	}
	return scUrl.String(), err
}

func FilterParams(path string) string {
	arr := strings.Split(path, "?")
	return arr[0]
}

func ReplaceDomain(origin string, replaceDomain string) (string, error) {
	u, err := url.Parse(origin)
	if err != nil {
		return "", err
	}
	d, err := url.Parse(replaceDomain)
	if err != nil {
		return "", err
	}
	u.Scheme = d.Scheme
	u.Host = d.Host
	return u.String(), nil
}