package titan

import (
	"context"
	"fmt"
	"net/url"
	"os"
	"os/signal"
	"sync"
	"syscall"

	"gitee.com/titan-kit/titan/errors"
	"gitee.com/titan-kit/titan/log"
	"gitee.com/titan-kit/titan/registry"
	"gitee.com/titan-kit/titan/starter"

	"github.com/google/uuid"
	"golang.org/x/sync/errgroup"
)

func init() {
	fmt.Println("Loading Titan ver:1.0.0")
}

// appInfo 是应用程序上下文值.
type appInfo struct {
	ID      string
	Name    string
	Version string
}

type appKey struct{}

// NewContext 返回一个带有值的新上下文。
func NewContext(ctx context.Context, s appInfo) context.Context {
	return context.WithValue(ctx, appKey{}, s)
}

// FromContext 返回存储在上下文中的传输值（如果有）。
func FromContext(ctx context.Context) (s appInfo, ok bool) {
	s, ok = ctx.Value(appKey{}).(appInfo)
	return
}

// Option 是一个应用程序选项.
type Option func(o *options)

// options 是一个应用程序选项.
type options struct {
	id        string
	name      string
	version   string
	metadata  map[string]string
	endpoints []*url.URL
	ctx       context.Context
	signals   []os.Signal
	logger    log.Logger
	registrar registry.Registrar
	servers   []starter.Server
}

// ID 唯一实例ID
func ID(id string) Option {
	return func(o *options) { o.id = id }
}

// Name 服务名称.
func Name(name string) Option {
	return func(o *options) { o.name = name }
}

// Version 带有服务版本.
func Version(version string) Option {
	return func(o *options) { o.version = version }
}

// Metadata 与服务元数据.
func Metadata(md map[string]string) Option {
	return func(o *options) { o.metadata = md }
}

// Endpoint 与服务节点.
func Endpoint(endpoints ...*url.URL) Option {
	return func(o *options) { o.endpoints = endpoints }
}

// Context 与服务上下文.
func Context(ctx context.Context) Option {
	return func(o *options) { o.ctx = ctx }
}

// Signal 有退出信号.
func Signal(signals ...os.Signal) Option {
	return func(o *options) { o.signals = signals }
}

// Logger 与服务记录器.
func Logger(logger log.Logger) Option {
	return func(o *options) { o.logger = logger }
}

// Registrar 带有服务注册器.
func Registrar(r registry.Registrar) Option {
	return func(o *options) { o.registrar = r }
}

// Server 与启动器服务器.
func Server(srv ...starter.Server) Option {
	return func(o *options) { o.servers = srv }
}

// Titan 是应用组件生命周期管理器
type Titan struct {
	opts     options
	ctx      context.Context
	cancel   func()
	instance *registry.ServiceInstance
	log      *log.Slf4g
}

// NewTitan 创建一个应用程序生命周期管理器.
func NewTitan(opts ...Option) *Titan {
	opt := options{
		id:      uuid.NewString(),
		ctx:     context.Background(),
		logger:  log.DefaultLogger,
		signals: []os.Signal{syscall.SIGTERM, syscall.SIGQUIT, syscall.SIGINT},
	}
	for _, o := range opts {
		o(&opt)
	}
	ctx, cancel := context.WithCancel(opt.ctx)
	return &Titan{
		opts:   opt,
		ctx:    ctx,
		cancel: cancel,
		log:    log.NewSlf4g("titan", opt.logger),
	}
}

// Run 执行在应用程序的生命周期中注册的所有OnStart挂钩.
func (a *Titan) Run() error {
	a.log.InfoW("serviceId", a.opts.id,
		"serviceName", a.opts.name,
		"version", a.opts.version)

	instance, err := a.buildInstance()
	if err != nil {
		return err
	}
	ctx := NewContext(a.ctx, appInfo{
		ID:      a.opts.id,
		Name:    a.opts.name,
		Version: a.opts.version,
	})
	g, ctx := errgroup.WithContext(ctx)
	wg := sync.WaitGroup{}
	for _, srv := range a.opts.servers {
		srv := srv
		g.Go(func() error {
			<-ctx.Done() // 等待停止信号
			return srv.Stop(ctx)
		})
		wg.Add(1)
		g.Go(func() error {
			wg.Done()
			return srv.Start(ctx)
		})
	}
	wg.Wait()
	if a.opts.registrar != nil {
		if err := a.opts.registrar.Register(a.opts.ctx, instance); err != nil {
			return err
		}
		a.instance = instance
	}
	c := make(chan os.Signal, 1)
	signal.Notify(c, a.opts.signals...)
	g.Go(func() error {
		for {
			select {
			case <-ctx.Done():
				return ctx.Err()
			case <-c:
				_ = a.Stop()
			}
		}
	})
	if err := g.Wait(); err != nil && !errors.Is(err, context.Canceled) {
		return err
	}
	return nil
}

// Stop 优雅地停止应用程序.
func (a *Titan) Stop() error {
	if a.opts.registrar != nil {
		if err := a.opts.registrar.Deregister(a.opts.ctx, a.instance); err != nil {
			return err
		}
	}
	if a.cancel != nil {
		a.cancel()
	}
	return nil
}
func (a *Titan) buildInstance() (*registry.ServiceInstance, error) {
	var endpoints []string
	for _, e := range a.opts.endpoints {
		endpoints = append(endpoints, e.String())
	}
	if len(endpoints) == 0 {
		for _, srv := range a.opts.servers {
			e, err := srv.Endpoint()
			if err != nil {
				return nil, err
			}
			endpoints = append(endpoints, e.String())
		}
	}

	return &registry.ServiceInstance{
		ID:        a.opts.id,
		Name:      a.opts.name,
		Version:   a.opts.version,
		Metadata:  a.opts.metadata,
		Endpoints: endpoints,
	}, nil
}