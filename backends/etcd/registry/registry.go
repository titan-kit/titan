package registry

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	etcd "go.etcd.io/etcd/client/v3"

	"gitee.com/titan-kit/titan/registry"
)

var (
	_ registry.Registrar = &Registry{}
	_ registry.Discovery = &Registry{}
)

// Option 是etcd注册中心选项.
type Option func(o *options)

type options struct {
	ctx       context.Context
	namespace string
	ttl       time.Duration
}

// Context 与注册中心上下文.
func Context(ctx context.Context) Option {
	return func(o *options) { o.ctx = ctx }
}

// Namespace 具有注册中心名称空间.
func Namespace(ns string) Option {
	return func(o *options) { o.namespace = ns }
}

// RegisterTTL 与注册中心生存时间.
func RegisterTTL(ttl time.Duration) Option {
	return func(o *options) { o.ttl = ttl }
}

// Registry 是etcd注册中心.
type Registry struct {
	opts   *options
	client *etcd.Client
	kv     etcd.KV
	lease  etcd.Lease
}

// New 创建etcd注册中心
func New(client *etcd.Client, opts ...Option) (r *Registry) {
	options := &options{namespace: "/app", ctx: context.Background(), ttl: time.Second * 5}
	for _, o := range opts {
		o(options)
	}
	return &Registry{opts: options, client: client, kv: etcd.NewKV(client)}
}

// Register 进行注册.
func (r *Registry) Register(ctx context.Context, service *registry.ServiceInstance) error {
	key := fmt.Sprintf("%s/%s/%s", r.opts.namespace, service.Name, service.ID)
	value, err := marshal(service)
	if err != nil {
		return err
	}
	if r.lease != nil {
		r.lease.Close()
	}
	r.lease = etcd.NewLease(r.client)
	grant, err := r.lease.Grant(ctx, int64(r.opts.ttl.Seconds()))
	if err != nil {
		return err
	}
	_, err = r.client.Put(ctx, key, value, etcd.WithLease(grant.ID))
	if err != nil {
		return err
	}
	hb, err := r.client.KeepAlive(ctx, grant.ID)
	if err != nil {
		return err
	}
	go func() {
		for {
			select {
			case _, ok := <-hb:
				if !ok {
					return
				}
			case <-r.opts.ctx.Done():
				return
			}
		}
	}()
	return nil
}

// Deregister 解除注册.
func (r *Registry) Deregister(ctx context.Context, service *registry.ServiceInstance) error {
	defer func() {
		if r.lease != nil {
			r.lease.Close()
		}
	}()
	key := fmt.Sprintf("%s/%s/%s", r.opts.namespace, service.Name, service.ID)
	_, err := r.client.Delete(ctx, key)
	return err
}

// GetService 根据服务名称返回注册中心中的服务实例.
func (r *Registry) GetService(ctx context.Context, name string) ([]*registry.ServiceInstance, error) {
	key := fmt.Sprintf("%s/%s", r.opts.namespace, name)
	resp, err := r.kv.Get(ctx, key, etcd.WithPrefix())
	if err != nil {
		return nil, err
	}
	var items []*registry.ServiceInstance
	for _, kv := range resp.Kvs {
		si, err := unmarshal(kv.Value)
		if err != nil {
			return nil, err
		}
		items = append(items, si)
	}
	return items, nil
}

// Watch 根据服务名称创建观察者.
func (r *Registry) Watch(ctx context.Context, name string) (registry.Watcher, error) {
	key := fmt.Sprintf("%s/%s", r.opts.namespace, name)
	return newWatcher(ctx, key, r.client), nil
}

func marshal(si *registry.ServiceInstance) (string, error) {
	data, err := json.Marshal(si)
	if err != nil {
		return "", err
	}
	return string(data), nil
}

func unmarshal(data []byte) (si *registry.ServiceInstance, err error) {
	err = json.Unmarshal(data, &si)
	return
}