package registry

import (
	"context"

	etcd "go.etcd.io/etcd/client/v3"

	"gitee.com/titan-kit/titan/registry"
)

var (
	_ registry.Watcher = &watcher{}
)

type watcher struct {
	key       string
	ctx       context.Context
	cancel    context.CancelFunc
	watchChan etcd.WatchChan
	watcher   etcd.Watcher
	kv        etcd.KV
	ch        etcd.WatchChan
}

func newWatcher(ctx context.Context, key string, client *etcd.Client) *watcher {
	w := &watcher{
		key:     key,
		watcher: etcd.NewWatcher(client),
		kv:      etcd.NewKV(client),
	}
	w.ctx, w.cancel = context.WithCancel(ctx)
	w.watchChan = w.watcher.Watch(w.ctx, key, etcd.WithPrefix(), etcd.WithRev(0))
	_ = w.watcher.RequestProgress(context.Background())
	return w
}

func (w *watcher) Next() ([]*registry.ServiceInstance, error) {
	for {
		select {
		case <-w.ctx.Done():
			return nil, w.ctx.Err()
		case <-w.watchChan:
		}
		resp, err := w.kv.Get(w.ctx, w.key, etcd.WithPrefix())
		if err != nil {
			return nil, err
		}
		var items []*registry.ServiceInstance
		for _, kv := range resp.Kvs {
			si, err := unmarshal(kv.Value)
			if err != nil {
				return nil, err
			}
			items = append(items, si)
		}
		return items, nil
	}
}

func (w *watcher) Stop() error {
	w.cancel()
	return w.watcher.Close()
}