package config

import (
	"path/filepath"
	"strings"
	"time"

	"gitee.com/titan-kit/titan/config"
	"gitee.com/titan-kit/titan/log"

	"github.com/samuel/go-zookeeper/zk"
)

var _ config.StoreClient = &Client{}

// Client 在Zookeeper客户端提供包装器
type Client struct {
	logger *log.Slf4g
	client *zk.Conn
}

func NewZookeeperClient(machines []string, user, password, openUser, openPassword string) (*Client, error) {
	logger := log.NewSlf4g("backends/zookeeper", log.DefaultLogger)
	c, _, err := zk.Connect(machines, time.Second*5)
	if err != nil {
		panic(err)
	}
	up := user + ":" + password
	if len(up) > 2 {
		if err := c.AddAuth("digest", []byte(up)); err != nil {
			logger.ErrorF("AddAuth user returned error:%+v", err)
		}
	}
	op := openUser + ":" + openPassword
	if len(op) > 2 {
		if err := c.AddAuth("digest", []byte(op)); err != nil {
			logger.ErrorF("AddAuth openUser returned error:%+v", err)
		}
	}
	return &Client{logger, c}, nil
}

func (c *Client) GetValues(keys []string) (map[string]string, error) {
	vars := make(map[string]string)
	for _, v := range keys {
		v = strings.Replace(v, "/*", "", -1)
		_, _, err := c.client.Exists(v)
		if err != nil {
			return vars, err
		}
		err = nodeWalk(v, c, vars)
		if err != nil {
			return vars, err
		}
	}
	return vars, nil
}

func (c *Client) WatchPrefix(prefix string, keys []string, waitIndex uint64, stopChan chan bool) (uint64, error) {
	// 返回大于0的值以触发从储存器中检索密钥
	if waitIndex == 0 {
		return 1, nil
	}

	// 先列出孩子
	entries, err := c.GetValues([]string{prefix})
	if err != nil {
		return 0, err
	}

	respChan := make(chan watchResponse)
	cancelRoutine := make(chan bool)
	defer close(cancelRoutine)

	//守护所有子文件夹的更改
	watchMap := make(map[string]string)
	for k := range entries {
		for _, v := range keys {
			if strings.HasPrefix(k, v) {
				for dir := filepath.Dir(k); dir != "/"; dir = filepath.Dir(dir) {
					if _, ok := watchMap[dir]; !ok {
						watchMap[dir] = ""
						c.logger.Debug("Watching:", dir)
						go c.watch(dir, respChan, cancelRoutine)
					}
				}
				break
			}
		}
	}

	//守护前缀中的所有键的更改
	for k := range entries {
		for _, v := range keys {
			if strings.HasPrefix(k, v) {
				c.logger.Debug("Watching:", k)
				go c.watch(k, respChan, cancelRoutine)
				break
			}
		}
	}

	for {
		select {
		case <-stopChan:
			return waitIndex, nil
		case r := <-respChan:
			return r.waitIndex, r.err
		}
	}
}
func (c *Client) Close() error {
	c.client.Close()
	return nil
}
func nodeWalk(prefix string, c *Client, vars map[string]string) error {
	var s string
	l, stat, err := c.client.Children(prefix)
	if err != nil {
		return err
	}

	if stat.NumChildren == 0 {
		b, _, err := c.client.Get(prefix)
		if err != nil {
			return err
		}
		vars[prefix] = string(b)

	} else {
		for _, key := range l {
			if prefix == "/" {
				s = "/" + key
			} else {
				s = prefix + "/" + key
			}
			_, stat, err := c.client.Exists(s)
			if err != nil {
				return err
			}
			if stat.NumChildren == 0 {
				b, _, err := c.client.Get(s)
				if err != nil {
					return err
				}
				vars[s] = string(b)
			} else {
				_ = nodeWalk(s, c, vars)
			}
		}
	}
	return nil
}

type watchResponse struct {
	waitIndex uint64
	err       error
}

func (c *Client) watch(key string, respChan chan watchResponse, cancelRoutine chan bool) {
	_, _, keyEventCh, err := c.client.GetW(key)
	if err != nil {
		respChan <- watchResponse{0, err}
	}
	_, _, childEventCh, err := c.client.ChildrenW(key)
	if err != nil {
		respChan <- watchResponse{0, err}
	}

	for {
		select {
		case e := <-keyEventCh:
			if e.Type == zk.EventNodeDataChanged {
				respChan <- watchResponse{1, e.Err}
			}
		case e := <-childEventCh:
			if e.Type == zk.EventNodeChildrenChanged {
				respChan <- watchResponse{1, e.Err}
			}
		case <-cancelRoutine: // T没有办法阻止GetW/ChildrenW,所以退出吧
			c.logger.Debug("Stop watching:", key)
			return
		}
	}
}