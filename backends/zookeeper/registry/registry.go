package registry

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"gitee.com/titan-kit/titan/registry"

	"github.com/samuel/go-zookeeper/zk"
)

var (
	_ registry.Registrar = &Registry{}
	_ registry.Discovery = &Registry{}
)

// Option 是zookeeper注册中心选项.
type Option func(o *options)

type options struct {
	ctx       context.Context
	namespace string
	ttl       time.Duration
}

// Context 与注册中心上下文.
func Context(ctx context.Context) Option {
	return func(o *options) { o.ctx = ctx }
}

// Namespace 具有注册中心名称空间.
func Namespace(ns string) Option {
	return func(o *options) { o.namespace = ns }
}

// RegisterTTL 与注册中心生存时间.
func RegisterTTL(ttl time.Duration) Option {
	return func(o *options) { o.ttl = ttl }
}

// Registry 是zookeeper注册中心.
type Registry struct {
	opts   *options
	client *zk.Conn
}

// New 创建zookeeper注册中心
func New(client *zk.Conn, opts ...Option) (r *Registry) {
	options := &options{namespace: "/app", ctx: context.Background(), ttl: time.Second * 15}
	for _, o := range opts {
		o(options)
	}
	return &Registry{opts: options, client: client}
}

func (r *Registry) Register(ctx context.Context, service *registry.ServiceInstance) error {
	panic("implement me")
}

// Deregister 解除注册.
func (r *Registry) Deregister(ctx context.Context, service *registry.ServiceInstance) error {
	defer func() {
		if r.client != nil {
			r.client.Close()
		}
	}()
	key := fmt.Sprintf("%s/%s/%s", r.opts.namespace, service.Name, service.ID)
	return r.client.Delete(key, -1)
}

// GetService 根据服务名称返回注册中心中的服务实例.
func (r *Registry) GetService(ctx context.Context, name string) ([]*registry.ServiceInstance, error) {
	key := fmt.Sprintf("%s/%s", r.opts.namespace, name)
	l, _, err := r.client.Children(key)
	if err != nil {
		return nil, err
	}
	var items []*registry.ServiceInstance
	for _, key = range l {
		if value, _, err := r.client.Get(key); err == nil {
			if si, err := unmarshal(value); err == nil {
				items = append(items, si)
			} else {
				return nil, err
			}
		}
	}
	return items, nil
}

// Watch 根据服务名称创建观察者.
func (r *Registry) Watch(ctx context.Context, name string) (registry.Watcher, error) {
	key := fmt.Sprintf("%s/%s", r.opts.namespace, name)
	panic(key)
}
func marshal(si *registry.ServiceInstance) (string, error) {
	data, err := json.Marshal(si)
	if err != nil {
		return "", err
	}
	return string(data), nil
}

func unmarshal(data []byte) (si *registry.ServiceInstance, err error) {
	err = json.Unmarshal(data, &si)
	return
}