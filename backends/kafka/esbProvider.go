package kafka

import (
	"encoding/json"
	"fmt"

	"gitee.com/titan-kit/titan/cache"
	"gitee.com/titan-kit/titan/esb"
	"gitee.com/titan-kit/titan/integrated/convert"
	"gitee.com/titan-kit/titan/log"
)

var _ esb.Provider = &kafkaProvider{}

const cpKey = "titan:kafka-offset:"

func NewProvider(node esb.Node, logger log.Logger, cp cache.Provider, client *kafka) *kafkaProvider {
	return &kafkaProvider{node.String(), cp, client, log.NewSlf4g("backends/kafkaProvider", logger)}
}

type kafkaProvider struct {
	node   string
	cp     cache.Provider
	client *kafka
	log    *log.Slf4g
}

func (kp *kafkaProvider) Listen(name string, listener esb.MessageListener) (closer func(), err error) {
	topic, key := DestroyQueueName(name)
	ref := kp.client.StartReader(topic, key, &defaultReader{kp.node, kp.log, listener, kp.cp, kp.client}, func(err error) {
		panic(fmt.Sprintf("Kafka消费者线程(StartDefaultReader)发生错误:%+v\n", err))
	})
	return func() {
		kp.log.Info("关闭")
		kp.client.CloseByReaderRef(ref)
	}, nil
}

func (kp *kafkaProvider) Cancel(name string) {
	kp.client.CloseByReaderRef(newReaderRef(name))
}

func (kp *kafkaProvider) Send(msg interface{}) error {
	var mpl *esb.MsgPayload
	switch msg.(type) {
	case *esb.NoticeMessage:
		mpl = esb.NoticePayload(msg.(*esb.NoticeMessage))
	case *esb.SimplexMessage:
		mpl = esb.SimplexPayload(msg.(*esb.SimplexMessage))
	case *esb.DuplexMessage:
		mpl = esb.DuplexPayload(msg.(*esb.DuplexMessage))
	default:
		mpl = &esb.MsgPayload{}
	}
	return sendMessage(kp.node, kp.client, kp.log, mpl)
}
func (kp *kafkaProvider) Close() {
	kp.client.Close()
}

type defaultReader struct {
	node     string
	log      *log.Slf4g
	listener esb.MessageListener
	cp       cache.Provider
	client   *kafka
}

func (r defaultReader) GetOffset(topic string) []PartitionOffset {
	var offsetReader = make([]PartitionOffset, 0)
	for k, v := range r.cp.HGetAll(cpKey + topic) {
		offsetReader = append(offsetReader, PartitionOffset{Partition: int32(convert.StrTo(k).MustInt()), Offset: convert.StrTo(v).MustInt64()})
	}
	return offsetReader
}

func (r defaultReader) OnRead(topic string, partition int32, offset int64, key, value []byte) error {
	r.cp.HSet(cpKey+topic, convert.ToStr(partition), convert.ToStr(offset))
	payload := &esb.MsgPayload{}
	var err error
	if err = json.Unmarshal(value, payload); err != nil {
		r.log.ErrorF("[KAFKA-%s] Decode msg failed: %s", r.node, err)
		return err
	}
	r.log.DebugF("[KAFKA-%s]收到消息 %+v", r.node, payload)
	// 检查签名
	if esb.Signature(payload) != payload.Sign {
		r.log.WarningF("[ESBClient-%s]收到无效签名消息 %s", r.node, payload.MsgId)
		return nil
	}
	var retMsg *esb.MsgPayload
	if payload.Phase == esb.SenderReq {
		retMsg, err = esb.HandleNew(payload, r.listener)
	} else {
		retMsg, err = esb.HandleAck(payload, r.listener)
	}
	if err != nil {
		r.log.ErrorF("Handle msg error: %s", err)
		return err
	}
	if retMsg != nil {
		if err = sendMessage(r.node, r.client, r.log, retMsg); err != nil {
			r.log.ErrorF("[KAFKA-%s]发送消息到ESB发生错误:%+v", r.node, err)
			return err
		}
	}
	return nil
}

// DestroyQueueName 使用当前客户端分解一个ESB消息的队列名称，{name}名称满足格式：sys_esb_{systemId}_{node}
// 其中{systemId}为目标系统的四位数数字ID，{node}为目标系统监听的ESB节点标示(参考{@link ESBNode}。
//
// @param name ESB消息的队列名称
// @return "sys_esb","{systemId}_{node}"
func DestroyQueueName(name string) (string, string) {
	return "sys_esb", name[8:]
}

func sendMessage(node string, client *kafka, log *log.Slf4g, payload *esb.MsgPayload) error {
	if name, err := payload.SendQueueName(); err == nil {
		log.DebugF("[KAFKA-%s]发送消息到ESB(%s):%s", node, name, payload)
		if p := client.Sender(); p != nil {
			topic, key := DestroyQueueName(name)
			p.SendPartition(topic, key, payload.String())
		}
		return nil
	} else {
		return err
	}
}