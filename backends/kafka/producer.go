package kafka

import (
	"time"

	"gitee.com/titan-kit/titan/log"

	"github.com/Shopify/sarama"
)

var _ Sender = &producer{}

// newProducer 按提供的地址初始化一个生产者
func newProducer(log *log.Slf4g, backends []string) producer {
	config := sarama.NewConfig()
	config.Producer.Return.Successes = true
	// 等待服务器所有副本都保存成功后的响应
	config.Producer.RequiredAcks = sarama.WaitForAll
	// NewReferenceHashPartitioner与NewHashPartitioner相似，只是它以与引用Java实现相同的方式处理绝对值.
	config.Producer.Partitioner = sarama.NewReferenceHashPartitioner
	config.Producer.RequiredAcks = sarama.NoResponse //不发送任何响应，只等待TCPack
	config.Producer.Compression = sarama.CompressionSnappy
	config.Version = sarama.V2_1_0_0
	prod, err := sarama.NewSyncProducer(backends, config)
	if err != nil {
		log.CriticalF("启动Kafka生产者失败:%+v", err)
	}
	return producer{config: config, producer: prod}
}

// producer kafka数据发送客户端
type producer struct {
	log      *log.Slf4g
	config   *sarama.Config
	producer sarama.SyncProducer
}

// Send 将给定的数据content发送到kafka特定的topic上
func (p producer) Send(topic, content string) {
	msg := &sarama.ProducerMessage{Topic: topic}
	msg.Value = sarama.ByteEncoder(content)
	_, _, err := p.producer.SendMessage(msg)
	if err != nil {
		p.log.ErrorF("数据发送失败:%+v", err)
	}
}

// SendPartition 将给定的数据content发送到kafka特定的topic上，如果指定了key则根据key的hash结果来选择topic的分区，如果没有指定key则随机选择一个分区。
func (p producer) SendPartition(topic, key, content string) {
	msg := &sarama.ProducerMessage{Topic: topic, Key: sarama.StringEncoder(key), Timestamp: time.Now()}
	msg.Value = sarama.ByteEncoder(content)
	_, _, err := p.producer.SendMessage(msg)
	if err != nil {
		p.log.ErrorF("数据发送分区失败:%+v", err)
	}
}

// Close 关闭处理，程序终止时的资源释放。
func (p producer) Close() {
	if err := p.producer.Close(); err != nil {
		p.log.ErrorF("关闭kafka生产者发生错误:%+v", err)
	}
}