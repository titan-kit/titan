package redis

import (
	"fmt"
	"log"
	"time"

	"gitee.com/titan-kit/titan/cache"

	"github.com/go-redis/redis"
)

var _ cache.Provider = &singleRedisProvider{}

// New 返回一个*redis.Client并建立连接
func New(opt *redis.Options) *singleRedisProvider {
	fmt.Println("Loading Cache Engine ver:1.4.0")
	client := redis.NewClient(opt)
	if pin, err := client.Ping().Result(); err != nil {
		log.Panicf("Redis Ping:%+v\n", err)
	} else {
		log.Printf("Redis Pong:%s\n", pin)
	}
	return &singleRedisProvider{client: client}
}

// singleRedisProvider 基于Redis单实例的缓存实现(单机)
type singleRedisProvider struct {
	client *redis.Client
}

func (r singleRedisProvider) Name() string {
	return "redis"
}

// Exists 判断缓存中是否存在指定的key
//
// @param key 缓存唯一键
func (r singleRedisProvider) Exists(key string) bool {
	num, err := r.client.Exists(key).Result()
	return num > 0 && err == nil
}

// String 根据给定的key从分布式缓存中读取数据并返回，如果不存在或已过期则返回Null。
//
//	@param key 缓存唯一键
func (r singleRedisProvider) String(key string) string {
	return r.client.Get(key).Val()
}

// GetByProvider 使用给定的key从缓存中查询数据，如果查询不到则使用给定的数据提供器来查询数据，然后将数据存入缓存中再返回。
// @param key 缓存唯一键
// @param dataProvider 数据提供器
func (r singleRedisProvider) GetByProvider(key string, provider cache.DataProvider) string {
	v, err := r.client.Get(key).Result()
	if err == redis.Nil {
		v = provider.Data(key)
		if v != "null" {
			expiry := provider.Expires()
			if expiry > 0 {
				r.SetExpires(key, v, time.Duration(expiry))
			} else {
				r.Set(key, v)
			}
		}
	}
	return v
}

// Set 使用指定的key将对象存入分布式缓存中，并使用缓存的默认过期设置，注意，存入的对象必须是可序列化的。
//
// @param key   缓存唯一键
// @param value 对应的值
func (r singleRedisProvider) Set(key, value string) {
	r.SetExpires(key, value, -1)
}

// SetExpires 使用指定的key将对象存入分部式缓存中，并指定过期时间，注意，存入的对象必须是可序列化的
//
// @param key     缓存唯一键
// @param value   对应的值
// @param expires 过期时间
func (r singleRedisProvider) SetExpires(key, value string, expires time.Duration) bool {
	err := r.client.Set(key, value, expires).Err()
	return err == nil
}

// Delete 从缓存中删除指定key的缓存数据。
//
// @param key 缓存唯一键
func (r singleRedisProvider) Delete(key string) {
	r.client.Del(key)
}

// BatchDelete 批量删除缓存中的key。
//
// @param keys 缓存唯一键数组
func (r singleRedisProvider) BatchDelete(keys ...string) {
	r.client.Del(keys...)
}

// HSet 将指定key的map数据的某个字段设置为给定的值。
//
// @param key   map数据的键
// @param field map的字段名称
// @param value 要设置的字段值
func (r singleRedisProvider) HSet(key, field, value string) {
	r.client.HSet(key, field, value)
}

// HGet 获取指定key的map数据某个字段的值，如果不存在则返回Null
//
// @param key   map数据的键
// @param field map的字段名称
func (r singleRedisProvider) HGet(key, field string) string {
	return r.client.HGet(key, field).Val()
}

// HGetAll 获取指定key的map对象，如果不存在则返回Null
//
// @param key map数据的键
func (r singleRedisProvider) HGetAll(key string) map[string]string {
	return r.client.HGetAll(key).Val()
}

// HDelete 将指定key的map数据中的某个字段删除。
//
// @param key   map数据的键
// @param field map中的key名称
func (r singleRedisProvider) HDelete(key string, fields ...string) {
	r.client.HDel(key, fields...)
}

// HExists 判断缓存中指定key的map是否存在指定的字段，如果key或字段不存在则返回false。
//
// @param key 要操作的缓存key
// @param field map中的key名称
func (r singleRedisProvider) HExists(key, field string) bool {
	v, _ := r.client.HExists(key, field).Result()
	return v
}

// Val 对指定的key结果集执行指定的脚本并返回最终脚本执行的结果。
//
// @param script 脚本
// @param key    要操作的缓存key
// @param args   脚本的参数列表
func (r singleRedisProvider) Val(script string, keys []string, args ...interface{}) {
	r.client.Eval(script, keys, args...)
}

// Operate 通过直接调用缓存客户端进行缓存操作，该操作适用于高级操作，如果执行失败会返回Null。
//
//	@param operator 高级操作函数
func (r singleRedisProvider) Operate(operator interface{}) error {
	return r.client.Process(operator.(*redis.Cmd))
}

// Close 关闭客户端
func (r singleRedisProvider) Close() {
	_ = r.client.Close()
}