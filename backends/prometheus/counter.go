package prometheus

import (
	"gitee.com/titan-kit/titan/metrics"

	"github.com/prometheus/client_golang/prometheus"
)

var (
	_                            metrics.Counter = (*counter)(nil)
	MetricHttpServerReqCodeTotal                 = NewCounter(prometheus.NewCounterVec(prometheus.CounterOpts{
		Namespace: "http_server",
		Subsystem: "requests",
		Name:      "code_total",
		Help:      "HTTP服务器请求响应码计数."}, []string{"caller", "path", "method", "code"},
	))

	MetricRpcServerReqCodeTotal = NewCounter(prometheus.NewCounterVec(prometheus.CounterOpts{
		Namespace: "rpc_server",
		Subsystem: "requests",
		Name:      "code_total",
		Help:      "rpc服务器请求响应码计数."}, []string{"caller", "method", "code"}))

	MetricRpcClientReqCodeTotal = NewCounter(prometheus.NewCounterVec(prometheus.CounterOpts{
		Namespace: "rpc_client",
		Subsystem: "requests",
		Name:      "code_total",
		Help:      "rpc客户端请求响应码计数."}, []string{"method", "code"}))
)

// NewCounter 新建一个prometheus斯计数器并返回计数器.
func NewCounter(cv *prometheus.CounterVec) metrics.Counter {
	prometheus.MustRegister(cv)
	return &counter{cv: cv}
}

type counter struct {
	cv  *prometheus.CounterVec
	lvs []string
}

func (c *counter) With(lvs ...string) metrics.Counter {
	return &counter{cv: c.cv, lvs: lvs}
}

func (c *counter) Inc() {
	c.cv.WithLabelValues(c.lvs...).Inc()
}

func (c *counter) Add(delta float64) {
	c.cv.WithLabelValues(c.lvs...).Add(delta)
}