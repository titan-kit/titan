package prometheus

import (
	"gitee.com/titan-kit/titan/metrics"

	"github.com/prometheus/client_golang/prometheus"
)

var (
	_                      metrics.Observer = (*histogram)(nil)
	_                      metrics.Observer = (*summary)(nil)
	MetricHttpServerReqDur                  = NewHistogram(prometheus.NewHistogramVec(prometheus.HistogramOpts{
		Namespace: "http_server",
		Subsystem: "requests",
		Name:      "duration",
		Help:      "http服务器请求持续时间(ms).",
		Buckets:   []float64{5, 10, 25, 50, 100, 250, 500, 1000}}, []string{"caller", "path", "method"}))

	MetricRpcServerReqDur = NewHistogram(prometheus.NewHistogramVec(prometheus.HistogramOpts{
		Namespace: "rpc_server",
		Subsystem: "requests",
		Name:      "duration",
		Help:      "rpc服务器请求持续时间(ms).",
		Buckets:   []float64{5, 10, 25, 50, 100, 250, 500, 1000}}, []string{"caller", "method"}))
	MetricRpcClientReqDur = NewHistogram(prometheus.NewHistogramVec(prometheus.HistogramOpts{
		Namespace: "rpc_client",
		Subsystem: "requests",
		Name:      "duration",
		Help:      "rpc客户端请求持续时间(ms).",
		Buckets:   []float64{5, 10, 25, 50, 100, 250, 500, 1000}}, []string{"method"}))
)

// NewHistogram 新的prometheus柱状图和返回柱状图.
func NewHistogram(hv *prometheus.HistogramVec) metrics.Observer {
	prometheus.MustRegister(hv)
	return &histogram{hv: hv}
}

type histogram struct {
	hv  *prometheus.HistogramVec
	lvs []string
}

func (h *histogram) With(lvs ...string) metrics.Observer {
	return &histogram{hv: h.hv, lvs: lvs}
}

func (h *histogram) Observe(value float64) {
	h.hv.WithLabelValues(h.lvs...).Observe(value)
}

// NewSummary 新的prometheus摘要和返回摘要.
func NewSummary(sv *prometheus.SummaryVec) metrics.Observer {
	prometheus.MustRegister(sv)
	return &summary{sv: sv}
}

type summary struct {
	sv  *prometheus.SummaryVec
	lvs []string
}

func (s *summary) With(lvs ...string) metrics.Observer {
	return &summary{sv: s.sv, lvs: lvs}
}

func (s *summary) Observe(value float64) {
	s.sv.WithLabelValues(s.lvs...).Observe(value)
}