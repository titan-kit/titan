package prometheus

import (
	"gitee.com/titan-kit/titan/metrics"

	"github.com/prometheus/client_golang/prometheus"
)

var _ metrics.Gauge = (*gauge)(nil)

// NewGauge 新的prometheus指标并返回仪表.
func NewGauge(gv *prometheus.GaugeVec) metrics.Gauge {
	prometheus.MustRegister(gv)
	return &gauge{gv: gv}
}

type gauge struct {
	gv  *prometheus.GaugeVec
	lvs []string
}

func (g *gauge) With(lvs ...string) metrics.Gauge {
	return &gauge{gv: g.gv, lvs: lvs}
}

func (g *gauge) Set(value float64) {
	g.gv.WithLabelValues(g.lvs...).Set(value)
}

func (g *gauge) Add(delta float64) {
	g.gv.WithLabelValues(g.lvs...).Add(delta)
}

func (g *gauge) Sub(delta float64) {
	g.gv.WithLabelValues(g.lvs...).Sub(delta)
}