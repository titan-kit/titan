package esb

import (
	"fmt"
	"regexp"

	"gitee.com/titan-kit/titan/log"

	"go.uber.org/atomic"
)

// Option 是ESB选项.
type Option func(*options)

type options struct {
	systemId string
	logger   log.Logger
	node     Node
}

func WithSystemId(systemId string) Option {
	return func(o *options) {
		o.systemId = systemId
	}
}
func WithLogger(logger log.Logger) Option {
	return func(o *options) {
		o.logger = logger
	}
}
func WithNode(node Node) Option {
	return func(o *options) {
		o.node = node
	}
}

// New 给定ESB节点的唯一标示和连接到ESB的配置来构造一个ESB客户端，如果初始化失败则抛出异常。该初始化方法会 同步初始化对应节点的所有消息处理器并启动监听。
func New(provider Provider, opts ...Option) *Client {
	options := options{systemId: "9999", logger: log.DefaultLogger, node: BIZ}
	for _, o := range opts {
		o(&options)
	}
	logger := log.NewSlf4g("esb", options.logger)
	logger.InfoF("[ESBClient-%s]客户端初始化完成:config=%v\n", options.node.String(), options)
	queueNamePattern, _ := regexp.Compile("(sys_esb_\\d{4})_(.+)")
	processorMap := make(map[string]Processor, 0)
	return &Client{
		log:              logger,
		opt:              options,
		queueNamePattern: queueNamePattern,
		provider:         provider,
		processorMap:     processorMap,
	}
}

// Client 提供给业务系统使用和ESB进行交互的接口，允许业务系统发送消息到ESB和处理从ESB中收到的消息。每个ESB客户端都需要指定一个唯一的标示以及初始化ESB客户端所需要的配置参数.
// 需要特别注意的是，每个系统实例化一个`Client`后，该实例会唯一的只监听使用该系统ID标示的一个队列，而这个队列的名称格式为：<b>sys_esb_{systemId}_{node}</b>，其中{systemId}即为当前系统的唯一四位数数字标示，而{node}则标明该客户端连接到的ESB节点。
//
// 温馨提示：获取目标系统的队列名称可使用方法`BuildQueueName`来获取
type Client struct {
	provider         Provider
	opt              options
	log              *log.Slf4g
	queueNamePattern *regexp.Regexp
	processorMap     map[string]Processor
	started          atomic.Bool
}

// AddProcessor 为当前客户端添加一个或多个`processors`消息处理器，需要确保该方法在`start`方法之前调用，否则系统会抛出异常。
func (c *Client) AddProcessor(processors ...Processor) {
	if c.started.Swap(true) {
		c.log.WarningF("[ESBClient-%s]该客户端已启动，无法添加消息处理器\n", c.opt.node.String())
		return
	}
	if len(processors) > 0 {
		for _, p := range processors {
			c.processorMap[p.GetType()] = p
		}
	}
}

// BuildQueueName 使用当前客户端构建一个ESB消息的目标队列名称，目标队列名称满足格式：sys_esb_{systemId}_{node}，其中`systemId`为目标系统的四位数数字ID，`node`为目标系统监听的ESB节点标示}。
func (c *Client) BuildQueueName(systemId string) string {
	return fmt.Sprintf("sys_esb_%s_%s", systemId, c.opt.node.String())
}

// Start 在当前节点上启动进行本地系统的队列监听，该方法请务必在`AddProcessor(...Processor)`方法之后调用，否则可能会导致部分消息由于没有对应的消息处理器而丢失。
// 特别注意：如果收到的消息类型没有对应的消息处理器，系统只会简单的丢弃并打印告警信息;同时注意，该方法只能被调用一次，如果多次调用则后续的调用会抛出异常。
func (c *Client) Start() (closer func(), err error) {
	// 只能被启动一次
	if c.started.Swap(true) {
		return nil, fmt.Errorf("[ESBClient-%s]该客户端已启动，无法多次启动", c.opt.node.String())
	}
	// 监听当前系统在ESB节点上的队列
	queueName := c.BuildQueueName(c.opt.systemId)
	c.log.InfoF("[ESBClient-%s]启动监听ESB单分区消息队列:queue=%s", c.opt.node.String(), queueName)
	return c.provider.Listen(queueName, NewMessageListener(c.log, c.opt.node, func(genre string) Processor {
		processor := c.processorMap[genre]
		if processor == nil {
			c.log.ErrorF("[ESBClient-%s]未定义消息类型的处理器:%s", c.opt.node.String(), genre)
		}
		return processor
	}))
}

// Send 发送新消息到ESB中，这里是所有新消息的发送入口，如果发送失败则会抛出异常。请注意，消息的目标队列名称请使用方法`buildQueueName`来构建并设置，不满足格式的目标队列名称会导致消息发送失败。
func (c *Client) Send(msg interface{}) error {
	//message的格式检查
	nameList := make([]string, 0)
	switch msg.(type) {
	case *NoticeMessage:
		msg := msg.(*NoticeMessage)
		nameList = append(nameList, msg.Destination)
	case *SimplexMessage:
		msg := msg.(*SimplexMessage)
		nameList = append(nameList, msg.Source)
		nameList = append(nameList, msg.Destination)
	case *DuplexMessage:
		msg := msg.(*DuplexMessage)
		nameList = append(nameList, msg.Source)
		nameList = append(nameList, msg.DestinationNew)
		nameList = append(nameList, msg.DestinationAck)
	}
	// 检查名称规范
	for _, name := range nameList {
		m := c.queueNamePattern.FindStringSubmatch(name)
		if len(m) == 0 {
			return fmt.Errorf("ESB消息队列名称不符合规范:%s", name)
		} else {
			nodeName := m[2]
			if Str2Node(nodeName).IsValid() != nil {
				c.log.ErrorF("ESB消息队列节点错误:%s", nodeName)
			}
		}
	}
	// 发送消息
	return c.provider.Send(msg)
}

// Close 关闭所有的资源，该方法不会抛出任何异常。
func (c *Client) Close() {
	c.provider.Close()
}

func NewMessageListener(log *log.Slf4g, node Node, processor func(genre string) Processor) *defaultMessageListener {
	return &defaultMessageListener{log: log, processor: processor, node: node}
}

type defaultMessageListener struct {
	log       *log.Slf4g
	node      Node
	processor func(genre string) Processor
}

func (l *defaultMessageListener) OnReceived(msg interface{}) (*MsgBody, error) {
	l.log.DebugF("[ESBClient-%s]收到新消息:%v", l.node.String(), msg)
	processor := l.processor(GetGenre(msg))
	if processor != nil {
		return processor.OnReceived(msg)
	} else {
		return nil, fmt.Errorf("此类型ESB消息的处理器接口定义:无")
	}
}

func (l *defaultMessageListener) OnRecipientAckReceived(genre, msgId string, rsp *MsgBody) (*MsgBody, error) {
	l.log.DebugF("[ESBClient-%s]收到接收方应答消息：type=%s,msgId=%s,rsp=%v", l.node.String(), genre, msgId, rsp)
	processor := l.processor(genre)
	if processor != nil {
		return processor.OnRecipientAckReceived(msgId, rsp)
	} else {
		return nil, fmt.Errorf("此类型ESB消息的处理器接口定义:无")
	}
}
func (l *defaultMessageListener) OnSenderAckReceived(genre, msgId string, rsp *MsgBody) error {
	l.log.DebugF("[ESBClient-%s]收到发送方应答消息:type=%s,msgId=%s,rsp=%v", l.node.String(), genre, msgId, rsp)
	processor := l.processor(genre)
	if processor != nil {
		return processor.OnSenderAckReceived(msgId, rsp)
	} else {
		return nil
	}
}