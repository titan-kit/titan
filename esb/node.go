package esb

import (
	"errors"
	"fmt"
	"strconv"
	"sync/atomic"
)

var (
	_messages atomic.Value         // 注意: 储存的map[int]string
	_nodes    = map[int]struct{}{} // 登记结点.
	BIZ       = add(0)
)

func Register(nm map[int]string) {
	_messages.Store(nm)
}

type Node int64

func NewNode(c int) Node {
	if c <= 0 {
		panic("business node must greater than zero")
	}
	return add(c)
}
func add(n int) Node {
	if _, ok := _nodes[n]; ok {
		panic(fmt.Sprintf("Node: %d already exist", n))
	}
	_nodes[n] = struct{}{}
	return Node(n)
}

func (n Node) String() string {
	if cm, ok := _messages.Load().(map[int]string); ok {
		if msg, ok := cm[int(n)]; ok {
			return msg
		}
	}
	return "Unknown"
}
func (n Node) IsValid() error {
	if _, ok := _nodes[int(n)]; ok {
		return nil
	} else {
		return errors.New("invalid leave type")
	}
}
func Str2Node(n string) Node {
	if n == "" {
		return -1
	}
	if i, err := strconv.Atoi(n); err != nil {
		return -1
	} else {
		return Node(i)
	}
}