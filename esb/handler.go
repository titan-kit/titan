package esb

import (
	"fmt"
)

// HandleNew 处理收到的新消息（包括通知消息和事务消息）
func HandleNew(msg *MsgPayload, listener MessageListener) (*MsgPayload, error) {
	if msg.Category == NOTICE {
		return noticeNew(msg, listener)
	} else if msg.Category == SIMPLEX {
		return simplexNew(msg, listener)
	} else if msg.Category == DUPLEX {
		return duplexNew(msg, listener)
	} else {
		return nil, fmt.Errorf("无效的消息类型:%s", msg.Category)
	}
}

// HandleAck 处理收到的单向/双向事务消息的应答消息。
func HandleAck(msg *MsgPayload, listener MessageListener) (*MsgPayload, error) {
	if msg.Category == SIMPLEX {
		phase := msg.Phase
		if phase == ReceiverAck {
			return simplexRecipientACK(msg, listener)
		} else {
			return nil, fmt.Errorf("无效的消息阶段：%s", msg.Phase)
		}
	} else if msg.Category == DUPLEX {
		phase := msg.Phase
		if phase == ReceiverAck {
			return duplexRecipientACK(msg, listener)
		} else if phase == SenderAck {
			return duplexSenderACK(msg, listener)
		} else {

			return nil, fmt.Errorf("无效的消息阶段：%s", msg.Phase)
		}
	} else {
		return nil, fmt.Errorf("无效的消息类型:%s", msg.Category)
	}
}
func noticeNew(msg *MsgPayload, listener MessageListener) (*MsgPayload, error) {
	phase := msg.Phase
	if phase != SenderReq {
		return nil, fmt.Errorf("无效的消息阶段：%s", msg.Phase)
	}
	nm, err := msg.ConvertToNotice()
	if err != nil {
		return nil, err
	}
	_, err = listener.OnReceived(nm)
	return nil, err
}
func simplexNew(mpl *MsgPayload, listener MessageListener) (*MsgPayload, error) {
	phase := mpl.Phase
	if phase != SenderReq {
		return nil, fmt.Errorf("无效的消息阶段:%s", phase)
	}
	m, err := mpl.ConvertToSimplex()
	if err != nil {
		return nil, err
	}
	// 单向事务新消息送达，接收方处理并应答
	rsp, err := listener.OnReceived(m)
	if rsp == nil {
		return nil, err
	}
	returnMsg := NewPayload(mpl, ReceiverAck)
	returnMsg.SetBody(rsp)
	returnMsg.SetSign(Signature(returnMsg))
	return returnMsg, err
}
func duplexNew(mpl *MsgPayload, listener MessageListener) (*MsgPayload, error) {
	phase := mpl.Phase
	if phase != SenderReq {
		return nil, fmt.Errorf("无效的消息阶段:%s", phase)
	}
	m, err := mpl.ConvertToDuplex()
	if err != nil {
		return nil, err
	}
	// 双向事务新消息送达，接收方处理并应答
	rsp, err := listener.OnReceived(m)
	if rsp == nil {
		return nil, err
	}
	returnMsg := NewPayload(mpl, ReceiverAck)
	returnMsg.SetBody(rsp)
	returnMsg.SetSign(Signature(returnMsg))
	return returnMsg, err
}

func simplexRecipientACK(mpl *MsgPayload, listener MessageListener) (*MsgPayload, error) {
	phase := mpl.Phase
	if phase != ReceiverAck {
		return nil, fmt.Errorf("无效的消息阶段：%s", mpl.Phase)
	}
	// 单向事务应答消息，发送方处理
	_, err := listener.OnRecipientAckReceived(mpl.Genre, mpl.MsgId, mpl.Body)
	return nil, err
}
func duplexRecipientACK(mpl *MsgPayload, listener MessageListener) (*MsgPayload, error) {
	phase := mpl.Phase
	if phase != ReceiverAck {
		return nil, fmt.Errorf("无效的消息阶段：%s", mpl.Phase)
	}
	// 双向事务的接收方应答消息送达，发送方处理并进行应答
	rsp, err := listener.OnRecipientAckReceived(mpl.Genre, mpl.MsgId, mpl.Body)
	if rsp == nil {
		return nil, err
	}
	returnMsg := NewPayload(mpl, SenderAck)
	returnMsg.SetBody(rsp)
	returnMsg.SetSign(Signature(returnMsg))
	return returnMsg, err
}
func duplexSenderACK(mpl *MsgPayload, listener MessageListener) (*MsgPayload, error) {
	phase := mpl.Phase
	if phase != SenderAck {
		return nil, fmt.Errorf("无效的消息阶段：%s", mpl.Phase)
	}
	// 双向事务的发送方应答消息送达，接收方处理
	err := listener.OnSenderAckReceived(mpl.Genre, mpl.MsgId, mpl.Body)
	return nil, err
}