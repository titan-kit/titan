package esb

// MessageListener ESB消息的监听器接口定义。
type MessageListener interface {
	// OnReceived 接收消息`msg`并进行处理，如果处理失败则抛出异常。如果需要返回消息（如事务消息需要返回）请返回消息体内容，否则请返回nil。
	OnReceived(msg interface{}) (*MsgBody, error)

	// OnRecipientAckReceived 处理接收方发出的ACK消息，由消息发送方处理。在双向事务消息中消息发送方还应该在处理完成后返回一个应答消息通知接收方处理结果`rsp`。如果返回结果为nil则不发送。
	OnRecipientAckReceived(genre, msgId string, rsp *MsgBody) (*MsgBody, error)

	// OnSenderAckReceived 处理发送方发出的ACK消息，由接收方处理，该方法仅在双向事务消息中有效，通知接收方最终发送方的处理结果`rsp`。
	OnSenderAckReceived(genre, msgId string, rsp *MsgBody) error
}

// Provider 企业服务总线（ESB）的客户端接口，通过ESB实现系统解耦并实现系统之间的交互需求，目前支持的系统交互有如下几类：
// 消息通知：发送方单纯的发出通知给接收方，不对接收方的处理结果进行响应;
// 单向事务：发送方发出请求给接收方，等待接收方反馈处理结果（成功或失败）后进行处理;
// 双向事务：发送方发出请求给接收方，等待接收方处理结果后，再次通知接收方我方的处理结果;
//
// 基于上述交互需求，所选择的ESB中间件要满足如下条件:
// 消息的顺序性：发送方发出的消息顺序和接收方接收到的消息顺序要一致;
// 唯一消费保证：同一个消息队列中的消息只能被一个消费者消费;
// 消息通知保证：中间件要保证消息正确无误的送达给消费者并且被成功消费;
type Provider interface {
	// Listen 监听指定名称`name`的消息队列，当有新消息或事务应答消息送达时会调用监听器`listener`接口并返回关闭动作`closer`，可同时监听多个不同的队列。
	// 特别注意：同一个消息队列只能有一个监听器！
	Listen(name string, listener MessageListener) (closer func(), err error)

	// Cancel 取消对指定队列名称`name`的监听。
	Cancel(name string)

	// Send 发送ESB消息
	Send(message interface{}) error

	// Close 关闭到消息中间件的连接，清除资源。
	Close()
}

// Processor ESB消息的处理器接口定义，业务系统实现该接口后需要手动注册到{Client}中去方可生效。
type Processor interface {
	// GetType 获取要处理的消息类型，对应{Message}中的type字段。
	GetType() string

	// OnReceived 接收新消息并进行处理，如果新消息为事务消息并且需要返回处理结果则请返回处理结果，否则返回Null即可。
	OnReceived(msg interface{}) (*MsgBody, error)

	// OnRecipientAckReceived 单向/双向事务消息中的接收方应答消息的接收处理，对于单向事务消息处理完成后返回Null即可，对于双向事务消息则还需要返回系统的处理结果给到接收方进行后续处理。
	OnRecipientAckReceived(msgId string, rsp *MsgBody) (*MsgBody, error)

	// OnSenderAckReceived 双向事务消息中的发送方的确认应答消息(msgId:事务消息的唯一ID)，由接收方进行处理发送方返回的应答消息`rsp`。
	OnSenderAckReceived(msgId string, rsp *MsgBody) error
}