package env

import (
	"gitee.com/titan-kit/titan/log"
	"os"
	"strings"
)

var replacer = strings.NewReplacer("/", "_")

// Client 为环境变量客户端提供命令解释器
type Client struct {
	logger *log.Slf4g
}

// NewEnvClient 返回一个新客户
func NewEnvClient() (*Client, error) {
	logger := log.NewSlf4g("config/env", log.DefaultLogger)
	return &Client{logger}, nil
}

// GetValues 通过keys查找环境变量中值
func (c *Client) GetValues(keys []string) (map[string]string, error) {
	allEnvVars := os.Environ()
	envMap := make(map[string]string)
	for _, e := range allEnvVars {
		index := strings.Index(e, "=")
		envMap[e[:index]] = e[index+1:]
	}
	vars := make(map[string]string)
	for _, key := range keys {
		k := transform(key)
		for envKey, envValue := range envMap {
			if strings.HasPrefix(envKey, k) {
				vars[key] = envValue
			}
		}
	}
	c.logger.DebugF("Key Map: %#v", vars)
	return vars, nil
}

func (c *Client) WatchPrefix(prefix string, keys []string, waitIndex uint64, stopChan chan bool) (uint64, error) {
	<-stopChan
	return 0, nil
}
func (c *Client) Close() error {
	return nil
}
func transform(key string) string {
	k := strings.TrimPrefix(key, "/")
	return strings.ToUpper(replacer.Replace(k))
}