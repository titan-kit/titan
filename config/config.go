package config

import (
	"errors"
	"fmt"

	"gitee.com/titan-kit/titan/config/env"
	"gitee.com/titan-kit/titan/log"
)

var (
	// ErrNotFound is key not found.
	ErrNotFound = errors.New("key not found")
	// ErrTypeAssert is type assert error.
	ErrTypeAssert = errors.New("type assert error")

	_ Configuration = (*config)(nil)
)

// Nodes 是一个自定义标志Var，代表etcd节点的列表.
type Nodes []string

// String 返回节点var的字符串表示形式.
func (n *Nodes) String() string {
	return fmt.Sprintf("%s", *n)
}

// Set 将节点追加到etcd节点列表中.
func (n *Nodes) Set(node string) error {
	*n = append(*n, node)
	return nil
}

type Config struct {
	AuthToken      string
	AuthType       string
	BasicAuth      bool
	ClientCaKeys   string
	ClientCert     string
	ClientKey      string
	ClientInsecure bool
	BackendNodes   Nodes
	Username       string
	Password       string
	Scheme         string
	Table          string
	Separator      string
	AppID          string
	UserID         string
	RoleID         string
	SecretID       string
	YamlFile       Nodes
	Filter         string
	Path           string
	Role           string
}

// StoreClient 接口由可以从后端存储检索键/值对的对象实现.
type StoreClient interface {
	GetValues(keys []string) (map[string]string, error)
	WatchPrefix(prefix string, keys []string, waitIndex uint64, stopChan chan bool) (uint64, error)
	Close() error
}

// Option 是配置选项.
type Option func(*options)

type options struct {
	stores   []StoreClient
	logger   log.Logger
	stopChan chan bool
	errChan  chan error
}

// WithStore 配置数据后端存储.
func WithStore(s ...StoreClient) Option {
	return func(o *options) {
		o.stores = s
	}
}

// WithLogger 配置记录器.
func WithLogger(l log.Logger) Option {
	return func(o *options) {
		o.logger = l
	}
}
func WithStopChan(stopChan chan bool) Option {
	return func(o *options) {
		o.stopChan = stopChan
	}
}
func WithErrorChan(errChan chan error) Option {
	return func(o *options) {
		o.errChan = errChan
	}
}

//ChangedListener 配置数据发生变化时的监听器接口。
type ChangedListener interface {
	// Changed 指定配置变化后的通知接口，标示变化的路径和变化后的vars(变化后的新数据值)。
	Changed(vars map[string]string)
}

// Configuration 是一个配置接口.
type Configuration interface {
	Value(key string) Value
	Watch(prefix string, key []string, o ChangedListener) error
	Close() error
}

// New 使用选项新建配置.
func New(opts ...Option) Configuration {
	store, _ := env.NewEnvClient()
	options := options{
		stores:   []StoreClient{store},
		logger:   log.DefaultLogger,
		stopChan: make(chan bool),
		errChan:  make(chan error, 10),
	}
	for _, o := range opts {
		o(&options)
	}
	return &config{log: log.NewSlf4g("config", options.logger), opts: options}
}

type config struct {
	log  *log.Slf4g
	opts options
}

func (c config) Value(key string) Value {
	for _, src := range c.opts.stores {
		if vl, err := src.GetValues([]string{key}); err == nil {
			av := &atomicValue{}
			av.Store(vl[key])
			return av
		}
	}
	return &errValue{err: ErrNotFound}
}

func (c config) Watch(prefix string, key []string, listener ChangedListener) error {
	for _, src := range c.opts.stores {
		if _, err := src.GetValues(key); err == nil {
			go WatchProcessor(prefix, key, c.opts.stopChan, c.opts.errChan, src).Process(listener)
		}
	}
	return ErrNotFound
}

func (c config) Close() error {
	for _, src := range c.opts.stores {
		_ = src.Close()
	}
	return nil
}