package errors

// BadRequest 映射到400响应的新BadRequest错误.
func BadRequest(reason, message string) *Error {
	return NewFmt(400, reason, message)
}

// IsBadRequest 确定err是否是表示BadRequest的错误,它支持包装错误.
func IsBadRequest(err error) bool {
	return Code(err) == 400
}

// Unauthorized 映射到401响应的新的Unauthorized的错误.
func Unauthorized(reason, message string) *Error {
	return NewFmt(401, reason, message)
}

// IsUnauthorized 确定err是否是表示Unauthorized的错误,它支持包装错误.
func IsUnauthorized(err error) bool {
	return Code(err) == 401
}

// Forbidden 映射到403响应的新的Forbidden错误.
func Forbidden(reason, message string) *Error {
	return NewFmt(403, reason, message)
}

// IsForbidden 确定err是否是表示Forbidden的错误,它支持包装错误.
func IsForbidden(err error) bool {
	return Code(err) == 403
}

// NotFound 映射到404响应的新NotFound错误.
func NotFound(reason, message string) *Error {
	return NewFmt(404, reason, message)
}

// IsNotFound 确定err是否是表示NotFound的错误,它支持包装错误.
func IsNotFound(err error) bool {
	return Code(err) == 404
}

// Conflict 映射到409响应的新的Conflict错误.
func Conflict(reason, message string) *Error {
	return NewFmt(409, reason, message)
}

// IsConflict 确定err是否是表示Conflict的错误,它支持包装错误.
func IsConflict(err error) bool {
	return Code(err) == 409
}

// InternalServer 映射到500响应的新InternalServer错误.
func InternalServer(reason, message string) *Error {
	return NewFmt(500, reason, message)
}

// IsInternalServer 确定err是否是指示InternalServer的错误,它支持包装错误.
func IsInternalServer(err error) bool {
	return Code(err) == 500
}

// ServiceUnavailable 映射到HTTP 503响应的新ServiceUnavailable错误.
func ServiceUnavailable(reason, message string) *Error {
	return NewFmt(503, reason, message)
}

// IsServiceUnavailable 确定err是否是指示ServiceUnavailable的错误,它支持包装错误.
func IsServiceUnavailable(err error) bool {
	return Code(err) == 503
}