package errors

import (
	"errors"
	"fmt"
	"net/http"
	"testing"
)

func TestError(t *testing.T) {
	var base *Error
	err := NewFmt(http.StatusBadRequest, "reason", "message")
	err2 := NewFmt(http.StatusBadRequest, "reason", "message")
	err3 := err.WithMetadata(map[string]string{
		"foot": "bar",
	})
	wErr := fmt.Errorf("wrap %w", err)

	if errors.Is(err, new(Error)) {
		t.Errorf("不应该相等: %v", err)
	}
	if !errors.Is(wErr, err) {
		t.Errorf("应该相等l: %v", err)
	}
	if !errors.Is(wErr, err2) {
		t.Errorf("应该相等: %v", err)
	}
	if !errors.As(err, &base) {
		t.Errorf("应该匹配: %v", err)
	}
	if !IsBadRequest(err) {
		t.Errorf("应该匹配: %v", err)
	}
	if reason := Reason(err); reason != err3.Reason {
		t.Errorf("得到 %s 想要: %s", reason, err)
	}
	if err3.Metadata["foot"] != "bar" {
		t.Error("意外的元数据")
	}
	gs := err.GRPCStatus()
	se := FromError(gs.Err())
	if se.Reason != se.Reason {
		t.Errorf("得到 %+v 想要 %+v", se, err)
	}
}