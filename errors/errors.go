package errors

import (
	"errors"
	"fmt"

	"gitee.com/titan-kit/titan/integrated/http"
	"google.golang.org/genproto/googleapis/rpc/errdetails"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/proto"
)

// Error 包含来自服务器的错误响应.
// 有关更多详细信息，请参见https://github.com/googleapis/googleapis/blob/master/google/rpc/error_details.proto.
//go:generate protoc -I. --go_out=paths=source_relative:. errors.proto

const (
	// UnknownCode 是错误信息的未知代码.
	UnknownCode = 500
	// UnknownReason 是错误信息的未知原因.
	UnknownReason = ""
	// SupportPackageIsVersion1 此常量不应被任何其他代码引用。
	SupportPackageIsVersion1 = true
)

func (x *Error) Error() string {
	return fmt.Sprintf("error: code = %d reason = %s metadata = %v", x.Code, x.Reason, x.Metadata)
}

// StatusCode 返回一个WEB服务错误代码。
func (x *Error) StatusCode() int {
	return int(x.Code)
}

// Is 将链中的每个错误与目标值匹配.
func (x *Error) Is(err error) bool {
	if target := new(Error); errors.As(err, &target) {
		return target.Reason == x.Reason
	}
	return false
}

// GRPCStatus 返回由se表示的Status.
func (x *Error) GRPCStatus() *status.Status {
	s, _ := status.New(http.GRPCCodeFromStatus(x.StatusCode()), x.Message).
		WithDetails(&errdetails.ErrorInfo{
			Reason:   x.Reason,
			Metadata: x.Metadata,
		})
	return s
}

// WithMetadata 通过KV键值的映射形成的元数据.
func (x *Error) WithMetadata(md map[string]string) *Error {
	err := proto.Clone(x).(*Error)
	err.Metadata = md
	return err
}

// New 返回代码的错误对象,消息.
func New(code int, reason, message string) *Error {
	return &Error{
		Code:    int32(code),
		Message: message,
		Reason:  reason,
	}
}

// NewFmt NewFmt(code reason fmt.Sprintf(format, a...))
func NewFmt(code int, reason, format string, a ...interface{}) *Error {
	return New(code, reason, fmt.Sprintf(format, a...))
}

// ErrorFmt 返回代码,消息和错误信息的错误对象.
func ErrorFmt(code int, reason, format string, a ...interface{}) error {
	return New(code, reason, fmt.Sprintf(format, a...))
}

// Code 返回特定错误的代码,它支持包装错误.
func Code(err error) int {
	if err == nil {
		return 200
	}
	if se := FromError(err); err != nil {
		return int(se.Code)
	}
	return UnknownCode
}

// Reason 返回特定错误的原因,它支持包装错误.
func Reason(err error) string {
	if se := FromError(err); err != nil {
		return se.Reason
	}
	return UnknownReason
}

// FromError 尝试将错误转换为*Error,它支持包装错误.
func FromError(err error) *Error {
	if err == nil {
		return nil
	}
	if se := new(Error); errors.As(err, &se) {
		return se
	}
	gs, ok := status.FromError(err)
	if ok {
		for _, detail := range gs.Details() {
			switch d := detail.(type) {
			case *errdetails.ErrorInfo:
				return New(http.StatusFromGRPCCode(gs.Code()),
					d.Reason,
					gs.Message(),
				).WithMetadata(d.Metadata)
			}
		}
	}
	return New(UnknownCode, UnknownReason, err.Error())
}

// Is 报告err链中的任何错误是否与target匹配.
//
// 该链由err本身组成，其后是通过重复调用Unwrap获得的错误序列.
// 如果错误等于target，或者它实现了{Is(error)）bool}的方法使得Is(target)返回true,则认为该错误与该目标匹配
// An error is considered to match a target if it is equal to that target or if it implements a method Is(error) bool such that Is(target) returns true.
func Is(err, target error) bool { return errors.Is(err, target) }

// As 在err链中找到与target匹配的第一个错误,如果匹配则将target设置为该错误值并返回true.
//
// 该链由err本身组成,其后是通过重复调用Unwrap获得的错误序列.
//
// 如果错误的具体值可分配给target指向的值，或者错误具有方法{As(interface{}）bool}使得As(target)返回true,则错误与目标匹配。在后一种情况下,As方法负责设置目标.
// As 如果target不是实现错误的类型或任何接口类型的非nil指针将panic,如果err为nil,则返回false.
func As(err error, target interface{}) bool { return errors.As(err, target) }

// Unwrap 如果err的类型包含返回错误的Unwrap方法,则返回对err调用Unwrap方法的结果.否则Unwrap返回nil.
func Unwrap(err error) error { return errors.Unwrap(err) }