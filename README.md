# Titan

Titan 一套轻量级 Go 微服务框架，包含大量微服务相关框架及工具 (Java版本[详见](titan-java))。

> 名字来源于：提坦是在原始神之后出现的古老神族。第一代提坦均由天神乌拉诺斯和地神盖亚所生，共有六男六女。

## 目标

我们致力于提供完整的微服务研发体验，整合相关框架及工具后，微服务治理相关部分可对整体业务开发周期无感，从而更加聚焦于业务交付。对每位开发者而言，整套Titan框架也是不错的学习仓库，可以了解和参考到微服务方面的技术积累和经验。

### 原则

* 简单：不过度设计，代码平实简单；
* 通用：通用业务开发所需要的基础库的功能；
* 高效：提高业务迭代的效率；
* 稳定：基础库可测试性高，覆盖率高，有线上实践安全可靠；
* 健壮：通过良好的基础库设计，减少错用；
* 高性能：性能高，但不特定为了性能做hack优化，引入unsafe；
* 扩展性：良好的接口设计，来扩展实现，或者通过新增基础库目录来扩展功能；
* 容错性：为失败设计，大量引入对SRE的理解，鲁棒性高；
* 工具链：包含大量工具链，比如项目代码生成，lint工具等等；

## 特征

* APIs：协议通信以HTTP/gRPC为基础，通过Protobuf进行定义；
* 元数据：在协议通信HTTP/gRPC中，通过中间件规范化服务元信息传递；
* 错误：通过Protobuf的枚举类型作为错误码定义，以及工具生成判定接口；
* 配置：支持多数据源方式，进行配置合并铺平，通过Atomic方式支持动态配置；
* 日志：标准日志接口，可方便集成三方log库，并可通过fluentd收集日志；
* 监控：统一指标接口，可以实现各种指标系统，默认集成Prometheus；
* 追踪：遵循[OpenTelemetry](docs/OpenTelemetry.md)规范定义，以实现微服务链路追踪；
* 编码：支持Accept和Content-Type进行自动选择内容编码；
* 启动：通用的HTTP/gRPC服务层，实现统一的中间件插件支持；
* 注册：实现统一注册中心接口，可插件化对接各种注册中心(支持zookeeper,etcd等)；

## Getting Started

### 必需的

- [go](https://golang.org/dl/)
- [protoc](https://github.com/protocolbuffers/protobuf)
- [protoc-gen-go](https://github.com/protocolbuffers/protobuf-go)

### 安装

```
go install google.golang.org/protobuf/cmd/protoc-gen-go
go install google.golang.org/grpc/cmd/protoc-gen-go-grpc
go install gitee.com/titan-kit/protoc-gen-go-web
go install gitee.com/titan-kit/cmd-titan
```

### 创建服务

```
# 创建项目模板
titan new demo

cd demo
# 拉取项目依赖
go mod download

# 生成proto模板
titan proto add api/demo/demo.proto
# 生成proto源码
titan proto client api/demo/demo.proto
# 生成server模板
titan proto server api/demo/demo.proto -t internal/service

# 生成所有proto源码、wire等等
go generate ./...
# 编译成可执行文件
go build -o ./bin/ ./...
# 运行程序
./bin/demo -conf ./configs
```

### Titan启动

```
import (
    "gitee.com/titan-kit/titan"
    "gitee.com/titan-kit/titan/starter/grpc"
    "gitee.com/titan-kit/titan/starter/web"
)

webSrv := web.NewServer(web.Address(":8000"))
grpcSrv := grpc.NewServer(grpc.Address(":9000"))

app := titan.New(
    titan.Name("titan"),
    titan.Version("latest"),
    titan.Server(webSrv, grpcSrv),
)
app.Run()
```

## 相关的

* [文档](https://gitee.com/titan-kit/titan/)
* [例子](./examples)
* [服务布局](https://gitee.com/titan-kit/go-layout)

## 社区

## 协议

Titan使用MIT协议。有关详细信息请参见[LICENSE](./LICENSE) 文件