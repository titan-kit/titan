package log

import (
	"io/ioutil"
	"os"
	"testing"
)

func TestLogger(t *testing.T) {
	logger := DefaultLogger
	Debug(logger).Log("log", "test debug")
	Info(logger).Log("log", "test info")
	Warning(logger).Log("log", "test warning")
	Error(logger).Log("log", "test error")
	Critical(logger).Log("log", "test critical")
}

func TestWrapper(t *testing.T) {
	out := NewStdLogger(os.Stdout)
	err := NewStdLogger(os.Stderr)

	l := MultiLogger(out, err)
	l.Log("msg", "test")
}

func TestSlf4g(t *testing.T) {
	logger := With(DefaultLogger, "caller", Caller(5))
	log := NewSlf4g("test", logger)

	log.Debug("test debug")
	log.DebugF("test %s", "debug")
	log.DebugW("log", "test debug")
}

func TestSlf4gLevel(t *testing.T) {
	log := NewSlf4g("test", DefaultLogger)
	log.Debug("test debug")
	log.Info("test info")
	log.Warning("test warning")
	log.Error("test error")
}

func TestValue(t *testing.T) {
	logger := With(DefaultLogger, "caller", Caller(4))
	logger.Log("msg", "hello world")
}
func TestStdLogger(t *testing.T) {
	logger := DefaultLogger

	Debug(logger).Log("log", "test debug")
	Info(logger).Log("log", "test info")
	Warning(logger).Log("log", "test warning")
	Error(logger).Log("log", "test error")
	Critical(logger).Log("log", "test critical")
}
func BenchmarkSlf4gPrint(b *testing.B) {
	log := NewSlf4g("test", NewStdLogger(ioutil.Discard))
	for i := 0; i < b.N; i++ {
		log.Debug("test")
	}
}

func BenchmarkSlf4gPrintf(b *testing.B) {
	log := NewSlf4g("test", NewStdLogger(ioutil.Discard))
	for i := 0; i < b.N; i++ {
		log.DebugF("%s", "test")
	}
}

func BenchmarkSlf4gPrintW(b *testing.B) {
	log := NewSlf4g("test", NewStdLogger(ioutil.Discard))
	for i := 0; i < b.N; i++ {
		log.DebugW("key", "value")
	}
}