package direct

import (
	"strings"

	"google.golang.org/grpc/resolver"
)

func init() {
	resolver.Register(NewDirectBuilder())
}

type directBuilder struct{}

// NewDirectBuilder 创建一个directBuilder,用于工厂直接解析器.
// 示例: direct://<authority>/127.0.0.1:9000,127.0.0.2:9000
func NewDirectBuilder() resolver.Builder {
	return &directBuilder{}
}

func (d *directBuilder) Build(target resolver.Target, cc resolver.ClientConn, opts resolver.BuildOptions) (resolver.Resolver, error) {
	var address []resolver.Address
	for _, addr := range strings.Split(target.Endpoint, ",") {
		address = append(address, resolver.Address{Addr: addr})
	}
	cc.UpdateState(resolver.State{
		Addresses: address,
	})
	return newDirectResolver(), nil
}

func (d *directBuilder) Scheme() string {
	return "direct"
}

type directResolver struct{}

func newDirectResolver() resolver.Resolver {
	return &directResolver{}
}

func (r *directResolver) Close() {
}

func (r *directResolver) ResolveNow(options resolver.ResolveNowOptions) {
}