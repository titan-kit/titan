package rpc

import (
	"context"
	"gitee.com/titan-kit/titan/api/metadata"
	"net"
	"net/url"
	"sync"
	"time"

	"gitee.com/titan-kit/titan/integrated/host"
	"gitee.com/titan-kit/titan/log"
	"gitee.com/titan-kit/titan/middleware"
	"gitee.com/titan-kit/titan/starter"

	"google.golang.org/grpc"
	"google.golang.org/grpc/health"
	"google.golang.org/grpc/health/grpc_health_v1"
	"google.golang.org/grpc/reflection"
)

const loggerName = "starter/rpc"

var _ starter.Server = (*Server)(nil)

// ServerOption 是gRPC服务器选项.
type ServerOption func(o *Server)

// Network 返回带服务器网络协议选项.
func Network(network string) ServerOption {
	return func(s *Server) {
		s.network = network
	}
}

// Address 返回带服务器地址选项.
func Address(addr string) ServerOption {
	return func(s *Server) {
		s.address = addr
	}
}

// Timeout 返回带服务器超时选项.
func Timeout(timeout time.Duration) ServerOption {
	return func(s *Server) {
		s.timeout = timeout
	}
}

// Logger 返回带有定制日记适配器选项.
func Logger(logger log.Logger) ServerOption {
	return func(s *Server) {
		s.log = log.NewSlf4g(loggerName, logger)
	}
}

// Middleware 返回带服务器中间件选项.
func Middleware(m ...middleware.Middleware) ServerOption {
	return func(s *Server) {
		s.middleware = middleware.Chain(m...)
	}
}

// UnaryInterceptor 返回一个带服务器UnaryServerInterceptor选项.
func UnaryInterceptor(in ...grpc.UnaryServerInterceptor) ServerOption {
	return func(s *Server) {
		s.ints = in
	}
}

// Options 返回带有grpc选项.
func Options(opts ...grpc.ServerOption) ServerOption {
	return func(s *Server) {
		s.grpcOpts = opts
	}
}

// Server 是一个gRPC服务器包装器.
type Server struct {
	*grpc.Server
	ctx        context.Context
	lis        net.Listener
	once       sync.Once
	err        error
	network    string
	address    string
	endpoint   *url.URL
	timeout    time.Duration
	log        *log.Slf4g
	middleware middleware.Middleware
	ints       []grpc.UnaryServerInterceptor
	grpcOpts   []grpc.ServerOption
	health     *health.Server
	metadata   *metadata.Server
}

// NewServer 通过选项创建一个gRPC服务器.
func NewServer(opts ...ServerOption) *Server {
	srv := &Server{
		network: "tcp",
		address: ":0",
		timeout: time.Second,
		health:  health.NewServer(),
		log:     log.NewSlf4g(loggerName, log.DefaultLogger),
	}
	for _, o := range opts {
		o(srv)
	}
	var ints = []grpc.UnaryServerInterceptor{
		srv.unaryServerInterceptor(),
	}
	if len(srv.ints) > 0 {
		ints = append(ints, srv.ints...)
	}
	var grpcOpts = []grpc.ServerOption{
		grpc.ChainUnaryInterceptor(ints...),
	}
	if len(srv.grpcOpts) > 0 {
		grpcOpts = append(grpcOpts, srv.grpcOpts...)
	}
	srv.Server = grpc.NewServer(grpcOpts...)
	srv.metadata = metadata.NewServer(srv.Server)
	// grpc健康档案
	grpc_health_v1.RegisterHealthServer(srv.Server, srv.health)
	metadata.RegisterMetadataServer(srv.Server, srv.metadata)
	//注册元数据服务
	reflection.Register(srv.Server)
	return srv
}

// Endpoint 返回真实地址到注册表节点.
// examples:
//   grpc://127.0.0.1:9090?isSecure=false
func (s *Server) Endpoint() (*url.URL, error) {
	s.once.Do(func() {
		lis, err := net.Listen(s.network, s.address)
		if err != nil {
			s.err = err
			return
		}
		addr, err := host.Extract(s.address, s.lis)
		if err != nil {
			err = lis.Close()
			if err != nil {
				return
			}
			s.err = err
			return
		}
		s.lis = lis
		s.endpoint = &url.URL{Scheme: "grpc", Host: addr}
	})
	if s.err != nil {
		return nil, s.err
	}
	return s.endpoint, nil
}

// Start 启动gRPC服务器.
func (s *Server) Start(ctx context.Context) error {
	if _, err := s.Endpoint(); err != nil {
		return err
	}
	s.ctx = ctx
	s.log.InfoF("[RPC] 服务器正在监听: %s", s.lis.Addr().String())
	s.health.Resume()
	return s.Serve(s.lis)
}

// Stop 停止gRPC服务器.
func (s *Server) Stop(ctx context.Context) error {
	s.GracefulStop()
	s.health.Shutdown()
	s.log.Info("[RPC] 服务器正在停止")
	return nil
}

func (s *Server) unaryServerInterceptor() grpc.UnaryServerInterceptor {
	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
		ctx = starter.NewContext(ctx, starter.Starter{Kind: starter.KindRPC, Endpoint: s.endpoint.String()})
		ctx = NewServerContext(ctx, ServerInfo{Server: info.Server, FullMethod: info.FullMethod})
		if s.timeout > 0 {
			var cancel context.CancelFunc
			ctx, cancel = context.WithTimeout(ctx, s.timeout)
			defer cancel()
		}
		h := func(ctx context.Context, req interface{}) (interface{}, error) {
			return handler(ctx, req)
		}
		if s.middleware != nil {
			h = s.middleware(h)
		}
		return h(ctx, req)
	}
}