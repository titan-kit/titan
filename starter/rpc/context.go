package rpc

import "context"

// ServerInfo 表示gRPC服务器信息.
type ServerInfo struct {
	// Server 是用户提供的服务实现.这是只读的.
	Server interface{}
	// FullMethod 是完整的RPC方法字符串,即/package.service/method.
	FullMethod string
}

type serverKey struct{}

// NewServerContext 返回一个带有值(ServerInfo)的新Context.
func NewServerContext(ctx context.Context, info ServerInfo) context.Context {
	return context.WithValue(ctx, serverKey{}, info)
}

// FromServerContext 返回存储在ctx中的传输值(如果有).
func FromServerContext(ctx context.Context) (info ServerInfo, ok bool) {
	info, ok = ctx.Value(serverKey{}).(ServerInfo)
	return
}

// ClientInfo 表示gRPC服务器信息.
type ClientInfo struct {
	// FullMethod 是完整的RPC方法字符串，即/package.service/method.
	FullMethod string
}

type clientKey struct{}

// NewClientContext 返回一个带有值(ClientInfo)的新Context.
func NewClientContext(ctx context.Context, info ClientInfo) context.Context {
	return context.WithValue(ctx, clientKey{}, info)
}

// FromClientContext 返回存储在ctx中的传输值(如果有).
func FromClientContext(ctx context.Context) (info ClientInfo, ok bool) {
	info, ok = ctx.Value(clientKey{}).(ClientInfo)
	return
}