package web

import (
	"context"
	"net/http"
)

// ClientInfo 表示HTTP客户端信息.
type ClientInfo struct {
	Request *http.Request
}

type clientKey struct{}

// NewClientContext 返回一个带有值的新Context.
func NewClientContext(ctx context.Context, info ClientInfo) context.Context {
	return context.WithValue(ctx, clientKey{}, info)
}

// FromClientContext 返回存储在ctx中的传输值（如果有）.
func FromClientContext(ctx context.Context) (info ClientInfo, ok bool) {
	info, ok = ctx.Value(clientKey{}).(ClientInfo)
	return
}

// ServerInfo 表示HTTP服务器信息.
type ServerInfo struct {
	Request  *http.Request
	Response http.ResponseWriter
}

type serverKey struct{}

// NewServerContext 返回一个带有值的新Context.
func NewServerContext(ctx context.Context, info ServerInfo) context.Context {
	return context.WithValue(ctx, serverKey{}, info)
}

// FromServerContext 返回存储在ctx中的传输值(如果有).
func FromServerContext(ctx context.Context) (info ServerInfo, ok bool) {
	info, ok = ctx.Value(serverKey{}).(ServerInfo)
	return
}

type contextKey struct{}

var (
	varsKey  = contextKey{}
	routeKey = contextKey{}
)

// Vars 返回请求参数
func Vars(req *http.Request) map[string]string {
	values, ok := req.Context().Value(varsKey).(map[string]string)
	if ok {
		return values
	}

	return map[string]string{}
}

// PathValue 通过当前 *http.Request 返回指定key的值
func PathValue(req *http.Request, key string) string {
	return Vars(req)[key]
}

// CurrentRoute 返回当前请求的匹配路由(如果有).
// 这仅在匹配路由的处理程序内部调用时才有效,因为匹配的路由存储在请求上下文中,该上下文在处理程序返回后被清除.
func CurrentRoute(req *http.Request) *Node {
	values, ok := req.Context().Value(routeKey).(*Node)
	if ok {
		return values
	}
	return nil
}