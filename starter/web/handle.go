package web

import (
	"context"
	"io/ioutil"
	"net/http"

	"gitee.com/titan-kit/titan/encoding"
	ct "gitee.com/titan-kit/titan/integrated/http"
	"gitee.com/titan-kit/titan/middleware"
	"gitee.com/titan-kit/titan/starter/web/binding"

	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/encoding/protojson"
)

// SupportPackageIsVersion1 不应从任何其他代码中引用这些常量.
const SupportPackageIsVersion1 = true

// DecodeRequestFunc 是解码请求函数.
type DecodeRequestFunc func(*http.Request, interface{}) error

// EncodeResponseFunc 是编码响应函数.
type EncodeResponseFunc func(http.ResponseWriter, *http.Request, interface{}) error

// EncodeErrorFunc 是编码错误处理函数.
type EncodeErrorFunc func(http.ResponseWriter, *http.Request, error)

// HandleOption 是句柄选项.
type HandleOption func(*HandleOptions)

// HandleOptions 是处理器选项集.
type HandleOptions struct {
	Decode     DecodeRequestFunc
	Encode     EncodeResponseFunc
	Error      EncodeErrorFunc
	Middleware middleware.Middleware
}

// DefaultHandleOptions 返回默认的处理器选项.
func DefaultHandleOptions() HandleOptions {
	return HandleOptions{
		Decode: decodeRequest,
		Encode: encodeResponse,
		Error:  encodeError,
	}
}

// RequestDecoder 带请求解码器.
func RequestDecoder(dec DecodeRequestFunc) HandleOption {
	return func(o *HandleOptions) {
		o.Decode = dec
	}
}

// ResponseEncoder 带有响应编码器.
func ResponseEncoder(en EncodeResponseFunc) HandleOption {
	return func(o *HandleOptions) {
		o.Encode = en
	}
}

// ErrorEncoder 带有错误编码器.
func ErrorEncoder(en EncodeErrorFunc) HandleOption {
	return func(o *HandleOptions) {
		o.Error = en
	}
}

// Middleware 带中间件选项.
func Middleware(m ...middleware.Middleware) HandleOption {
	return func(o *HandleOptions) {
		o.Middleware = middleware.Chain(m...)
	}
}

// decodeRequest 将请求主体解码为对象.
func decodeRequest(req *http.Request, v interface{}) error {
	subtype := ct.ContentSubtype(req.Header.Get(ct.HeaderContentType))
	if codec := encoding.GetCodec(subtype); codec != nil {
		data, err := ioutil.ReadAll(req.Body)
		if err != nil {
			return err
		}
		return codec.Unmarshal(data, v)
	}
	return binding.BindForm(req, v)
}

// encodeResponse 将对象编码为HTTP响应.
func encodeResponse(w http.ResponseWriter, r *http.Request, v interface{}) error {
	codec := codecForRequest(r)
	data, err := codec.Marshal(v)
	if err != nil {
		return err
	}
	w.Header().Set(ct.HeaderContentType, ct.ContentType(codec.Name()))
	_, _ = w.Write(data)
	return nil
}

// encodeError 将错误编码为HTTP响应.
func encodeError(w http.ResponseWriter, r *http.Request, err error) {
	st, _ := status.FromError(err)
	data, err := protojson.Marshal(st.Proto())
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Header().Set(ct.HeaderContentType, "application/json; charset=utf-8")
	w.WriteHeader(ct.StatusFromGRPCCode(st.Code()))
	_, _ = w.Write(data)
}

// codecForRequest 通过http.Request获取encoding.Codec
func codecForRequest(r *http.Request) encoding.Codec {
	var codec encoding.Codec
	for _, accept := range r.Header[ct.HeaderAccept] {
		if codec = encoding.GetCodec(ct.ContentSubtype(accept)); codec != nil {
			break
		}
	}
	if codec == nil {
		codec = encoding.GetCodec(encoding.JsonName)
	}
	return codec
}

func Process(h HandleOptions, w http.ResponseWriter, r *http.Request, next func(ctx context.Context, req interface{}) (interface{}, error)) {
	var in interface{}
	if err := h.Decode(r, in); err != nil {
		h.Error(w, r, err)
		return
	}
	if h.Middleware != nil {
		next = h.Middleware(next)
	}
	if out, err := next(r.Context(), &in); err != nil {
		h.Error(w, r, err)
		return
	} else {
		if err := h.Encode(w, r, out); err != nil {
			h.Error(w, r, err)
		}
	}
}