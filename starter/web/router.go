package web

import (
	"net/http"
	"strings"
)

func valid(path string) bool {
	size := len(path)
	if size > 1 && path[size-1:] == "/" {
		return false
	}
	return true
}

// 清理网址路径
func cleanURL(url *string) {
	size := len(*url)
	if size > 1 {
		if (*url)[size-1:] == "/" {
			*url = (*url)[:size-1]
			cleanURL(url)
		}
	}
}

const static = "static"

var method = []string{"GET", "POST", "PUT", "DELETE", "HEAD", "PATCH", "OPTIONS"}

// Router 是一个 http.Handler 可用于通过可配置的路由将请求分派到不同的处理程序函数
type Router struct {
	nodes         map[string][]*Node // 所有注册的节点
	prefix        string
	notFound      http.Handler // 404 处理程序，如果未提供，则默认为 http.NotFound
	CaseSensitive bool
}

type router func(*Router) *Router

// 确保 Router 符合 http.Handler 接口
var _ http.Handler = NewRouter()

// NewRouter 创建一个指向 Router 实例
func NewRouter(routers ...router) *Router {
	m := &Router{nodes: make(map[string][]*Node), CaseSensitive: true}
	for _, _router := range routers {
		_router(m)
	}
	return m
}

// Prefix set a default prefix for all routes registered on the router
func (m *Router) Prefix(p string) *Router {
	m.prefix = strings.TrimSuffix(p, "/")
	return m
}

// GET 使用 GET 方法向路由器添加新路由
func (m *Router) GET(path string, handler http.HandlerFunc) {
	m.register("GET", path, handler)
}

// POST 使用 POST 方法向路由器添加新路由
func (m *Router) POST(path string, handler http.HandlerFunc) {
	m.register("POST", path, handler)
}

// PUT 使用 PUT 方法向路由器添加新路由
func (m *Router) PUT(path string, handler http.HandlerFunc) {
	m.register("PUT", path, handler)
}

// DELETE 使用 DELETE 方法向路由器添加新路由
func (m *Router) DELETE(path string, handler http.HandlerFunc) {
	m.register("DELETE", path, handler)
}

// HEAD 使用 HEAD 方法向路由器添加新路由
func (m *Router) HEAD(path string, handler http.HandlerFunc) {
	m.register("HEAD", path, handler)
}

// PATCH 使用 PATCH 方法向路由器添加新路由
func (m *Router) PATCH(path string, handler http.HandlerFunc) {
	m.register("PATCH", path, handler)
}

// OPTIONS 使用 OPTIONS 方法向路由器添加新路由
func (m *Router) OPTIONS(path string, handler http.HandlerFunc) {
	m.register("OPTIONS", path, handler)
}

// Handle 在没有HTTP方法的情况下向路由器添加新路由
func (m *Router) Handle(path string, handler http.Handler) {
	for _, mt := range method {
		m.register(mt, path, handler)
	}
}

// HandleFunc 用于传递 func(http.ResponseWriter, *Http.Request) 而不是 http.Handler
func (m *Router) HandleFunc(path string, handler http.HandlerFunc) {
	m.Handle(path, handler)
}

// SubRoute 将路由器注册为子路由器
func (m *Router) SubRoute(path string, router http.Handler) {
	r := NewNode(m, m.prefix+path, router)
	if valid(path) {
		r.Kind += SUB
		for _, mt := range method {
			m.nodes[mt] = append(m.nodes[mt], r)
		}
	}
}

// GetRequestRoute returns the route of given Request
func (m *Router) GetRequestRoute(req *http.Request) string {
	cleanURL(&req.URL.Path)
	for _, r := range m.nodes[req.Method] {
		if r.Kind != 0 {
			if r.Kind&SUB != 0 {
				return r.Handler.(*Router).GetRequestRoute(req)
			}
			if r.Match(req) {
				return r.Path
			}
		}
		if req.URL.Path == r.Path {
			return r.Path
		}
	}

	for _, s := range m.nodes[static] {
		if len(req.URL.Path) >= s.Size {
			if req.URL.Path[:s.Size] == s.Path {
				return s.Path
			}
		}
	}

	return "NotFound"
}

// NotFoundFunc 路由器自定义 404 处理程序
func (m *Router) NotFoundFunc(handler http.HandlerFunc) {
	m.notFound = handler
}

// NotFound 路由器自定义404处理程序
func (m *Router) NotFound(handler http.Handler) {
	m.notFound = handler
}

// ServeHTTP 是默认的 http 请求处理程序
func (m *Router) ServeHTTP(rw http.ResponseWriter, req *http.Request) {
	if !m.CaseSensitive {
		req.URL.Path = strings.ToLower(req.URL.Path)
	}
	// 检查路由是否匹配
	if !m.parse(rw, req) {
		// 检查它是否是静态资源
		for _, s := range m.nodes[static] {
			if len(req.URL.Path) >= s.Size {
				if req.URL.Path[:s.Size] == s.Path {
					s.Handler.ServeHTTP(rw, req)
					return
				}
			}
		}
		// 检查请求路径是否不以 / 结尾
		size := len(req.URL.Path)
		if size > 1 && req.URL.Path[size-1:] == "/" {
			cleanURL(&req.URL.Path)
			rw.Header().Set("Location", req.URL.String())
			rw.WriteHeader(http.StatusFound)
			return
		}
		// 重试查找匹配的路由
		if m.parse(rw, req) {
			return
		}
		// 检查另一个 HTTP 方法是否存在相同的路由
		for _, met := range method {
			if met != req.Method {
				for _, r := range m.nodes[met] {
					ok := r.exists(req)
					if ok {
						rw.WriteHeader(http.StatusMethodNotAllowed)
						return
					}
				}
			}
		}
		// 当请求与注册的处理程序不匹配时处理程序
		if m.notFound != nil {
			m.notFound.ServeHTTP(rw, req)
		} else {
			http.NotFound(rw, req)
		}
	}
}

// Register 使用提供的方法和处理程序在路由器中注册新路由
func (m *Router) register(method string, path string, handler http.Handler) {
	r := NewNode(m, m.prefix+path, handler)
	r.Method = method
	if valid(path) {
		m.nodes[method] = append(m.nodes[method], r)
	} else {
		m.nodes[static] = append(m.nodes[static], r)
	}
}

func (m *Router) parse(rw http.ResponseWriter, req *http.Request) bool {
	for _, r := range m.nodes[req.Method] {
		ok := r.parse(rw, req)
		if ok {
			return true
		}
	}
	// 如果没有 HEAD 方法，默认为 GET
	if req.Method == "HEAD" {
		for _, r := range m.nodes["GET"] {
			ok := r.parse(rw, req)
			if ok {
				return true
			}
		}
	}
	return false
}