package web

import (
	"context"
	"encoding/json"
	"net/http"
)

// HealthFunc 包装心跳检查方法.
//
// HealthFunc 如果资源状况良好,则返回nil;如果资源状况不健康,则返回non-nil错误.
// HealthFunc 必须安全地从多个goroutine调用.
type HealthFunc func(ctx context.Context) error

// HealthHandler 是一个HTTP处理器,报告了一组Checkers的成功.零值代表始终健康.
type HealthHandler struct {
	checkers  map[string]HealthFunc
	observers map[string]HealthFunc
}

// NewHealthHandler 新的处理器.
func NewHealthHandler() *HealthHandler {
	return &HealthHandler{checkers: make(map[string]HealthFunc), observers: make(map[string]HealthFunc)}
}

// AddChecker 向处理程序添加新的检查.
func (h *HealthHandler) AddChecker(name string, c HealthFunc) {
	h.checkers[name] = c
}

// AddObserver 向处理程序添加新的检查，但不会使整个状态失败.
func (h *HealthHandler) AddObserver(name string, c HealthFunc) {
	h.observers[name] = c
}

// ServeHTTP 如果健康，则返回200，否则返回500.
func (h *HealthHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	code := http.StatusOK
	results := make(map[string]string, len(h.checkers))

	for name, checker := range h.checkers {
		if err := checker(r.Context()); err != nil {
			code = http.StatusInternalServerError
			results[name] = err.Error()
		} else {
			results[name] = "我思故我在!"
		}
	}

	for name, checker := range h.observers {
		if err := checker(r.Context()); err != nil {
			results[name] = err.Error()
		} else {
			results[name] = "我思故我在!"
		}
	}

	w.WriteHeader(code)
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	_ = json.NewEncoder(w).Encode(results)
}