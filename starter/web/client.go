package web

import (
	"context"
	"io/ioutil"
	"net/http"
	"time"

	spb "google.golang.org/genproto/googleapis/rpc/status"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/encoding/protojson"

	"gitee.com/titan-kit/titan/encoding"
	ct "gitee.com/titan-kit/titan/integrated/http"
	"gitee.com/titan-kit/titan/middleware"
	"gitee.com/titan-kit/titan/starter"
)

// DecodeErrorFunc 是解码错误功能.
type DecodeErrorFunc func(ctx context.Context, w *http.Response) error

// ClientOption 是HTTP客户端选项.
type ClientOption func(*clientOptions)

// WithTimeout 客户请求超时.
func WithTimeout(d time.Duration) ClientOption {
	return func(o *clientOptions) {
		o.timeout = d
	}
}

// WithUserAgent 与客户用户代理.
func WithUserAgent(ua string) ClientOption {
	return func(o *clientOptions) {
		o.userAgent = ua
	}
}

// WithTransport 与客户传输层.
func WithTransport(trans http.RoundTripper) ClientOption {
	return func(o *clientOptions) {
		o.transport = trans
	}
}

// WithMiddleware 与客户端中间件.
func WithMiddleware(m middleware.Middleware) ClientOption {
	return func(o *clientOptions) {
		o.middleware = m
	}
}

// Client 是HTTP传输层客户端.
type clientOptions struct {
	ctx          context.Context
	timeout      time.Duration
	userAgent    string
	transport    http.RoundTripper
	errorDecoder DecodeErrorFunc
	middleware   middleware.Middleware
}

// NewClient 返回一个HTTP客户端.
func NewClient(ctx context.Context, opts ...ClientOption) (*http.Client, error) {
	trans, err := NewTransport(ctx, opts...)
	if err != nil {
		return nil, err
	}
	return &http.Client{Transport: trans}, nil
}

// NewTransport 创建一个http.RoundTripper.
func NewTransport(ctx context.Context, opts ...ClientOption) (http.RoundTripper, error) {
	options := &clientOptions{ctx: ctx, timeout: 500 * time.Millisecond, transport: http.DefaultTransport, errorDecoder: CheckResponse}
	for _, o := range opts {
		o(options)
	}
	return &baseTransport{
		middleware:   options.middleware,
		userAgent:    options.userAgent,
		timeout:      options.timeout,
		base:         options.transport,
		errorDecoder: options.errorDecoder,
	}, nil
}

type baseTransport struct {
	userAgent    string
	timeout      time.Duration
	base         http.RoundTripper
	errorDecoder DecodeErrorFunc
	middleware   middleware.Middleware
}

func (t *baseTransport) RoundTrip(req *http.Request) (*http.Response, error) {
	if t.userAgent != "" && req.Header.Get("User-Agent") == "" {
		req.Header.Set("User-Agent", t.userAgent)
	}
	ctx := starter.NewContext(req.Context(), starter.Starter{Kind: starter.KindWEB})
	ctx = NewClientContext(ctx, ClientInfo{Request: req})
	ctx, cancel := context.WithTimeout(ctx, t.timeout)
	defer cancel()

	h := func(ctx context.Context, in interface{}) (interface{}, error) {
		res, err := t.base.RoundTrip(in.(*http.Request))
		if err != nil {
			return nil, err
		}
		if err := t.errorDecoder(ctx, res); err != nil {
			return nil, err
		}
		return res, nil
	}
	if t.middleware != nil {
		h = t.middleware(h)
	}
	res, err := h(ctx, req)
	if err != nil {
		return nil, err
	}
	return res.(*http.Response), nil
}

// Do 发送HTTP请求并将响应主体解码为目标.
// 如果响应状态代码不是2xx，则返回错误(type *Error).
func Do(client *http.Client, req *http.Request, target interface{}) error {
	res, err := client.Do(req)
	if err != nil {
		return err
	}
	subtype := ct.ContentSubtype(res.Header.Get(ct.HeaderContentType))
	codec := encoding.GetCodec(subtype)
	if codec == nil {
		codec = encoding.GetCodec(encoding.JsonName)
	}
	if data, err := ioutil.ReadAll(res.Body); err != nil {
		return err
	} else {
		return codec.Unmarshal(data, target)
	}
}

// CheckResponse 如果响应状态代码不是2xx，则返回错误(*Error类型).
func CheckResponse(_ context.Context, res *http.Response) error {
	if res.StatusCode >= 200 && res.StatusCode <= 299 {
		return nil
	}
	defer res.Body.Close()
	if data, err := ioutil.ReadAll(res.Body); err == nil {
		st := new(spb.Status)
		if err = protojson.Unmarshal(data, st); err == nil {
			return status.ErrorProto(st)
		}
	}
	return status.Error(ct.GRPCCodeFromStatus(res.StatusCode), res.Status)
}