package web

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
)

// 测试路由是否有效
func TestRouting(t *testing.T) {
	router := NewRouter()
	call := false
	router.GET("/a/:id", func(http.ResponseWriter, *http.Request) {
		call = true
	})

	r, _ := http.NewRequest("GET", "/b/123", nil)
	w := httptest.NewRecorder()
	router.ServeHTTP(w, r)

	if call {
		t.Error("handler should not be called")
	}
}

// 测试自定义的非处理程序处理程序集 404 错误代码
func TestNotFoundCustomHandlerSends404(t *testing.T) {
	router := NewRouter()
	router.NotFoundFunc(func(rw http.ResponseWriter, req *http.Request) {
		rw.WriteHeader(404)
		rw.Write([]byte("These are not the droids you're looking for ..."))
	})

	r, _ := http.NewRequest("GET", "/b/123", nil)
	w := httptest.NewRecorder()
	router.ServeHTTP(w, r)

	if w.Code != 404 {
		t.Errorf("expecting error code 404, got %v", w.Code)
	}
}

// 测试http方法是否有效
func TestRoutingMethod(t *testing.T) {
	router := NewRouter()
	call := false
	router.GET("/t", func(http.ResponseWriter, *http.Request) {
		call = true
	})

	r, _ := http.NewRequest("POST", "/t", nil)
	w := httptest.NewRecorder()
	router.ServeHTTP(w, r)

	if call {
		t.Error("response to a wrong method")
	}
}

// 测试路由器是否不按前缀处理
func TestRoutingPath(t *testing.T) {
	router := NewRouter()
	call := false
	router.GET("/t", func(http.ResponseWriter, *http.Request) {
		call = true
	})
	router.GET("/t/x", func(http.ResponseWriter, *http.Request) {
		call = false
	})

	r, _ := http.NewRequest("GET", "/t/x", nil)
	w := httptest.NewRecorder()
	router.ServeHTTP(w, r)

	if call {
		t.Error("response with the wrong path")
	}
}

func TestPrefix(t *testing.T) {
	router := NewRouter()
	router.Prefix("/api")
	call := false
	router.GET("/t", func(http.ResponseWriter, *http.Request) {
		call = true
	})
	router.GET("/api/t", func(http.ResponseWriter, *http.Request) {
		call = false
	})

	r, _ := http.NewRequest("GET", "/api/t", nil)
	w := httptest.NewRecorder()
	router.ServeHTTP(w, r)

	if !call {
		t.Error("response with the wrong path")
	}
}

func TestPrefixWithTailSlash(t *testing.T) {
	router := NewRouter()
	router.Prefix("/api/")
	call := false
	router.GET("/t", func(http.ResponseWriter, *http.Request) {
		call = true
	})
	router.GET("/api/t", func(http.ResponseWriter, *http.Request) {
		call = false
	})

	r, _ := http.NewRequest("GET", "/api/t", nil)
	w := httptest.NewRecorder()
	router.ServeHTTP(w, r)

	if !call {
		t.Error("response with the wrong path")
	}
}

func TestRoutingVerbs(t *testing.T) {
	var (
		methods = []string{"DELETE", "GET", "HEAD", "PUT", "POST", "PATCH", "OPTIONS", "HEAD"}
		path    = "/"
		h       = func(http.ResponseWriter, *http.Request) {}
	)
	for _, meth := range methods {
		m := NewRouter()
		switch meth {
		case "DELETE":
			m.DELETE(path, h)
		case "GET":
			m.GET(path, h)
		case "HEAD":
			m.HEAD(path, h)
		case "POST":
			m.POST(path, h)
		case "PUT":
			m.PUT(path, h)
		case "PATCH":
			m.PATCH(path, h)
		case "OPTIONS":
			m.OPTIONS(path, h)
		}
		s := httptest.NewServer(m)
		req, err := http.NewRequest(meth, s.URL, nil)
		if err != nil {
			t.Fatal(err)
		}
		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			t.Fatal(err)
		}
		defer resp.Body.Close()
		if resp.StatusCode != 200 {
			t.Fatalf("%s: HTTP %d", meth, resp.StatusCode)
		}
		s.Close()
	}
}

// 如果没有 HEAD 方法，默认为 GET
func TestHeadToGet(t *testing.T) {
	path := "/"
	m := NewRouter()
	m.GET(path, func(rw http.ResponseWriter, r *http.Request) {
		rw.Write([]byte("text"))
	})

	s := httptest.NewServer(m)
	// GET
	reqGet, err := http.NewRequest("GET", s.URL, nil)
	if err != nil {
		t.Fatal(err)
	}
	resGet, err := http.DefaultClient.Do(reqGet)
	if err != nil {
		t.Fatal(err)
	}
	defer resGet.Body.Close()
	if resGet.StatusCode != 200 {
		t.Fatalf("GET: HTTP %d", resGet.StatusCode)
	}
	body, _ := ioutil.ReadAll(resGet.Body)
	if resGet.Header.Get("Content-Length") != "4" {
		t.Fatalf("GET: incorrect Content-Length")
	}
	if string(body) != "text" {
		t.Fatalf("GET: incorrect response body")
	}
	// HEAD
	reqHead, err := http.NewRequest("HEAD", s.URL, nil)
	if err != nil {
		t.Fatal(err)
	}
	resHead, err := http.DefaultClient.Do(reqHead)
	if err != nil {
		t.Fatal(err)
	}
	defer resHead.Body.Close()
	if resHead.StatusCode != 200 {
		t.Fatalf("If no HEAD method, default to GET: HTTP %d", resHead.StatusCode)
	}
	body, _ = ioutil.ReadAll(resHead.Body)
	if resGet.Header.Get("Content-Length") != "4" {
		t.Fatalf("HEAD: incorrect Content-Length")
	}
	if len(body) != 0 {
		t.Fatalf("HEAD: should not contain response body")
	}

	s.Close()
}

func TestRoutingSlash(t *testing.T) {
	router := NewRouter()
	call := false
	router.GET("/", func(http.ResponseWriter, *http.Request) {
		call = true
	})

	r, _ := http.NewRequest("GET", "/", nil)
	w := httptest.NewRecorder()
	router.ServeHTTP(w, r)

	if !call {
		t.Error("root not serve")
	}
}

func TestMultipleRoutingVariables(t *testing.T) {
	var (
		expected1 = "variable1"
		expected2 = "variable2"
		got1      string
		got2      string
		router    = NewRouter()
		w         = httptest.NewRecorder()
	)
	router.GET("/test/:var1/:var2", func(w http.ResponseWriter, r *http.Request) {
		got1 = PathValue(r, "var1")
		got2 = PathValue(r, "var2")
	})

	r, err := http.NewRequest("GET", fmt.Sprintf("/test/%s/%s", expected1, expected2), nil)
	if err != nil {
		t.Fatal(err)
	}
	router.ServeHTTP(w, r)

	if got1 != expected1 {
		t.Fatalf("expected %s, got %s", expected1, got1)
	}

	if got2 != expected2 {
		t.Fatalf("expected %s, got %s", expected2, got2)
	}
}

func TestRoutingVariable(t *testing.T) {
	var (
		expected = "variable"
		got      string
		router   = NewRouter()
		w        = httptest.NewRecorder()
	)
	router.GET("/:vartest", func(w http.ResponseWriter, r *http.Request) {
		got = PathValue(r, "vartest")
	})

	r, err := http.NewRequest("GET", fmt.Sprintf("/%s", expected), nil)
	if err != nil {
		t.Fatal(err)
	}
	router.ServeHTTP(w, r)

	if got != expected {
		t.Fatalf("expected %s, got %s", expected, got)
	}
}

func TestStaticFile(t *testing.T) {
	var data string
	router := NewRouter()
	router.GET("/file/", func(http.ResponseWriter, *http.Request) {
		data = "DATA"
	})

	r, _ := http.NewRequest("GET", "/file/", nil)
	w := httptest.NewRecorder()
	router.ServeHTTP(w, r)

	if data != "DATA" {
		t.Error("Data not serve")
	}
}

func TestStandAloneNode(t *testing.T) {
	valid := false
	router := http.NewServeMux()

	testRoute := NewNode(nil, "/test", http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		valid = true
	}))
	testRoute.Method = "GET"
	router.Handle("/test", testRoute)
	r, _ := http.NewRequest("GET", "/test", nil)
	w := httptest.NewRecorder()
	router.ServeHTTP(w, r)

	if !valid {
		t.Error("Route Handler not call")
	}
}

func TestRegexParam(t *testing.T) {
	valid := false
	router := NewRouter()

	router.GET("/Regex/#ttt^[a-z]$", func(rw http.ResponseWriter, req *http.Request) {
		valid = true
	})

	r, _ := http.NewRequest("GET", "/Regex/test", nil)
	w := httptest.NewRecorder()
	router.ServeHTTP(w, r)

	if !valid {
		t.Error("Route Handler not call")
	}
}

func TestRegexParam2(t *testing.T) {
	valid := false
	router := NewRouter()

	router.GET("/Regex/#tttt^[a-z]$", func(rw http.ResponseWriter, req *http.Request) {
		valid = true
	})

	r, _ := http.NewRequest("GET", "/Regex/1234", nil)
	w := httptest.NewRecorder()
	router.ServeHTTP(w, r)

	if valid {
		t.Error("Regex param not valid !")
	}
}

func TestRegexParamMutli(t *testing.T) {
	valid := false
	router := NewRouter()

	router.GET("/Regex/#ttt^[a-z]$/#yyy^[0-9]$", func(rw http.ResponseWriter, req *http.Request) {
		valid = true
	})

	r, _ := http.NewRequest("GET", "/Regex/first/2", nil)
	w := httptest.NewRecorder()
	router.ServeHTTP(w, r)

	if !valid {
		t.Error("Regex multi Params not valid !")
	}
}

func TestMultiParams(t *testing.T) {
	valid := false
	router := NewRouter()

	router.GET("/Regex/#num^[a-z]$/:test", func(rw http.ResponseWriter, req *http.Request) {
		valid = true
	})

	r, _ := http.NewRequest("GET", "/Regex/first/second", nil)
	w := httptest.NewRecorder()
	router.ServeHTTP(w, r)

	if !valid {
		t.Error("Regex multi Params not valid !")
	}
}

func TestWC(t *testing.T) {
	valid := false
	router := NewRouter()
	router.GET("/test/*", func(rw http.ResponseWriter, req *http.Request) {
		valid = true
	})

	req, _ := http.NewRequest("GET", "/test/random/route", nil)
	rw := httptest.NewRecorder()
	router.ServeHTTP(rw, req)

	if !valid {
		t.Error("WC doesn't work !")
	}
}

func TestSlashRemoving1(t *testing.T) {
	router := NewRouter()
	router.GET("/test", func(rw http.ResponseWriter, req *http.Request) {
		t.Error("/test got called but it should have been a redirect!")
	})

	req, _ := http.NewRequest("GET", "/test/////", nil)
	rw := httptest.NewRecorder()
	router.ServeHTTP(rw, req)

	if rw.Header().Get("Location") != "/test" {
		t.Error("Redirect 1 doesn't work")
	}
}

func TestSlashRemovingWithQuery(t *testing.T) {
	router := NewRouter()
	router.GET("/test", func(rw http.ResponseWriter, req *http.Request) {
		t.Error("/test got called but it should have been a redirect!")
	})

	req, _ := http.NewRequest("GET", "/test/?foo=bar&buzz=bazz", nil)
	rw := httptest.NewRecorder()
	router.ServeHTTP(rw, req)

	if rw.Header().Get("Location") != "/test?foo=bar&buzz=bazz" {
		t.Error("Redirect 2 doesn't work")
	}
}

func TestSubRouteExtracting(t *testing.T) {
	router := NewRouter()
	apiRouter := NewRouter()
	result := ""
	apiRouter.GET("/test", func(rw http.ResponseWriter, req *http.Request) {
		result = router.GetRequestRoute(req)
	})
	router.SubRoute("/api", apiRouter)

	req, _ := http.NewRequest("GET", "/api/test", nil)
	rw := httptest.NewRecorder()
	router.ServeHTTP(rw, req)

	if result != "/test" {
		t.Log(result)
		t.Error("SubRouter route extracting not working")
	}
}

// Test the ns/op
func BenchmarkRouter(b *testing.B) {
	request, _ := http.NewRequest("GET", "/sd", nil)
	response := httptest.NewRecorder()
	router := NewRouter()

	router.GET("/", Bench)
	router.GET("/a", Bench)
	router.GET("/aas", Bench)
	router.GET("/sd", Bench)

	for n := 0; n < b.N; n++ {
		router.ServeHTTP(response, request)
	}
}
func Bench(rw http.ResponseWriter, req *http.Request) {
	rw.Write([]byte("b"))
}