package web

import (
	"context"
	"net/http"
	"testing"
)

type HelloRequest struct {
	Name string `json:"name"`
}
type HelloReply struct {
	Message string `json:"message"`
}
type GreeterService struct {
}

func (s *GreeterService) SayHello(ctx context.Context, req *HelloRequest) (*HelloReply, error) {
	return &HelloReply{Message: "hello " + req.Name}, nil
}

func newGreeterHandler(srv *GreeterService, opts ...HandleOption) http.Handler {
	h := DefaultHandleOptions()
	for _, o := range opts {
		o(&h)
	}
	r := NewRouter()
	r.POST("/hello-world", func(w http.ResponseWriter, r *http.Request) {
		var in HelloRequest
		next := func(ctx context.Context, req interface{}) (interface{}, error) {
			return srv.SayHello(ctx, &in)
		}
		Process(h, w, r, next)
	})
	return r
}

func TestHandler(t *testing.T) {
	s := &GreeterService{}
	_ = newGreeterHandler(s)
}