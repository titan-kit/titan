package web

import (
	"context"
	"net/http"
	"net/url"
	"regexp"
	"strings"
)

const (
	//PARAM 如果路由有参数，则值存储在 Kind 中
	PARAM = 2
	//SUB 如果路由是子路由器，则值存储在 Kind 中
	SUB = 4
	//WC 如果路由有通配符，则值存储在 Kind 中
	WC = 8
	//REGEX 如果路由包含正则表达式，则值存储在 Kind 中
	REGEX = 16
)

// Node 有效路由所需的信息
type Node struct {
	Path    string // 是节点 URL
	Method  string // 在路由上定义 HTTP 方法
	Size    int    //是路径的长度
	Kind    int
	wildPos int
	Token   Token          // 是路径每个部分的值，分隔符为`/`
	Pattern map[int]string // 是关于路由的内容信息, 如果它有一个路由变量
	Compile map[int]*regexp.Regexp
	Tag     map[int]string
	Handler http.Handler // 是这条路由的处理者
	router  *Router
}

// Token 分割路由路径的所有值
type Token struct {
	raw    []int
	Tokens []string //每个令牌的字符串值
	Size   int      // 令牌数量
}

// NewNode 返回一个指向 Node 实例的指针并对其调用save方法
func NewNode(mux *Router, url string, h http.Handler) *Node {
	r := &Node{Path: url, Handler: h, router: mux}
	// 设置 Node.Size 和 Node.Pattern 值
	r.Size = len(r.Path)
	r.Token.Tokens = strings.Split(r.Path, "/")
	for i, s := range r.Token.Tokens {
		if len(s) >= 1 {
			switch s[:1] {
			case ":":
				s = s[1:]
				if r.Pattern == nil {
					r.Pattern = make(map[int]string)
				}
				r.Pattern[i] = s
				r.Kind |= PARAM
			case "#":
				if r.Compile == nil {
					r.Compile = make(map[int]*regexp.Regexp)
					r.Tag = make(map[int]string)
				}
				tmp := strings.Split(s, "^")
				r.Tag[i] = tmp[0][1:]
				r.Compile[i] = regexp.MustCompile("^" + tmp[1][:len(tmp[1])-1])
				r.Kind |= REGEX
			case "*":
				r.wildPos = i
				r.Kind |= WC
			default:
				r.Token.raw = append(r.Token.raw, i)
			}
		}
		r.Token.Size++
	}
	return r
}

// Match 检查请求是否与路由规则匹配
func (r *Node) Match(req *http.Request) bool {
	ok, _ := r.matchAndParse(req)
	return ok
}

func (r *Node) ServeHTTP(rw http.ResponseWriter, req *http.Request) {
	if r.Method != "" {
		if req.Method == r.Method {
			r.Handler.ServeHTTP(rw, req)
			return
		}
		http.NotFound(rw, req)
		return
	}
	r.Handler.ServeHTTP(rw, req)
}

// matchAndParse 检查请求是否与路由规则匹配，如果匹配则返回解析变量的映射
func (r *Node) matchAndParse(req *http.Request) (bool, map[string]string) {
	ss := strings.Split(req.URL.EscapedPath(), "/")
	if r.matchRawTokens(&ss) {
		if len(ss) == r.Token.Size || r.Kind&WC != 0 {
			totalSize := len(r.Pattern)
			if r.Kind&REGEX != 0 {
				totalSize += len(r.Compile)
			}

			vars := make(map[string]string, totalSize)
			for k, v := range r.Pattern {
				vars[v], _ = url.QueryUnescape(ss[k])
			}

			if r.Kind&REGEX != 0 {
				for k, v := range r.Compile {
					if !v.MatchString(ss[k]) {
						return false, nil
					}
					vars[r.Tag[k]], _ = url.QueryUnescape(ss[k])
				}
			}

			return true, vars
		}
	}
	return false, nil
}

func (r *Node) parse(rw http.ResponseWriter, req *http.Request) bool {
	if r.Kind != 0 {
		if r.Kind&SUB != 0 {
			if len(req.URL.Path) >= r.Size {
				if req.URL.Path[:r.Size] == r.Path {
					req.URL.Path = req.URL.Path[r.Size:]
					r.Handler.ServeHTTP(rw, req)
					return true
				}
			}
		}

		if ok, vars := r.matchAndParse(req); ok {
			ctx := context.WithValue(req.Context(), routeKey, r)
			ctx = context.WithValue(ctx, varsKey, vars)
			newReq := req.WithContext(ctx)
			r.Handler.ServeHTTP(rw, newReq)
			return true
		}
	}
	if req.URL.Path == r.Path {
		r.Handler.ServeHTTP(rw, req)
		return true
	}
	return false
}

func (r *Node) matchRawTokens(ss *[]string) bool {
	if len(*ss) >= r.Token.Size {
		for i, v := range r.Token.raw {
			if (*ss)[v] != r.Token.Tokens[v] {
				if r.Kind&WC != 0 && r.wildPos == i {
					return true
				}
				return false
			}
		}
		return true
	}
	return false
}

func (r *Node) exists(req *http.Request) bool {
	if r.Kind != 0 {
		if r.Kind&SUB != 0 {
			if len(req.URL.Path) >= r.Size {
				if req.URL.Path[:r.Size] == r.Path {
					return true
				}
			}
		}

		if ok, _ := r.matchAndParse(req); ok {
			return true
		}
	}
	if req.URL.Path == r.Path {
		return true
	}
	return false
}