package binding

import (
	"net/http"
	"net/url"
	"strings"

	"google.golang.org/protobuf/proto"
)

// BindForm 将表单参数绑定到target.
func BindForm(req *http.Request, target interface{}) error {
	if err := req.ParseForm(); err != nil {
		return err
	}
	if msg, ok := target.(proto.Message); ok {
		return mapProto(msg, req.Form)
	}
	return mapForm(target, req.Form)
}

// GetQuery return the key value, of the current *http.Request query
func GetQuery(req *http.Request, key string) []string {
	if ok, value := extractQueries(req); ok {
		return value[key]
	}
	return nil
}

// GetAllQueries return all queries of the current *http.Request
func GetAllQueries(req *http.Request) map[string][]string {
	if ok, values := extractQueries(req); ok {
		return values
	}
	return nil
}

func extractQueries(req *http.Request) (bool, map[string][]string) {
	if q, err := url.ParseQuery(req.URL.RawQuery); err == nil {
		var queries = make(map[string][]string)
		for k, v := range q {
			for _, item := range v {
				values := strings.Split(item, ",")
				queries[k] = append(queries[k], values...)
			}
		}
		return true, queries
	}
	return false, nil
}