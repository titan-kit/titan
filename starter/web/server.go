package web

import (
	"context"
	"errors"
	"net"
	"net/http"
	"net/url"
	"sync"
	"time"

	"gitee.com/titan-kit/titan/integrated/host"
	"gitee.com/titan-kit/titan/log"
	"gitee.com/titan-kit/titan/starter"
)

const loggerName = "starter/web"

var _ starter.Server = (*Server)(nil)

// ServerOption 是HTTP服务器选项.
type ServerOption func(*Server)

// Network 与服务器网络.
func Network(network string) ServerOption {
	return func(s *Server) {
		s.network = network
	}
}

// Address 与服务器地址.
func Address(addr string) ServerOption {
	return func(s *Server) {
		s.address = addr
	}
}

// Timeout 与服务器超时.
func Timeout(timeout time.Duration) ServerOption {
	return func(s *Server) {
		s.timeout = timeout
	}
}

// Logger 使用服务器记录器.
func Logger(logger log.Logger) ServerOption {
	return func(s *Server) {
		s.log = log.NewSlf4g(loggerName, logger)
	}
}

// Server 是一个HTTP服务器包装器.
type Server struct {
	*http.Server
	ctx      context.Context
	lis      net.Listener
	once     sync.Once
	err      error
	network  string
	address  string
	endpoint *url.URL
	timeout  time.Duration
	mux      *Router
	log      *log.Slf4g
}

// NewServer 通过选项创建HTTP服务器.
func NewServer(opts ...ServerOption) *Server {
	srv := &Server{network: "tcp", address: ":0", timeout: time.Second, mux: NewRouter(), log: log.NewSlf4g(loggerName, log.DefaultLogger)}
	for _, o := range opts {
		o(srv)
	}
	srv.Server = &http.Server{Handler: srv}
	return srv
}

// Handle 使用URL的匹配器注册新路由.
func (s *Server) Handle(path string, h http.Handler) {
	s.mux.Handle(path, h)
}

// PathPrefix 使用匹配器为URL路径前缀注册新路由.
func (s *Server) PathPrefix(prefix string, h http.Handler) {
	s.mux.SubRoute(prefix, h)
}

// HandleFunc 使用URL的匹配器注册新路由.
func (s *Server) HandleFunc(path string, h http.HandlerFunc) {
	s.mux.HandleFunc(path, h)
}

// ServeHTTP 应该将回复标头和数据写入ResponseWriter,然后返回.
func (s *Server) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	ctx, cancel := context.WithTimeout(req.Context(), s.timeout)
	defer cancel()
	ctx = starter.NewContext(ctx, starter.Starter{Kind: starter.KindWEB, Endpoint: s.endpoint.String()})
	ctx = NewServerContext(ctx, ServerInfo{Request: req, Response: res})
	s.mux.ServeHTTP(res, req.WithContext(ctx))
}

// Start 启动Web服务器.
func (s *Server) Start(ctx context.Context) error {
	if _, err := s.Endpoint(); err != nil {
		return err
	}
	s.ctx = ctx
	s.log.InfoF("[WEB] 服务器正在监听: %s", s.lis.Addr().String())
	if err := s.Serve(s.lis); !errors.Is(err, http.ErrServerClosed) {
		return err
	}
	return nil
}

// Endpoint 返回真实地址到注册表端点.
// 例子:
//   http://127.0.0.1:8080
func (s *Server) Endpoint() (*url.URL, error) {
	s.once.Do(func() {
		lis, err := net.Listen(s.network, s.address)
		if err != nil {
			s.err = err
			return
		}
		addr, err := host.Extract(s.address, s.lis)
		if err != nil {
			_ = lis.Close()
			s.err = err
			return
		}
		s.lis = lis
		s.endpoint = &url.URL{Scheme: "http", Host: addr}
	})
	if s.err != nil {
		return nil, s.err
	}
	return s.endpoint, nil
}

// Stop 停止Web服务器.
func (s *Server) Stop(ctx context.Context) error {
	s.log.Info("[WEB] 服务器正在停止")
	return s.Shutdown(ctx)
}