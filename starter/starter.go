package starter

import (
	"context"
	"net/url"

	// init encoding
	_ "gitee.com/titan-kit/titan/encoding"
)

// Server 是启动机服务器.
type Server interface {
	Endpoint() (*url.URL, error)
	Start(ctx context.Context) error
	Stop(ctx context.Context) error
}

// Starter 是启动机上下文值.
type Starter struct {
	Kind     Kind
	Endpoint string
}

// Kind 定义启动机类型
type Kind string

// 定义一套运输种类
const (
	KindRPC Kind = "RPC"
	KindWEB Kind = "WEB"
)

type starterKey struct{}

// NewContext 返回一个带有值的新Context.
func NewContext(ctx context.Context, tr Starter) context.Context {
	return context.WithValue(ctx, starterKey{}, tr)
}

// FromContext 返回存储在ctx中的传输值(如果有).
func FromContext(ctx context.Context) (tr Starter, ok bool) {
	tr, ok = ctx.Value(starterKey{}).(Starter)
	return
}