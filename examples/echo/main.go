package main

import (
	"gitee.com/titan-kit/titan"
	"gitee.com/titan-kit/titan/starter/web"

	"github.com/labstack/echo/v4"
)

func main() {

	httpSrv := web.NewServer(web.Address(":8080"))
	router := echo.New()
	// /echo/home
	router.GET("/home", func(c echo.Context) error {
		return c.String(200, "Hello Echo!")
	})
	httpSrv.PathPrefix("/echo", router)

	app := titan.NewTitan(titan.Name("echo"), titan.Version("0.0.1"), titan.Server(httpSrv))
	if err := app.Run(); err != nil {
		panic(err)
	}
}