module gitee.com/titan-kit/titan/examples

go 1.22

toolchain go1.22.3

require (
	gitee.com/titan-kit/titan v1.0.0
	github.com/gin-gonic/gin v1.7.1
	github.com/labstack/echo/v4 v4.2.2
	github.com/prometheus/client_golang v1.10.0
	go.etcd.io/etcd/client/v3 v3.5.0-beta.3
	go.opentelemetry.io/otel v0.20.0
	go.opentelemetry.io/otel/exporters/trace/jaeger v0.20.0
	go.opentelemetry.io/otel/sdk v0.20.0
	google.golang.org/genproto v0.0.0-20210521181308-5ccab8a35a9a
	google.golang.org/grpc v1.38.0
	google.golang.org/protobuf v1.26.0
)

replace gitee.com/titan-kit/titan v1.0.0 => ./../
