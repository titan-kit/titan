package main

import (
	"context"
	"errors"

	"gitee.com/titan-kit/titan"
	"gitee.com/titan-kit/titan/starter/web"
)

func main() {

	httpSrv := web.NewServer(web.Address(":8000"))

	healthy := web.NewHealthHandler()
	healthy.AddChecker("mysql", func(ctx context.Context) error {
		return nil
	})
	healthy.AddObserver("redis", func(ctx context.Context) error {
		return errors.New("connection refused")
	})
	httpSrv.Handle("/healthy", healthy)
	app := titan.NewTitan(titan.Name("healthy"), titan.Version("0.0.1"), titan.Server(httpSrv))
	if err := app.Run(); err != nil {
		panic(err)
	}
}