package main

import (
	"net/http"

	"gitee.com/titan-kit/titan"
	"gitee.com/titan-kit/titan/backends/etcd/registry"
	"gitee.com/titan-kit/titan/starter/web"

	etcd "go.etcd.io/etcd/client/v3"
)

func main() {
	cli, err := etcd.New(etcd.Config{Endpoints: []string{"127.0.0.1:2379"}})
	if err != nil {
		panic(err)
	}

	httpSrv := web.NewServer(web.Address(":8080"))
	httpSrv.HandleFunc("/home", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
		w.Header().Set("Content-Type", "application/json; charset=utf-8")
		_, _ = w.Write([]byte("Hello World!"))
	})
	app := titan.NewTitan(titan.Name("starter"), titan.Version("0.0.1"), titan.Server(httpSrv), titan.Registrar(registry.New(cli)))
	if err := app.Run(); err != nil {
		panic(err)
	}
}