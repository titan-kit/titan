package main

import (
	"context"
	"net/http"

	"gitee.com/titan-kit/titan"
	"gitee.com/titan-kit/titan/log"
	"gitee.com/titan-kit/titan/middleware"
	"gitee.com/titan-kit/titan/middleware/logging"
	"gitee.com/titan-kit/titan/middleware/recovery"
	"gitee.com/titan-kit/titan/middleware/tracing"
	"gitee.com/titan-kit/titan/starter/web"

	"gitee.com/titan-kit/titan/examples/tracer/internal/jaeger"
)

func main() {
	httpSrv := web.NewServer()

	ctx, cancel := context.WithCancel(context.Background())
	t := jaeger.NewJaeger("http://localhost:14268/api/traces")
	defer cancel()
	defer t.Close(ctx)

	h := middleware.Chain(tracing.Server(tracing.WithTracerProvider(t.TP())), logging.Server(log.DefaultLogger), recovery.Recovery())

	httpSrv.HandleFunc("/tracer/:articleID", func(w http.ResponseWriter, r *http.Request) {
		_, _ = h(func(ctx context.Context, req interface{}) (interface{}, error) {
			return req, nil
		})(r.Context(), "")

		vars := web.Vars(r)
		w.WriteHeader(http.StatusOK)
		w.Header().Set("Content-Type", "application/json; charset=utf-8")
		_, _ = w.Write([]byte("Hello Tracer!" + vars["articleID"]))
	})
	app := titan.NewTitan(titan.Name("starter"), titan.Version("0.0.1"), titan.Server(httpSrv))
	if err := app.Run(); err != nil {
		panic(err)
	}
}