package jaeger

import (
	"context"
	"time"

	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/exporters/trace/jaeger"
	"go.opentelemetry.io/otel/sdk/resource"
	"go.opentelemetry.io/otel/sdk/trace"
	"go.opentelemetry.io/otel/semconv"
)

func NewJaeger(url string, opts ...trace.TracerProviderOption) *tracer {
	t := &tracer{}
	opts = append(opts, trace.WithResource(resource.NewWithAttributes(
		semconv.ServiceNameKey.String("examples/metrics"),
		attribute.String("environment", "development"),
		attribute.Int64("ID", 1),
	)))
	//创建Jaeger输出者
	if exp, err := jaeger.NewRawExporter(jaeger.WithCollectorEndpoint(jaeger.WithEndpoint(url))); err == nil {
		opts = append(opts, trace.WithBatcher(exp))
		t.tp = trace.NewTracerProvider(opts...)
	}
	return t
}

type tracer struct {
	tp *trace.TracerProvider
}

func (t *tracer) TP() *trace.TracerProvider {
	return t.tp
}
func (t *tracer) Close(ctx context.Context) {
	// 关闭应用程序时不要使其挂起.
	ctx, cancel := context.WithTimeout(ctx, time.Second*5)
	defer cancel()
	if err := t.tp.Shutdown(ctx); err != nil {
		panic(err)
	}
}