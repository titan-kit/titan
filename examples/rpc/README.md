# Run the example

1、Compile proto file

```shell
protoc --proto_path=. --proto_path=../../api  --go_out=paths=source_relative:. --go-grpc_out=paths=source_relative:. --go-web_out=paths=source_relative:. demo/demo.proto 
```

2、Compile and execute the server code:

```shell
$ go run server/main.go
```

3、From a different terminal, compile and execute the client code:

```shell
$ go run client/main.go
```