package main

import (
	"context"
	"fmt"
	"strconv"

	"gitee.com/titan-kit/titan/backends/etcd/registry"
	"gitee.com/titan-kit/titan/errors"
	"gitee.com/titan-kit/titan/examples/rpc/demo"
	"gitee.com/titan-kit/titan/log"
	"gitee.com/titan-kit/titan/middleware"
	"gitee.com/titan-kit/titan/middleware/logging"
	"gitee.com/titan-kit/titan/middleware/recovery"
	"gitee.com/titan-kit/titan/starter/rpc"
	etcd "go.etcd.io/etcd/client/v3"
)

func main() {
	cli, err := etcd.New(etcd.Config{Endpoints: []string{"127.0.0.1:2379"}})
	if err != nil {
		panic(err)
	}
	logger := log.DefaultLogger
	conn, err := rpc.DialInsecure(
		context.Background(),
		rpc.WithEndpoint("discovery:///demo"),
		rpc.WithDiscovery(registry.New(cli)),
		rpc.WithMiddleware(
			middleware.Chain(
				recovery.Recovery(),
				logging.Client(logger),
			),
		),
	)
	if err != nil {
		panic(err)
	}
	client := demo.NewGreeterClient(conn)
	for i := 0; i <= 10; i++ {
		reply, err := client.SayHello(context.Background(), &demo.HelloRequest{Name: "titian-" + strconv.Itoa(i)})
		if err != nil {
			panic(err)
		}
		logger.Log(log.INFO, fmt.Sprintf("[rpc] SayHello %+v", reply))

		// returns error
		_, err = client.SayHello(context.Background(), &demo.HelloRequest{Name: "error"})
		if err != nil {
			logger.Log(log.INFO, fmt.Sprintf("[rpc] SayHello error: %v", err))
		}
		if errors.IsBadRequest(err) {
			logger.Log(log.INFO, fmt.Sprintf("[rpc] SayHello error is invalid argument: %v", err))
		}
	}
}