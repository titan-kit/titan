package main

import (
	"context"
	"fmt"
	"gitee.com/titan-kit/titan/backends/etcd/registry"
	etcd "go.etcd.io/etcd/client/v3"

	"gitee.com/titan-kit/titan"
	"gitee.com/titan-kit/titan/errors"
	"gitee.com/titan-kit/titan/examples/rpc/demo"
	"gitee.com/titan-kit/titan/log"
	"gitee.com/titan-kit/titan/middleware/logging"
	"gitee.com/titan-kit/titan/middleware/recovery"
	"gitee.com/titan-kit/titan/starter/rpc"
)

// go build -ldflags "-X main.Version=x.y.z"
var (
	// Name is the name of the compiled software.
	Name = "demo"
	// Version is the version of the compiled software.
	Version = "v1.0.0"
)

// server is used to implement demo.GreeterServer.
type server struct {
	demo.UnimplementedGreeterServer
}

// SayHello implements demo.GreeterServer
func (s *server) SayHello(ctx context.Context, in *demo.HelloRequest) (*demo.HelloReply, error) {
	if in.Name == "error" {
		return nil, errors.BadRequest("custom_error", fmt.Sprintf("invalid argument %s", in.Name))
	}
	if in.Name == "panic" {
		panic("grpc panic")
	}
	return &demo.HelloReply{Message: fmt.Sprintf("Hello %+v", in.Name)}, nil
}

func main() {
	cli, err := etcd.New(etcd.Config{Endpoints: []string{"127.0.0.1:2379"}})
	if err != nil {
		panic(err)
	}
	logger := log.DefaultLogger
	log := log.NewSlf4g("examoles/rpc", logger)

	grpcSrv := rpc.NewServer(
		rpc.Address(":9090"),
		rpc.Middleware(
			recovery.Recovery(),
			logging.Server(logger),
		))

	s := &server{}
	demo.RegisterGreeterServer(grpcSrv, s)
	app := titan.NewTitan(
		titan.Version("0.0.1"),
		titan.Name(Name),
		titan.Server(
			grpcSrv,
		), titan.Registrar(registry.New(cli)),
	)

	if err := app.Run(); err != nil {
		log.Error(err)
	}
}