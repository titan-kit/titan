package main

import (
	"fmt"

	"gitee.com/titan-kit/titan/backends/kafka"
)

func main() {
	eng := kafka.New()
	eng.StartDefaultReader("test_topic", "test-group", defaultReader{})
	sender := eng.Sender()
	defer eng.Close()
	var value = ""
	for {
		// 设置发送的真正内容
		if fmt.Scanln(&value); len(value) > 0 {
			sender.Send("my_topic", value)
		}
	}
}

type defaultReader struct {
}

func (r defaultReader) GetOffset(topic string) []kafka.PartitionOffset {
	return []kafka.PartitionOffset{}
}

func (r defaultReader) OnRead(topic string, partition int32, offset int64, key, value []byte) error {
	fmt.Printf("topic:{%s};partition{%d};offset{%3d};key:{%s};value:{%s}\n", topic, partition, offset, key, value)
	return nil
}