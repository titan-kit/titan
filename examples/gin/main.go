package main

import (
	"log"

	"gitee.com/titan-kit/titan"
	"gitee.com/titan-kit/titan/starter/web"

	"github.com/gin-gonic/gin"
)

func main() {
	gc := gin.Default()
	// /gin/home
	gc.GET("/home", func(ctx *gin.Context) {
		ctx.String(200, "Hello Gin!")
	})

	httpSrv := web.NewServer(web.Address(":8000"))
	httpSrv.PathPrefix("/gin", gc)

	app := titan.NewTitan(
		titan.Name("gin"),
		titan.Server(
			httpSrv,
		),
	)
	if err := app.Run(); err != nil {
		log.Println(err)
	}
}