package main

import (
	"context"
	"net/http"

	"gitee.com/titan-kit/titan"
	"gitee.com/titan-kit/titan/backends/prometheus"
	"gitee.com/titan-kit/titan/middleware"
	"gitee.com/titan-kit/titan/middleware/metrics"
	"gitee.com/titan-kit/titan/starter/web"

	"github.com/prometheus/client_golang/prometheus/promhttp"
)

func main() {
	httpSrv := web.NewServer(web.Address(":8080"))

	h := middleware.Chain(metrics.Metrics("server", "0000", metrics.WithRequests(prometheus.MetricHttpServerReqCodeTotal), metrics.WithObserver(prometheus.MetricHttpServerReqDur)))

	httpSrv.HandleFunc("/test", func(w http.ResponseWriter, r *http.Request) {
		_, _ = h(func(ctx context.Context, req interface{}) (interface{}, error) {
			return req, nil
		})(r.Context(), "")

		w.WriteHeader(http.StatusOK)
		w.Header().Set("Content-Type", "application/json; charset=utf-8")
		_, _ = w.Write([]byte("Hello Metrics!"))
	})

	metricSrv := web.NewServer(web.Address(":7070"))
	metricSrv.Handle("/metrics", promhttp.Handler())

	app := titan.NewTitan(titan.Name("metrics"), titan.Version("0.0.1"), titan.Server(httpSrv, metricSrv))
	if err := app.Run(); err != nil {
		panic(err)
	}
}