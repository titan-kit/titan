# Metrics

提供量化的系统内/外部各个维度的指标，一般包括Counter(计数器)、Gauge(仪表盘)、Histogram(柱状图)、Summary(摘要)等。

* [Prometheus](https://github.com/prometheus/client_golang)