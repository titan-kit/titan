package metrics

// Metrics 提供量化的系统内/外部各个维度的指标，一般包括Counter(计数器)、Gauge(仪表盘)、Histogram(柱状图)、Summary(摘要)等。

// Counter (计数器)代表一种样本数据单调递增的指标,，即只增不减，除非监控系统发生了重置。
// 例如，你可以使用 counter 类型的指标来表示服务的请求数、已完成的任务数、错误发生的次数等。
type Counter interface {
	With(cvs ...string) Counter
	// Inc 自增1
	Inc()
	// Add 把给定值加入到计数器中. 若注意小于0是控制
	Add(value float64)
}

// Gauge 代表一种样本数据可以任意变化的仪表，即可增可减。
// Gauge通常用于像温度或者内存使用率这种仪表，也可以表示能随时增加或减少的“总数”，例如：当前并发请求的数量。
type Gauge interface {
	With(gvs ...string) Gauge
	// Set 直接设置成给定值
	Set(value float64)
	// Add 增加给定值，可为负
	Add(value float64)
	// Sub 减少给定值，可为负
	Sub(value float64)
}

// Observer 获取离散时间点上的测量值.
type Observer interface {
	With(ovs ...string) Observer
	// Observe 将一个观测到的样本数据加入Observer
	Observe(value float64)
}